﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.Evaluation;

namespace MasterDataManagement.Domain.Repositories
{

    public interface IEvaluationRepo
    {
        Task<Evaluations> GetEvaluationById(int idEvaluation);
        Task<List<Evaluations>> GetEvaluations();
        Task CreateEvaluation(EvaluationPostDto evaluationPostDto);
        Task<Evaluations> UpdateEvaluation(Evaluations evaluation, EvaluationPostDto evaluationPostDto);
        Task DeleteEvaluation(Evaluations evaluation);
        Task<bool> ExistAll(List<int> evaluationsId);
        Evaluations MakeEvaluation(EvaluationPostDto evaluationPostDto);
    }
}
