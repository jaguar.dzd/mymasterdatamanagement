﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.AnalyseAutomate;

namespace MasterDataManagement.Domain.Repositories
{

    public interface IAnalyseAutomateRepo
    {
        Task<AnalyseAutomate> GetAnalyseAutomateById(int idAnalyseAutomate);
        Task<List<AnalyseAutomate>> GetAnalyseAutomates();
        Task CreateAnalyseAutomate(AnalyseAutomatePostDto analyseAutomatePostDto);
        Task<AnalyseAutomate> UpdateAnalyseAutomate(AnalyseAutomate analyseAutomate, AnalyseAutomatePostDto analyseAutomatePostDto);
        Task DeleteAnalyseAutomate(AnalyseAutomate analyseAutomate);
        Task<bool> ExistAll(List<int> analyseAutomatesIds);
        Task<bool> LinkAnalyseAutomateToValidations(List<int> idAnalyseAutomate, List<int> validationsIds);
        Task<bool> LinkAnalyseAutomateToBilans(List<int> idAnalyseAutomate, List<int> bilanIds);
        Task<bool> LinkAnalyseAutomateToFonctionReflexes(List<int> idAnalyseAutomate, List<int> fonctionReflexesIds);
        Task<bool> LinkAnalyseAutomateToEvaluations(List<int> idAnalyseAutomate, List<int> evaluationsIds);
        Task<bool> CreateFullAnalyseAutomate(AnalyseAutomateFullPostDto analyseAutomatePostDto);
        Task<bool> LinkAnalyseAutomateToTransBasics(List<int> idAnalyseAutomate, List<int> transBasicsIds);
        Task<bool> LinkAnalyseAutomateToTransformations(List<int> idAnalyseAutomate, List<int> transformationsIds);
        Task<bool> LinkAnalyseAutomateToIncidenceDesFlags(List<int> idAnalyseAutomate, List<int> incidenceDesFlagsIds);
        Task<bool> LinkAnalyseAutomateToCommAutos(List<int> idAnalyseAutomate, List<int> commAutosIds);
        Task<bool> UpdateFullAnalyseAutomate(AnalyseAutomateFullPostDto analyseAutomateFullPostDto);
        Task<bool> CreateFullAnalyseAutomateCatalogue(AnalyseAutomateFullPostDto analyseAutomatePostDto, Catalogue catalogue);
    }
}
