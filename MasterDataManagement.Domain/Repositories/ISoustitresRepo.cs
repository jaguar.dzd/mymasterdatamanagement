﻿using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.Soustitres;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MasterDataManagement.Domain.Repositories
{
    public interface ISoustitresRepo
    {
        Task<Soustitres> GetSoustitresById(int idSoustitres);
        Task<List<Soustitres>> GetSoustitres();
        Task<Soustitres> UpdateSoustitres(Soustitres soustitres, SoustitresPutDto soustitresPutDto);
        Task DeleteSousTitre(Soustitres t);
        Task CreateSoustitres(SoustitresPostDto soustitresPostDto);
    }
}
