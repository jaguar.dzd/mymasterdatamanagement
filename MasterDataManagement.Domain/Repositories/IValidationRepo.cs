﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.Validation;

namespace MasterDataManagement.Domain.Repositories
{
    public interface IValidationRepo
    {
        Task<Validations> GetValidationById(int idValidation);
        Task<List<Validations>> GetValidations();
        Task CreateValidation(ValidationPostDto validationPostDto);
        Task<Validations> UpdateValidation(Validations validation, ValidationPostDto validationPostDto);
        Task DeleteValidation(Validations validation);
        Task<bool> ExistAll(List<int> validationsIds);
        Validations MakeValidations(ValidationPostDto validationPostDto);
    }
}
