﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.Bilan;

namespace MasterDataManagement.Domain.Repositories
{

    public interface IBilanRepo
    {
        Task<Bilan> GetBilanById(int idBilan);
        Task<List<Bilan>> GetBilans();
        Task CreateBilan(BilanPostDto bilanPostDto);
        Task<Bilan> UpdateBilan(Bilan Bilan, BilanPostDto bilanPostDto);
        Task DeleteBilan(Bilan bilan);
        Task<bool> ExistAll(List<int> bilansIds);
        Bilan MakeBilan(BilanPostDto bilanPostDto);
    }
}
