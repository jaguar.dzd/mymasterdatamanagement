﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;

namespace MasterDataManagement.Domain.Repositories
{
    public interface IUserInstanceRepo
    {
       
        Task CreateUserInstances(ushort idUser , List<byte> idInstances);

        Task<List<Instance>> GetUserInstances(ushort idUser);
        Task Delete(ushort idUser, List<int> idInstances);
    }
}
