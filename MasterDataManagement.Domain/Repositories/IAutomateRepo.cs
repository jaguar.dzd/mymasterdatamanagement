﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.Automate;

namespace MasterDataManagement.Domain.Repositories
{
    public interface IAutomateRepo
    {
        Task<Automate> GetAutomateById(ushort idAutomate);
        Task<List<Automate>> GetAutomates();
        Task CreateAutomate(AutomatePostDto automatePostDto);
        Task<Automate> UpdateAutomate(Automate automate, AutomatePostDto automatePostDto);
        Task DeleteAutomate(Automate automate);
        Task CreateAutomates(List<AutomatePostDto> automatePostDtos);
        Task<bool> ExistAll(List<int> automates);
        Automate MakeAutomate(AutomatePostDto data);
        Task<List<Automate>> GetCatalogueAutomates(int idCatalogue);
    }
}
