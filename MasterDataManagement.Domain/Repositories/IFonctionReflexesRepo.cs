﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.FonctionReflexes;

namespace MasterDataManagement.Domain.Repositories
{
    public interface IFonctionReflexesRepo
    {
        Task<Fonctionsreflexes> GetFonctionReflexesById(int idFonctionReflexes);
        Task<List<Fonctionsreflexes>> GetFonctionReflexes();
        Task CreateFonctionReflexes(FonctionReflexesPostDto fonctionReflexesPostDto);
        Task<Fonctionsreflexes> UpdateFonctionReflexes(Fonctionsreflexes fonctionReflexes, FonctionReflexesPostDto fonctionReflexesPostDto);
        Task DeleteFonctionReflexes(Fonctionsreflexes fonctionReflexes);
        Task<bool> ExistAll(List<int> fonctionReflexesId);
        Fonctionsreflexes MakeFonctionsreflexes(FonctionReflexesPostDto fonctionReflexesPostDto);
    }
}
