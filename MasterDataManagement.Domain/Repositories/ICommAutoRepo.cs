﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.CommAuto;

namespace MasterDataManagement.Domain.Repositories
{
    public interface ICommAutoRepo
    {
        Task<Commauto> GetCommAutoById(int idCommAuto);
        Task<List<Commauto>> GetCommAutos();
        Task CreateCommAuto(CommAutoPostDto commAutoPostDto);
        Task<Commauto> UpdateCommAuto(Commauto commAuto, CommAutoPostDto commAutoPostDto);
        Task DeleteCommAuto(Commauto commAuto);
        Task<bool> ExistAll(List<int> commAutosIds);
        Commauto MakeCommAuto(CommAutoPostDto commAutoPostDto);
    }

}
