﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.Secteur;

namespace MasterDataManagement.Domain.Repositories
{

    public interface ISecteurRepo
    {
        Task<Secteur> GetSecteurById(int idSecteur);
        Task<List<Secteur>> GetSecteurs();
        Task CreateSecteur(SecteurPostDto secteurPostDto);
        Task<Secteur> UpdateSecteur(Secteur secteur, SecteurPostDto secteurPostDto);
        Task DeleteSecteur(Secteur secteur);
    }
}
