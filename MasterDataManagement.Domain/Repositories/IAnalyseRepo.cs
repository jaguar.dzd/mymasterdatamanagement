﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.Domain.Others;
using MasterDataManagement.DTO.Analyse;

namespace MasterDataManagement.Domain.Repositories
{

    public interface IAnalyseRepo
    {
        Task<Analyse> GetAnalyseById(int idAnalyse);
        Task<Analyse> GetAnalyseByCode(string code);
        Task<List<Analyse>> GetAnalyses();
        Task CreateAnalyse(AnalysePostDto analysePostDto);
        Task<Analyse> UpdateAnalyse(Analyse analyse, AnalysePostDto analysePostDto);
        Task DeleteAnalyse(Analyse analyse);
        Task CreateAnalyses(List<AnalysePostDto> analysePostDtos);
        Task<bool> ExistAll(List<int> automates);
        Task<bool> LinkAnalyseToValidations(int idAnalyse, List<int> validationsIds);
        Task<bool> LinkAnalyseToBilans(int idAnalyse, List<int> bilansIds);
        Task<bool> LinkAnalyseToFonctionReflexes(int idAnalyse, List<int> fonctionReflexesId);
        Task<bool> LinkAnalyseToEvaluations(int idAnalyse, List<int> evaluationsId);
        Task CreateFullAnalyse(AnalysePostFullDto fullAnalyse);
        Task<bool> LinkAnalyseToTransBasics(int idAnalyse, List<int> transBasicsIds);
        Task<bool> LinkAnalyseToTransformations(int idAnalyse, List<int> transformationsIds);
        Task<bool> LinkAnalyseToIncidenceDesFlags(int idAnalyse, List<int> incidenceDesFlagsIds);
        Task<bool> LinkAnalyseToCommAutos(int idAnalyse, List<int> commAutosIds);
        Task UpdateFullAnalyse(Analyse analyse, AnalysePostFullDto fullAnalyse);
        Analyse MakeAnalyse(AnalysePostDto analysePostDto);
        Task CreateOrUpdateAnalyses(List<AnalysePostDto> analyse, bool isReplacingExistingvalues);
        Task<FullAnalyse> GetFullAnalyse(int idAnalyse);
        Task<List<Analyse>> GetCatalogueAnalyses(int idCatalogue);
    }
}
