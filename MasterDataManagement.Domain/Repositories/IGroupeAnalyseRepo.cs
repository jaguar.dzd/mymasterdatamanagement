﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.GroupeAnalyse;


namespace MasterDataManagement.Domain.Repositories
{
    public interface IGroupeAnalyseRepo
    {
        Task<Groupeanalyse> GetGroupeAnalyseById(int idGroupeAnalyse);
        Task<List<Groupeanalyse>> GetGroupeAnalyses();
        Task CreateGroupeAnalyse(GroupeAnalysePostDto groupeAnalysePostDto);
        Task<Groupeanalyse> UpdateGroupeAnalyse(Groupeanalyse groupeAnalyse, GroupeAnalysePostDto groupeAnalysePostDto);
        Task DeleteGroupeAnalyse(Groupeanalyse groupeAnalyse);
    }
}
