﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.TypeDeTube;

namespace MasterDataManagement.Domain.Repositories
{
    public interface ITypeDeTubeRepo
    {
        Task<Typedetube> GetTypeDeTubeById(int idTypeDeTube);
        Task<List<Typedetube>> GetTypeDeTubes();
        Task CreateTypeDeTube(TypeDeTubePostDto typeDeTubePostDto);
        Task<Typedetube> UpdateTypeDeTube(Typedetube typeDeTube, TypeDeTubePostDto typeDeTubePostDto);
        Task DeleteTypeDeTube(Typedetube typeDeTube);
    }
}
