﻿using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.IncidencesDesFlags;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MasterDataManagement.Domain.Repositories
{
    public interface IIncidenceDesFlagsRepo
    {
        Task<Incidencedesflags> GetIncidenceDesFlagsById(int idIncidenceDesFlags);
        Task<List<Incidencedesflags>> GetIncidenceDesFlags();
        Task CreateIncidenceDesFlags(IncidencedesflagsPostDto incidencedesflagsPostDto);
        Task<Incidencedesflags> UpdateIncidenceDesFlags(Incidencedesflags incidencedesflags, IncidencedesflagsPutDto incidencedesflagsPutDto);
        Task DeleteIncidenceDesFlags(Incidencedesflags incidencedesflags);
        Task<bool> ExistAll(List<int> incidenceDesFlagsIds);
        Incidencedesflags MakeIncidencedesflags(IncidencedesflagsPostDto data);
    }
}
