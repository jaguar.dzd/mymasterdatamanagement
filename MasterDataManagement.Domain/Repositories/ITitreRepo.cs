﻿using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.Titre;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MasterDataManagement.Domain.Repositories
{
    public interface ITitreRepo
    {
        Task<Titres> GetTitreById(int idTitres);
        Task<Titres> UpdateTitre(Titres titre, TitrePutDto titrePutDto);
        Task<List<Titres>> GetTitres();
        Task DeleteTitre(Titres titre);
        Task CreateTitre(TitrePostDto titrePostDto);
    }
}
