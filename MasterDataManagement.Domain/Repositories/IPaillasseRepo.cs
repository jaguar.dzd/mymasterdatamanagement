﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.Paillasse;

namespace MasterDataManagement.Domain.Repositories
{
    public interface IPaillasseRepo
    {
        Task<Paillasse> GetPaillasseById(int idPaillasse);
        Task<List<Paillasse>> GetPaillasses();
        Task CreatePaillasse(PaillassePostDto paillassePostDto);
        Task<Paillasse> UpdatePaillasse(Paillasse paillasse, PaillassePostDto paillassePostDto);
        Task DeletePaillasse(Paillasse paillasse);
    }
}
