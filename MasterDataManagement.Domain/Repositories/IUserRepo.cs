﻿using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.User;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MasterDataManagement.Domain.Repositories
{
    public interface IUserRepo
    {
        Task CreateUser(UserPostDto user, string hashedPassword);
        Task<User> GetUserByEmail(string email);
        Task<List<UserReadDto>> GetUsers();
        Task<User> GetUserById(ushort idUser);
        Task<bool> AddDroits(ushort idUser, List<int> droitsIds);
        Task<bool> RemoveDroits(ushort idUser, List<int> droitsIds);
        Task<List<User>> GetUsersWithDetails();
        Task<User> GetUserWithDetailsByEmail(string email);
        Task<bool> HasRight(int idUser , string right);
        Task<bool> UserExists(string email);
    }
}
