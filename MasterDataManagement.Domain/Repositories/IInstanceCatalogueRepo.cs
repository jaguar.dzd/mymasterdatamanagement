﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.Enum;
using MasterDataManagement.DTO.Instance;
using MasterDataManagement.DTO.InstanceCatalogue;

namespace MasterDataManagement.Domain.Repositories
{
    public interface IInstanceCatalogueRepo
    {
        Task<bool> ApproveOrRefuseCatalogueApplication(List<InstanceCatalogueDetailPostDto> data);
    }
}
