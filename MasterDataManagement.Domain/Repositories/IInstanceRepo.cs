﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.Instance;

namespace MasterDataManagement.Domain.Repositories
{
    public interface IInstanceRepo
    {
        Task<Instance> GetInstanceById(byte idInstance);
        Task<List<Instance>> GetInstances();
        Task CreateInstance(InstancePostDto instancePostDto);
        Task<Instance> UpdateInstance(Instance instance, InstancePostDto instancePostDto);
        Task DeleteInstance(Instance instance);
        Task<bool> ExistAll(List<int> idInstances);
        Task LinkInstanceToCatalogues(int idCatalogue, List<int> instancesIds);
        Task RemoveInstanceFromCatalogues(int idCatalogue, List<int> instancesIds);
        Task<List<Instance>> GetCatalogueInstances(int idCatalogue);
    }
}
