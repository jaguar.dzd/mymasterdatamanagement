﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.Site;

namespace MasterDataManagement.Domain.Repositories
{
    public interface ITransformationRepo
    {
        Task<Transformations> GetTransformationById(int idTransformation);
        Task<List<Transformations>> GetTransformations();
        Task DeleteTransformation(Transformations t);
        Task CreateTransformation(TransformationPostDto transformationPostDto);
        Task<Transformations> UpdateTransformation(Transformations transformation, TransformationPutDto transformationPutDto);
        Task<bool> ExistAll(List<int> transformationsIds);
        Transformations MakeTransformations(TransformationPostDto t);
    }
}
