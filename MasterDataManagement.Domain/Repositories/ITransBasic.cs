﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.Site;
using MasterDataManagement.DTO.TransBasic;

namespace MasterDataManagement.Domain.Repositories
{
    public interface ITransBasicRepo
    {
        Task<Transfbasic> GetTransBasicById(int idTransBasic);
        Task<List<Transfbasic>> GetTransBasics();
        Task CreateTransBasic(TransBasicPostDto TransBasicPostDto);
        Task<Transfbasic> UpdateTransBasic(Transfbasic TransBasic, TransBasicPutDto TransBasicPostDto);
        Task DeleteTransBasic(Transfbasic TransBasic);
        Task<bool> ExistAll(List<int> transBasicsIds);
        Transfbasic MakeTransfbasic(TransBasicPostDto t);
    }
}
