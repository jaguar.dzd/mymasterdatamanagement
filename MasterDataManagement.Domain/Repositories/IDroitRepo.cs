﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.Droit;

namespace MasterDataManagement.Domain.Repositories
{

    public interface IDroitRepo
    {
        Task<Droit> GetDroitById(int idDroit);
        Task<List<Droit>> GetDroits();
        Task CreateDroit(DroitPostDto droitPostDto);
        Task<Droit> UpdateDroit(Droit droit, DroitPostDto droitPostDto);
        Task DeleteDroit(Droit droit);
        Task<bool> ExistAll(List<int> droitsIds);
    }
}
