﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.Catalogue;
using MasterDataManagement.DTO.Enum;

namespace MasterDataManagement.Domain.Repositories
{

    public interface ICatalogueRepo
    {
        Task<Catalogue> GetCatalogueById(int idCatalogue);
        Task<List<Catalogue>> GetCatalogues();
        Task CreateCatalogue(CataloguePostDto cataloguePostDto);
        Task<Catalogue> UpdateCatalogue(Catalogue catalogue, CataloguePostDto cataloguePostDto);
        Task DeleteCatalogue(Catalogue catalogue);
        Task<bool> LinkCatalogueToAutomates(int idCatalogue, List<int> automates);
        Task<bool> LinkCatalogueToAnalyses(int idCatalogue, List<int> analyses);
        Task<bool> LinkCatalogueToAnalyaseAutomates(int idCatalogue, List<int> analyseAutomatesIds);
        Task<Catalogue> MakeCatalogue(CataloguePostDto cataloguePostDto);
        Task<bool> CreateFullCatalogue(CatalogueFullPostDto cataloguePostDto);
        Task UpdateFullCatalogue(Catalogue catagloue, CatalogueFullPostDto cataloguePostDto);
        Task<bool> DuplicateCatalogue(CatalogueDuplicatePostDto catalogueDuplicate);
        Task<CatalogueChangeStateResponseDto> ChangeState(int catalogueId);
        Task<Catalogue> ValidateOrReject(int catalogueId, ValidateOrRejectActionEnum action);
        Task<bool> LinkCatalogueToInstances(int idCatalogue, List<int> instancesIds);
        Task<bool> ExistAll(List<int> cataloguesIds);
        Task<bool> LinkCataloguesToAnalyses(List<int> cataloguesIds, List<int> analysesIds);
        Task<List<Catalogue>> GetCataloguesWithDetails();
        Task<List<Catalogue>> GetCataloguesByStatus(string status);
        Task<List<Catalogue>> GetUserInstancesCataloguesByStatus(int idUser, string status);
    }
}
