﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class AnalyseBilan
    {
        public int IdAnalyse { get; set; }
        public int IdBilan { get; set; }

        public virtual Analyse IdAnalyseNavigation { get; set; }
        public virtual Bilan IdBilanNavigation { get; set; }
    }
}
