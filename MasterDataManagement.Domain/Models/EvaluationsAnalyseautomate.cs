﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class EvaluationsAnalyseautomate
    {
        public int IdEvaluations { get; set; }
        public int IdAnalyseAutomate { get; set; }

        public virtual AnalyseAutomate IdAnalyseAutomateNavigation { get; set; }
        public virtual Evaluations IdEvaluationsNavigation { get; set; }
    }
}
