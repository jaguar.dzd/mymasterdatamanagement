﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class InstanceCatalogue
    {
        public int IdInstance { get; set; }
        public int IdCatalogue { get; set; }
        public string Statut { get; set; }

        public virtual Catalogue IdCatalogueNavigation { get; set; }
        public virtual Instance IdInstanceNavigation { get; set; }
    }
}
