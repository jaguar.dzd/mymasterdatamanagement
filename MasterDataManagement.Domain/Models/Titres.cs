﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class Titres
    {
        public Titres()
        {
            Analyse = new HashSet<Analyse>();
        }

        public int IdTitres { get; set; }
        public sbyte? Ordre { get; set; }
        public string Titre { get; set; }
        public sbyte? Style { get; set; }

        public virtual ICollection<Analyse> Analyse { get; set; }
    }
}
