﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class AnalyseAutomateCatalog
    {
        public int IdAnalyseAutomateCatalogue { get; set; }
        public int IdAnalyseAutomate { get; set; }
        public int IdCatalogue { get; set; }

        public virtual AnalyseAutomate IdAnalyseAutomateNavigation { get; set; }
        public virtual Catalogue IdCatalogueNavigation { get; set; }
    }
}
