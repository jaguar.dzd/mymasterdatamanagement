﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class Bilan
    {
        public Bilan()
        {
            AnalyseBilan = new HashSet<AnalyseBilan>();
            BilanAnalyseautomate = new HashSet<BilanAnalyseautomate>();
        }

        public int IdBilan { get; set; }
        public string Analyse1 { get; set; }
        public string Analyse2 { get; set; }
        public string Analyse3 { get; set; }
        public string Analyse4 { get; set; }
        public string Analyse5 { get; set; }
        public string Analyse6 { get; set; }
        public string Analyse7 { get; set; }
        public string Analyse8 { get; set; }
        public string Analyse9 { get; set; }
        public string Analyse10 { get; set; }
        public string Analyse11 { get; set; }
        public string Analyse12 { get; set; }
        public string Analyse13 { get; set; }
        public string Analyse14 { get; set; }
        public string Analyse15 { get; set; }
        public string Analyse16 { get; set; }
        public string Analyse17 { get; set; }
        public string Analyse18 { get; set; }
        public string Analyse19 { get; set; }
        public string Analyse20 { get; set; }
        public string Analyse21 { get; set; }
        public string Analyse22 { get; set; }
        public string Analyse23 { get; set; }
        public string Analyse24 { get; set; }

        public virtual ICollection<AnalyseBilan> AnalyseBilan { get; set; }
        public virtual ICollection<BilanAnalyseautomate> BilanAnalyseautomate { get; set; }
    }
}
