﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class Instance
    {
        public Instance()
        {
            InstanceCatalogue = new HashSet<InstanceCatalogue>();
            UserInstance = new HashSet<UserInstance>();
        }

        public int IdInstance { get; set; }
        public string Code { get; set; }
        public string Nom { get; set; }
        public string NomMasterInstance { get; set; }
        public string NomContactSite { get; set; }
        public string EmailContactSite { get; set; }
        public string TelContactSite { get; set; }
        public string AdresseSite { get; set; }
        public string TelSite { get; set; }
        public string IpInstance { get; set; }

        public virtual ICollection<InstanceCatalogue> InstanceCatalogue { get; set; }
        public virtual ICollection<UserInstance> UserInstance { get; set; }
    }
}
