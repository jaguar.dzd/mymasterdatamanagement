﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class TransfbasicAnalyseautomate
    {
        public int IdTransfbasic { get; set; }
        public int IdAnalyseAutomate { get; set; }

        public virtual AnalyseAutomate IdAnalyseAutomateNavigation { get; set; }
        public virtual Transfbasic IdTransfbasicNavigation { get; set; }
    }
}
