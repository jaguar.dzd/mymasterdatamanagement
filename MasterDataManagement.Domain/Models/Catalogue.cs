﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class Catalogue
    {
        public Catalogue()
        {
            AnalyseAutomateCatalog = new HashSet<AnalyseAutomateCatalog>();
            AnalyseCatalogue = new HashSet<AnalyseCatalogue>();
            AutomateCatalogue = new HashSet<AutomateCatalogue>();
            InstanceCatalogue = new HashSet<InstanceCatalogue>();
        }

        public int IdCatalogue { get; set; }
        public string NomCatalogue { get; set; }
        public int Version { get; set; }
        public string Statut { get; set; }
        public string CreatorName { get; set; }
        public DateTime? CreationDate { get; set; }
        public bool IsActivated { get; set; }

        public virtual ICollection<AnalyseAutomateCatalog> AnalyseAutomateCatalog { get; set; }
        public virtual ICollection<AnalyseCatalogue> AnalyseCatalogue { get; set; }
        public virtual ICollection<AutomateCatalogue> AutomateCatalogue { get; set; }
        public virtual ICollection<InstanceCatalogue> InstanceCatalogue { get; set; }
    }
}
