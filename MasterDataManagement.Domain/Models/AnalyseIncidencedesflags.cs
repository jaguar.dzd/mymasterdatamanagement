﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class AnalyseIncidencedesflags
    {
        public int IdAnalyse { get; set; }
        public int Idincidencedesflags { get; set; }

        public virtual Analyse IdAnalyseNavigation { get; set; }
        public virtual Incidencedesflags IdincidencedesflagsNavigation { get; set; }
    }
}
