﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class Typedetube
    {
        public Typedetube()
        {
            Analyse = new HashSet<Analyse>();
            AnalyseAutomate = new HashSet<AnalyseAutomate>();
        }

        public int IdTypeDeTube { get; set; }
        public string NomTypeDuTube { get; set; }

        public virtual ICollection<Analyse> Analyse { get; set; }
        public virtual ICollection<AnalyseAutomate> AnalyseAutomate { get; set; }
    }
}
