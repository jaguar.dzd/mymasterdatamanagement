﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class DroitUser
    {
        public int IdDroitUser { get; set; }
        public int IdDroit { get; set; }
        public int IdUser { get; set; }

        public virtual Droit IdDroitNavigation { get; set; }
        public virtual User IdUserNavigation { get; set; }
    }
}
