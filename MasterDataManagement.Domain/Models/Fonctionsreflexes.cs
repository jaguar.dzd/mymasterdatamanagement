﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class Fonctionsreflexes
    {
        public Fonctionsreflexes()
        {
            AnalyseFonctionsreflexes = new HashSet<AnalyseFonctionsreflexes>();
            AnalyseautomateFonctionsreflexes = new HashSet<AnalyseautomateFonctionsreflexes>();
        }

        public int IdFonctionsReflexes { get; set; }
        public sbyte? Ordre { get; set; }
        public string Condition { get; set; }
        public string Scenario { get; set; }

        public virtual ICollection<AnalyseFonctionsreflexes> AnalyseFonctionsreflexes { get; set; }
        public virtual ICollection<AnalyseautomateFonctionsreflexes> AnalyseautomateFonctionsreflexes { get; set; }
    }
}
