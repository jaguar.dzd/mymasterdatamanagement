﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class Transfbasic
    {
        public Transfbasic()
        {
            AnalyseTransfbasic = new HashSet<AnalyseTransfbasic>();
            TransfbasicAnalyseautomate = new HashSet<TransfbasicAnalyseautomate>();
        }

        public int IdTransfbasic { get; set; }
        public string OperateurLogique { get; set; }
        public short? Valeur1 { get; set; }
        public short? ValeurResultat { get; set; }

        public virtual ICollection<AnalyseTransfbasic> AnalyseTransfbasic { get; set; }
        public virtual ICollection<TransfbasicAnalyseautomate> TransfbasicAnalyseautomate { get; set; }
    }
}
