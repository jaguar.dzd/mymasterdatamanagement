﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class Droit
    {
        public Droit()
        {
            DroitUser = new HashSet<DroitUser>();
        }

        public int IdDroit { get; set; }
        public string Droit1 { get; set; }

        public virtual ICollection<DroitUser> DroitUser { get; set; }
    }
}
