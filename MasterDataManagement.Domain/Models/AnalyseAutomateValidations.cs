﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class AnalyseAutomateValidations
    {
        public int IdAnalyseAutomate { get; set; }
        public int IdValidations { get; set; }

        public virtual AnalyseAutomate IdAnalyseAutomateNavigation { get; set; }
        public virtual Validations IdValidationsNavigation { get; set; }
    }
}
