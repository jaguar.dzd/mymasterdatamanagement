﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class BilanAnalyseautomate
    {
        public int IdBilan { get; set; }
        public int IdAnalyseAutomate { get; set; }

        public virtual AnalyseAutomate IdAnalyseAutomateNavigation { get; set; }
        public virtual Bilan IdBilanNavigation { get; set; }
    }
}
