﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class AnalyseCatalogue
    {
        public int AnalyseIdAnalyse { get; set; }
        public int CatalogueIdCatalogue { get; set; }

        public virtual Analyse AnalyseIdAnalyseNavigation { get; set; }
        public virtual Catalogue CatalogueIdCatalogueNavigation { get; set; }
    }
}
