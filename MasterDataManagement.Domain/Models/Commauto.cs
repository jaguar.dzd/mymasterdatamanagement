﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class Commauto
    {
        public Commauto()
        {
            AnalyseCommauto = new HashSet<AnalyseCommauto>();
            CommautoAnalyseautomate = new HashSet<CommautoAnalyseautomate>();
        }

        public int IdCommAuto { get; set; }
        public sbyte? Ordre { get; set; }
        public string Condition { get; set; }
        public short? Valeur { get; set; }

        public virtual ICollection<AnalyseCommauto> AnalyseCommauto { get; set; }
        public virtual ICollection<CommautoAnalyseautomate> CommautoAnalyseautomate { get; set; }
    }
}
