﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class AnalyseAutomate
    {
        public AnalyseAutomate()
        {
            AnalyseAutomateCatalog = new HashSet<AnalyseAutomateCatalog>();
            AnalyseAutomateValidations = new HashSet<AnalyseAutomateValidations>();
            AnalyseautomateFonctionsreflexes = new HashSet<AnalyseautomateFonctionsreflexes>();
            AnalyseautomateTransformations = new HashSet<AnalyseautomateTransformations>();
            BilanAnalyseautomate = new HashSet<BilanAnalyseautomate>();
            CommautoAnalyseautomate = new HashSet<CommautoAnalyseautomate>();
            EvaluationsAnalyseautomate = new HashSet<EvaluationsAnalyseautomate>();
            IncidencedesflagsAnalyseautomate = new HashSet<IncidencedesflagsAnalyseautomate>();
            TransfbasicAnalyseautomate = new HashSet<TransfbasicAnalyseautomate>();
        }

        public int IdAnalyseAutomate { get; set; }
        public int IdAnalyse { get; set; }
        public int IdAutomate { get; set; }
        public int? IdPaillasse { get; set; }
        public string Paillasse2 { get; set; }
        public int? IdTypeDeTube { get; set; }
        public string Unites1 { get; set; }
        public string Unites2 { get; set; }
        public short? Decimales1 { get; set; }
        public short? Decimales2 { get; set; }
        public short? Coefficient { get; set; }
        public short? Longueur { get; set; }
        public string FormuleX1 { get; set; }
        public string FormuleX2 { get; set; }
        public string Formule { get; set; }
        public string Titre { get; set; }
        public string SousTitre { get; set; }
        public string Ligne1AvantResult { get; set; }
        public string Ligne2AvantResult { get; set; }
        public string Ligne3AvantResult { get; set; }
        public string Ligne4AvantResult { get; set; }
        public string Ligne5AvantResult { get; set; }
        public string Ligne1ApresResult { get; set; }
        public string Ligne2ApresResult { get; set; }
        public string Ligne3ApresResult { get; set; }
        public string Ligne4ApresResult { get; set; }
        public string Ligne5ApresResult { get; set; }
        public string Ligne6ApresResult { get; set; }
        public string TexteOptionnel1 { get; set; }
        public string TexteOptionnel2 { get; set; }
        public string TexteOptionnel3 { get; set; }
        public string TexteOptionnel4 { get; set; }
        public string TexteOptionnel5 { get; set; }
        public string TexteOptionnel6 { get; set; }
        public string TexteOptionnel7 { get; set; }
        public string TexteOptionnel8 { get; set; }
        public string TexteOptionnel9 { get; set; }
        public string TexteOptionnel10 { get; set; }
        public string TexteOptionnel11 { get; set; }
        public string TexteOptionnel12 { get; set; }
        public string TexteOptionnel13 { get; set; }
        public string TexteOptionnel14 { get; set; }
        public string TexteOptionnel15 { get; set; }
        public short? FacteurDilution { get; set; }
        public short? ValeurAajouter { get; set; }
        public short? Arrondi { get; set; }
        public string CodeAller1 { get; set; }
        public string CodeAller2 { get; set; }
        public string CodeAller3 { get; set; }
        public string CodeAller4 { get; set; }
        public string CodeAller5 { get; set; }
        public string CodeAller6 { get; set; }
        public string CodeAller7 { get; set; }
        public string CodeAller8 { get; set; }
        public string CodeAller9 { get; set; }
        public string AnalyseAutomatecol { get; set; }
        public string CodeRetour1 { get; set; }
        public string CodeRetour2 { get; set; }
        public bool? IsCreationAutomatiqueAnalyse { get; set; }
        public bool? GenererFlag { get; set; }
        public double? DifValeurSupa { get; set; }
        public double? MinvaleurEntrePassages { get; set; }
        public bool? MiseControleAvecDilution { get; set; }
        public string SiInfA { get; set; }
        public string EtOuSupA { get; set; }
        public string EtOuEgalA { get; set; }
        public string RepasserAvecLaDilution { get; set; }
        public bool? IsMiseEnControleSurCondi { get; set; }

        public virtual Analyse IdAnalyseNavigation { get; set; }
        public virtual Automate IdAutomateNavigation { get; set; }
        public virtual Paillasse IdPaillasseNavigation { get; set; }
        public virtual Typedetube IdTypeDeTubeNavigation { get; set; }
        public virtual ICollection<AnalyseAutomateCatalog> AnalyseAutomateCatalog { get; set; }
        public virtual ICollection<AnalyseAutomateValidations> AnalyseAutomateValidations { get; set; }
        public virtual ICollection<AnalyseautomateFonctionsreflexes> AnalyseautomateFonctionsreflexes { get; set; }
        public virtual ICollection<AnalyseautomateTransformations> AnalyseautomateTransformations { get; set; }
        public virtual ICollection<BilanAnalyseautomate> BilanAnalyseautomate { get; set; }
        public virtual ICollection<CommautoAnalyseautomate> CommautoAnalyseautomate { get; set; }
        public virtual ICollection<EvaluationsAnalyseautomate> EvaluationsAnalyseautomate { get; set; }
        public virtual ICollection<IncidencedesflagsAnalyseautomate> IncidencedesflagsAnalyseautomate { get; set; }
        public virtual ICollection<TransfbasicAnalyseautomate> TransfbasicAnalyseautomate { get; set; }
    }
}
