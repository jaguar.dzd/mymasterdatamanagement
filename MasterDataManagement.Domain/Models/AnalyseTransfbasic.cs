﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class AnalyseTransfbasic
    {
        public int IdAnalyse { get; set; }
        public int IdTransfbasic { get; set; }

        public virtual Analyse IdAnalyseNavigation { get; set; }
        public virtual Transfbasic IdTransfbasicNavigation { get; set; }
    }
}
