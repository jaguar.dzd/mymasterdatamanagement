﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class User
    {
        public User()
        {
            DroitUser = new HashSet<DroitUser>();
            UserInstance = new HashSet<UserInstance>();
        }

        public int IdUser { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Password { get; set; }

        public virtual ICollection<DroitUser> DroitUser { get; set; }
        public virtual ICollection<UserInstance> UserInstance { get; set; }
    }
}
