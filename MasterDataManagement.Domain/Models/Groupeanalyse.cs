﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class Groupeanalyse
    {
        public Groupeanalyse()
        {
            Analyse = new HashSet<Analyse>();
        }

        public int IdGroupeAnalyse { get; set; }
        public string NomGroupe { get; set; }

        public virtual ICollection<Analyse> Analyse { get; set; }
    }
}
