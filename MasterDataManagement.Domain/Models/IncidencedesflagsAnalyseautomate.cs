﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class IncidencedesflagsAnalyseautomate
    {
        public int IdIncidenceDesFlags { get; set; }
        public int IdAnalyseAutomate { get; set; }

        public virtual AnalyseAutomate IdAnalyseAutomateNavigation { get; set; }
        public virtual Incidencedesflags IdIncidenceDesFlagsNavigation { get; set; }
    }
}
