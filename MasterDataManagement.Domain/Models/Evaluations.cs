﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class Evaluations
    {
        public Evaluations()
        {
            AnalyseEvaluations = new HashSet<AnalyseEvaluations>();
            EvaluationsAnalyseautomate = new HashSet<EvaluationsAnalyseautomate>();
        }

        public int IdEvaluations { get; set; }
        public sbyte? Ordre { get; set; }
        public string Condition { get; set; }
        public short? NouvelleValeur { get; set; }

        public virtual ICollection<AnalyseEvaluations> AnalyseEvaluations { get; set; }
        public virtual ICollection<EvaluationsAnalyseautomate> EvaluationsAnalyseautomate { get; set; }
    }
}
