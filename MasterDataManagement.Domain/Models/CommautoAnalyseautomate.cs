﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class CommautoAnalyseautomate
    {
        public int IdCommAuto { get; set; }
        public int IdAnalyseAutomate { get; set; }

        public virtual AnalyseAutomate IdAnalyseAutomateNavigation { get; set; }
        public virtual Commauto IdCommAutoNavigation { get; set; }
    }
}
