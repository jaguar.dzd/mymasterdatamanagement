﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class Paillasse
    {
        public Paillasse()
        {
            Analyse = new HashSet<Analyse>();
            AnalyseAutomate = new HashSet<AnalyseAutomate>();
        }

        public int IdPaillasse { get; set; }
        public string NomPaillasse { get; set; }

        public virtual ICollection<Analyse> Analyse { get; set; }
        public virtual ICollection<AnalyseAutomate> AnalyseAutomate { get; set; }
    }
}
