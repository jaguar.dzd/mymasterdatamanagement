﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class Validations
    {
        public Validations()
        {
            AnalyseAutomateValidations = new HashSet<AnalyseAutomateValidations>();
            AnalyseValidations = new HashSet<AnalyseValidations>();
        }

        public int IdValidations { get; set; }
        public string Sexe { get; set; }
        public sbyte? Min { get; set; }
        public sbyte? Max { get; set; }
        public string Unites { get; set; }
        public string Service { get; set; }
        public string Origine { get; set; }
        public double? ValTresBasse { get; set; }
        public double? ValBasse { get; set; }
        public double? ValHaute { get; set; }
        public double? ValTresHaute { get; set; }

        public virtual ICollection<AnalyseAutomateValidations> AnalyseAutomateValidations { get; set; }
        public virtual ICollection<AnalyseValidations> AnalyseValidations { get; set; }
    }
}
