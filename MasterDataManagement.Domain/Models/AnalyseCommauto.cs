﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class AnalyseCommauto
    {
        public int IdAnalyse { get; set; }
        public int IdCommAuto { get; set; }

        public virtual Analyse IdAnalyseNavigation { get; set; }
        public virtual Commauto IdCommAutoNavigation { get; set; }
    }
}
