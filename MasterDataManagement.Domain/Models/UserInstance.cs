﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class UserInstance
    {
        public int IdUser { get; set; }
        public int IdInstance { get; set; }

        public virtual Instance IdInstanceNavigation { get; set; }
        public virtual User IdUserNavigation { get; set; }
    }
}
