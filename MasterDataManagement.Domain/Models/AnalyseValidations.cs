﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class AnalyseValidations
    {
        public int IdAnalyse { get; set; }
        public int IdValidations { get; set; }

        public virtual Analyse IdAnalyseNavigation { get; set; }
        public virtual Validations IdValidationsNavigation { get; set; }
    }
}
