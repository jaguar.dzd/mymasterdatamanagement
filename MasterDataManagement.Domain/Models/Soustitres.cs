﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class Soustitres
    {
        public Soustitres()
        {
            Analyse = new HashSet<Analyse>();
        }

        public int IdSousTitres { get; set; }
        public sbyte? Ordre { get; set; }
        public string Stitre { get; set; }
        public sbyte? Style { get; set; }

        public virtual ICollection<Analyse> Analyse { get; set; }
    }
}
