﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class AnalyseFonctionsreflexes
    {
        public int IdAnalyse { get; set; }
        public int IdFonctionsReflexes { get; set; }

        public virtual Analyse IdAnalyseNavigation { get; set; }
        public virtual Fonctionsreflexes IdFonctionsReflexesNavigation { get; set; }
    }
}
