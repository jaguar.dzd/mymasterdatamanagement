﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class Secteur
    {
        public Secteur()
        {
            Automate = new HashSet<Automate>();
        }

        public int IdSecteur { get; set; }
        public string SecteurNom { get; set; }

        public virtual ICollection<Automate> Automate { get; set; }
    }
}
