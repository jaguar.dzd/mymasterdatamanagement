﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class Incidencedesflags
    {
        public Incidencedesflags()
        {
            AnalyseIncidencedesflags = new HashSet<AnalyseIncidencedesflags>();
            IncidencedesflagsAnalyseautomate = new HashSet<IncidencedesflagsAnalyseautomate>();
        }

        public int Idincidencedesflags { get; set; }
        public string ValeurDuFlag { get; set; }
        public string ValeurDuResultat { get; set; }

        public virtual ICollection<AnalyseIncidencedesflags> AnalyseIncidencedesflags { get; set; }
        public virtual ICollection<IncidencedesflagsAnalyseautomate> IncidencedesflagsAnalyseautomate { get; set; }
    }
}
