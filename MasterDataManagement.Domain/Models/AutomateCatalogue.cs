﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class AutomateCatalogue
    {
        public int IdAutomate { get; set; }
        public int IdCatalogue { get; set; }

        public virtual Automate IdAutomateNavigation { get; set; }
        public virtual Catalogue IdCatalogueNavigation { get; set; }
    }
}
