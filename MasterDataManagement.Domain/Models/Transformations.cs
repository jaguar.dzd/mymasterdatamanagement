﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class Transformations
    {
        public Transformations()
        {
            AnalyseTransformations = new HashSet<AnalyseTransformations>();
            AnalyseautomateTransformations = new HashSet<AnalyseautomateTransformations>();
        }

        public int IdTransformations { get; set; }
        public string OperateurLogique { get; set; }
        public short? Valeur1 { get; set; }
        public short? ValeurResultat { get; set; }

        public virtual ICollection<AnalyseTransformations> AnalyseTransformations { get; set; }
        public virtual ICollection<AnalyseautomateTransformations> AnalyseautomateTransformations { get; set; }
    }
}
