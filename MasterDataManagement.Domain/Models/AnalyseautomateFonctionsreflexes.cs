﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class AnalyseautomateFonctionsreflexes
    {
        public int IdAnalyseAutomate { get; set; }
        public int IdFonctionsReflexes { get; set; }

        public virtual AnalyseAutomate IdAnalyseAutomateNavigation { get; set; }
        public virtual Fonctionsreflexes IdFonctionsReflexesNavigation { get; set; }
    }
}
