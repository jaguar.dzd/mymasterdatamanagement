﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class AnalyseEvaluations
    {
        public int IdAnalyse { get; set; }
        public int IdEvaluations { get; set; }

        public virtual Analyse IdAnalyseNavigation { get; set; }
        public virtual Evaluations IdEvaluationsNavigation { get; set; }
    }
}
