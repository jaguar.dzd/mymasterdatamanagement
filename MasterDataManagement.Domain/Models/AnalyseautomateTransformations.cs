﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class AnalyseautomateTransformations
    {
        public int IdAnalyseAutomate { get; set; }
        public int IdTransformations { get; set; }

        public virtual AnalyseAutomate IdAnalyseAutomateNavigation { get; set; }
        public virtual Transformations IdTransformationsNavigation { get; set; }
    }
}
