﻿using System;
using System.Collections.Generic;

namespace MasterDataManagement.Domain.Models
{
    public partial class AnalyseTransformations
    {
        public int IdAnalyse { get; set; }
        public int IdTransformations { get; set; }

        public virtual Analyse IdAnalyseNavigation { get; set; }
        public virtual Transformations IdTransformationsNavigation { get; set; }
    }
}
