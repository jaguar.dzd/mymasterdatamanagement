﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.Domain.Services
{
    public interface IHelperService
    {
        string HashPassword(string password);
    }
}
