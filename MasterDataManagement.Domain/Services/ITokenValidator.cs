﻿using MasterDataManagement.DTO.Authentication;
using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.Domain.Services
{
    public interface ITokenValidator
    {
        TokenDto ValidateUserToken();
    }
}
