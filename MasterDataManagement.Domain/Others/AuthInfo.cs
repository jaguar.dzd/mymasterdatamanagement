﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.Domain.Others
{
    public static class AuthInfo
    {
        public static string[] Issuers { get; } = { "MasterDataManagement" };
        public static string[] Audiences { get; } = { "MasterDataManagement" };
    }
}
