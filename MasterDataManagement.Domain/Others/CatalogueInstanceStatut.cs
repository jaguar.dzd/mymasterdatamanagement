﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.Domain.Others
{
    public class CatalogueInstanceStatut
    {
        public static string EnAttenteDeApprobation = "En Attente D'Approbation";
        public static string ApplicationCatagloueApprouver = "Approuvé";
        public static string ApplicationCatagloueRefuser = "Refusé";
    }
}
