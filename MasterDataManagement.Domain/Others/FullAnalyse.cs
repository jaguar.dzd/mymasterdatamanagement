﻿using MasterDataManagement.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.Domain.Others
{
    public class FullAnalyse
    {
        public Analyse Analyse { get; set; }
        public List<Transfbasic> TransBasics { get; set; }
        public List<Transformations> Transformations { get; set; }
        public List<Incidencedesflags> IncidenceDesFlags { get; set; }
        public List<Commauto> CommAutos { get; set; }
        public List<Bilan> Bilans { get; set; }
        public List<Validations> Validations { get; set; }
        public List<Evaluations> Evaluations { get; set; }
        public List<Fonctionsreflexes> FonctionReflexes { get; set; }
    }
}
