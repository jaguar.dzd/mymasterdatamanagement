﻿namespace MasterDataManagement.Domain.Others
{
    public class UserRights
    {
        public static string ValiderRejeterCatalogue = "ValiderRejeterCatalogue";
        public static string ApprouverRejeterApplicationCatalogue = "ApprouverRejeterApplicationCatalogue";
        public static string GestionDesCatalogues = "GestionDesCatalogues";
        public static string AppliquerCatalogueSites = "AppliquerCatalogueSites";
    }
}
