﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.Domain.Others
{
    public class CatalogueStatut
    {
        public static string EnAttenteDeValidation = "En Attente De Validation";
        public static string Valider = "Validé";
        public static string Obsolete = "Obsolete";
        public static string Rejeter = "Rejeté";
    }
}
