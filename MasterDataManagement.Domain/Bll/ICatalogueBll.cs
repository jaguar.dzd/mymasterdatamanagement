﻿using System.Collections.Generic;
using System.Linq;
using MasterDataManagement.Domain.Repositories;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.Domain.Others;
using MasterDataManagement.DTO.Catalogue;
using MasterDataManagement.DTO.Enum;
using MasterDataManagement.DTO.Instance;
using MasterDataManagement.DTO.Notifications;
using MasterDataManagement.Domain.Services;
using System;

namespace MasterDataManagement.Domain.Bll
{
    public interface ICatalogueBll
    {
        Task<Catalogue> GetCatalogue(int idCatalogue);
        Task<List<Catalogue>> GetCatalogues();
        Task CreateCatalogue(CataloguePostDto cataloguePostDto);
        Task<Catalogue> UpdateCatalogue(CatalogueUpdateDto cataloguePostDto);

        Task<Catalogue> DeleteCatalogue(int idCatalogue);
        Task<bool> LinkCatalogueToAutomates(int idCatalogue, List<int> automates);
        Task<bool> LinkCatalogueToAnalyases(int idCatalogue, List<int> analyses);
        Task<bool> LinkCatalogueToAnalyaseAutomates(int idCatalogue, List<int> analyseAutomatesIds);
        Task<bool> CreateFullCatalogue(CatalogueFullPostDto cataloguePostDto);
        Task<bool> UpdateFullCatalogue(CatalogueFullPostDto cataloguePostDto);
        Task<bool> DuplicateCatalogue(CatalogueDuplicatePostDto catalogueDuplicate);
        Task<CatalogueChangeStateResponseDto> ChangeState(int catalogueId);
        Task<Catalogue> ValidateOrReject(int catalogueId, ValidateOrRejectActionEnum action);
        Task<bool> LinkCatalogueToInstances(int idCatalogue, List<int> instancesIds);
        Task<bool> LinkCataloguesToAnalyases(List<int> cataloguesIds, List<int> analysesIds);
        Task<List<Analyse>> GetCatalogueAnalyses(int idCatalogue);
        Task<List<Automate>> GetCatalogueAutomates(int idCatalogue);
        Task<List<Instance>> GetCatalogueInstances(int idCatalogue);
        Task<List<Catalogue>> GetCataloguesWithDetails();
        Task<CatalogueNotificationsDto> GetCatalogueNotifications();
    }
    public class CatalogueBll : ICatalogueBll
    {
        private readonly ICatalogueRepo _catalogueRepo;
        private readonly IAnalyseRepo _analyseRepo;
        private readonly IAutomateRepo _automateRepo;
        private readonly IAnalyseAutomateRepo _analyseAutomateRepo;
        private readonly IInstanceRepo _instanceRepo;
        private readonly IUserRepo _userRepo;
        private readonly ITokenValidator tokenValidator;

        public CatalogueBll(ICatalogueRepo catalogueRepo, IAnalyseRepo analyseRepo, IAutomateRepo automateRepo, 
            IAnalyseAutomateRepo analyseAutomateRepo, IInstanceRepo instanceRepo, IUserRepo userRepo, ITokenValidator tokenValidator)
        {
            _catalogueRepo = catalogueRepo;
            _analyseRepo = analyseRepo;
            _automateRepo = automateRepo;
            _analyseAutomateRepo = analyseAutomateRepo;
            _instanceRepo = instanceRepo;
            _userRepo = userRepo;
            this.tokenValidator = tokenValidator;
        }


        async Task<Catalogue> ICatalogueBll.GetCatalogue(int idCatalogue)
        {
            var catalogue = await _catalogueRepo.GetCatalogueById(idCatalogue);

            return catalogue;
        }

        async Task<List<Catalogue>> ICatalogueBll.GetCatalogues()
        {
            var catalogues = await _catalogueRepo.GetCatalogues();

            return catalogues;
        }

        async Task<List<Catalogue>> ICatalogueBll.GetCataloguesWithDetails()
        {
            var catalogues = await _catalogueRepo.GetCataloguesWithDetails();

            return catalogues;
        }



        async Task ICatalogueBll.CreateCatalogue(CataloguePostDto cataloguePostDto)
        {
            await _catalogueRepo.CreateCatalogue(cataloguePostDto);
        }

        async Task<Catalogue> ICatalogueBll.UpdateCatalogue(CatalogueUpdateDto cataloguePostDto)
        {
            var catalogue = await _catalogueRepo.GetCatalogueById(cataloguePostDto.IdCatalogue);

            if (catalogue == null)
                return null;

            return await _catalogueRepo.UpdateCatalogue(catalogue, cataloguePostDto);
        }

        async Task<Catalogue> ICatalogueBll.DeleteCatalogue(int idCatalogue)
        {
            var catalogue = await _catalogueRepo.GetCatalogueById(idCatalogue);

            if (catalogue == null)
                return null;

            await _catalogueRepo.DeleteCatalogue(catalogue);

            return catalogue;
        }

        async Task<bool> ICatalogueBll.LinkCatalogueToAutomates(int idCatalogue, List<int> automates)
        {
            var catalogue = await _catalogueRepo.GetCatalogueById(idCatalogue);
            if (catalogue == null) return false;

            if (await _automateRepo.ExistAll(automates))
            {
                return await _catalogueRepo.LinkCatalogueToAutomates(catalogue.IdCatalogue, automates);
            }

            return false;
        }

        async Task<bool> ICatalogueBll.LinkCatalogueToAnalyases(int idCatalogue, List<int> analyses)
        {
            var catalogue = await _catalogueRepo.GetCatalogueById(idCatalogue);
            if (catalogue == null)
                return false;

            if (!await _analyseRepo.ExistAll(analyses))
                return false;

            var result = await _catalogueRepo.LinkCatalogueToAnalyses(catalogue.IdCatalogue, analyses);
            return result;
        }

        async Task<bool> ICatalogueBll.LinkCatalogueToAnalyaseAutomates(int idCatalogue, List<int> analyseAutomatesIds)
        {
            var catalogue = await _catalogueRepo.GetCatalogueById(idCatalogue);
            if (catalogue == null)
                return false;

            if (!await _analyseAutomateRepo.ExistAll(analyseAutomatesIds))
                return false;

            var result = await _catalogueRepo.LinkCatalogueToAnalyaseAutomates(catalogue.IdCatalogue, analyseAutomatesIds);
            return result;

        }

        async Task<bool> ICatalogueBll.CreateFullCatalogue(CatalogueFullPostDto cataloguePostDto)
        {
            return await _catalogueRepo.CreateFullCatalogue(cataloguePostDto);
        }

        async Task<bool> ICatalogueBll.UpdateFullCatalogue(CatalogueFullPostDto cataloguePostDto)
        {
            if (cataloguePostDto.Catalogue == null)
                return false;

            var catagloue = await _catalogueRepo.GetCatalogueById(cataloguePostDto.Catalogue.IdCatalogue);
            if (catagloue == null)
                return false;

            await _catalogueRepo.UpdateFullCatalogue(catagloue, cataloguePostDto);
            return true;
        }

        async Task<bool> ICatalogueBll.DuplicateCatalogue(CatalogueDuplicatePostDto catalogueDuplicate)
        {
            return await _catalogueRepo.DuplicateCatalogue(catalogueDuplicate);
        }

        async Task<CatalogueChangeStateResponseDto> ICatalogueBll.ChangeState(int catalogueId)
        {
            return await _catalogueRepo.ChangeState(catalogueId);
        }

        async Task<Catalogue> ICatalogueBll.ValidateOrReject(int catalogueId, ValidateOrRejectActionEnum action)
        {
            return await _catalogueRepo.ValidateOrReject(catalogueId, action);
        }

        public async Task<bool> LinkCatalogueToInstances(int idCatalogue, List<int> instancesIds)
        {
            var catalogue = await _catalogueRepo.GetCatalogueById(idCatalogue);
            if (catalogue == null)
                return false;

            if (!await _instanceRepo.ExistAll(instancesIds))
                return false;

            var result = await _catalogueRepo.LinkCatalogueToInstances(idCatalogue, instancesIds);
            return result;
        }

        async Task<bool> ICatalogueBll.LinkCataloguesToAnalyases(List<int> cataloguesIds, List<int> analysesIds)
        {

            if (!await _catalogueRepo.ExistAll(cataloguesIds))
                return false;
            if (!await _analyseRepo.ExistAll(analysesIds))
                return false;

            var result = await _catalogueRepo.LinkCataloguesToAnalyses(cataloguesIds, analysesIds);
            return result;
        }


        public async Task<List<Analyse>> GetCatalogueAnalyses(int idCatalogue)
        {
            var analyses = await _analyseRepo.GetCatalogueAnalyses(idCatalogue);

            return analyses;
        }

        public async Task<List<Automate>> GetCatalogueAutomates(int idCatalogue)
        {
            var automates = await _automateRepo.GetCatalogueAutomates(idCatalogue);

            return automates;
        }

        public async Task<List<Instance>> GetCatalogueInstances(int idCatalogue)
        {
            var instances = await _instanceRepo.GetCatalogueInstances(idCatalogue);

            return instances;
        }

        public async Task<CatalogueNotificationsDto> GetCatalogueNotifications()
        {
            var user = tokenValidator.ValidateUserToken();
            if (user == null) throw new Exception("ValidateUserToken returned null");

            var idUser = Convert.ToInt32(user.UserId);

            var result = new CatalogueNotificationsDto();

            if (await _userRepo.HasRight(idUser, UserRights.ValiderRejeterCatalogue))
            {
                var enAttenteDeValidations = await _catalogueRepo.GetCataloguesByStatus(CatalogueStatut.EnAttenteDeValidation);

                result.EnAttenteDeValidation = enAttenteDeValidations.Select(x => new EnAttenteDeValidationDto
                {
                    IdCatalogue = x.IdCatalogue,
                    CreationDate = x.CreationDate,
                    NomCatalogue = x.NomCatalogue,
                    Version = x.Version,
                    CreatorName = x.CreatorName

                }).ToList();
            }

            if (await _userRepo.HasRight(idUser, UserRights.ApprouverRejeterApplicationCatalogue) || await _userRepo.HasRight(idUser, UserRights.AppliquerCatalogueSites))
            {
                var enAttenteDeApprobation = await _catalogueRepo.GetUserInstancesCataloguesByStatus(idUser, CatalogueInstanceStatut.EnAttenteDeApprobation);

                result.EnAttenteDeAprobation = enAttenteDeApprobation.Select(x => new EnAttenteDeAprobationDto
                {
                    IdCatalogue = x.IdCatalogue,
                    CreationDate = x.CreationDate,
                    NomCatalogue = x.NomCatalogue,
                    Version = x.Version,
                    Requester = x.CreatorName,
                    Instances = x.InstanceCatalogue.Select(i => new InstanceGetDto
                    {
                        Code = i.IdInstanceNavigation.Code,
                        IdInstance = i.IdInstanceNavigation.IdInstance,
                        IpInstance = i.IdInstanceNavigation.IpInstance,
                        NomContactSite = i.IdInstanceNavigation.NomContactSite,
                        Nom = i.IdInstanceNavigation.Nom,
                        EmailContactSite = i.IdInstanceNavigation.EmailContactSite,
                        NomMasterInstance = i.IdInstanceNavigation.NomMasterInstance,
                        TelContactSite = i.IdInstanceNavigation.TelContactSite,
                        TelSite = i.IdInstanceNavigation.TelSite,
                        AdresseSite = i.IdInstanceNavigation.AdresseSite
                    }).ToList()

                }).ToList();

            }

            if (await _userRepo.HasRight(idUser, UserRights.GestionDesCatalogues))
            {
                var rejeter = await _catalogueRepo.GetCataloguesByStatus(CatalogueStatut.Rejeter);

                result.Rejeter = rejeter.Select(x => new RejeterDto
                {
                    IdCatalogue = x.IdCatalogue,
                    CreationDate = x.CreationDate,
                    NomCatalogue = x.NomCatalogue,
                    Version = x.Version

                }).ToList();

            }

            var refuser = await _catalogueRepo.GetUserInstancesCataloguesByStatus(idUser, CatalogueInstanceStatut.ApplicationCatagloueRefuser);

            result.Refuser = refuser.Select(x => new RefuserDto
            {
                IdCatalogue = x.IdCatalogue,
                CreationDate = x.CreationDate,
                NomCatalogue = x.NomCatalogue,
                Version = x.Version,
                Instances = x.InstanceCatalogue.Select(i => new InstanceGetDto
                {
                    Code = i.IdInstanceNavigation.Code,
                    IdInstance = i.IdInstanceNavigation.IdInstance,
                    IpInstance = i.IdInstanceNavigation.IpInstance,
                    NomContactSite = i.IdInstanceNavigation.NomContactSite,
                    Nom = i.IdInstanceNavigation.Nom,
                    EmailContactSite = i.IdInstanceNavigation.EmailContactSite,
                    NomMasterInstance = i.IdInstanceNavigation.NomMasterInstance,
                    TelContactSite = i.IdInstanceNavigation.TelContactSite,
                    TelSite = i.IdInstanceNavigation.TelSite,
                    AdresseSite = i.IdInstanceNavigation.AdresseSite
                }).ToList()

            }).ToList();

            return result;
        }
    }
}
