﻿using MasterDataManagement.Domain.Models;
using MasterDataManagement.Domain.Repositories;
using MasterDataManagement.DTO.Site;
using MasterDataManagement.DTO.TransBasic;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MasterDataManagement.Domain.Bll
{
    public interface ITransBasicBll
    {
        Task<Transfbasic> GetTransBasic(int idTransBasic);
        Task<List<Transfbasic>> GetTransBasics();
        Task CreateTransBasic(TransBasicPostDto TransBasicPostDto);
        Task<Transfbasic> UpdateTransBasic(TransBasicPutDto TransBasicPutDto);
        Task<Transfbasic> DeleteTypeDeTube(int idTransBasic);
    }
    public class TransBasicBll : ITransBasicBll
    {
        private readonly ITransBasicRepo TransBasicRepo;

        public TransBasicBll(ITransBasicRepo TransBasicRepo)
        {
            this.TransBasicRepo = TransBasicRepo;
        }

        async Task<Transfbasic> ITransBasicBll.GetTransBasic(int idTransBasic)
        {
            return await TransBasicRepo.GetTransBasicById(idTransBasic);
        }

        async Task<List<Transfbasic>> ITransBasicBll.GetTransBasics()
        {
            return await TransBasicRepo.GetTransBasics();
        }

        async Task ITransBasicBll.CreateTransBasic(TransBasicPostDto TransBasicPostDto)
        {
            await TransBasicRepo.CreateTransBasic(TransBasicPostDto);
        }

        async Task<Transfbasic> ITransBasicBll.DeleteTypeDeTube(int idTransBasic)
        {
            var t = await TransBasicRepo.GetTransBasicById(idTransBasic);
            if (t == null)
                return null;

            await TransBasicRepo.DeleteTransBasic(t);
            return t;
        }

        async Task<Transfbasic> ITransBasicBll.UpdateTransBasic(TransBasicPutDto TransBasicPutDto)
        {
            var TransBasic = await TransBasicRepo.GetTransBasicById(TransBasicPutDto.IdTransfbasic);
            if (TransBasic == null)
                return null;

            await TransBasicRepo.UpdateTransBasic(TransBasic, TransBasicPutDto);

            return TransBasic;
        }
    }
}
