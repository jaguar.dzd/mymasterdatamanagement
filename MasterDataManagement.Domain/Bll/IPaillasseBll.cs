﻿using System.Collections.Generic;
using MasterDataManagement.Domain.Repositories;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.Paillasse;

namespace MasterDataManagement.Domain.Bll
{
    public interface IPaillasseBll
    {
        Task<Paillasse> GetPaillasse(int idPaillasse);
        Task<List<Paillasse>> GetPaillasses();
        Task CreatePaillasse(PaillassePostDto paillassePostDto);
        Task<Paillasse> UpdatePaillasse(PaillassePostDto paillassePostDto);

        Task<Paillasse> DeletePaillasse(int idPaillasse);
    }
    public class PaillasseBll : IPaillasseBll
    {
        private readonly IPaillasseRepo _paillasseRepo;

        public PaillasseBll(IPaillasseRepo paillasseRepo)
        {
            _paillasseRepo = paillasseRepo;
        }


        async Task<Paillasse> IPaillasseBll.GetPaillasse(int idPaillasse)
        {
            var paillasse = await _paillasseRepo.GetPaillasseById(idPaillasse);

            return paillasse;
        }

        async Task<List<Paillasse>> IPaillasseBll.GetPaillasses()
        {
            var paillasses = await _paillasseRepo.GetPaillasses();

            return paillasses;
        }

        async Task IPaillasseBll.CreatePaillasse(PaillassePostDto paillassePostDto)
        {
            await _paillasseRepo.CreatePaillasse(paillassePostDto);
        }

        async Task<Paillasse> IPaillasseBll.UpdatePaillasse(PaillassePostDto paillassePostDto)
        {
            var paillasse = await _paillasseRepo.GetPaillasseById(paillassePostDto.IdPaillasse);

            if (paillasse == null)
                return null;

            return await _paillasseRepo.UpdatePaillasse(paillasse, paillassePostDto);

        }

        async Task<Paillasse> IPaillasseBll.DeletePaillasse(int idPaillasse)
        {
            var paillasse = await _paillasseRepo.GetPaillasseById(idPaillasse);

            if (paillasse == null)
                return null;

            await _paillasseRepo.DeletePaillasse(paillasse);

            return paillasse;
        }
    }
}
