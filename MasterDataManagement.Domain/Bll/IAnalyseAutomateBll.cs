﻿using System;
using System.Collections.Generic;
using MasterDataManagement.Domain.Repositories;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.AnalyseAutomate;

namespace MasterDataManagement.Domain.Bll
{
    public interface IAnalyseAutomateBll
    {
        Task<AnalyseAutomate> GetAnalyseAutomate(int idAnalyseAutomate);
        Task<List<AnalyseAutomate>> GetAnalyseAutomates();
        Task CreateAnalyseAutomate(AnalyseAutomatePostDto analyseAutomatePostDto);
        Task<AnalyseAutomate> UpdateAnalyseAutomate(AnalyseAutomatePostDto analyseAutomatePostDto);
        Task<AnalyseAutomate> DeleteAnalyseAutomate(int idAnalyseAutomate);
        Task<bool> CreateFullAnalyseAutomate(AnalyseAutomateFullPostDto analyseAutomatePostDto);
        Task<bool> UpdateFullAnalyseAutomate(AnalyseAutomateFullPostDto analyseAutomatePostDto);
        Task<bool> CreateAndLinkFullAnalyseAutomate(AnalyseAutomateFullPostDto analyseAutomatePostDto, int catalogueId);
        Task<bool> LinkAnalyseAutomateToCommAutos(LinkAnalyseAutomateToCommAutos data);
        Task<bool> LinkAnalyseAutomateToIncidenceDesFlags(LinkAnalyseAutomateToIncidenceDesFlags data);
        Task<bool> LinkAnalyseAutomateToTransformations(LinkAnalyseAutomateToTransformations data);
        Task<bool> LinkAnalyseAutomateToTransBasics(LinkAnalyseToTransBasic data);
        Task<bool> LinkAnalyseAutomateToEvaluations(LinkAnalyseAutomateToEvaluations data);
        Task<bool> LinkAnalyseAutomateToFonctionReflexes(LinkAnalyseAutomateToFonctionReflexes data);
        Task<bool> LinkAnalyseAutomateToBilans(LinkAnalyseAutomateToBilans data);
        Task<bool> LinkAnalyseAutomateToValidations(LinkAnalyseAutomateToValidations data);
    }
    public class AnalyseAutomateBll : IAnalyseAutomateBll
    {
        private readonly IAnalyseAutomateRepo _analyseAutomateRepo;
        private readonly IPaillasseRepo _paillasseRepo;
        private readonly ITypeDeTubeRepo _typeDeTubeRepo;
        private readonly IAnalyseRepo _analyseRepo;
        private readonly IAutomateRepo _automateRepo;
        private readonly IValidationRepo _validationRepo;
        private readonly IBilanRepo _bilanRepo;
        private readonly IFonctionReflexesRepo _fonctionReflexesRepo;
        private readonly IEvaluationRepo _evaluationRepo;
        private readonly ITransBasicRepo _transBasicRepo;
        private readonly ITransformationRepo _transformationRepo;
        private readonly IIncidenceDesFlagsRepo _incidenceDesFlagsRepo;
        private readonly ICommAutoRepo _commAutoRepo;
        private readonly ICatalogueRepo catalogueRepo;

        public AnalyseAutomateBll(IAnalyseAutomateRepo analyseAutomateRepo, IPaillasseRepo paillasseRepo, 
            ITypeDeTubeRepo typeDeTubeRepo, IAnalyseRepo analyseRepo, IAutomateRepo automateRepo, IValidationRepo validationRepo, 
            IBilanRepo bilanRepo, IFonctionReflexesRepo fonctionReflexesRepo, IEvaluationRepo evaluationRepo, ITransBasicRepo transBasicRepo, 
            ITransformationRepo transformationRepo, IIncidenceDesFlagsRepo incidenceDesFlagsRepo, ICommAutoRepo commAutoRepo,
            ICatalogueRepo catalogueRepo)
        {
            _analyseAutomateRepo = analyseAutomateRepo;
            _paillasseRepo = paillasseRepo;
            _typeDeTubeRepo = typeDeTubeRepo;
            _analyseRepo = analyseRepo;
            _automateRepo = automateRepo;
            _validationRepo = validationRepo;
            _bilanRepo = bilanRepo;
            this._fonctionReflexesRepo = fonctionReflexesRepo;
            this._evaluationRepo = evaluationRepo;
            _transBasicRepo = transBasicRepo;
            _transformationRepo = transformationRepo;
            _incidenceDesFlagsRepo = incidenceDesFlagsRepo;
            _commAutoRepo = commAutoRepo;
            this.catalogueRepo = catalogueRepo;
        }


        async Task<AnalyseAutomate> IAnalyseAutomateBll.GetAnalyseAutomate(int idAnalyseAutomate)
        {
            var analyseAutomate = await _analyseAutomateRepo.GetAnalyseAutomateById(idAnalyseAutomate);

            return analyseAutomate;
        }

        async Task<List<AnalyseAutomate>> IAnalyseAutomateBll.GetAnalyseAutomates()
        {
            var analyseAutomates = await _analyseAutomateRepo.GetAnalyseAutomates();

            return analyseAutomates;
        }

        async Task IAnalyseAutomateBll.CreateAnalyseAutomate(AnalyseAutomatePostDto analyseAutomatePostDto)
        {

            if (!await TryValidateAnalyseAutomatePostDto(analyseAutomatePostDto))
                return;

            await _analyseAutomateRepo.CreateAnalyseAutomate(analyseAutomatePostDto);
        }

        async Task<AnalyseAutomate> IAnalyseAutomateBll.UpdateAnalyseAutomate(AnalyseAutomatePostDto analyseAutomatePostDto)
        {
            var analyseAutomate = await _analyseAutomateRepo.GetAnalyseAutomateById(analyseAutomatePostDto.IdAnalyseAutomate);

            if (analyseAutomate == null)
                return null;

            if (!await TryValidateAnalyseAutomatePostDto(analyseAutomatePostDto))
                return null;

            return await _analyseAutomateRepo.UpdateAnalyseAutomate(analyseAutomate, analyseAutomatePostDto);

        }

        async Task<AnalyseAutomate> IAnalyseAutomateBll.DeleteAnalyseAutomate(int idAnalyseAutomate)
        {
            var analyseAutomate = await _analyseAutomateRepo.GetAnalyseAutomateById(idAnalyseAutomate);

            if (analyseAutomate == null)
                return null;

            await _analyseAutomateRepo.DeleteAnalyseAutomate(analyseAutomate);

            return analyseAutomate;
        }


        private async Task<bool> TryValidateAnalyseAutomatePostDto(AnalyseAutomatePostDto analyseAutomatePostDto)
        {
            var analyse = await _analyseRepo.GetAnalyseById(analyseAutomatePostDto.IdAnalyse);

            if (analyse == null)
                return false;

            var automate = await _automateRepo.GetAutomateById(analyseAutomatePostDto.IdAutomate);

            if (automate == null)
                return false;

            var paillasse = await _paillasseRepo.GetPaillasseById(Convert.ToInt32(analyseAutomatePostDto.IdPaillasse));

            if (paillasse == null)
                analyseAutomatePostDto.IdPaillasse = null;

            var typDeTube = await _typeDeTubeRepo.GetTypeDeTubeById(Convert.ToInt32(analyseAutomatePostDto.IdTypeDeTube));

            if (typDeTube == null)
                analyseAutomatePostDto.IdTypeDeTube = null;

            return true;
        }


        async Task<bool> IAnalyseAutomateBll.CreateFullAnalyseAutomate(AnalyseAutomateFullPostDto analyseAutomatePostDto)
        {
            foreach (var item in analyseAutomatePostDto.AnalyseAutomate)
            {
                if (!await TryValidateAnalyseAutomatePostDto(item))
                    return false;
            }

            return await _analyseAutomateRepo.CreateFullAnalyseAutomate(analyseAutomatePostDto);
        }

      
        async Task<bool> IAnalyseAutomateBll.UpdateFullAnalyseAutomate(AnalyseAutomateFullPostDto analyseAutomateFullPostDto)
        {
            foreach (var item in analyseAutomateFullPostDto.AnalyseAutomate)
            {
                if (!await TryValidateAnalyseAutomatePostDto(item))
                    return false;
            }

            return await _analyseAutomateRepo.UpdateFullAnalyseAutomate(analyseAutomateFullPostDto);

        }

        async Task<bool> IAnalyseAutomateBll.CreateAndLinkFullAnalyseAutomate(AnalyseAutomateFullPostDto analyseAutomatePostDto, int catalogueId)
        {
            foreach (var item in analyseAutomatePostDto.AnalyseAutomate)
            {
                if (!await TryValidateAnalyseAutomatePostDto(item))
                    return false;
            }

            var catalogue = await catalogueRepo.GetCatalogueById(catalogueId);
            if (catalogue == null)
                return false;

            return await _analyseAutomateRepo.CreateFullAnalyseAutomateCatalogue(analyseAutomatePostDto, catalogue);
        }

        async Task<bool> IAnalyseAutomateBll.LinkAnalyseAutomateToCommAutos(LinkAnalyseAutomateToCommAutos data)
        {
            if (!await _analyseAutomateRepo.ExistAll(data.IdAnalyseAutomates)) return false;

            if (!await _commAutoRepo.ExistAll(data.CommAutosIds)) return false;

            var result = await _analyseAutomateRepo.LinkAnalyseAutomateToCommAutos(data.IdAnalyseAutomates, data.CommAutosIds);
            return result;
        }

        async Task<bool> IAnalyseAutomateBll.LinkAnalyseAutomateToIncidenceDesFlags(LinkAnalyseAutomateToIncidenceDesFlags data)
        {
            if (!await _analyseAutomateRepo.ExistAll(data.IdAnalyseAutomates)) return false;

            if (!await _incidenceDesFlagsRepo.ExistAll(data.IncidenceDesFlagsIds)) return false;

            var result = await _analyseAutomateRepo.LinkAnalyseAutomateToIncidenceDesFlags(data.IdAnalyseAutomates, data.IncidenceDesFlagsIds);
            return result;
        }

        async Task<bool> IAnalyseAutomateBll.LinkAnalyseAutomateToTransformations(LinkAnalyseAutomateToTransformations data)
        {
            if (!await _analyseAutomateRepo.ExistAll(data.IdAnalyseAutomates)) return false;

            if (!await _transformationRepo.ExistAll(data.TransformationsIds)) return false;

            var result = await _analyseAutomateRepo.LinkAnalyseAutomateToTransformations(data.IdAnalyseAutomates, data.TransformationsIds);
            return result;
        }

        async Task<bool> IAnalyseAutomateBll.LinkAnalyseAutomateToTransBasics(LinkAnalyseToTransBasic data)
        {
            if (!await _analyseAutomateRepo.ExistAll(data.IdAnalyseAutomates)) return false;

            if (!await _transBasicRepo.ExistAll(data.TransBasicIds)) return false;

            var result = await _analyseAutomateRepo.LinkAnalyseAutomateToTransBasics(data.IdAnalyseAutomates, data.TransBasicIds);
            return result;
        }

        async Task<bool> IAnalyseAutomateBll.LinkAnalyseAutomateToEvaluations(LinkAnalyseAutomateToEvaluations data)
        {
            if (!await _analyseAutomateRepo.ExistAll(data.IdAnalyseAutomates)) return false;

            if (!await _evaluationRepo.ExistAll(data.Evaluationsds)) return false;

            var result = await _analyseAutomateRepo.LinkAnalyseAutomateToEvaluations(data.IdAnalyseAutomates, data.Evaluationsds);
            return result;
        }

        async Task<bool> IAnalyseAutomateBll.LinkAnalyseAutomateToFonctionReflexes(LinkAnalyseAutomateToFonctionReflexes data)
        {
            if (!await _analyseAutomateRepo.ExistAll(data.IdAnalyseAutomates)) return false;

            if (!await _fonctionReflexesRepo.ExistAll(data.FonctionReflexesIds)) return false;

            var result = await _analyseAutomateRepo.LinkAnalyseAutomateToFonctionReflexes(data.IdAnalyseAutomates, data.FonctionReflexesIds);
            return result;
        }

        async Task<bool> IAnalyseAutomateBll.LinkAnalyseAutomateToBilans(LinkAnalyseAutomateToBilans data)
        {
            if (!await _analyseAutomateRepo.ExistAll(data.IdAnalyseAutomates)) return false;

            if (!await _bilanRepo.ExistAll(data.BilansIds)) return false;

            var result = await _analyseAutomateRepo.LinkAnalyseAutomateToBilans(data.IdAnalyseAutomates, data.BilansIds);
            return result;
        }

        async Task<bool> IAnalyseAutomateBll.LinkAnalyseAutomateToValidations(LinkAnalyseAutomateToValidations data)
        {
            if (!await _analyseAutomateRepo.ExistAll(data.IdAnalyseAutomates)) return false;

            if (!await _validationRepo.ExistAll(data.ValidationsIds)) return false;

            var result = await _analyseAutomateRepo.LinkAnalyseAutomateToValidations(data.IdAnalyseAutomates, data.ValidationsIds);
            return result;
        }
    }
}
