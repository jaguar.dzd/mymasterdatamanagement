﻿using System.Collections.Generic;
using MasterDataManagement.Domain.Repositories;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.Droit;

namespace MasterDataManagement.Domain.Bll
{
    public interface IDroitBll
    {
        Task<Droit> GetDroit(int idDroit);
        Task<List<Droit>> GetDroits();
        Task CreateDroit(DroitPostDto droitPostDto);
        Task<Droit> UpdateDroit(DroitPostDto droitPostDto);

        Task<Droit> DeleteDroit(int idDroit);
    }
    public class DroitBll : IDroitBll
    {
        private readonly IDroitRepo _droitRepo;

        public DroitBll(IDroitRepo droitRepo)
        {
            _droitRepo = droitRepo;
        }


        async Task<Droit> IDroitBll.GetDroit(int idDroit)
        {
            var droit = await _droitRepo.GetDroitById(idDroit);

            return droit;
        }

        async Task<List<Droit>> IDroitBll.GetDroits()
        {
            var droits = await _droitRepo.GetDroits();

            return droits;
        }

        async Task IDroitBll.CreateDroit(DroitPostDto droitPostDto)
        {
            await _droitRepo.CreateDroit(droitPostDto);
        }

        async Task<Droit> IDroitBll.UpdateDroit(DroitPostDto droitPostDto)
        {
            var Droit = await _droitRepo.GetDroitById(droitPostDto.IdDroit);

            if (Droit == null)
                return null;

            return await _droitRepo.UpdateDroit(Droit, droitPostDto);

        }

        async Task<Droit> IDroitBll.DeleteDroit(int idDroit)
        {
            var droit = await _droitRepo.GetDroitById(idDroit);

            if (droit == null)
                return null;

            await _droitRepo.DeleteDroit(droit);

            return droit;
        }
    }
}
