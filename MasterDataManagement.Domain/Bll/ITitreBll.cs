﻿using MasterDataManagement.Domain.Models;
using MasterDataManagement.Domain.Repositories;
using MasterDataManagement.DTO.Titre;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MasterDataManagement.Domain.Bll
{
    public interface ITitreBll
    {
        Task<Titres> GetTitre(int idTitre);
        Task<List<Titres>> GetTitres();
        Task CreateTitre(TitrePostDto titrePostDto);
        Task<Titres> UpdateTitre(TitrePutDto titrePutDto);
        Task<Titres> DeleteTitre(int idTitre);
    }
    public class TitreBll : ITitreBll
    {
        private readonly ITitreRepo titreRepo;

        public TitreBll(ITitreRepo titreRepo)
        {
            this.titreRepo = titreRepo;
        }

        async Task ITitreBll.CreateTitre(TitrePostDto titrePostDto)
        {
            await titreRepo.CreateTitre(titrePostDto);
        }

        async Task<Titres> ITitreBll.DeleteTitre(int idTitre)
        {
            var Titre = await titreRepo.GetTitreById(idTitre);

            if (Titre == null)
                return null;

            await titreRepo.DeleteTitre(Titre);

            return Titre;
        }

        async Task<Titres> ITitreBll.GetTitre(int idTitre)
        {
            var Titre = await titreRepo.GetTitreById(idTitre);

            return Titre;
        }

        async Task<List<Titres>> ITitreBll.GetTitres()
        {
            var Titres = await titreRepo.GetTitres();

            return Titres;
        }

        async Task<Titres> ITitreBll.UpdateTitre(TitrePutDto titrePutDto)
        {
            var Titre = await titreRepo.GetTitreById(titrePutDto.IdTitres);

            if (Titre == null)
                return null;

            return await titreRepo.UpdateTitre(Titre, titrePutDto);

        }
    }
}
