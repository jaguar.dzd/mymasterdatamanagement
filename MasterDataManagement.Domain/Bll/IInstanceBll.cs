﻿using System.Collections.Generic;
using MasterDataManagement.Domain.Repositories;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.Instance;
using MasterDataManagement.DTO.InstanceCatalogue;

namespace MasterDataManagement.Domain.Bll
{
    public interface IInstanceBll
    {
        Task<Instance> GetInstance(byte idInstance);
        Task<List<Instance>> GetInstances();
        Task CreateInstance(InstancePostDto instancePostDto);
        Task<Instance> UpdateInstance(InstanceUpdateDto instancePostDto);
        Task<Instance> DeleteInstance(byte idInstance);
        Task<bool> LinkInstanceToCatalogues(int idCatalogue, List<int> instancesIds);
        Task<bool> RemoveInstanceFromCatalogues(int idCatalogue, List<int> instancesIds);
        Task<bool> ApproveOrRefuseCatalogueApplication(InstanceCataloguePostDto data);
    }
    public class InstanceBll : IInstanceBll
    {
        private readonly IInstanceRepo _instanceRepo;
        private readonly ICatalogueRepo catalogueRepo;
        private readonly IInstanceCatalogueRepo instanceCatalogue;

        public InstanceBll(IInstanceRepo instanceRepo, ICatalogueRepo catalogueRepo, IInstanceCatalogueRepo instanceCatalogue)
        {
            _instanceRepo = instanceRepo;
            this.catalogueRepo = catalogueRepo;
            this.instanceCatalogue = instanceCatalogue;
        }

        async Task<Instance> IInstanceBll.GetInstance(byte idInstance)
        {
            var instance = await _instanceRepo.GetInstanceById(idInstance);
            return instance;
        }

        async Task<List<Instance>> IInstanceBll.GetInstances()
        {
            var instances = await _instanceRepo.GetInstances();
            return instances;
        }

        async Task IInstanceBll.CreateInstance(InstancePostDto instancePostDto)
        {
            await _instanceRepo.CreateInstance(instancePostDto);
        }

        async Task<Instance> IInstanceBll.UpdateInstance(InstanceUpdateDto instancePostDto)
        {
            var instance = await _instanceRepo.GetInstanceById(instancePostDto.IdInstance);

            if (instance == null)
                return null;

            return await _instanceRepo.UpdateInstance(instance, instancePostDto);

        }

        async Task<Instance> IInstanceBll.DeleteInstance(byte idInstance)
        {
            var instance = await _instanceRepo.GetInstanceById(idInstance);

            if (instance == null)
                return null;

            await _instanceRepo.DeleteInstance(instance);

            return instance;
        }

        async Task<bool> IInstanceBll.LinkInstanceToCatalogues(int idCatalogue, List<int> instancesIds)
        {
            var catalogue = await catalogueRepo.GetCatalogueById(idCatalogue);
            if (catalogue == null)
                return false;

            if (await _instanceRepo.ExistAll(instancesIds))
            {
                await _instanceRepo.LinkInstanceToCatalogues(idCatalogue, instancesIds);
                return true;
            }
            return false;
        }

        async Task<bool> IInstanceBll.RemoveInstanceFromCatalogues(int idCatalogue, List<int> instancesIds)
        {
            var catalogue = await catalogueRepo.GetCatalogueById(idCatalogue);
            if (catalogue == null)
                return false;

            if (await _instanceRepo.ExistAll(instancesIds))
            {
                await _instanceRepo.RemoveInstanceFromCatalogues(idCatalogue, instancesIds);
                return true;
            }
            return false;
        }

        async Task<bool> IInstanceBll.ApproveOrRefuseCatalogueApplication(InstanceCataloguePostDto data)
        {
            if (data == null)
                return false;

            var result = await instanceCatalogue.ApproveOrRefuseCatalogueApplication(data.Data);
            return result;
        }
    }
}
