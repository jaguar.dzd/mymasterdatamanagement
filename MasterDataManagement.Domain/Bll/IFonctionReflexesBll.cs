﻿using System.Collections.Generic;
using MasterDataManagement.Domain.Repositories;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.FonctionReflexes;

namespace MasterDataManagement.Domain.Bll
{
    public interface IFonctionReflexesBll
    {
        Task<Fonctionsreflexes> GetFonctionReflexes(int idFonctionReflexes);
        Task<List<Fonctionsreflexes>> GetFonctionReflexess();
        Task CreateFonctionReflexes(FonctionReflexesPostDto fonctionReflexesPostDto);
        Task<Fonctionsreflexes> UpdateFonctionReflexes(FonctionReflexesPostDto fonctionReflexesPostDto);

        Task<Fonctionsreflexes> DeleteFonctionReflexes(int idFonctionReflexes);
    }
    public class FonctionReflexesBll : IFonctionReflexesBll
    {
        private readonly IFonctionReflexesRepo _fonctionReflexesRepo;

        public FonctionReflexesBll(IFonctionReflexesRepo fonctionReflexesRepo)
        {
            _fonctionReflexesRepo = fonctionReflexesRepo;
        }


        async Task<Fonctionsreflexes> IFonctionReflexesBll.GetFonctionReflexes(int idFonctionReflexes)
        {
            var fonctionReflexes = await _fonctionReflexesRepo.GetFonctionReflexesById(idFonctionReflexes);

            return fonctionReflexes;
        }

        async Task<List<Fonctionsreflexes>> IFonctionReflexesBll.GetFonctionReflexess()
        {
            var fonctionReflexess = await _fonctionReflexesRepo.GetFonctionReflexes();

            return fonctionReflexess;
        }

        async Task IFonctionReflexesBll.CreateFonctionReflexes(FonctionReflexesPostDto fonctionReflexesPostDto)
        {
            await _fonctionReflexesRepo.CreateFonctionReflexes(fonctionReflexesPostDto);
        }

        async Task<Fonctionsreflexes> IFonctionReflexesBll.UpdateFonctionReflexes(FonctionReflexesPostDto fonctionReflexesPostDto)
        {
            var fonctionReflexes = await _fonctionReflexesRepo.GetFonctionReflexesById(fonctionReflexesPostDto.IdFonctionsReflexes);

            if (fonctionReflexes == null)
                return null;

            return await _fonctionReflexesRepo.UpdateFonctionReflexes(fonctionReflexes, fonctionReflexesPostDto);

        }

        async Task<Fonctionsreflexes> IFonctionReflexesBll.DeleteFonctionReflexes(int idFonctionReflexes)
        {
            var fonctionReflexes = await _fonctionReflexesRepo.GetFonctionReflexesById(idFonctionReflexes);

            if (fonctionReflexes == null)
                return null;

            await _fonctionReflexesRepo.DeleteFonctionReflexes(fonctionReflexes);

            return fonctionReflexes;
        }
    }
}
