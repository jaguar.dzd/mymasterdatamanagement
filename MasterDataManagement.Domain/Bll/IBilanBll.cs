﻿using System.Collections.Generic;
using MasterDataManagement.Domain.Repositories;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.Bilan;

namespace MasterDataManagement.Domain.Bll
{
    public interface IBilanBll
    {
        Task<Bilan> GetBilan(int idBilan);
        Task<List<Bilan>> GetBilans();
        Task CreateBilan(BilanPostDto bilanPostDto);
        Task<Bilan> UpdateBilan(BilanPostDto bilanPostDto);

        Task<Bilan> DeleteBilan(int idBilan);
    }
    public class BilanBll : IBilanBll
    {
        private readonly IBilanRepo _bilanRepo;

        public BilanBll(IBilanRepo bilanRepo)
        {
            _bilanRepo = bilanRepo;
        }


        async Task<Bilan> IBilanBll.GetBilan(int idBilan)
        {
            var bilan = await _bilanRepo.GetBilanById(idBilan);

            return bilan;
        }

        async Task<List<Bilan>> IBilanBll.GetBilans()
        {
            var bilans = await _bilanRepo.GetBilans();

            return bilans;
        }

        async Task IBilanBll.CreateBilan(BilanPostDto bilanPostDto)
        {
            await _bilanRepo.CreateBilan(bilanPostDto);
        }

        async Task<Bilan> IBilanBll.UpdateBilan(BilanPostDto bilanPostDto)
        {
            var bilan = await _bilanRepo.GetBilanById(bilanPostDto.IdBilan);

            if (bilan == null)
                return null;

            return await _bilanRepo.UpdateBilan(bilan, bilanPostDto);

        }

        async Task<Bilan> IBilanBll.DeleteBilan(int idBilan)
        {
            var bilan = await _bilanRepo.GetBilanById(idBilan);

            if (bilan == null)
                return null;

            await _bilanRepo.DeleteBilan(bilan);

            return bilan;
        }
    }
}
