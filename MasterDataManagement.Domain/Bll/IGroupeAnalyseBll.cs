﻿using System.Collections.Generic;
using MasterDataManagement.Domain.Repositories;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.GroupeAnalyse;

namespace MasterDataManagement.Domain.Bll
{
    public interface IGroupeAnalyseBll
    {
        Task<Groupeanalyse> GetGroupeAnalyse(int idGroupeAnalyse);
        Task<List<Groupeanalyse>> GetGroupeAnalyses();
        Task CreateGroupeAnalyse(GroupeAnalysePostDto groupeAnalysePostDto);
        Task<Groupeanalyse> UpdateGroupeAnalyse(GroupeAnalysePostDto groupeAnalysePostDto);

        Task<Groupeanalyse> DeleteGroupeAnalyse(int idGroupeAnalyse);
    }
    public class GroupeAnalyseBll : IGroupeAnalyseBll
    {
        private readonly IGroupeAnalyseRepo _groupeAnalyseRepo;

        public GroupeAnalyseBll(IGroupeAnalyseRepo groupeAnalyseRepo)
        {
            _groupeAnalyseRepo = groupeAnalyseRepo;
        }


        async Task<Groupeanalyse> IGroupeAnalyseBll.GetGroupeAnalyse(int idGroupeAnalyse)
        {
            var groupeAnalyse = await _groupeAnalyseRepo.GetGroupeAnalyseById(idGroupeAnalyse);

            return groupeAnalyse;
        }

        async Task<List<Groupeanalyse>> IGroupeAnalyseBll.GetGroupeAnalyses()
        {
            var groupeAnalyses = await _groupeAnalyseRepo.GetGroupeAnalyses();

            return groupeAnalyses;
        }

        async Task IGroupeAnalyseBll.CreateGroupeAnalyse(GroupeAnalysePostDto groupeAnalysePostDto)
        {
            await _groupeAnalyseRepo.CreateGroupeAnalyse(groupeAnalysePostDto);
        }

        async Task<Groupeanalyse> IGroupeAnalyseBll.UpdateGroupeAnalyse(GroupeAnalysePostDto groupeAnalysePostDto)
        {
            var groupeAnalyse = await _groupeAnalyseRepo.GetGroupeAnalyseById(groupeAnalysePostDto.IdGroupeAnalyse);

            if (groupeAnalyse == null)
                return null;

            return await _groupeAnalyseRepo.UpdateGroupeAnalyse(groupeAnalyse, groupeAnalysePostDto);
        }

        async Task<Groupeanalyse> IGroupeAnalyseBll.DeleteGroupeAnalyse(int idGroupeAnalyse)
        {
            var groupeAnalyse = await _groupeAnalyseRepo.GetGroupeAnalyseById(idGroupeAnalyse);

            if (groupeAnalyse == null)
                return null;

            await _groupeAnalyseRepo.DeleteGroupeAnalyse(groupeAnalyse);

            return groupeAnalyse;
        }
    }
}
