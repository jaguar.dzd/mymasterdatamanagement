﻿using System.Collections.Generic;
using MasterDataManagement.Domain.Repositories;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.Validation;

namespace MasterDataManagement.Domain.Bll
{
    public interface IValidationBll
    {
        Task<Validations> GetValidation(int idValidation);
        Task<List<Validations>> GetValidations();
        Task CreateValidation(ValidationPostDto validationPostDto);
        Task<Validations> UpdateValidation(ValidationPostDto validationPostDto);

        Task<Validations> DeleteValidation(int idValidation);
    }
    public class ValidationBll : IValidationBll
    {
        private readonly IValidationRepo _validationRepo;

        public ValidationBll(IValidationRepo validationRepo)
        {
            _validationRepo = validationRepo;
        }


        async Task<Validations> IValidationBll.GetValidation(int idValidation)
        {
            var validation = await _validationRepo.GetValidationById(idValidation);

            return validation;
        }

        async Task<List<Validations>> IValidationBll.GetValidations()
        {
            var validations = await _validationRepo.GetValidations();

            return validations;
        }

        async Task IValidationBll.CreateValidation(ValidationPostDto validationPostDto)
        {
            await _validationRepo.CreateValidation(validationPostDto);
        }

        async Task<Validations> IValidationBll.UpdateValidation(ValidationPostDto validationPostDto)
        {
            var validation = await _validationRepo.GetValidationById(validationPostDto.IdValidations);

            if (validation == null)
                return null;

            return await _validationRepo.UpdateValidation(validation, validationPostDto);

        }

        async Task<Validations> IValidationBll.DeleteValidation(int idValidation)
        {
            var validation = await _validationRepo.GetValidationById(idValidation);

            if (validation == null)
                return null;

            await _validationRepo.DeleteValidation(validation);

            return validation;
        }
    }
}
