﻿using MasterDataManagement.Domain.Models;
using MasterDataManagement.Domain.Repositories;
using MasterDataManagement.DTO.Soustitres;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MasterDataManagement.Domain.Bll
{
    public interface ISoustitresBll
    {
        Task<Soustitres> GetSoustitres(int idSoustitres);
        Task<List<Soustitres>> GetSoustitress();
        Task CreateSoustitres(SoustitresPostDto soustitresPostDto);
        Task<Soustitres> UpdateSoustitres(SoustitresPutDto soustitresPutDto);
        Task<Soustitres> DeleteSoustitres(int idSoustitres);
    }
    public class SoustitresBll : ISoustitresBll
    {
        private readonly ISoustitresRepo soustitresRepo;

        public SoustitresBll(ISoustitresRepo soustitresRepo)
        {
            this.soustitresRepo = soustitresRepo;
        }

        async Task ISoustitresBll.CreateSoustitres(SoustitresPostDto soustitresPostDto)
        {
            await soustitresRepo.CreateSoustitres(soustitresPostDto);
        }

        async Task<Soustitres> ISoustitresBll.DeleteSoustitres(int idSoustitres)
        {
            var st = await soustitresRepo.GetSoustitresById(idSoustitres);
            if (st == null)
                return null;

            await soustitresRepo.DeleteSousTitre(st);
            return st;
        }

        async Task<Soustitres> ISoustitresBll.GetSoustitres(int idSoustitres)
        {
            return await soustitresRepo.GetSoustitresById(idSoustitres);
        }

        async Task<List<Soustitres>> ISoustitresBll.GetSoustitress()
        {
            return await soustitresRepo.GetSoustitres();
        }

        async Task<Soustitres> ISoustitresBll.UpdateSoustitres(SoustitresPutDto soustitresPutDto)
        {
            var sousTitre = await soustitresRepo.GetSoustitresById(soustitresPutDto.IdSousTitres);
            if (sousTitre == null)
                return null;

            await soustitresRepo.UpdateSoustitres(sousTitre, soustitresPutDto);

            return sousTitre;
        }
    }
}
