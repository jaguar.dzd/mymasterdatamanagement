﻿using MasterDataManagement.Domain.Models;
using MasterDataManagement.Domain.Repositories;
using MasterDataManagement.DTO.IncidencesDesFlags;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MasterDataManagement.Domain.Bll
{
    public interface IIncidenceDesFlagsBll
    {
        Task<Incidencedesflags> GetIncidenceDesFlag(int idIncidenceDesFlags);
        Task<List<Incidencedesflags>> GetIncidenceDesFlags();
        Task CreateIncidenceDesFlags(IncidencedesflagsPostDto incidencedesflags);
        Task<Incidencedesflags> UpdateIncidenceDesFlags(IncidencedesflagsPutDto incidencedesflags);

        Task<Incidencedesflags> DeleteIncidenceDesFlags(int idIncidenceDesFlags);
    }
    public class IncidenceDesFlagsBll : IIncidenceDesFlagsBll
    {
        private readonly IIncidenceDesFlagsRepo incidenceDesFlagsRepo;

        public IncidenceDesFlagsBll(IIncidenceDesFlagsRepo incidenceDesFlagsRepo)
        {
            this.incidenceDesFlagsRepo = incidenceDesFlagsRepo;
        }

        async Task IIncidenceDesFlagsBll.CreateIncidenceDesFlags(IncidencedesflagsPostDto incidencedesflags)
        {
            await incidenceDesFlagsRepo.CreateIncidenceDesFlags(incidencedesflags);
        }

        async Task<Incidencedesflags> IIncidenceDesFlagsBll.DeleteIncidenceDesFlags(int idIncidenceDesFlags)
        {
            var data = await incidenceDesFlagsRepo.GetIncidenceDesFlagsById(idIncidenceDesFlags);
            if (data == null)
                return null;

            await incidenceDesFlagsRepo.DeleteIncidenceDesFlags(data);
            return data;
        }

        async Task<Incidencedesflags> IIncidenceDesFlagsBll.GetIncidenceDesFlag(int idIncidenceDesFlags)
        {
            return await incidenceDesFlagsRepo.GetIncidenceDesFlagsById(idIncidenceDesFlags);
        }

        async Task<List<Incidencedesflags>> IIncidenceDesFlagsBll.GetIncidenceDesFlags()
        {
            return await incidenceDesFlagsRepo.GetIncidenceDesFlags();
        }

        async Task<Incidencedesflags> IIncidenceDesFlagsBll.UpdateIncidenceDesFlags(IncidencedesflagsPutDto incidencedesflags)
        {
            var data = await incidenceDesFlagsRepo.GetIncidenceDesFlagsById(incidencedesflags.Idincidencedesflags);
            if (data == null)
                return null;

            return await incidenceDesFlagsRepo.UpdateIncidenceDesFlags(data, incidencedesflags);
        }
    }
}
