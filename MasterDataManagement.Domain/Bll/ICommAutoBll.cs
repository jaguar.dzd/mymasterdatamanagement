﻿using System.Collections.Generic;
using MasterDataManagement.Domain.Repositories;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.CommAuto;

namespace MasterDataManagement.Domain.Bll
{
    public interface ICommAutoBll
    {
        Task<Commauto> GetCommAuto(int idCommAuto);
        Task<List<Commauto>> GetCommAutos();
        Task CreateCommAuto(CommAutoPostDto commAutoPostDto);
        Task<Commauto> UpdateCommAuto(CommAutoPostDto commAutoPostDto);

        Task<Commauto> DeleteCommAuto(int idCommAuto);
    }
    public class CommAutoBll : ICommAutoBll
    {
        private readonly ICommAutoRepo _commAutoRepo;

        public CommAutoBll(ICommAutoRepo commAutoRepo)
        {
            _commAutoRepo = commAutoRepo;
        }


        async Task<Commauto> ICommAutoBll.GetCommAuto(int idCommAuto)
        {
            var commAuto = await _commAutoRepo.GetCommAutoById(idCommAuto);

            return commAuto;
        }

        async Task<List<Commauto>> ICommAutoBll.GetCommAutos()
        {
            var commAutos = await _commAutoRepo.GetCommAutos();

            return commAutos;
        }

        async Task ICommAutoBll.CreateCommAuto(CommAutoPostDto commAutoPostDto)
        {
            await _commAutoRepo.CreateCommAuto(commAutoPostDto);
        }

        async Task<Commauto> ICommAutoBll.UpdateCommAuto(CommAutoPostDto commAutoPostDto)
        {
            var commAuto = await _commAutoRepo.GetCommAutoById(commAutoPostDto.IdCommAuto);

            if (commAuto == null)
                return null;

            return await _commAutoRepo.UpdateCommAuto(commAuto, commAutoPostDto);

        }

        async Task<Commauto> ICommAutoBll.DeleteCommAuto(int idCommAuto)
        {
            var commAuto = await _commAutoRepo.GetCommAutoById(idCommAuto);

            if (commAuto == null)
                return null;

            await _commAutoRepo.DeleteCommAuto(commAuto);

            return commAuto;
        }
    }
}
