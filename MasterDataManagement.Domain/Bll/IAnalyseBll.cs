﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterDataManagement.Domain.Repositories;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.Analyse;
using MasterDataManagement.Domain.Others;

namespace MasterDataManagement.Domain.Bll
{
    public interface IAnalyseBll
    {
        Task<Analyse> GetAnalyse(int idAnalyse);
        Task<List<Analyse>> GetAnalyses();
        Task CreateAnalyse(AnalysePostDto analysePostDto);
        Task<Analyse> UpdateAnalyse(AnalysePostDto analysePostDto);
        Task<Analyse> DeleteAnalyse(int idAnalyse);
        Task CreateAnalyses(List<AnalysePostDto> analysePostDtos);
        Task<bool> LinkAnalyseToValidations(int idAnalyse, List<int> validationsIds);
        Task<bool> LinkAnalyseToBilans(int idAnalyse, List<int> bilansIds);
        Task<bool> LinkAnalyseToFonctionReflexes(int idAnalyse, List<int> fonctionReflexesId);
        Task<bool> LinkAnalyseToEvaluations(int idAnalyse, List<int> evaluationsId);
        Task CreateFullAnalyses(AnalysePostFullDto analyse);
        Task<bool> LinkAnalyseToTransBasics(int idAnalyse, List<int> transBasicsIds);
        Task<bool> LinkAnalyseToTransformations(int idAnalyse, List<int> transformationsIds);
        Task<bool> LinkAnalyseToIncidenceDesFlags(int idAnalyse, List<int> incidenceDesFlagsIds);
        Task<bool> LinkAnalyseToCommAutos(int idAnalyse, List<int> commAutosIds);
        Task UpdateFullAnalyses(AnalysePostFullDto analyse);
        Task<FullAnalyse> GetFullAnalyse(int idAnalyse);
        Task CreateOrUpdateAnalyses(List<AnalysePostDto> analyse, bool isReplacingExistingvalues);
    }
    public class AnalyseBll : IAnalyseBll
    {
        private readonly IAnalyseRepo _analyseRepo;
        private readonly IGroupeAnalyseRepo _groupeAnalyseRepo;
        private readonly IPaillasseRepo _paillasseRepo;
        private readonly ITypeDeTubeRepo _typeDeTubeRepo;
        private readonly ISoustitresRepo _soustitresRepo;
        private readonly ITitreRepo _titreRepo;
        private readonly IValidationRepo _validationRepo;
        private readonly IBilanRepo _bilanRepo;
        private readonly IFonctionReflexesRepo _fonctionReflexesRepo;
        private readonly IEvaluationRepo _evaluationRepo;
        private readonly ITransBasicRepo _transBasicRepo;
        private readonly ITransformationRepo _transformationRepo;
        private readonly IIncidenceDesFlagsRepo _incidenceDesFlagsRepo;
        private readonly ICommAutoRepo _commAutoRepo;
        public AnalyseBll(IAnalyseRepo analyseRepo, IGroupeAnalyseRepo groupeAnalyseRepo, IPaillasseRepo paillasseRepo, 
            ITypeDeTubeRepo typeDeTubeRepo, ISoustitresRepo soustitresRepo, ITitreRepo titreRepo, IValidationRepo validationRepo, 
            IBilanRepo bilanRepo, IFonctionReflexesRepo fonctionReflexesRepo, IEvaluationRepo evaluationRepo, ITransBasicRepo transBasicRepo, ITransformationRepo transformationRepo, IIncidenceDesFlagsRepo incidenceDesFlagsRepo, ICommAutoRepo commAutoRepo)
        {
            _analyseRepo = analyseRepo;
            _groupeAnalyseRepo = groupeAnalyseRepo;
            _paillasseRepo = paillasseRepo;
            _typeDeTubeRepo = typeDeTubeRepo;
            _soustitresRepo = soustitresRepo;
            _titreRepo = titreRepo;
            _validationRepo = validationRepo;
            _bilanRepo = bilanRepo;
            this._fonctionReflexesRepo = fonctionReflexesRepo;
            this._evaluationRepo = evaluationRepo;
            _transBasicRepo = transBasicRepo;
            _transformationRepo = transformationRepo;
            _incidenceDesFlagsRepo = incidenceDesFlagsRepo;
            _commAutoRepo = commAutoRepo;
        }


        async Task<Analyse> IAnalyseBll.GetAnalyse(int idAnalyse)
        {
            var analyse = await _analyseRepo.GetAnalyseById(idAnalyse);

            return analyse;
        }

        async Task<List<Analyse>> IAnalyseBll.GetAnalyses()
        {
            var analyses = await _analyseRepo.GetAnalyses();

            return analyses;
        }

        async Task IAnalyseBll.CreateAnalyse(AnalysePostDto analysePostDto)
        {
            analysePostDto = await ValidateAnalysePostDto(analysePostDto);

            await _analyseRepo.CreateAnalyse(analysePostDto);
        }

        async Task IAnalyseBll.CreateAnalyses(List<AnalysePostDto> analysePostDtos)
        {
            analysePostDtos = await ValidateAnalysePostDtos(analysePostDtos);

            await _analyseRepo.CreateAnalyses(analysePostDtos);
        }

        async Task<Analyse> IAnalyseBll.UpdateAnalyse(AnalysePostDto analysePostDto)
        {
            var analyse = await _analyseRepo.GetAnalyseById(analysePostDto.IdAnalyse);

            if (analyse == null)
                return null;

            analysePostDto = await ValidateAnalysePostDto(analysePostDto);

            return await _analyseRepo.UpdateAnalyse(analyse, analysePostDto);

        }

        async Task<Analyse> IAnalyseBll.DeleteAnalyse(int idAnalyse)
        {
            var analyse = await _analyseRepo.GetAnalyseById(idAnalyse);

            if (analyse == null)
                return null;

            await _analyseRepo.DeleteAnalyse(analyse);

            return analyse;
        }

        async Task<bool> IAnalyseBll.LinkAnalyseToValidations(int idAnalyse, List<int> validationsIds)
        {
            var analyse = await _analyseRepo.GetAnalyseById(idAnalyse);

            if (analyse == null)
                return false;


            if (!await _validationRepo.ExistAll(validationsIds)) return false;

            var result = await _analyseRepo.LinkAnalyseToValidations(analyse.IdAnalyse, validationsIds);
            return result;

        }

        async Task<bool> IAnalyseBll.LinkAnalyseToBilans(int idAnalyse, List<int> bilansIds)
        {
            var analyse = await _analyseRepo.GetAnalyseById(idAnalyse);

            if (analyse == null)
                return false;


            if (!await _bilanRepo.ExistAll(bilansIds)) return false;

            var result = await _analyseRepo.LinkAnalyseToBilans(analyse.IdAnalyse, bilansIds);
            return result;
        }


        private async Task<AnalysePostDto> ValidateAnalysePostDto(AnalysePostDto analysePostDto)
        {
            var groupeAnalyse =
                await _groupeAnalyseRepo.GetGroupeAnalyseById(
                    Convert.ToInt32(analysePostDto.GroupeAnalyseIdGroupeAnalyse));

            if (groupeAnalyse == null)
                analysePostDto.GroupeAnalyseIdGroupeAnalyse = null;

            var paillasse = await _paillasseRepo.GetPaillasseById(Convert.ToInt32(analysePostDto.PaillasseIdPaillasse));

            if (paillasse == null)
                analysePostDto.PaillasseIdPaillasse = null;

            var typeDeTube =
                await _typeDeTubeRepo.GetTypeDeTubeById(Convert.ToInt32(analysePostDto.TypeDeTubeIdTypeDeTube));

            if (typeDeTube == null)
                analysePostDto.TypeDeTubeIdTypeDeTube = null;

            var soustitre =
                await _soustitresRepo.GetSoustitresById(Convert.ToInt32(analysePostDto.SousTitresIdSousTitres));

            if (soustitre == null)
                analysePostDto.SousTitresIdSousTitres = null;

            var titre =
                await _titreRepo.GetTitreById(Convert.ToInt32(analysePostDto.TitresIdTitres));

            if (titre == null)
                analysePostDto.TitresIdTitres = null;

            return analysePostDto;
        }


        private async Task<List<AnalysePostDto>> ValidateAnalysePostDtos(List<AnalysePostDto> analysePostDtos)
        {
            var groupeAnalyses = await _groupeAnalyseRepo.GetGroupeAnalyses();

            var paillasses = await _paillasseRepo.GetPaillasses();

            var typeDeTubes = await _typeDeTubeRepo.GetTypeDeTubes();

            var soustitres = await _soustitresRepo.GetSoustitres();

            var titres = await _titreRepo.GetTitres();

            foreach (var analysePostDto in analysePostDtos)
            {
                if (groupeAnalyses.All(x => x.IdGroupeAnalyse != analysePostDto.GroupeAnalyseIdGroupeAnalyse))
                    analysePostDto.GroupeAnalyseIdGroupeAnalyse = null;

                if (paillasses.All(x => x.IdPaillasse != analysePostDto.PaillasseIdPaillasse))
                    analysePostDto.PaillasseIdPaillasse = null;

                if (typeDeTubes.All(x => x.IdTypeDeTube != analysePostDto.TypeDeTubeIdTypeDeTube))
                    analysePostDto.TypeDeTubeIdTypeDeTube = null;

                if (soustitres.All(x => x.IdSousTitres != analysePostDto.SousTitresIdSousTitres))
                    analysePostDto.SousTitresIdSousTitres = null;

                if (titres.All(x => x.IdTitres != analysePostDto.TitresIdTitres))
                    analysePostDto.TitresIdTitres = null;
            }

            return analysePostDtos;
        }

        async Task<bool> IAnalyseBll.LinkAnalyseToFonctionReflexes(int idAnalyse, List<int> fonctionReflexesId)
        {
            var analyse = await _analyseRepo.GetAnalyseById(idAnalyse);

            if (analyse == null)
                return false;

            if (!await _fonctionReflexesRepo.ExistAll(fonctionReflexesId)) return false;

            var result = await _analyseRepo.LinkAnalyseToFonctionReflexes(analyse.IdAnalyse, fonctionReflexesId);
            return result;
        }

        async Task<bool> IAnalyseBll.LinkAnalyseToEvaluations(int idAnalyse, List<int> evaluationsId)
        {
            var analyse = await _analyseRepo.GetAnalyseById(idAnalyse);

            if (analyse == null)
                return false;

            if (!await _evaluationRepo.ExistAll(evaluationsId)) return false;

            var result = await _analyseRepo.LinkAnalyseToEvaluations(analyse.IdAnalyse, evaluationsId);
            return result;
        }

        async Task IAnalyseBll.CreateFullAnalyses(AnalysePostFullDto fullAnalyse)
        {
            fullAnalyse.Analyse = await ValidateAnalysePostDto(fullAnalyse.Analyse);

            await _analyseRepo.CreateFullAnalyse(fullAnalyse);
        }

        async Task<bool> IAnalyseBll.LinkAnalyseToTransBasics(int idAnalyse, List<int> transBasicsIds)
        {
            var analyse = await _analyseRepo.GetAnalyseById(idAnalyse);

            if (analyse == null)
                return false;

            if (!await _transBasicRepo.ExistAll(transBasicsIds)) return false;

            var result = await _analyseRepo.LinkAnalyseToTransBasics(idAnalyse, transBasicsIds);
            return result;
        }

        async Task<bool> IAnalyseBll.LinkAnalyseToTransformations(int idAnalyse, List<int> transformationsIds)
        {
            var analyse = await _analyseRepo.GetAnalyseById(idAnalyse);

            if (analyse == null)
                return false;

            if (!await _transformationRepo.ExistAll(transformationsIds)) return false;

            var result = await _analyseRepo.LinkAnalyseToTransformations(idAnalyse, transformationsIds);
            return result;
        }

        async Task<bool> IAnalyseBll.LinkAnalyseToIncidenceDesFlags(int idAnalyse, List<int> incidenceDesFlagsIds)
        {
            var analyse = await _analyseRepo.GetAnalyseById(idAnalyse);

            if (analyse == null)
                return false;

            if (!await _incidenceDesFlagsRepo.ExistAll(incidenceDesFlagsIds)) return false;

            var result = await _analyseRepo.LinkAnalyseToIncidenceDesFlags(idAnalyse, incidenceDesFlagsIds);
            return result;
        }

        async Task<bool> IAnalyseBll.LinkAnalyseToCommAutos(int idAnalyse, List<int> commAutosIds)
        {
            var analyse = await _analyseRepo.GetAnalyseById(idAnalyse);

            if (analyse == null)
                return false;

            if (!await _commAutoRepo.ExistAll(commAutosIds)) return false;

            var result = await _analyseRepo.LinkAnalyseToCommAutos(idAnalyse, commAutosIds);
            return result;
        }

        async Task IAnalyseBll.UpdateFullAnalyses(AnalysePostFullDto analysePostFullDto)
        {
            var analyse = await _analyseRepo.GetAnalyseById(analysePostFullDto.Analyse.IdAnalyse);

            if (analyse == null)
                return;

            analysePostFullDto.Analyse = await ValidateAnalysePostDto(analysePostFullDto.Analyse);
            
            await _analyseRepo.UpdateFullAnalyse(analyse, analysePostFullDto);
        }

        async Task<FullAnalyse> IAnalyseBll.GetFullAnalyse(int idAnalyse)
        {
            var result = await _analyseRepo.GetFullAnalyse(idAnalyse);
            return result;
        }

        async Task IAnalyseBll.CreateOrUpdateAnalyses(List<AnalysePostDto> analyses, bool isReplacingExistingvalues)
        {
            analyses = await ValidateAnalysePostDtos(analyses);

            await _analyseRepo.CreateOrUpdateAnalyses(analyses, isReplacingExistingvalues);
        }
    }
}
