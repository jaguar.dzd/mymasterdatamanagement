﻿using System.Collections.Generic;
using MasterDataManagement.Domain.Repositories;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.Secteur;

namespace MasterDataManagement.Domain.Bll
{
    public interface ISecteurBll
    {
        Task<Secteur> GetSecteur(int idSecteur);
        Task<List<Secteur>> GetSecteurs();
        Task CreateSecteur(SecteurPostDto secteurPostDto);
        Task<Secteur> UpdateSecteur(SecteurPostDto secteurPostDto);

        Task<Secteur> DeleteSecteur(int idSecteur);
    }
    public class SecteurBll : ISecteurBll
    {
        private readonly ISecteurRepo _secteurRepo;

        public SecteurBll(ISecteurRepo secteurRepo)
        {
            _secteurRepo = secteurRepo;
        }


        async Task<Secteur> ISecteurBll.GetSecteur(int idSecteur)
        {
            var secteur = await _secteurRepo.GetSecteurById(idSecteur);

            return secteur;
        }

        async Task<List<Secteur>> ISecteurBll.GetSecteurs()
        {
            var secteurs = await _secteurRepo.GetSecteurs();

            return secteurs;
        }

        async Task ISecteurBll.CreateSecteur(SecteurPostDto secteurPostDto)
        {
            await _secteurRepo.CreateSecteur(secteurPostDto);
        }

        async Task<Secteur> ISecteurBll.UpdateSecteur(SecteurPostDto secteurPostDto)
        {
            var secteur = await _secteurRepo.GetSecteurById(secteurPostDto.IdSecteur);

            if (secteur == null)
                return null;

            return await _secteurRepo.UpdateSecteur(secteur, secteurPostDto);

        }

        async Task<Secteur> ISecteurBll.DeleteSecteur(int idSecteur)
        {
            var secteur = await _secteurRepo.GetSecteurById(idSecteur);

            if (secteur == null)
                return null;

            await _secteurRepo.DeleteSecteur(secteur);

            return secteur;
        }
    }
}
