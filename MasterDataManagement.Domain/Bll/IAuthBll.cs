﻿using MasterDataManagement.Domain.Models;
using MasterDataManagement.Domain.Repositories;
using MasterDataManagement.Domain.Services;
using MasterDataManagement.DTO.Authentication;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MasterDataManagement.Domain.Bll
{
    public interface IAuthBll
    {
        Task<(User user, GeneratedTokenInfo token)?> Authenticate(AuthPostDto auth);
    }

    public class AuthBll : IAuthBll
    {
        private readonly ITokenGenerator tokenGenerator;
        private readonly IUserRepo userRepo;
        private readonly IHelperService helperService;

        public AuthBll(ITokenGenerator tokenGenerator, IUserRepo userRepo, IHelperService helperService)
        {
            this.tokenGenerator = tokenGenerator;
            this.userRepo = userRepo;
            this.helperService = helperService;
        }

        async Task<(User user, GeneratedTokenInfo token)?> IAuthBll.Authenticate(AuthPostDto auth)
        {
            var user = await userRepo.GetUserWithDetailsByEmail(auth.Email);
            if (user != null)
            {
                var hashedPass = helperService.HashPassword(auth.Password);
                if (hashedPass == user.Password)
                {
                    var token = tokenGenerator.GenerateToken(new TokenDto { UserId = user.IdUser.ToString() });
                    return  (user, token);
                }
            }
            return null;
        }

    }
}
