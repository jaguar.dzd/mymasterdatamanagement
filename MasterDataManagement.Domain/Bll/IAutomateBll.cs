﻿using System.Collections.Generic;
using MasterDataManagement.Domain.Repositories;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.Automate;

namespace MasterDataManagement.Domain.Bll
{
    public interface IAutomateBll
    {
        Task<Automate> GetAutomateById(ushort idAutomate);
        Task<List<Automate>> GetAutomates();
        Task CreateAutomate(AutomatePostDto automatePostDto);
        Task<Automate> UpdateAutomate(AutomatePostDto automatePostDto);
        Task<Automate> DeleteAutomate(ushort idAutomate);
        Task CreateAutomates(List<AutomatePostDto> automatePostDtos);
    }
    public class AutomateBll : IAutomateBll
    {
        private readonly IAutomateRepo _automateRepo;

        public AutomateBll(IAutomateRepo automateRepo)
        {
            _automateRepo = automateRepo;
        }


        async Task<Automate> IAutomateBll.GetAutomateById(ushort idAutomate)
        {
            var automate = await _automateRepo.GetAutomateById(idAutomate);

            return automate;
        }

        async Task<List<Automate>> IAutomateBll.GetAutomates()
        {
            var automates = await _automateRepo.GetAutomates();

            return automates;
        }

        async Task IAutomateBll.CreateAutomate(AutomatePostDto automatePostDto)
        {
            await _automateRepo.CreateAutomate(automatePostDto);
        }

        async Task IAutomateBll.CreateAutomates(List<AutomatePostDto> automatePostDtos)
        {
            await _automateRepo.CreateAutomates(automatePostDtos);
        }

        async Task<Automate> IAutomateBll.UpdateAutomate(AutomatePostDto automatePostDto)
        {
            var automate = await _automateRepo.GetAutomateById(automatePostDto.IdAutomate);

            if (automate == null)
                return null;

            return await _automateRepo.UpdateAutomate(automate, automatePostDto);
        }

        async Task<Automate> IAutomateBll.DeleteAutomate(ushort idAutomate)
        {
            var automate = await _automateRepo.GetAutomateById(idAutomate);

            if (automate == null)
                return null;

            await _automateRepo.DeleteAutomate(automate);

            return automate;
        }
    }
}
