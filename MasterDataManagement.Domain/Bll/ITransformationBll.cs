﻿using MasterDataManagement.Domain.Models;
using MasterDataManagement.Domain.Services;
using MasterDataManagement.DTO.Site;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Repositories;

namespace MasterDataManagement.Domain.Bll
{
    public interface ITransformationBll
    {
        Task<Transformations> GetTransformation(int idTransformation);
        Task<List<Transformations>> GetTransformations();
        Task CreateTransformation(TransformationPostDto transformationPostDto);
        Task<Transformations> UpdateTransformation(TransformationPutDto transformationPutDto);
        Task<Transformations> DeleteTypeDeTube(int idTransformation);
    }
    public class TransformationBll : ITransformationBll
    {
        private readonly ITransformationRepo transformationRepo;

        public TransformationBll(ITransformationRepo transformationRepo)
        {
            this.transformationRepo = transformationRepo;
        }

        async Task<Transformations> ITransformationBll.GetTransformation(int idTransformation)
        {
            return await transformationRepo.GetTransformationById(idTransformation);
        }

        async Task<List<Transformations>> ITransformationBll.GetTransformations()
        {
            return await transformationRepo.GetTransformations();
        }

        async Task ITransformationBll.CreateTransformation(TransformationPostDto transformationPostDto)
        {
            await transformationRepo.CreateTransformation(transformationPostDto);
        }

        async Task<Transformations> ITransformationBll.DeleteTypeDeTube(int idTransformation)
        {
            var t = await transformationRepo.GetTransformationById(idTransformation);
            if (t == null)
                return null;

            await transformationRepo.DeleteTransformation(t);
            return t;
        }

        async Task<Transformations> ITransformationBll.UpdateTransformation(TransformationPutDto transformationPutDto)
        {
            var transformation = await transformationRepo.GetTransformationById(transformationPutDto.IdTransformations);
            if (transformation == null)
                return null;

            await transformationRepo.UpdateTransformation(transformation, transformationPutDto);

            return transformation;
        }
    }
}
