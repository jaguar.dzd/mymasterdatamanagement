﻿using MasterDataManagement.Domain.Models;
using MasterDataManagement.Domain.Repositories;
using MasterDataManagement.Domain.Services;
using MasterDataManagement.DTO.User;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MasterDataManagement.Domain.Bll
{
    public interface IUserBll
    {
        Task<UserPostResponseDto> CreateUser(UserPostDto user);
        Task<List<UserReadDto>> GetUsers();
        Task<bool> AddDroits(ushort idUser, List<int> droitsIds);
        Task<bool> RemoveDroits(ushort idUser, List<int> droitsIds);
        Task<List<User>> GetUsersWithDetails();
    }
    public class UserBll : IUserBll
    {
        private readonly IUserRepo _userRepo;
        private readonly IHelperService  _helperService;
        private readonly IDroitRepo _droitRepo;

        public UserBll(IUserRepo userRepo, IHelperService helperService, IDroitRepo droitRepo)
        {
            _userRepo = userRepo;
            _helperService = helperService;
            _droitRepo = droitRepo;
        }
        async Task<UserPostResponseDto> IUserBll.CreateUser(UserPostDto user)
        {
            if (await _userRepo.UserExists(user.Email))
            {
                return new UserPostResponseDto { Success = false, Reason = "email_already_exists" };
            }

            var hashedPassword = _helperService.HashPassword(user.MotDePasse);
            await _userRepo.CreateUser(user, hashedPassword);
            return new UserPostResponseDto { Success = true };
        }

        async Task<List<UserReadDto>> IUserBll.GetUsers()
        {
            return await _userRepo.GetUsers();
        }

        async Task<List<User>> IUserBll.GetUsersWithDetails()
        {
            return await _userRepo.GetUsersWithDetails();
        }

        async Task<bool> IUserBll.AddDroits(ushort idUser, List<int> droitsIds)
        {
            var user = await _userRepo.GetUserById(idUser);
            if (user == null)
                return false;

            if (!await _droitRepo.ExistAll(droitsIds))
                return false;

            var result = await _userRepo.AddDroits(idUser, droitsIds);
            return result;
        }

        async Task<bool> IUserBll.RemoveDroits(ushort idUser, List<int> droitsIds)
        {
            var user = await _userRepo.GetUserById(idUser);
            if (user == null)
                return false;

            if (!await _droitRepo.ExistAll(droitsIds))
                return false;

            var result = await _userRepo.RemoveDroits(idUser, droitsIds);
            return result;
        }
    }
}
