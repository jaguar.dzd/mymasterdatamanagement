﻿using System.Collections.Generic;
using System.Linq;
using MasterDataManagement.Domain.Repositories;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;

namespace MasterDataManagement.Domain.Bll
{
    public interface IUserInstanceBll
    {
        Task<User> CreateUserInstances(ushort idUser, List<byte> idInstances);
        Task<List<Instance>> GetUserInstances(ushort idUser);
        Task<bool> DeleteUserFromInstances(ushort idUtilisateur, List<int> idInstances);
    }
    public class UserInstanceBll : IUserInstanceBll
    {
        private readonly IUserInstanceRepo _UserInstanceRepo;
        private readonly IUserRepo _userRepo;
        private readonly IInstanceRepo _instanceRepo;

        public UserInstanceBll(IUserInstanceRepo UserInstanceRepo, IUserRepo userRepo, IInstanceRepo instanceRepo)
        {
            _UserInstanceRepo = UserInstanceRepo;
            _userRepo = userRepo;
            _instanceRepo = instanceRepo;
        }

        async Task<User> IUserInstanceBll.CreateUserInstances(ushort idUser, List<byte> idInstances)
        {
            var User = await _userRepo.GetUserById(idUser);

            if (User == null)
                return null;

            idInstances = await ValidateIdInstances(idInstances);

            await _UserInstanceRepo.CreateUserInstances(idUser, idInstances);

            return User;
        }

        async Task<bool> IUserInstanceBll.DeleteUserFromInstances(ushort idUser, List<int> idInstances)
        {
            var user = await _userRepo.GetUserById(idUser);
            if (user == null)
                return false;

            if (await _instanceRepo.ExistAll(idInstances))
            {
                await _UserInstanceRepo.Delete(idUser, idInstances);
                return true;
            }
            return false;
        }

        async Task<List<Instance>> IUserInstanceBll.GetUserInstances(ushort idUser)
        {
            var UserInstances = await _UserInstanceRepo.GetUserInstances(idUser);

            return UserInstances;
        }


        private async Task<List<byte>> ValidateIdInstances(List<byte> idInstances)
        {
            var validIdInstances = new List<byte>();

            if (idInstances == null)
                return validIdInstances;

            var instances = await _instanceRepo.GetInstances();

            foreach (var idInstance in idInstances)
            {
                if (instances.Any(x => x.IdInstance == idInstance))
                    validIdInstances.Add(idInstance);
            }

            return validIdInstances;
        }
    }
}
