﻿using System.Collections.Generic;
using MasterDataManagement.Domain.Repositories;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.TypeDeTube;

namespace MasterDataManagement.Domain.Bll
{
    public interface ITypeDeTubeBll
    {
        Task<Typedetube> GetTypeDeTube(int idTypeDeTube);
        Task<List<Typedetube>> GetTypeDeTubes();
        Task CreateTypeDeTube(TypeDeTubePostDto TypeDeTubePostDto);
        Task<Typedetube> UpdateTypeDeTube(TypeDeTubePostDto TypeDeTubePostDto);

        Task<Typedetube> DeleteTypeDeTube(int idTypeDeTube);
    }
    public class TypeDeTubeBll : ITypeDeTubeBll
    {
        private readonly ITypeDeTubeRepo _TypeDeTubeRepo;

        public TypeDeTubeBll(ITypeDeTubeRepo TypeDeTubeRepo)
        {
            _TypeDeTubeRepo = TypeDeTubeRepo;
        }


        async Task<Typedetube> ITypeDeTubeBll.GetTypeDeTube(int idTypeDeTube)
        {
            var TypeDeTube = await _TypeDeTubeRepo.GetTypeDeTubeById(idTypeDeTube);

            return TypeDeTube;
        }

        async Task<List<Typedetube>> ITypeDeTubeBll.GetTypeDeTubes()
        {
            var TypeDeTubes = await _TypeDeTubeRepo.GetTypeDeTubes();

            return TypeDeTubes;
        }

        async Task ITypeDeTubeBll.CreateTypeDeTube(TypeDeTubePostDto TypeDeTubePostDto)
        {
            await _TypeDeTubeRepo.CreateTypeDeTube(TypeDeTubePostDto);
        }

        async Task<Typedetube> ITypeDeTubeBll.UpdateTypeDeTube(TypeDeTubePostDto TypeDeTubePostDto)
        {
            var TypeDeTube = await _TypeDeTubeRepo.GetTypeDeTubeById(TypeDeTubePostDto.IdTypeDeTube);

            if (TypeDeTube == null)
                return null;

            return await _TypeDeTubeRepo.UpdateTypeDeTube(TypeDeTube, TypeDeTubePostDto);

        }

        async Task<Typedetube> ITypeDeTubeBll.DeleteTypeDeTube(int idTypeDeTube)
        {
            var TypeDeTube = await _TypeDeTubeRepo.GetTypeDeTubeById(idTypeDeTube);

            if (TypeDeTube == null)
                return null;

            await _TypeDeTubeRepo.DeleteTypeDeTube(TypeDeTube);

            return TypeDeTube;
        }
    }
}
