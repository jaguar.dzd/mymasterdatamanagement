﻿using System.Collections.Generic;
using MasterDataManagement.Domain.Repositories;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.DTO.Evaluation;

namespace MasterDataManagement.Domain.Bll
{
    public interface IEvaluationBll
    {
        Task<Evaluations> GetEvaluation(int idEvaluation);
        Task<List<Evaluations>> GetEvaluations();
        Task CreateEvaluation(EvaluationPostDto evaluationPostDto);
        Task<Evaluations> UpdateEvaluation(EvaluationPostDto evaluationPostDto);

        Task<Evaluations> DeleteEvaluation(int idEvaluation);
    }
    public class EvaluationBll : IEvaluationBll
    {
        private readonly IEvaluationRepo _evaluationRepo;

        public EvaluationBll(IEvaluationRepo evaluationRepo)
        {
            _evaluationRepo = evaluationRepo;
        }


        async Task<Evaluations> IEvaluationBll.GetEvaluation(int idEvaluation)
        {
            var evaluation = await _evaluationRepo.GetEvaluationById(idEvaluation);

            return evaluation;
        }

        async Task<List<Evaluations>> IEvaluationBll.GetEvaluations()
        {
            var evaluations = await _evaluationRepo.GetEvaluations();

            return evaluations;
        }

        async Task IEvaluationBll.CreateEvaluation(EvaluationPostDto evaluationPostDto)
        {
            await _evaluationRepo.CreateEvaluation(evaluationPostDto);
        }

        async Task<Evaluations> IEvaluationBll.UpdateEvaluation(EvaluationPostDto evaluationPostDto)
        {
            var evaluation = await _evaluationRepo.GetEvaluationById(evaluationPostDto.IdEvaluations);

            if (evaluation == null)
                return null;

            return await _evaluationRepo.UpdateEvaluation(evaluation, evaluationPostDto);

        }

        async Task<Evaluations> IEvaluationBll.DeleteEvaluation(int idEvaluation)
        {
            var evaluation = await _evaluationRepo.GetEvaluationById(idEvaluation);

            if (evaluation == null)
                return null;

            await _evaluationRepo.DeleteEvaluation(evaluation);

            return evaluation;
        }
    }
}
