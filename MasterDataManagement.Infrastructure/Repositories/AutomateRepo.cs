﻿using System.Collections.Generic;
using System.Linq;
using MasterDataManagement.Domain.Models;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Repositories;
using MasterDataManagement.DTO.Automate;
using Microsoft.EntityFrameworkCore;
using MasterDataManagement.Infrastructure.ManagementDbContext;

namespace MasterDataManagement.Infrastructure.Repositories
{
    public class AutomateRepo : IAutomateRepo
    {
        private readonly MyDbContext _myDbContext;

        public AutomateRepo(MyDbContext myDbContext)
        {
            _myDbContext = myDbContext;
        }
        async Task IAutomateRepo.CreateAutomate(AutomatePostDto automatePostDto)
        {
            await _myDbContext.Automate.AddAsync(((IAutomateRepo)this).MakeAutomate(automatePostDto));
            await _myDbContext.SaveChangesAsync();
        }

        async Task IAutomateRepo.CreateAutomates(List<AutomatePostDto> automatePostDtos)
        {
            foreach (var automatePostDto in automatePostDtos)
            {
                await _myDbContext.Automate.AddAsync(((IAutomateRepo)this).MakeAutomate(automatePostDto));
            }
            await _myDbContext.SaveChangesAsync();
        }


        async Task<Automate> IAutomateRepo.GetAutomateById(ushort idAutomate)
        {
            var automate = await _myDbContext.Automate.FirstOrDefaultAsync(x => x.IdAutomate == idAutomate);
            return automate;
        }

        async Task<Automate> IAutomateRepo.UpdateAutomate(Automate automate, AutomatePostDto automatePostDto)
        {
            automate.Code = automatePostDto.Code;
            automate.AnalyseLeft = automatePostDto.AnalyseLeft;
            automate.AnalyseRight = automatePostDto.AnalyseRight;
            automate.Canal1 = automatePostDto.Canal1;
            automate.Canal2 = automatePostDto.Canal2;
            automate.ChargementAutoAcquisition = automatePostDto.ChargementAutoAcquisition;
            automate.CodePourReception = automatePostDto.CodePourReception;
            automate.CommentaireLeft = automatePostDto.CommentaireLeft;
            automate.CommentaireRight = automatePostDto.CommentaireRight;
            automate.Connexion = automatePostDto.Connexion;
            automate.ControleEclair = automatePostDto.ControleEclair;
            automate.ContrôleDeFlux = automatePostDto.ContrôleDeFlux;
            automate.CoutPatientTypeAutomate = automatePostDto.CoutPatientTypeAutomate;
            automate.DilutionLeft = automatePostDto.DilutionLeft;
            automate.DilutionRight = automatePostDto.DilutionRight;
            automate.DocumentProvisoir = automatePostDto.DocumentProvisoir;
            automate.ExceptionsValidationAutomate = automatePostDto.ExceptionsValidationAutomate;
            automate.FlagLeft = automatePostDto.FlagLeft;
            automate.FlagRight = automatePostDto.FlagRight;
            automate.FormatFichePaillasse = automatePostDto.FormatFichePaillasse;
            automate.FormatNomPatientCq = automatePostDto.FormatNomPatientCq;
            automate.Identification1Left = automatePostDto.Identification1Left;
            automate.Identification1Right = automatePostDto.Identification1Right;
            automate.Identification2Left = automatePostDto.Identification2Left;
            automate.Identification2Right = automatePostDto.Identification2Right;
            automate.IdtubeOrIgrImpose = automatePostDto.IdtubeOrIgrImpose;
            automate.ImprimanteCb = automatePostDto.ImprimanteCb;
            automate.Increment = automatePostDto.Increment;
            automate.IsAfficherJournalConnex = automatePostDto.IsAfficherJournalConnex;
            automate.IsBoutonForcerNonAutorized = automatePostDto.IsBoutonForcerNonAutorized;
            automate.IsControleQualiteActif = automatePostDto.IsControleQualiteActif;
            automate.IsDesValidationBiologique = automatePostDto.IsDesValidationBiologique;
            automate.IsForcerModeEscalve = automatePostDto.IsForcerModeEscalve;
            automate.IsGereDemandesSupression = automatePostDto.IsGereDemandesSupression;
            automate.IsIgnorerLeLotCrConnexion = automatePostDto.IsIgnorerLeLotCrConnexion;
            automate.IsInscrireAutoDansListe = automatePostDto.IsInscrireAutoDansListe;
            automate.IsLotsProbatoires = automatePostDto.IsLotsProbatoires;
            automate.IsNotArchiverTubes = automatePostDto.IsNotArchiverTubes;
            automate.IsPreanalyticAutomate = automatePostDto.IsPreanalyticAutomate;
            automate.IsPrefixerPidNumDemande = automatePostDto.IsPrefixerPidNumDemande;
            automate.IsRespectArboYyyymm = automatePostDto.IsRespectArboYyyymm;
            automate.IsValidateAutoCq = automatePostDto.IsValidateAutoCq;
            automate.IsValidateResultpatientAutoCq = automatePostDto.IsValidateResultpatientAutoCq;
            automate.ModeValidation = automatePostDto.ModeValidation;
            automate.ModelCompteRendu = automatePostDto.ModelCompteRendu;
            automate.MonNom = automatePostDto.MonNom;
            automate.NbPosition = automatePostDto.NbPosition;
            automate.NbrBits = automatePostDto.NbrBits;
            automate.NbreCarTronq = automatePostDto.NbreCarTronq;
            automate.NomPatient = automatePostDto.NomPatient;
            automate.NumAutomate = automatePostDto.NumAutomate;
            automate.OrdreLiaisonLis = automatePostDto.OrdreLiaisonLis;
            automate.OrdreValidation = automatePostDto.OrdreValidation;
            automate.Parite = automatePostDto.Parite;
            automate.PieceJointesToSend = automatePostDto.PieceJointesToSend;
            automate.PlateauDepart = automatePostDto.PlateauDepart;
            automate.Port = automatePostDto.Port;
            automate.PosDepart = automatePostDto.PosDepart;
            automate.ProprietaireAutomate = automatePostDto.ProprietaireAutomate;
            automate.Protocole = automatePostDto.Protocole;
            automate.RepWhereDupliquer = automatePostDto.RepWhereDupliquer;
            automate.ResultatLeft = automatePostDto.ResultatLeft;
            automate.ResultatRight = automatePostDto.ResultatRight;
            automate.Secteur = automatePostDto.Secteur;
            automate.SeqDepart = automatePostDto.SeqDepart;
            automate.Start1 = automatePostDto.Start1;
            automate.Start10 = automatePostDto.Start10;
            automate.Start2 = automatePostDto.Start2;
            automate.Start3 = automatePostDto.Start3;
            automate.Start4 = automatePostDto.Start4;
            automate.Start5 = automatePostDto.Start5;
            automate.Start6 = automatePostDto.Start6;
            automate.Start7 = automatePostDto.Start7;
            automate.Start8 = automatePostDto.Start8;
            automate.Start9 = automatePostDto.Start9;
            automate.Stop9 = automatePostDto.Stop9;
            automate.Stop1 = automatePostDto.Stop1;
            automate.Stop10 = automatePostDto.Stop10;
            automate.Stop2 = automatePostDto.Stop2;
            automate.Stop3 = automatePostDto.Stop3;
            automate.Stop4 = automatePostDto.Stop4;
            automate.Stop5 = automatePostDto.Stop5;
            automate.Stop6 = automatePostDto.Stop6;
            automate.Stop7 = automatePostDto.Stop7;
            automate.Stop8 = automatePostDto.Stop8;
            automate.StopBits = automatePostDto.StopBits;
            automate.TailleMax = automatePostDto.TailleMax;
            automate.Tempo = automatePostDto.Tempo;
            automate.Trace = automatePostDto.Trace;
            automate.TrameOrEchantillon = automatePostDto.TrameOrEchantillon;
            automate.TrameResultatPosition = automatePostDto.TrameResultatPosition;
            automate.TrameResultatValeur = automatePostDto.TrameResultatValeur;
            automate.TubeN = automatePostDto.TubeN;
            automate.TypeCb = automatePostDto.TypeCb;
            automate.TypeInstrument = automatePostDto.TypeInstrument;
            automate.TypePort = automatePostDto.TypePort;
            automate.TypePrelevementLeft = automatePostDto.TypePrelevementLeft;
            automate.TypePrelevementRight = automatePostDto.TypePrelevementRight;
            automate.TypeTupeInstrument = automatePostDto.TypeTupeInstrument;
            automate.ValeurParDefaut = automatePostDto.ValeurParDefaut;
            automate.ValeurPersonnalisee = automatePostDto.ValeurPersonnalisee;
            automate.ValmethodeAutre1 = automatePostDto.ValmethodeAutre1;
            automate.ValmethodeAutre2 = automatePostDto.ValmethodeAutre2;
            automate.ValmethodeDetailDesTechniques = automatePostDto.ValmethodeDetailDesTechniques;
            automate.ValmethodeFournisseur = automatePostDto.ValmethodeFournisseur;
            automate.ValmethodeIdentification = automatePostDto.ValmethodeIdentification;
            automate.VitesseTransmission = automatePostDto.VitesseTransmission;

            await _myDbContext.SaveChangesAsync();

            return automate;
        }

        async Task<List<Automate>> IAutomateRepo.GetAutomates()
        {
            var automates = await _myDbContext.Automate.ToListAsync();

            return automates;
        }
        async Task IAutomateRepo.DeleteAutomate(Automate automate)
        {
            _myDbContext.Automate.Remove(automate);
            await _myDbContext.SaveChangesAsync();
        }


        Automate IAutomateRepo.MakeAutomate(AutomatePostDto automatePostDto)
        {
            var Automate = new Automate
            {
                Code = automatePostDto.Code,
                AnalyseLeft = automatePostDto.AnalyseLeft,
                AnalyseRight = automatePostDto.AnalyseRight,
                Canal1 = automatePostDto.Canal1,
                Canal2 = automatePostDto.Canal2,
                ChargementAutoAcquisition = automatePostDto.ChargementAutoAcquisition,
                CodePourReception = automatePostDto.CodePourReception,
                CommentaireLeft = automatePostDto.CommentaireLeft,
                CommentaireRight = automatePostDto.CommentaireRight,
                Connexion = automatePostDto.Connexion,
                ControleEclair = automatePostDto.ControleEclair,
                ContrôleDeFlux = automatePostDto.ContrôleDeFlux,
                CoutPatientTypeAutomate = automatePostDto.CoutPatientTypeAutomate,
                DilutionLeft = automatePostDto.DilutionLeft,
                DilutionRight = automatePostDto.DilutionRight,
                DocumentProvisoir = automatePostDto.DocumentProvisoir,
                ExceptionsValidationAutomate = automatePostDto.ExceptionsValidationAutomate,
                FlagLeft = automatePostDto.FlagLeft,
                FlagRight = automatePostDto.FlagRight,
                FormatFichePaillasse = automatePostDto.FormatFichePaillasse,
                FormatNomPatientCq = automatePostDto.FormatNomPatientCq,
                Identification1Left = automatePostDto.Identification1Left,
                Identification1Right = automatePostDto.Identification1Right,
                Identification2Left = automatePostDto.Identification2Left,
                Identification2Right = automatePostDto.Identification2Right,
                IdtubeOrIgrImpose = automatePostDto.IdtubeOrIgrImpose,
                ImprimanteCb = automatePostDto.ImprimanteCb,
                Increment = automatePostDto.Increment,
                IsAfficherJournalConnex = automatePostDto.IsAfficherJournalConnex,
                IsBoutonForcerNonAutorized = automatePostDto.IsBoutonForcerNonAutorized,
                IsControleQualiteActif = automatePostDto.IsControleQualiteActif,
                IsDesValidationBiologique = automatePostDto.IsDesValidationBiologique,
                IsForcerModeEscalve = automatePostDto.IsForcerModeEscalve,
                IsGereDemandesSupression = automatePostDto.IsGereDemandesSupression,
                IsIgnorerLeLotCrConnexion = automatePostDto.IsIgnorerLeLotCrConnexion,
                IsInscrireAutoDansListe = automatePostDto.IsInscrireAutoDansListe,
                IsLotsProbatoires = automatePostDto.IsLotsProbatoires,
                IsNotArchiverTubes = automatePostDto.IsNotArchiverTubes,
                IsPreanalyticAutomate = automatePostDto.IsPreanalyticAutomate,
                IsPrefixerPidNumDemande = automatePostDto.IsPrefixerPidNumDemande,
                IsRespectArboYyyymm = automatePostDto.IsRespectArboYyyymm,
                IsValidateAutoCq = automatePostDto.IsValidateAutoCq,
                IsValidateResultpatientAutoCq = automatePostDto.IsValidateResultpatientAutoCq,
                ModeValidation = automatePostDto.ModeValidation,
                ModelCompteRendu = automatePostDto.ModelCompteRendu,
                MonNom = automatePostDto.MonNom,
                NbPosition = automatePostDto.NbPosition,
                NbrBits = automatePostDto.NbrBits,
                NbreCarTronq = automatePostDto.NbreCarTronq,
                NomPatient = automatePostDto.NomPatient,
                NumAutomate = automatePostDto.NumAutomate,
                OrdreLiaisonLis = automatePostDto.OrdreLiaisonLis,
                OrdreValidation = automatePostDto.OrdreValidation,
                Parite = automatePostDto.Parite,
                PieceJointesToSend = automatePostDto.PieceJointesToSend,
                PlateauDepart = automatePostDto.PlateauDepart,
                Port = automatePostDto.Port,
                PosDepart = automatePostDto.PosDepart,
                ProprietaireAutomate = automatePostDto.ProprietaireAutomate,
                Protocole = automatePostDto.Protocole,
                RepWhereDupliquer = automatePostDto.RepWhereDupliquer,
                ResultatLeft = automatePostDto.ResultatLeft,
                ResultatRight = automatePostDto.ResultatRight,
                Secteur = automatePostDto.Secteur,
                SeqDepart = automatePostDto.SeqDepart,
                Start1 = automatePostDto.Start1,
                Start10 = automatePostDto.Start10,
                Start2 = automatePostDto.Start2,
                Start3 = automatePostDto.Start3,
                Start4 = automatePostDto.Start4,
                Start5 = automatePostDto.Start5,
                Start6 = automatePostDto.Start6,
                Start7 = automatePostDto.Start7,
                Start8 = automatePostDto.Start8,
                Start9 = automatePostDto.Start9,
                Stop9 = automatePostDto.Stop9,
                Stop1 = automatePostDto.Stop1,
                Stop10 = automatePostDto.Stop10,
                Stop2 = automatePostDto.Stop2,
                Stop3 = automatePostDto.Stop3,
                Stop4 = automatePostDto.Stop4,
                Stop5 = automatePostDto.Stop5,
                Stop6 = automatePostDto.Stop6,
                Stop7 = automatePostDto.Stop7,
                Stop8 = automatePostDto.Stop8,
                StopBits = automatePostDto.StopBits,
                TailleMax = automatePostDto.TailleMax,
                Tempo = automatePostDto.Tempo,
                Trace = automatePostDto.Trace,
                TrameOrEchantillon = automatePostDto.TrameOrEchantillon,
                TrameResultatPosition = automatePostDto.TrameResultatPosition,
                TrameResultatValeur = automatePostDto.TrameResultatValeur,
                TubeN = automatePostDto.TubeN,
                TypeCb = automatePostDto.TypeCb,
                TypeInstrument = automatePostDto.TypeInstrument,
                TypePort = automatePostDto.TypePort,
                TypePrelevementLeft = automatePostDto.TypePrelevementLeft,
                TypePrelevementRight = automatePostDto.TypePrelevementRight,
                TypeTupeInstrument = automatePostDto.TypeTupeInstrument,
                ValeurParDefaut = automatePostDto.ValeurParDefaut,
                ValeurPersonnalisee = automatePostDto.ValeurPersonnalisee,
                ValmethodeAutre1 = automatePostDto.ValmethodeAutre1,
                ValmethodeAutre2 = automatePostDto.ValmethodeAutre2,
                ValmethodeDetailDesTechniques = automatePostDto.ValmethodeDetailDesTechniques,
                ValmethodeFournisseur = automatePostDto.ValmethodeFournisseur,
                ValmethodeIdentification = automatePostDto.ValmethodeIdentification,
                VitesseTransmission = automatePostDto.VitesseTransmission

            };

            return Automate;
        }

        public async Task<List<Automate>> GetCatalogueAutomates(int idCatalogue)
        {
            var automates = await _myDbContext.AutomateCatalogue.Where(x => x.IdCatalogue == idCatalogue)
                .Select(x => x.IdAutomateNavigation).ToListAsync();
            return automates;
        }

        async Task<bool> IAutomateRepo.ExistAll(List<int> automates)
        {
            foreach (var item in automates)
            {
                if (!await _myDbContext.Automate.AnyAsync(x => x.IdAutomate == item))
                    return false;
            }
            return true;
        }
    }
}
