﻿using MasterDataManagement.Domain.Models;
using MasterDataManagement.Domain.Repositories;
using MasterDataManagement.DTO.User;
using MasterDataManagement.Infrastructure.ManagementDbContext;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MasterDataManagement.Infrastructure.Repositories
{
    public class UserRepo : IUserRepo
    {
        private readonly MyDbContext _myDbContext;

        public UserRepo(MyDbContext myDbContext)
        {
            _myDbContext = myDbContext;
        }
        async Task IUserRepo.CreateUser(UserPostDto user, string hashedPassword)
        {
            await _myDbContext.User.AddAsync(new User
            {
                Email = user.Email,
                Nom = user.FirstName,
                Prenom = user.LastName,
                Password = hashedPassword,
                Username = user.Username
            });
            await _myDbContext.SaveChangesAsync();
        }

        async Task<User> IUserRepo.GetUserByEmail(string email)
        {

            return await _myDbContext.User.FirstOrDefaultAsync(x => x.Email == email);
        }

        async Task<User> IUserRepo.GetUserById(ushort idUtilisateur)
        {
            return await _myDbContext.User.FirstOrDefaultAsync(x => x.IdUser == idUtilisateur);
        }

        async Task<bool> IUserRepo.AddDroits(ushort idUser, List<int> droitsIds)
        {
            foreach (var item in droitsIds)
            {
                var droitUser = await _myDbContext.DroitUser.FirstOrDefaultAsync(x => x.IdUser == idUser && x.IdDroit == item);

                if (droitUser != null)
                    continue;
               
                await _myDbContext.DroitUser.AddAsync(new DroitUser
                {
                    IdUser = idUser,
                    IdDroit = item
                });
            }
            await _myDbContext.SaveChangesAsync();

            return true;
        }

        async Task<bool> IUserRepo.RemoveDroits(ushort idUser, List<int> droitsIds)
        {
            foreach (var item in droitsIds)
            {
               var droitUser =  await _myDbContext.DroitUser.FirstOrDefaultAsync(x => x.IdUser == idUser && x.IdDroit == item);

               if (droitUser != null)
                   _myDbContext.DroitUser.Remove(droitUser);

            }
            await _myDbContext.SaveChangesAsync();

            return true;
        }


        async Task<List<UserReadDto>> IUserRepo.GetUsers()
        {
            return await _myDbContext.User.Select(x => new UserReadDto
            {
                Email = x.Email,
                FirstName = x.Nom, 
                LastName = x.Prenom
            }).ToListAsync();
        }
        async Task<User> IUserRepo.GetUserWithDetailsByEmail(string email)
        {
            return await _myDbContext.User
                .Include(x => x.DroitUser).ThenInclude(x => x.IdDroitNavigation)
                .Include(x => x.UserInstance).ThenInclude(x => x.IdInstanceNavigation)
                .FirstOrDefaultAsync(x => x.Email == email);
        }

        
        async Task<List<User>> IUserRepo.GetUsersWithDetails()
        {
            return await _myDbContext.User
                .Include(x => x.DroitUser).ThenInclude(x => x.IdDroitNavigation)
                .Include(x => x.UserInstance).ThenInclude(x => x.IdInstanceNavigation)
                .ToListAsync();
        }

        public async Task<bool> HasRight(int idUser, string right)
        {
            return await _myDbContext.DroitUser.AnyAsync(x =>
                x.IdUser == idUser && x.IdDroitNavigation.Droit1 == right);
        }

        async Task<bool> IUserRepo.UserExists(string email)
        {
            return await _myDbContext.User.AnyAsync(x => x.Email == email);
        }
    }
}
