﻿using MasterDataManagement.Domain.Models;
using MasterDataManagement.Domain.Repositories;
using MasterDataManagement.DTO.Soustitres;
using MasterDataManagement.Infrastructure.ManagementDbContext;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MasterDataManagement.Infrastructure.Repositories
{
    public class SoustitresRepo : ISoustitresRepo
    {
        private readonly MyDbContext dbContext;

        public SoustitresRepo(MyDbContext dbContext)
        {
            this.dbContext = dbContext;
        }
        async Task ISoustitresRepo.CreateSoustitres(SoustitresPostDto s)
        {
            await dbContext.Soustitres.AddAsync(new Soustitres
            {
                Ordre = s.Ordre,
                Stitre = s.Stitre,
                Style = s.Style
            });
            await dbContext.SaveChangesAsync();
        }

        async Task ISoustitresRepo.DeleteSousTitre(Soustitres t)
        {
            dbContext.Soustitres.Remove(t);
            await dbContext.SaveChangesAsync();
        }

        async Task<Soustitres> ISoustitresRepo.GetSoustitresById(int idSoustitres)
        {
            return await dbContext.Soustitres.FirstOrDefaultAsync(x => x.IdSousTitres == idSoustitres);
        }

        async Task<List<Soustitres>> ISoustitresRepo.GetSoustitres()
        {
            return await dbContext.Soustitres.ToListAsync();
        }

        async Task<Soustitres> ISoustitresRepo.UpdateSoustitres(Soustitres soustitres, SoustitresPutDto soustitresPutDto)
        {
            soustitres.Ordre = soustitresPutDto.Ordre;
            soustitres.Stitre = soustitresPutDto.Stitre;
            soustitres.Style = soustitresPutDto.Style;
            await dbContext.SaveChangesAsync();

            return soustitres;
        }
    }
}
