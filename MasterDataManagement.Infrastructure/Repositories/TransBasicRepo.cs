﻿using MasterDataManagement.Domain.Models;
using MasterDataManagement.Domain.Repositories;
using MasterDataManagement.DTO.TransBasic;
using MasterDataManagement.Infrastructure.ManagementDbContext;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MasterDataManagement.Infrastructure.Repositories
{
    public class TransBasicRepo : ITransBasicRepo
    {
        private readonly MyDbContext _dbContext;

        public TransBasicRepo(MyDbContext dbContext)
        {
            this._dbContext = dbContext;
        }
        async Task ITransBasicRepo.CreateTransBasic(TransBasicPostDto t)
        {
            var transfBasic = ((ITransBasicRepo)this).MakeTransfbasic(t);
            await _dbContext.Transfbasic.AddAsync(transfBasic);
            await _dbContext.SaveChangesAsync();
        }
        Transfbasic ITransBasicRepo.MakeTransfbasic(TransBasicPostDto t)
        {
            return new Transfbasic
            {
                OperateurLogique = t.OperateurLogique,
                Valeur1 = t.Valeur1,
                ValeurResultat = t.ValeurResultat
            };
        }

        async Task ITransBasicRepo.DeleteTransBasic(Transfbasic t)
        {
            _dbContext.Transfbasic.Remove(t);
            await _dbContext.SaveChangesAsync();
        }

        async Task<bool> ITransBasicRepo.ExistAll(List<int> transBasicsIds)
        {
            foreach (var item in transBasicsIds)
            {
                if (!await _dbContext.Transfbasic.AnyAsync(x => x.IdTransfbasic == item))
                    return false;
            }
            return true;
        }

        async Task<Transfbasic> ITransBasicRepo.GetTransBasicById(int idTransBasic)
        {
            return await _dbContext.Transfbasic.FirstOrDefaultAsync(x => x.IdTransfbasic == idTransBasic);
        }

        async Task<List<Transfbasic>> ITransBasicRepo.GetTransBasics()
        {
            return await _dbContext.Transfbasic.ToListAsync();
        }

        async Task<Transfbasic> ITransBasicRepo.UpdateTransBasic(Transfbasic transBasic, TransBasicPutDto transBasicPostDto)
        {
            transBasic.OperateurLogique = transBasicPostDto.OperateurLogique;
            transBasic.Valeur1 = transBasicPostDto.Valeur1;
            transBasic.ValeurResultat = transBasicPostDto.ValeurResultat;

            await _dbContext.SaveChangesAsync();

            return transBasic;
        }
    }
}
