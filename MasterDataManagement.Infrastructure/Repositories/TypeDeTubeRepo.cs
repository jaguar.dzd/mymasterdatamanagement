﻿using System.Collections.Generic;
using MasterDataManagement.Domain.Models;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Repositories;
using MasterDataManagement.DTO.TypeDeTube;
using Microsoft.EntityFrameworkCore;
using MasterDataManagement.Infrastructure.ManagementDbContext;

namespace MasterDataManagement.Infrastructure.Repositories
{
    public class TypeDeTubeRepo : ITypeDeTubeRepo
    {
        private readonly MyDbContext _myDbContext;

        public TypeDeTubeRepo(MyDbContext myDbContext)
        {
            _myDbContext = myDbContext;
        }
        async Task ITypeDeTubeRepo.CreateTypeDeTube(TypeDeTubePostDto typeDeTubePostDto)
        {
            await _myDbContext.Typedetube.AddAsync(new Typedetube
            {
               NomTypeDuTube = typeDeTubePostDto.NomTypeDuTube
            });
            await _myDbContext.SaveChangesAsync();
        }

        async Task<Typedetube> ITypeDeTubeRepo.GetTypeDeTubeById(int idTypeDeTube)
        {
            var typeDeTube = await _myDbContext.Typedetube.FirstOrDefaultAsync(x => x.IdTypeDeTube == idTypeDeTube);
            return typeDeTube;
        }

        async Task<Typedetube> ITypeDeTubeRepo.UpdateTypeDeTube(Typedetube typeDeTube, TypeDeTubePostDto typeDeTubePostDto)
        {

            typeDeTube.NomTypeDuTube = typeDeTubePostDto.NomTypeDuTube;

            await _myDbContext.SaveChangesAsync();

            return typeDeTube;
        }


        async Task<List<Typedetube>> ITypeDeTubeRepo.GetTypeDeTubes()
        {
            var typeDeTubes = await _myDbContext.Typedetube.ToListAsync();

            return typeDeTubes;
        }
        async Task ITypeDeTubeRepo.DeleteTypeDeTube(Typedetube typeDeTube)
        {
            _myDbContext.Typedetube.Remove(typeDeTube);
            await _myDbContext.SaveChangesAsync();
        }
    }
}
