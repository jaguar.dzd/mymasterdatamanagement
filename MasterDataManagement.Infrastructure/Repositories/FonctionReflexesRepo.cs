﻿using System.Collections.Generic;
using MasterDataManagement.Domain.Models;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Repositories;
using MasterDataManagement.DTO.FonctionReflexes;
using Microsoft.EntityFrameworkCore;
using MasterDataManagement.Infrastructure.ManagementDbContext;

namespace MasterDataManagement.Infrastructure.Repositories
{
    public class FonctionReflexesRepo : IFonctionReflexesRepo
    {
        private readonly MyDbContext _myDbContext;

        public FonctionReflexesRepo(MyDbContext myDbContext)
        {
            _myDbContext = myDbContext;
        }
        async Task IFonctionReflexesRepo.CreateFonctionReflexes(FonctionReflexesPostDto fonctionReflexesPostDto)
        {
            var fctrflx = ((IFonctionReflexesRepo)this).MakeFonctionsreflexes(fonctionReflexesPostDto);

            await _myDbContext.Fonctionsreflexes.AddAsync(fctrflx);
            await _myDbContext.SaveChangesAsync();
        }
        Fonctionsreflexes IFonctionReflexesRepo.MakeFonctionsreflexes(FonctionReflexesPostDto fonctionReflexesPostDto)
        {
            return new Fonctionsreflexes
            {
                Condition = fonctionReflexesPostDto.Condition,
                Ordre = fonctionReflexesPostDto.Ordre,
                Scenario = fonctionReflexesPostDto.Scenario
            };
        }

        async Task<Fonctionsreflexes> IFonctionReflexesRepo.GetFonctionReflexesById(int idFonctionReflexes)
        {
            var fonctionReflexes = await _myDbContext.Fonctionsreflexes.FirstOrDefaultAsync(x => x.IdFonctionsReflexes == idFonctionReflexes);
            return fonctionReflexes;
        }

        async Task<Fonctionsreflexes> IFonctionReflexesRepo.UpdateFonctionReflexes(Fonctionsreflexes fonctionReflexes, FonctionReflexesPostDto fonctionReflexesPostDto)
        {

            fonctionReflexes.Condition = fonctionReflexesPostDto.Condition;
            fonctionReflexes.Ordre = fonctionReflexesPostDto.Ordre;
            fonctionReflexes.Scenario = fonctionReflexesPostDto.Scenario;

            await _myDbContext.SaveChangesAsync();

            return fonctionReflexes;
        }


        async Task<List<Fonctionsreflexes>> IFonctionReflexesRepo.GetFonctionReflexes()
        {
            var fonctionReflexess = await _myDbContext.Fonctionsreflexes.ToListAsync();

            return fonctionReflexess;
        }
        async Task IFonctionReflexesRepo.DeleteFonctionReflexes(Fonctionsreflexes fonctionReflexes)
        {
            _myDbContext.Fonctionsreflexes.Remove(fonctionReflexes);
            await _myDbContext.SaveChangesAsync();
        }

        async Task<bool> IFonctionReflexesRepo.ExistAll(List<int> fonctionReflexesId)
        {
            foreach (var item in fonctionReflexesId)
            {
                if (!await _myDbContext.Fonctionsreflexes.AnyAsync(x => x.IdFonctionsReflexes == item))
                    return false;
            }
            return true;
        }
    }
}
