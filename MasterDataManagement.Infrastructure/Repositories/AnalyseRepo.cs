﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Repositories;
using MasterDataManagement.DTO.Analyse;
using Microsoft.EntityFrameworkCore;
using MasterDataManagement.Infrastructure.ManagementDbContext;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.Domain.Others;

namespace MasterDataManagement.Infrastructure.Repositories
{
    public class AnalyseRepo : IAnalyseRepo
    {
        private readonly MyDbContext _myDbContext;
        private readonly ITransBasicRepo _transBasicRepo;
        private readonly ITransformationRepo _transformationRepo;
        private readonly IIncidenceDesFlagsRepo _incidenceDesFlagsRepo;
        private readonly ICommAutoRepo _commAutoRepo;
        private readonly IBilanRepo _bilanRepo;
        private readonly IValidationRepo _validationRepo;
        private readonly IEvaluationRepo _evaluationRepo;
        private readonly IFonctionReflexesRepo _fonctionReflexesRepo;

        public AnalyseRepo(MyDbContext myDbContext, ITransBasicRepo transBasicRepo, ITransformationRepo transformationRepo,
            IIncidenceDesFlagsRepo incidenceDesFlagsRepo, ICommAutoRepo commAutoRepo, IBilanRepo bilanRepo, IValidationRepo validationRepo,
            IEvaluationRepo evaluationRepo, IFonctionReflexesRepo fonctionReflexesRepo)
        {
            _myDbContext = myDbContext;
            this._transBasicRepo = transBasicRepo;
            this._transformationRepo = transformationRepo;
            this._incidenceDesFlagsRepo = incidenceDesFlagsRepo;
            this._commAutoRepo = commAutoRepo;
            this._bilanRepo = bilanRepo;
            this._validationRepo = validationRepo;
            this._evaluationRepo = evaluationRepo;
            this._fonctionReflexesRepo = fonctionReflexesRepo;
        }

        async Task IAnalyseRepo.CreateAnalyse(AnalysePostDto analysePostDto)
        {
            await _myDbContext.Analyse.AddAsync(((IAnalyseRepo)this).MakeAnalyse(analysePostDto));
            await _myDbContext.SaveChangesAsync();
        }

        async Task IAnalyseRepo.CreateAnalyses(List<AnalysePostDto> analysePostDtos)
        {
            foreach (var analysePostDto in analysePostDtos)
            {
                if (analysePostDto.Code != null && await _myDbContext.Analyse.AnyAsync(x => x.Code == analysePostDto.Code)) continue;                
                await _myDbContext.Analyse.AddAsync(((IAnalyseRepo)this).MakeAnalyse(analysePostDto));
            }

            await _myDbContext.SaveChangesAsync();
        }

        async Task<Analyse> IAnalyseRepo.GetAnalyseById(int idAnalyse)
        {
            var analyse = await _myDbContext.Analyse.FirstOrDefaultAsync(x => x.IdAnalyse == idAnalyse);
            return analyse;
        }

        async Task<Analyse> IAnalyseRepo.GetAnalyseByCode(string code)
        {
            var analyse = await _myDbContext.Analyse.FirstOrDefaultAsync(x => x.Code == code);
            return analyse;
        }
        async Task<Analyse> IAnalyseRepo.UpdateAnalyse(Analyse analyse, AnalysePostDto analysePostDto)
        {
            UpdateAnalyse(analyse, analysePostDto);

            await _myDbContext.SaveChangesAsync();

            return analyse;
        }    

        async Task<List<Analyse>> IAnalyseRepo.GetAnalyses()
        {
            var analyses = await _myDbContext.Analyse.ToListAsync();

            return analyses;
        }
        async Task IAnalyseRepo.DeleteAnalyse(Analyse analyse)
        {
            _myDbContext.Analyse.Remove(analyse);
            await _myDbContext.SaveChangesAsync();
        }


        async Task<bool> IAnalyseRepo.ExistAll(List<int> automates)
        {
            foreach (var item in automates)
            {
                if (!await _myDbContext.Analyse.AnyAsync(x => x.IdAnalyse == item))
                    return false;
            }
            return true;
        }
        async Task<bool> IAnalyseRepo.LinkAnalyseToValidations(int idAnalyse, List<int> validationsIds)
        {
            foreach (var item in validationsIds)
            {
                await _myDbContext.AnalyseValidations.AddAsync(new AnalyseValidations
                {
                    IdAnalyse = idAnalyse,
                    IdValidations = item
                });
            }
            await _myDbContext.SaveChangesAsync();

            return true;
        }

        async Task<bool> IAnalyseRepo.LinkAnalyseToBilans(int idAnalyse, List<int> bilansIds)
        {
            foreach (var item in bilansIds)
            {
                await _myDbContext.AnalyseBilan.AddAsync(new AnalyseBilan
                {
                    IdAnalyse = idAnalyse,
                    IdBilan = item
                });
            }
            await _myDbContext.SaveChangesAsync();

            return true;
        }

        async Task<bool> IAnalyseRepo.LinkAnalyseToFonctionReflexes(int idAnalyse, List<int> fonctionReflexesId)
        {
            foreach (var item in fonctionReflexesId)
            {
                await _myDbContext.AnalyseFonctionsreflexes.AddAsync(new AnalyseFonctionsreflexes
                {
                    IdAnalyse = idAnalyse,
                    IdFonctionsReflexes = item
                });
            }
            await _myDbContext.SaveChangesAsync();

            return true;
        }

        async Task<bool> IAnalyseRepo.LinkAnalyseToEvaluations(int idAnalyse, List<int> evaluationsId)
        {
            foreach (var item in evaluationsId)
            {
                await _myDbContext.AnalyseEvaluations.AddAsync(new AnalyseEvaluations
                {
                    IdAnalyse = idAnalyse,
                    IdEvaluations = item
                });
            }
            await _myDbContext.SaveChangesAsync();

            return true;
        }

        async Task IAnalyseRepo.CreateFullAnalyse(AnalysePostFullDto fullAnalyse)
        {
            var analyse = ((IAnalyseRepo)this).MakeAnalyse(fullAnalyse.Analyse);

            await _myDbContext.Analyse.AddAsync(analyse);

            await CreateAnalyseLinks(analyse, fullAnalyse);

            await _myDbContext.SaveChangesAsync();
        }

        async Task IAnalyseRepo.UpdateFullAnalyse(Analyse analyse, AnalysePostFullDto fullAnalyse)
        {
            UpdateAnalyse(analyse, fullAnalyse.Analyse);
           
            await RemoveAnalyseLinks(analyse.IdAnalyse);

            await CreateAnalyseLinks(analyse, fullAnalyse);

            await _myDbContext.SaveChangesAsync();
        }

        async Task<bool> IAnalyseRepo.LinkAnalyseToTransBasics(int idAnalyse, List<int> transBasicsIds)
        {
            foreach (var item in transBasicsIds)
            {
                if (await _myDbContext.AnalyseTransfbasic.AnyAsync(x =>
                    x.IdAnalyse == idAnalyse && x.IdTransfbasic == item))
                    continue;

                await _myDbContext.AnalyseTransfbasic.AddAsync(new AnalyseTransfbasic
                {
                    IdAnalyse = idAnalyse,
                    IdTransfbasic = item
                });
            }
            await _myDbContext.SaveChangesAsync();

            return true;
        }

        async Task<bool> IAnalyseRepo.LinkAnalyseToTransformations(int idAnalyse, List<int> transformationsIds)
        {
            foreach (var item in transformationsIds)
            {
                if (await _myDbContext.AnalyseTransformations.AnyAsync(x =>
                    x.IdAnalyse == idAnalyse && x.IdTransformations == item))
                    continue;

                await _myDbContext.AnalyseTransformations.AddAsync(new AnalyseTransformations
                {
                    IdAnalyse = idAnalyse,
                    IdTransformations = item
                });
            }
            await _myDbContext.SaveChangesAsync();

            return true;
        }

        async Task<bool> IAnalyseRepo.LinkAnalyseToIncidenceDesFlags(int idAnalyse, List<int> incidenceDesFlagsIds)
        {
            foreach (var item in incidenceDesFlagsIds)
            {
                if (await _myDbContext.AnalyseIncidencedesflags.AnyAsync(x =>
                    x.IdAnalyse == idAnalyse && x.Idincidencedesflags == item))
                    continue;

                await _myDbContext.AnalyseIncidencedesflags.AddAsync(new AnalyseIncidencedesflags
                {
                    IdAnalyse = idAnalyse,
                    Idincidencedesflags = item
                });
            }
            await _myDbContext.SaveChangesAsync();

            return true;
        }

        async Task<bool> IAnalyseRepo.LinkAnalyseToCommAutos(int idAnalyse, List<int> commAutosIds)
        {
            foreach (var item in commAutosIds)
            {
                if (await _myDbContext.AnalyseCommauto.AnyAsync(x =>
                    x.IdAnalyse == idAnalyse && x.IdCommAuto == item))
                    continue;

                await _myDbContext.AnalyseCommauto.AddAsync(new AnalyseCommauto
                {
                    IdAnalyse = idAnalyse,
                    IdCommAuto = item
                });
            }
            await _myDbContext.SaveChangesAsync();

            return true;
        }

        Analyse IAnalyseRepo.MakeAnalyse(AnalysePostDto analysePostDto)
        {
            var analyse = new Analyse
            {
                Code = analysePostDto.Code,
                ValeurParDefaut = analysePostDto.ValeurParDefaut,
                Arrondi = analysePostDto.Arrondi,
                AutreCode = analysePostDto.AutreCode,
                CodeAller1 = analysePostDto.CodeAller1,
                CodeAller2 = analysePostDto.CodeAller2,
                CodeAller3 = analysePostDto.CodeAller3,
                CodeAller4 = analysePostDto.CodeAller4,
                CodeAller5 = analysePostDto.CodeAller5,
                CodeAller6 = analysePostDto.CodeAller6,
                CodeAller7 = analysePostDto.CodeAller7,
                CodeAller8 = analysePostDto.CodeAller8,
                CodeAller9 = analysePostDto.CodeAller9,
                CodeRetour1 = analysePostDto.CodeRetour1,
                CodeRetour2 = analysePostDto.CodeRetour2,
                CodeTransmExComplem = analysePostDto.CodeTransmExComplem,
                CodeTransmission1 = analysePostDto.CodeTransmission1,
                CodeTransmission2 = analysePostDto.CodeTransmission2,
                Coefficient = analysePostDto.Coefficient,
                Decimales1 = analysePostDto.Decimales1,
                Decimales2 = analysePostDto.Decimales2,
                Deltacheck = analysePostDto.Deltacheck,
                Paillasse2 = analysePostDto.Paillasse2,
                DifValeurSupa = analysePostDto.DifValeurSupa,
                DureeValidite = analysePostDto.DureeValidite,
                EtOuEgalA = analysePostDto.EtOuEgalA,
                EtOuSupA = analysePostDto.EtOuSupA,
                EtatSortieConnexion = analysePostDto.EtatSortieConnexion,
                FacteurDilution = analysePostDto.FacteurDilution,
                Formule = analysePostDto.Formule,
                FormuleX1 = analysePostDto.FormuleX1,
                FormuleX2 = analysePostDto.FormuleX2,
                GenererFlag = analysePostDto.GenererFlag,
                HasComptage = analysePostDto.HasComptage,
                HasGraphique = analysePostDto.HasGraphique,
                HasNotCommentaireAuto = analysePostDto.HasNotCommentaireAuto,
                IdgroupeAnalyseValidation = analysePostDto.IdgroupeAnalyseValidation,
                Impression = analysePostDto.Impression,
                IsBilanOrProfil = analysePostDto.IsBilanOrProfil,
                IsControleEclairActifInactif = analysePostDto.IsControleEclairActifInactif,
                IsCreationAutomatiqueAnalyse = analysePostDto.IsCreationAutomatiqueAnalyse,
                IsMiseEnControleSurCondi = analysePostDto.IsMiseEnControleSurCondi,
                IsOneInstanceParPatient = analysePostDto.IsOneInstanceParPatient,
                IsResultatArenvoyer = analysePostDto.IsResultatArenvoyer,
                IsThisAnalyseProfil = analysePostDto.IsThisAnalyseProfil,
                IsValidationConditionCq = analysePostDto.IsValidationConditionCq,
                IsanalyseInscriteCq = analysePostDto.IsanalyseInscriteCq,
                Ligne1ApresResult = analysePostDto.Ligne1ApresResult,
                Ligne2ApresResult = analysePostDto.Ligne2ApresResult,
                Ligne3ApresResult = analysePostDto.Ligne3ApresResult,
                Ligne4ApresResult = analysePostDto.Ligne4ApresResult,
                Ligne5ApresResult = analysePostDto.Ligne5ApresResult,
                Ligne6ApresResult = analysePostDto.Ligne6ApresResult,
                Ligne1AvantResult = analysePostDto.Ligne1AvantResult,
                Ligne2AvantResult = analysePostDto.Ligne2AvantResult,
                Ligne3AvantResult = analysePostDto.Ligne3AvantResult,
                Ligne4AvantResult = analysePostDto.Ligne4AvantResult,
                Ligne5AvantResult = analysePostDto.Ligne5AvantResult,
                Longueur = analysePostDto.Longueur,
                MemeSiNi = analysePostDto.MemeSiNi,
                Minvaleur = analysePostDto.Minvaleur,
                MinvaleurEntrePassages = analysePostDto.MinvaleurEntrePassages,
                MiseControleAvecDilution = analysePostDto.MiseControleAvecDilution,
                MiseControleSiAlarmDelta = analysePostDto.MiseControleSiAlarmDelta,
                MiseControleSiDeltaAbsent = analysePostDto.MiseControleSiDeltaAbsent,
                MoyenneMobileTotalpoints = analysePostDto.MoyenneMobileTotalpoints,
                MoyenneMobileTriParGroupe = analysePostDto.MoyenneMobileTriParGroupe,
                NbrAnalyseMinCalcul = analysePostDto.NbrAnalyseMinCalcul,
                Rang = analysePostDto.Rang,
                ReglesDeWestgard = analysePostDto.ReglesDeWestgard,
                RepasserAvecLaDilution = analysePostDto.RepasserAvecLaDilution,
                SeuilRepasse = analysePostDto.SeuilRepasse,
                SiInfA = analysePostDto.SiInfA,
                SousTitre = analysePostDto.SousTitre,
                Texte = analysePostDto.Texte,
                TexteOptionnel1 = analysePostDto.TexteOptionnel1,
                TexteOptionnel10 = analysePostDto.TexteOptionnel10,
                TexteOptionnel11 = analysePostDto.TexteOptionnel11,
                TexteOptionnel12 = analysePostDto.TexteOptionnel12,
                TexteOptionnel13 = analysePostDto.TexteOptionnel13,
                TexteOptionnel14 = analysePostDto.TexteOptionnel14,
                TexteOptionnel15 = analysePostDto.TexteOptionnel15,
                TexteOptionnel2 = analysePostDto.TexteOptionnel2,
                TexteOptionnel3 = analysePostDto.TexteOptionnel3,
                TexteOptionnel4 = analysePostDto.TexteOptionnel4,
                TexteOptionnel5 = analysePostDto.TexteOptionnel5,
                TexteOptionnel6 = analysePostDto.TexteOptionnel6,
                TexteOptionnel7 = analysePostDto.TexteOptionnel7,
                TexteOptionnel8 = analysePostDto.TexteOptionnel8,
                TexteOptionnel9 = analysePostDto.TexteOptionnel9,
                Titre = analysePostDto.Titre,
                TypeResult = analysePostDto.TypeResult,
                Unites1 = analysePostDto.Unites1,
                Unites2 = analysePostDto.Unites2,
                ValeurAajouter = analysePostDto.ValeurAajouter,
                WestgardPersoT = analysePostDto.WestgardPersoT,
                WestgardPersoX = analysePostDto.WestgardPersoX,

                TypeDeTubeIdTypeDeTube = analysePostDto.TypeDeTubeIdTypeDeTube,
                PaillasseIdPaillasse = analysePostDto.PaillasseIdPaillasse,
                SousTitresIdSousTitres = analysePostDto.SousTitresIdSousTitres,
                TitresIdTitres = analysePostDto.TitresIdTitres,
                GroupeAnalyseIdGroupeAnalyse = analysePostDto.GroupeAnalyseIdGroupeAnalyse

            };
            return analyse;
        }

        private void UpdateAnalyse(Analyse analyse, AnalysePostDto analysePostDto)
        {
            analyse.Code = analysePostDto.Code;
            analyse.ValeurParDefaut = analysePostDto.ValeurParDefaut;
            analyse.Arrondi = analysePostDto.Arrondi;
            analyse.AutreCode = analysePostDto.AutreCode;
            analyse.CodeAller1 = analysePostDto.CodeAller1;
            analyse.CodeAller2 = analysePostDto.CodeAller2;
            analyse.CodeAller3 = analysePostDto.CodeAller3;
            analyse.CodeAller4 = analysePostDto.CodeAller4;
            analyse.CodeAller5 = analysePostDto.CodeAller5;
            analyse.CodeAller6 = analysePostDto.CodeAller6;
            analyse.CodeAller7 = analysePostDto.CodeAller7;
            analyse.CodeAller8 = analysePostDto.CodeAller8;
            analyse.CodeAller9 = analysePostDto.CodeAller9;
            analyse.CodeRetour1 = analysePostDto.CodeRetour1;
            analyse.CodeRetour2 = analysePostDto.CodeRetour2;
            analyse.CodeTransmExComplem = analysePostDto.CodeTransmExComplem;
            analyse.CodeTransmission1 = analysePostDto.CodeTransmission1;
            analyse.CodeTransmission2 = analysePostDto.CodeTransmission2;
            analyse.Coefficient = analysePostDto.Coefficient;
            analyse.Decimales1 = analysePostDto.Decimales1;
            analyse.Decimales2 = analysePostDto.Decimales2;
            analyse.Deltacheck = analysePostDto.Deltacheck;
            analyse.Paillasse2 = analysePostDto.Paillasse2;
            analyse.DifValeurSupa = analysePostDto.DifValeurSupa;
            analyse.DureeValidite = analysePostDto.DureeValidite;
            analyse.EtOuEgalA = analysePostDto.EtOuEgalA;
            analyse.EtOuSupA = analysePostDto.EtOuSupA;
            analyse.EtatSortieConnexion = analysePostDto.EtatSortieConnexion;
            analyse.FacteurDilution = analysePostDto.FacteurDilution;
            analyse.Formule = analysePostDto.Formule;
            analyse.FormuleX1 = analysePostDto.FormuleX1;
            analyse.FormuleX2 = analysePostDto.FormuleX2;
            analyse.GenererFlag = analysePostDto.GenererFlag;
            analyse.HasComptage = analysePostDto.HasComptage;
            analyse.HasGraphique = analysePostDto.HasGraphique;
            analyse.HasNotCommentaireAuto = analysePostDto.HasNotCommentaireAuto;
            analyse.IdgroupeAnalyseValidation = analysePostDto.IdgroupeAnalyseValidation;
            analyse.Impression = analysePostDto.Impression;
            analyse.IsBilanOrProfil = analysePostDto.IsBilanOrProfil;
            analyse.IsControleEclairActifInactif = analysePostDto.IsControleEclairActifInactif;
            analyse.IsCreationAutomatiqueAnalyse = analysePostDto.IsCreationAutomatiqueAnalyse;
            analyse.IsMiseEnControleSurCondi = analysePostDto.IsMiseEnControleSurCondi;
            analyse.IsOneInstanceParPatient = analysePostDto.IsOneInstanceParPatient;
            analyse.IsResultatArenvoyer = analysePostDto.IsResultatArenvoyer;
            analyse.IsThisAnalyseProfil = analysePostDto.IsThisAnalyseProfil;
            analyse.IsValidationConditionCq = analysePostDto.IsValidationConditionCq;
            analyse.IsanalyseInscriteCq = analysePostDto.IsanalyseInscriteCq;
            analyse.Ligne1ApresResult = analysePostDto.Ligne1ApresResult;
            analyse.Ligne2ApresResult = analysePostDto.Ligne2ApresResult;
            analyse.Ligne3ApresResult = analysePostDto.Ligne3ApresResult;
            analyse.Ligne4ApresResult = analysePostDto.Ligne4ApresResult;
            analyse.Ligne5ApresResult = analysePostDto.Ligne5ApresResult;
            analyse.Ligne6ApresResult = analysePostDto.Ligne6ApresResult;
            analyse.Ligne1AvantResult = analysePostDto.Ligne1AvantResult;
            analyse.Ligne2AvantResult = analysePostDto.Ligne2AvantResult;
            analyse.Ligne3AvantResult = analysePostDto.Ligne3AvantResult;
            analyse.Ligne4AvantResult = analysePostDto.Ligne4AvantResult;
            analyse.Ligne5AvantResult = analysePostDto.Ligne5AvantResult;
            analyse.Longueur = analysePostDto.Longueur;
            analyse.MemeSiNi = analysePostDto.MemeSiNi;
            analyse.Minvaleur = analysePostDto.Minvaleur;
            analyse.MinvaleurEntrePassages = analysePostDto.MinvaleurEntrePassages;
            analyse.MiseControleAvecDilution = analysePostDto.MiseControleAvecDilution;
            analyse.MiseControleSiAlarmDelta = analysePostDto.MiseControleSiAlarmDelta;
            analyse.MiseControleSiDeltaAbsent = analysePostDto.MiseControleSiDeltaAbsent;
            analyse.MoyenneMobileTotalpoints = analysePostDto.MoyenneMobileTotalpoints;
            analyse.MoyenneMobileTriParGroupe = analysePostDto.MoyenneMobileTriParGroupe;
            analyse.NbrAnalyseMinCalcul = analysePostDto.NbrAnalyseMinCalcul;
            analyse.Rang = analysePostDto.Rang;
            analyse.ReglesDeWestgard = analysePostDto.ReglesDeWestgard;
            analyse.RepasserAvecLaDilution = analysePostDto.RepasserAvecLaDilution;
            analyse.SeuilRepasse = analysePostDto.SeuilRepasse;
            analyse.SiInfA = analysePostDto.SiInfA;
            analyse.SousTitre = analysePostDto.SousTitre;
            analyse.Texte = analysePostDto.Texte;
            analyse.TexteOptionnel1 = analysePostDto.TexteOptionnel1;
            analyse.TexteOptionnel10 = analysePostDto.TexteOptionnel10;
            analyse.TexteOptionnel11 = analysePostDto.TexteOptionnel11;
            analyse.TexteOptionnel12 = analysePostDto.TexteOptionnel12;
            analyse.TexteOptionnel13 = analysePostDto.TexteOptionnel13;
            analyse.TexteOptionnel14 = analysePostDto.TexteOptionnel14;
            analyse.TexteOptionnel15 = analysePostDto.TexteOptionnel15;
            analyse.TexteOptionnel2 = analysePostDto.TexteOptionnel2;
            analyse.TexteOptionnel3 = analysePostDto.TexteOptionnel3;
            analyse.TexteOptionnel4 = analysePostDto.TexteOptionnel4;
            analyse.TexteOptionnel5 = analysePostDto.TexteOptionnel5;
            analyse.TexteOptionnel6 = analysePostDto.TexteOptionnel6;
            analyse.TexteOptionnel7 = analysePostDto.TexteOptionnel7;
            analyse.TexteOptionnel8 = analysePostDto.TexteOptionnel8;
            analyse.TexteOptionnel9 = analysePostDto.TexteOptionnel9;
            analyse.Titre = analysePostDto.Titre;
            analyse.TypeResult = analysePostDto.TypeResult;
            analyse.Unites1 = analysePostDto.Unites1;
            analyse.Unites2 = analysePostDto.Unites2;
            analyse.ValeurAajouter = analysePostDto.ValeurAajouter;
            analyse.WestgardPersoT = analysePostDto.WestgardPersoT;
            analyse.WestgardPersoX = analysePostDto.WestgardPersoX;

            analyse.TypeDeTubeIdTypeDeTube = analysePostDto.TypeDeTubeIdTypeDeTube;
            analyse.PaillasseIdPaillasse = analysePostDto.PaillasseIdPaillasse;
            analyse.SousTitresIdSousTitres = analysePostDto.SousTitresIdSousTitres;
            analyse.TitresIdTitres = analysePostDto.TitresIdTitres;
            analyse.GroupeAnalyseIdGroupeAnalyse = analysePostDto.GroupeAnalyseIdGroupeAnalyse;
        }

        private async Task CreateAnalyseLinks(Analyse analyse, AnalysePostFullDto fullAnalyse)
        {
            foreach (var item in fullAnalyse.Bilans)
            {
                await _myDbContext.AnalyseBilan.AddAsync(new AnalyseBilan
                {
                    IdAnalyseNavigation = analyse,
                    IdBilanNavigation = _bilanRepo.MakeBilan(item)
                });
            }


            foreach (var item in fullAnalyse.CommAutos)
            {
                await _myDbContext.AnalyseCommauto.AddAsync(new AnalyseCommauto
                {
                    IdAnalyseNavigation = analyse,
                    IdCommAutoNavigation = _commAutoRepo.MakeCommAuto(item)
                });
            }

            foreach (var item in fullAnalyse.Evaluations)
            {
                await _myDbContext.AnalyseEvaluations.AddAsync(new AnalyseEvaluations
                {
                    IdAnalyseNavigation = analyse,
                    IdEvaluationsNavigation = _evaluationRepo.MakeEvaluation(item)
                });
            }

            foreach (var item in fullAnalyse.FonctionReflexes)
            {
                await _myDbContext.AnalyseFonctionsreflexes.AddAsync(new AnalyseFonctionsreflexes
                {
                    IdAnalyseNavigation = analyse,
                    IdFonctionsReflexesNavigation = _fonctionReflexesRepo.MakeFonctionsreflexes(item)
                });
            }

            foreach (var item in fullAnalyse.IncidenceDesFlags)
            {
                await _myDbContext.AnalyseIncidencedesflags.AddAsync(new AnalyseIncidencedesflags
                {
                    IdAnalyseNavigation = analyse,
                    IdincidencedesflagsNavigation = _incidenceDesFlagsRepo.MakeIncidencedesflags(item)
                });
            }

            foreach (var item in fullAnalyse.TransBasics)
            {
                await _myDbContext.AnalyseTransfbasic.AddAsync(new AnalyseTransfbasic
                {
                    IdAnalyseNavigation = analyse,
                    IdTransfbasicNavigation = _transBasicRepo.MakeTransfbasic(item)
                });
            }

            foreach (var item in fullAnalyse.Transformations)
            {
                await _myDbContext.AnalyseTransformations.AddAsync(new AnalyseTransformations
                {
                    IdAnalyseNavigation = analyse,
                    IdTransformationsNavigation = _transformationRepo.MakeTransformations(item)
                });
            }

            foreach (var item in fullAnalyse.Validations)
            {
                await _myDbContext.AnalyseValidations.AddAsync(new AnalyseValidations
                {
                    IdAnalyseNavigation = analyse,
                    IdValidationsNavigation = _validationRepo.MakeValidations(item)
                });
            }
        }

        private async Task RemoveAnalyseLinks(int analyseId)
        {

            var analyseBilans = await _myDbContext.AnalyseBilan.Where(x => x.IdAnalyse == analyseId).ToListAsync();
            _myDbContext.AnalyseBilan.RemoveRange(analyseBilans);

            var analyseCommautos = await _myDbContext.AnalyseCommauto.Where(x => x.IdAnalyse == analyseId).ToListAsync();
            _myDbContext.AnalyseCommauto.RemoveRange(analyseCommautos);

            var analyseEvaluations = await _myDbContext.AnalyseEvaluations.Where(x => x.IdAnalyse == analyseId).ToListAsync();
            _myDbContext.AnalyseEvaluations.RemoveRange(analyseEvaluations);

            var analyseFonctionsreflexes = await _myDbContext.AnalyseFonctionsreflexes.Where(x => x.IdAnalyse == analyseId).ToListAsync();
            _myDbContext.AnalyseFonctionsreflexes.RemoveRange(analyseFonctionsreflexes);

            var analyseIncidencedesflags = await _myDbContext.AnalyseIncidencedesflags.Where(x => x.IdAnalyse == analyseId).ToListAsync();
            _myDbContext.AnalyseIncidencedesflags.RemoveRange(analyseIncidencedesflags);

            var analyseTransfbasics = await _myDbContext.AnalyseTransfbasic.Where(x => x.IdAnalyse == analyseId).ToListAsync();
            _myDbContext.AnalyseTransfbasic.RemoveRange(analyseTransfbasics);

            var analyseTransformations = await _myDbContext.AnalyseTransformations.Where(x => x.IdAnalyse == analyseId).ToListAsync();
            _myDbContext.AnalyseTransformations.RemoveRange(analyseTransformations);

            var analyseValidations = await _myDbContext.AnalyseValidations.Where(x => x.IdAnalyse == analyseId).ToListAsync();
            _myDbContext.AnalyseValidations.RemoveRange(analyseValidations);


        }

        async Task IAnalyseRepo.CreateOrUpdateAnalyses(List<AnalysePostDto> analyses, bool isReplacingExistingvalues)
        {
            if (isReplacingExistingvalues)
            {
                await CreateOrUpdate(analyses);
            }
            else
            {
                await ((IAnalyseRepo)this).CreateAnalyses(analyses);
            }
        }

        private async Task CreateOrUpdate(List<AnalysePostDto> analyses)
        {
            foreach (var analyseDto in analyses)
            {
                if (analyseDto.Code != null)
                {
                    var analyse = await _myDbContext.Analyse.FirstOrDefaultAsync(x => x.Code == analyseDto.Code);
                    if (analyse != null)
                    {
                        UpdateAnalyse(analyse, analyseDto);
                    }
                }
                else
                {
                    await _myDbContext.Analyse.AddAsync(((IAnalyseRepo)this).MakeAnalyse(analyseDto));
                }
            }
            await _myDbContext.SaveChangesAsync();
        }

        async Task<FullAnalyse> IAnalyseRepo.GetFullAnalyse(int idAnalyse)
        {
            return await _myDbContext.Analyse
                    .Where(x => x.IdAnalyse == idAnalyse)
                    .Select(analyse => new FullAnalyse
                    {
                        Analyse = analyse,
                        Bilans = analyse.AnalyseBilan.Select(x => x.IdBilanNavigation).ToList(),
                        CommAutos = analyse.AnalyseCommauto.Select(x => x.IdCommAutoNavigation).ToList(),
                        Evaluations = analyse.AnalyseEvaluations.Select(x => x.IdEvaluationsNavigation).ToList(),
                        FonctionReflexes = analyse.AnalyseFonctionsreflexes.Select(x => x.IdFonctionsReflexesNavigation).ToList(),
                        IncidenceDesFlags = analyse.AnalyseIncidencedesflags.Select(x => x.IdincidencedesflagsNavigation).ToList(),
                        TransBasics = analyse.AnalyseTransfbasic.Select(x => x.IdTransfbasicNavigation).ToList(),
                        Transformations = analyse.AnalyseTransformations.Select(x => x.IdTransformationsNavigation).ToList(),
                        Validations = analyse.AnalyseValidations.Select(x => x.IdValidationsNavigation).ToList()
                    }
                    ).FirstOrDefaultAsync();
        }

        public async Task<List<Analyse>> GetCatalogueAnalyses(int idCatalogue)
        {
            var analyses = await _myDbContext.AnalyseCatalogue.Where(x => x.CatalogueIdCatalogue == idCatalogue)
                .Select(x => x.AnalyseIdAnalyseNavigation).ToListAsync();

            return analyses;
        }
    }
}
