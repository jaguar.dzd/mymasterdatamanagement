﻿using MasterDataManagement.Domain.Models;
using MasterDataManagement.Domain.Services;
using MasterDataManagement.DTO.Site;
using MasterDataManagement.Infrastructure.ManagementDbContext;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Repositories;

namespace MasterDataManagement.Infrastructure.Repositories
{
    public class TransformationRepo : ITransformationRepo
    {
        private readonly MyDbContext dbContext;

        public TransformationRepo(MyDbContext dbContext)
        {
            this.dbContext = dbContext;
        }
        async Task ITransformationRepo.CreateTransformation(TransformationPostDto t)
        {
            var transfForma = ((ITransformationRepo)this).MakeTransformations(t);
            await dbContext.Transformations.AddAsync(transfForma);
            await dbContext.SaveChangesAsync();
        }
        Transformations ITransformationRepo.MakeTransformations(TransformationPostDto t)
        {
            return new Transformations
            {
                OperateurLogique = t.OperateurLogique,
                Valeur1 = t.Valeur1,
                ValeurResultat = t.ValeurResultat
            };
        }

        async Task ITransformationRepo.DeleteTransformation(Transformations t)
        {
            dbContext.Transformations.Remove(t);
            await dbContext.SaveChangesAsync();
        }

        async Task<Transformations> ITransformationRepo.GetTransformationById(int idTransformation)
        {
            return await dbContext.Transformations.FirstOrDefaultAsync(x => x.IdTransformations == idTransformation);
        }

        async Task<List<Transformations>> ITransformationRepo.GetTransformations()
        {
            return await dbContext.Transformations.ToListAsync();
        }

        async Task<Transformations> ITransformationRepo.UpdateTransformation(Transformations transformation, TransformationPutDto transformationPutDto)
        {
            transformation.OperateurLogique = transformationPutDto.OperateurLogique;
            transformation.Valeur1 = transformationPutDto.Valeur1;
            transformation.ValeurResultat = transformationPutDto.ValeurResultat;

            await dbContext.SaveChangesAsync();

            return transformation;
        }

        async Task<bool> ITransformationRepo.ExistAll(List<int> transformationsIds)
        {
            foreach (var item in transformationsIds)
            {
                if (!await dbContext.Transformations.AnyAsync(x => x.IdTransformations == item))
                    return false;
            }
            return true;
        }
    }
}
