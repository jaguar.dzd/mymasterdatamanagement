﻿using System.Collections.Generic;
using MasterDataManagement.Domain.Models;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Repositories;
using MasterDataManagement.DTO.Droit;
using Microsoft.EntityFrameworkCore;
using MasterDataManagement.Infrastructure.ManagementDbContext;

namespace MasterDataManagement.Infrastructure.Repositories
{
    public class DroitRepo : IDroitRepo
    {
        private readonly MyDbContext _myDbContext;

        public DroitRepo(MyDbContext myDbContext)
        {
            _myDbContext = myDbContext;
        }
        async Task IDroitRepo.CreateDroit(DroitPostDto droitPostDto)
        {
            await _myDbContext.Droit.AddAsync(new Droit
            {
               Droit1 = droitPostDto.Droit

            });
            await _myDbContext.SaveChangesAsync();
        }

        async Task<Droit> IDroitRepo.GetDroitById(int idDroit)
        {
            var droit = await _myDbContext.Droit.FirstOrDefaultAsync(x => x.IdDroit == idDroit);
            return droit;
        }

        async Task<Droit> IDroitRepo.UpdateDroit(Droit droit, DroitPostDto droitPostDto)
        {

            droit.Droit1 = droitPostDto.Droit;
            
            await _myDbContext.SaveChangesAsync();

            return droit;
        }


        async Task<List<Droit>> IDroitRepo.GetDroits()
        {
            var droits = await _myDbContext.Droit.ToListAsync();

            return droits;
        }
        async Task IDroitRepo.DeleteDroit(Droit droit)
        {
            _myDbContext.Droit.Remove(droit);
            await _myDbContext.SaveChangesAsync();
        }

        async Task<bool> IDroitRepo.ExistAll(List<int> droitsIds)
        {
            foreach (var item in droitsIds)
            {
                if (!await _myDbContext.Droit.AnyAsync(x => x.IdDroit == item))
                    return false;
            }
            return true;
        }
    }
}
