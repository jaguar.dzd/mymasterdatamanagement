﻿using System.Collections.Generic;
using System.Linq;
using MasterDataManagement.Domain.Models;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Repositories;
using MasterDataManagement.DTO.AnalyseAutomate;
using Microsoft.EntityFrameworkCore;
using MasterDataManagement.Infrastructure.ManagementDbContext;

namespace MasterDataManagement.Infrastructure.Repositories
{
    public class AnalyseAutomateRepo : IAnalyseAutomateRepo
    {
        private readonly MyDbContext _myDbContext;
        private readonly ITransBasicRepo _transBasicRepo;
        private readonly ITransformationRepo _transformationRepo;
        private readonly IIncidenceDesFlagsRepo _incidenceDesFlagsRepo;
        private readonly ICommAutoRepo _commAutoRepo;
        private readonly IValidationRepo _validationRepo;
        private readonly IEvaluationRepo _evaluationRepo;
        private readonly IFonctionReflexesRepo _fonctionReflexesRepo;

        public AnalyseAutomateRepo(MyDbContext myDbContext, ITransBasicRepo transBasicRepo, ITransformationRepo transformationRepo,
            IIncidenceDesFlagsRepo incidenceDesFlagsRepo, ICommAutoRepo commAutoRepo, IValidationRepo validationRepo,
            IEvaluationRepo evaluationRepo, IFonctionReflexesRepo fonctionReflexesRepo)
        {
            _myDbContext = myDbContext;
            this._transBasicRepo = transBasicRepo;
            this._transformationRepo = transformationRepo;
            this._incidenceDesFlagsRepo = incidenceDesFlagsRepo;
            this._commAutoRepo = commAutoRepo;
            this._validationRepo = validationRepo;
            this._evaluationRepo = evaluationRepo;
            this._fonctionReflexesRepo = fonctionReflexesRepo;
        }
        async Task IAnalyseAutomateRepo.CreateAnalyseAutomate(AnalyseAutomatePostDto analyseAutomatePostDto)
        {
            var anaAuto = makeAnalyseAutomate(analyseAutomatePostDto);
            await _myDbContext.AnalyseAutomate.AddAsync(anaAuto);
            await _myDbContext.SaveChangesAsync();
        }

        async Task<bool> IAnalyseAutomateRepo.CreateFullAnalyseAutomate(AnalyseAutomateFullPostDto analyseAutomateFullPostDto)
        {
            foreach (var item in analyseAutomateFullPostDto.AnalyseAutomate)
            {
                var analyseAutomate = makeAnalyseAutomate(item);
                await _myDbContext.AnalyseAutomate.AddAsync(analyseAutomate);

                await CreateAnalyseAutomateLinks(analyseAutomate, analyseAutomateFullPostDto);
            }

            await _myDbContext.SaveChangesAsync();
            return true;
        }

        async Task<bool> IAnalyseAutomateRepo.UpdateFullAnalyseAutomate(AnalyseAutomateFullPostDto analyseAutomateFullPostDto)
        {
            foreach (var item in analyseAutomateFullPostDto.AnalyseAutomate)
            {
                var analyseAutomate = await ((IAnalyseAutomateRepo)this).GetAnalyseAutomateById(item.IdAnalyseAutomate);
                if (analyseAutomate == null)
                    return false;

                UpdateAnalyseAutomate(analyseAutomate, item);

                await RemoveAnalyseAutomateLinks(analyseAutomate.IdAnalyseAutomate);

                await CreateAnalyseAutomateLinks(analyseAutomate, analyseAutomateFullPostDto);
            }

            await _myDbContext.SaveChangesAsync();
            return true;
        }

        async Task<AnalyseAutomate> IAnalyseAutomateRepo.GetAnalyseAutomateById(int idAnalyseAutomate)
        {
            var analyseAutomate = await _myDbContext.AnalyseAutomate.FirstOrDefaultAsync(x => x.IdAnalyseAutomate == idAnalyseAutomate);
            return analyseAutomate;
        }

        async Task<AnalyseAutomate> IAnalyseAutomateRepo.UpdateAnalyseAutomate(AnalyseAutomate analyseAutomate, AnalyseAutomatePostDto analyseAutomatePostDto)
        {
            UpdateAnalyseAutomate(analyseAutomate, analyseAutomatePostDto);

            await _myDbContext.SaveChangesAsync();

            return analyseAutomate;
        }
        
        async Task<List<AnalyseAutomate>> IAnalyseAutomateRepo.GetAnalyseAutomates()
        {
            var analyseAutomates = await _myDbContext.AnalyseAutomate.ToListAsync();

            return analyseAutomates;
        }

        async Task IAnalyseAutomateRepo.DeleteAnalyseAutomate(AnalyseAutomate analyseAutomate)
        {
            _myDbContext.AnalyseAutomate.Remove(analyseAutomate);
            await _myDbContext.SaveChangesAsync();
        }

        async Task<bool> IAnalyseAutomateRepo.ExistAll(List<int> analyseAutomatesIds)
        {
            foreach (var item in analyseAutomatesIds)
            {
                if (!await _myDbContext.AnalyseAutomate.AnyAsync(x => x.IdAnalyseAutomate == item))
                    return false;
            }
            return true;
        }

        async Task<bool> IAnalyseAutomateRepo.LinkAnalyseAutomateToValidations(List<int> idAnalyseAutomate, List<int> validationsIds)
        {
            foreach (var item in validationsIds)
            {
                foreach (var analyse in idAnalyseAutomate)
                {
                    var analyseAutomateValidation = await _myDbContext.AnalyseAutomateValidations.FirstOrDefaultAsync(x =>
                    x.IdAnalyseAutomate == analyse && x.IdValidations == item);

                    if (analyseAutomateValidation != null)
                        continue;

                    await _myDbContext.AnalyseAutomateValidations.AddAsync(new AnalyseAutomateValidations
                    {
                        IdAnalyseAutomate = analyse,
                        IdValidations = item
                    });
                }
                
            }
            await _myDbContext.SaveChangesAsync();

            return true;
        }

        async Task<bool> IAnalyseAutomateRepo.LinkAnalyseAutomateToBilans(List<int> idAnalyseAutomate, List<int> bilansIds)
        {
            foreach (var item in bilansIds)
            {
                foreach (var analayse in idAnalyseAutomate)
                {
                    var bilanAnalyseautomate = await _myDbContext.BilanAnalyseautomate.FirstOrDefaultAsync(x =>
                    x.IdAnalyseAutomate == analayse && x.IdBilan == item);

                    if (bilanAnalyseautomate != null)
                        continue;

                    await _myDbContext.BilanAnalyseautomate.AddAsync(new BilanAnalyseautomate
                    {
                        IdAnalyseAutomate = analayse,
                        IdBilan = item
                    });
                }
            }
            await _myDbContext.SaveChangesAsync();

            return true;
        }

        async Task<bool> IAnalyseAutomateRepo.LinkAnalyseAutomateToFonctionReflexes(List<int> idAnalyseAutomate, List<int> fonctionReflexesIds)
        {
            foreach (var item in fonctionReflexesIds)
            {
                foreach (var analyse in idAnalyseAutomate)
                {
                    if (await _myDbContext.AnalyseautomateFonctionsreflexes.AnyAsync(x =>
                   x.IdAnalyseAutomate == analyse && x.IdFonctionsReflexes == item))
                        continue;

                    await _myDbContext.AnalyseautomateFonctionsreflexes.AddAsync(new AnalyseautomateFonctionsreflexes
                    {
                        IdAnalyseAutomate = analyse,
                        IdFonctionsReflexes = item
                    });
                }
            }
            await _myDbContext.SaveChangesAsync();

            return true;
        }

        async Task<bool> IAnalyseAutomateRepo.LinkAnalyseAutomateToEvaluations(List<int> idAnalyseAutomate, List<int> evaluationsIds)
        {
            foreach (var item in evaluationsIds)
            {
                foreach (var analyse in idAnalyseAutomate)
                {
                    if (await _myDbContext.EvaluationsAnalyseautomate.AnyAsync(x =>
                   x.IdAnalyseAutomate == analyse && x.IdEvaluations == item))
                        continue;

                    await _myDbContext.EvaluationsAnalyseautomate.AddAsync(new EvaluationsAnalyseautomate
                    {
                        IdAnalyseAutomate = analyse,
                        IdEvaluations = item
                    });

                }
            }
            await _myDbContext.SaveChangesAsync();

            return true;
        }

        async Task<bool> IAnalyseAutomateRepo.LinkAnalyseAutomateToTransBasics(List<int> idAnalyseAutomate, List<int> transBasicsIds)
        {
            foreach (var item in transBasicsIds)
            {
                foreach (var analyse in idAnalyseAutomate)
                {
                    if (await _myDbContext.TransfbasicAnalyseautomate.AnyAsync(x =>
                    x.IdAnalyseAutomate == analyse && x.IdTransfbasic == item))
                        continue;

                    await _myDbContext.TransfbasicAnalyseautomate.AddAsync(new TransfbasicAnalyseautomate
                    {
                        IdAnalyseAutomate = analyse,
                        IdTransfbasic = item
                    });
                }
                
            }
            await _myDbContext.SaveChangesAsync();

            return true;
        }

        async Task<bool> IAnalyseAutomateRepo.LinkAnalyseAutomateToTransformations(List<int> idAnalyseAutomate, List<int> transformationsIds)
        {
            foreach (var item in transformationsIds)
            {
                foreach (var analyse in idAnalyseAutomate)
                {
                    if (await _myDbContext.AnalyseautomateTransformations.AnyAsync(x => x.IdAnalyseAutomate == analyse && x.IdTransformations == item))
                        continue;

                    await _myDbContext.AnalyseautomateTransformations.AddAsync(new AnalyseautomateTransformations
                    {
                        IdAnalyseAutomate = analyse,
                        IdTransformations = item
                    });
                }
                
            }
            await _myDbContext.SaveChangesAsync();

            return true;
        }

        async Task<bool> IAnalyseAutomateRepo.LinkAnalyseAutomateToIncidenceDesFlags(List<int> idAnalyseAutomate, List<int> incidenceDesFlagsIds)
        {
            foreach (var incidence in incidenceDesFlagsIds)
            {
                foreach (var analyse in idAnalyseAutomate)
                {
                    if (await _myDbContext.IncidencedesflagsAnalyseautomate.AnyAsync(x =>
                   x.IdAnalyseAutomate == analyse && x.IdIncidenceDesFlags == incidence))
                        continue;

                    await _myDbContext.IncidencedesflagsAnalyseautomate.AddAsync(new IncidencedesflagsAnalyseautomate
                    {
                        IdAnalyseAutomate = analyse,
                        IdIncidenceDesFlags = incidence
                    });
                }
               
            }
            await _myDbContext.SaveChangesAsync();

            return true;
        }

        async Task<bool> IAnalyseAutomateRepo.LinkAnalyseAutomateToCommAutos(List<int> idAnalyseAutomate, List<int> commAutosIds)
        {
            foreach (var commeAuto in commAutosIds)
            {
                foreach (var analyse in idAnalyseAutomate)
                {
                    if (await _myDbContext.CommautoAnalyseautomate.AnyAsync(x => x.IdAnalyseAutomate == analyse && x.IdCommAuto == commeAuto))
                        continue;

                    await _myDbContext.CommautoAnalyseautomate.AddAsync(new CommautoAnalyseautomate
                    {
                        IdAnalyseAutomate = analyse,
                        IdCommAuto = commeAuto
                    });
                }
            }
            await _myDbContext.SaveChangesAsync();

            return true;
        }

        private AnalyseAutomate makeAnalyseAutomate(AnalyseAutomatePostDto analyseAutomatePostDto)
        {
            return new AnalyseAutomate
            {
                CodeAller4 = analyseAutomatePostDto.CodeAller4,
                Decimales1 = analyseAutomatePostDto.Decimales1,
                Ligne1ApresResult = analyseAutomatePostDto.Ligne1ApresResult,
                SousTitre = analyseAutomatePostDto.SousTitre,
                IdAnalyse = analyseAutomatePostDto.IdAnalyse,
                RepasserAvecLaDilution = analyseAutomatePostDto.RepasserAvecLaDilution,
                CodeAller2 = analyseAutomatePostDto.CodeAller2,
                CodeAller8 = analyseAutomatePostDto.CodeAller8,
                Ligne1AvantResult = analyseAutomatePostDto.Ligne1AvantResult,
                CodeAller6 = analyseAutomatePostDto.CodeAller6,
                CodeAller7 = analyseAutomatePostDto.CodeAller7,
                CodeAller3 = analyseAutomatePostDto.CodeAller3,
                CodeRetour2 = analyseAutomatePostDto.CodeRetour2,
                CodeAller1 = analyseAutomatePostDto.CodeAller1,
                IsCreationAutomatiqueAnalyse = analyseAutomatePostDto.IsCreationAutomatiqueAnalyse,
                EtOuSupA = analyseAutomatePostDto.EtOuSupA,
                TexteOptionnel12 = analyseAutomatePostDto.TexteOptionnel12,
                Unites1 = analyseAutomatePostDto.Unites1,
                Unites2 = analyseAutomatePostDto.Unites2,
                Longueur = analyseAutomatePostDto.Longueur,
                MiseControleAvecDilution = analyseAutomatePostDto.MiseControleAvecDilution,
                ValeurAajouter = analyseAutomatePostDto.ValeurAajouter,
                Ligne2ApresResult = analyseAutomatePostDto.Ligne2ApresResult,
                TexteOptionnel13 = analyseAutomatePostDto.TexteOptionnel13,
                MinvaleurEntrePassages = analyseAutomatePostDto.MinvaleurEntrePassages,
                IdTypeDeTube = analyseAutomatePostDto.IdTypeDeTube,
                Arrondi = analyseAutomatePostDto.Arrondi,
                Decimales2 = analyseAutomatePostDto.Decimales1,
                Titre = analyseAutomatePostDto.Titre,
                SiInfA = analyseAutomatePostDto.SiInfA,
                Ligne4ApresResult = analyseAutomatePostDto.Ligne4ApresResult,
                Ligne2AvantResult = analyseAutomatePostDto.Ligne2AvantResult,
                Ligne3ApresResult = analyseAutomatePostDto.Ligne3ApresResult,
                GenererFlag = analyseAutomatePostDto.GenererFlag,
                Ligne5AvantResult = analyseAutomatePostDto.Ligne5AvantResult,
                TexteOptionnel10 = analyseAutomatePostDto.TexteOptionnel10,
                TexteOptionnel11 = analyseAutomatePostDto.TexteOptionnel11,
                TexteOptionnel14 = analyseAutomatePostDto.TexteOptionnel14,
                Ligne3AvantResult = analyseAutomatePostDto.Ligne3AvantResult,
                CodeAller9 = analyseAutomatePostDto.CodeAller9,
                Ligne6ApresResult = analyseAutomatePostDto.Ligne6ApresResult,
                EtOuEgalA = analyseAutomatePostDto.EtOuEgalA,
                Ligne4AvantResult = analyseAutomatePostDto.Ligne4AvantResult,
                IsMiseEnControleSurCondi = analyseAutomatePostDto.IsMiseEnControleSurCondi,
                DifValeurSupa = analyseAutomatePostDto.DifValeurSupa,
                FacteurDilution = analyseAutomatePostDto.FacteurDilution,
                FormuleX2 = analyseAutomatePostDto.FormuleX2,
                FormuleX1 = analyseAutomatePostDto.FormuleX1,
                TexteOptionnel15 = analyseAutomatePostDto.TexteOptionnel15,
                IdPaillasse = analyseAutomatePostDto.IdPaillasse,
                TexteOptionnel9 = analyseAutomatePostDto.TexteOptionnel9,
                TexteOptionnel8 = analyseAutomatePostDto.TexteOptionnel8,
                TexteOptionnel1 = analyseAutomatePostDto.TexteOptionnel1,
                TexteOptionnel3 = analyseAutomatePostDto.TexteOptionnel3,
                TexteOptionnel2 = analyseAutomatePostDto.TexteOptionnel2,
                TexteOptionnel5 = analyseAutomatePostDto.TexteOptionnel5,
                TexteOptionnel4 = analyseAutomatePostDto.TexteOptionnel4,
                TexteOptionnel7 = analyseAutomatePostDto.TexteOptionnel7,
                TexteOptionnel6 = analyseAutomatePostDto.TexteOptionnel6,
                CodeRetour1 = analyseAutomatePostDto.CodeRetour1,
                IdAutomate = analyseAutomatePostDto.IdAutomate,
                Ligne5ApresResult = analyseAutomatePostDto.Ligne5ApresResult,
                Coefficient = analyseAutomatePostDto.Coefficient,
                Formule = analyseAutomatePostDto.Formule,
                CodeAller5 = analyseAutomatePostDto.CodeAller5,
                AnalyseAutomatecol = analyseAutomatePostDto.AnalyseAutomatecol,
                Paillasse2 = analyseAutomatePostDto.Paillasse2

            };
        }

        private void UpdateAnalyseAutomate(AnalyseAutomate analyseAutomate, AnalyseAutomatePostDto analyseAutomatePostDto)
        {
            analyseAutomate.CodeAller4 = analyseAutomatePostDto.CodeAller4;
            analyseAutomate.Decimales1 = analyseAutomatePostDto.Decimales1;
            analyseAutomate.Ligne1ApresResult = analyseAutomatePostDto.Ligne1ApresResult;
            analyseAutomate.SousTitre = analyseAutomatePostDto.SousTitre;
            analyseAutomate.IdAnalyse = analyseAutomatePostDto.IdAnalyse;
            analyseAutomate.RepasserAvecLaDilution = analyseAutomatePostDto.RepasserAvecLaDilution;
            analyseAutomate.CodeAller2 = analyseAutomatePostDto.CodeAller2;
            analyseAutomate.CodeAller8 = analyseAutomatePostDto.CodeAller8;
            analyseAutomate.Ligne1AvantResult = analyseAutomatePostDto.Ligne1AvantResult;
            analyseAutomate.CodeAller6 = analyseAutomatePostDto.CodeAller6;
            analyseAutomate.CodeAller7 = analyseAutomatePostDto.CodeAller7;
            analyseAutomate.CodeAller3 = analyseAutomatePostDto.CodeAller3;
            analyseAutomate.CodeRetour2 = analyseAutomatePostDto.CodeRetour2;
            analyseAutomate.CodeAller1 = analyseAutomatePostDto.CodeAller1;
            analyseAutomate.IsCreationAutomatiqueAnalyse = analyseAutomatePostDto.IsCreationAutomatiqueAnalyse;
            analyseAutomate.EtOuSupA = analyseAutomatePostDto.EtOuSupA;
            analyseAutomate.TexteOptionnel12 = analyseAutomatePostDto.TexteOptionnel12;
            analyseAutomate.Unites1 = analyseAutomatePostDto.Unites1;
            analyseAutomate.Unites2 = analyseAutomatePostDto.Unites2;
            analyseAutomate.Longueur = analyseAutomatePostDto.Longueur;
            analyseAutomate.MiseControleAvecDilution = analyseAutomatePostDto.MiseControleAvecDilution;
            analyseAutomate.ValeurAajouter = analyseAutomatePostDto.ValeurAajouter;
            analyseAutomate.Ligne2ApresResult = analyseAutomatePostDto.Ligne2ApresResult;
            analyseAutomate.TexteOptionnel13 = analyseAutomatePostDto.TexteOptionnel13;
            analyseAutomate.MinvaleurEntrePassages = analyseAutomatePostDto.MinvaleurEntrePassages;
            analyseAutomate.IdTypeDeTube = analyseAutomatePostDto.IdTypeDeTube;
            analyseAutomate.Arrondi = analyseAutomatePostDto.Arrondi;
            analyseAutomate.Decimales2 = analyseAutomatePostDto.Decimales1;
            analyseAutomate.Titre = analyseAutomatePostDto.Titre;
            analyseAutomate.SiInfA = analyseAutomatePostDto.SiInfA;
            analyseAutomate.Ligne4ApresResult = analyseAutomatePostDto.Ligne4ApresResult;
            analyseAutomate.Ligne2AvantResult = analyseAutomatePostDto.Ligne2AvantResult;
            analyseAutomate.Ligne3ApresResult = analyseAutomatePostDto.Ligne3ApresResult;
            analyseAutomate.GenererFlag = analyseAutomatePostDto.GenererFlag;
            analyseAutomate.Ligne5AvantResult = analyseAutomatePostDto.Ligne5AvantResult;
            analyseAutomate.TexteOptionnel10 = analyseAutomatePostDto.TexteOptionnel10;
            analyseAutomate.TexteOptionnel11 = analyseAutomatePostDto.TexteOptionnel11;
            analyseAutomate.TexteOptionnel14 = analyseAutomatePostDto.TexteOptionnel14;
            analyseAutomate.Ligne3AvantResult = analyseAutomatePostDto.Ligne3AvantResult;
            analyseAutomate.CodeAller9 = analyseAutomatePostDto.CodeAller9;
            analyseAutomate.Ligne6ApresResult = analyseAutomatePostDto.Ligne6ApresResult;
            analyseAutomate.EtOuEgalA = analyseAutomatePostDto.EtOuEgalA;
            analyseAutomate.Ligne4AvantResult = analyseAutomatePostDto.Ligne4AvantResult;
            analyseAutomate.IsMiseEnControleSurCondi = analyseAutomatePostDto.IsMiseEnControleSurCondi;
            analyseAutomate.DifValeurSupa = analyseAutomatePostDto.DifValeurSupa;
            analyseAutomate.FacteurDilution = analyseAutomatePostDto.FacteurDilution;
            analyseAutomate.FormuleX2 = analyseAutomatePostDto.FormuleX2;
            analyseAutomate.FormuleX1 = analyseAutomatePostDto.FormuleX1;
            analyseAutomate.TexteOptionnel15 = analyseAutomatePostDto.TexteOptionnel15;
            analyseAutomate.IdPaillasse = analyseAutomatePostDto.IdPaillasse;
            analyseAutomate.TexteOptionnel9 = analyseAutomatePostDto.TexteOptionnel9;
            analyseAutomate.TexteOptionnel8 = analyseAutomatePostDto.TexteOptionnel8;
            analyseAutomate.TexteOptionnel1 = analyseAutomatePostDto.TexteOptionnel1;
            analyseAutomate.TexteOptionnel3 = analyseAutomatePostDto.TexteOptionnel3;
            analyseAutomate.TexteOptionnel2 = analyseAutomatePostDto.TexteOptionnel2;
            analyseAutomate.TexteOptionnel5 = analyseAutomatePostDto.TexteOptionnel5;
            analyseAutomate.TexteOptionnel4 = analyseAutomatePostDto.TexteOptionnel4;
            analyseAutomate.TexteOptionnel7 = analyseAutomatePostDto.TexteOptionnel7;
            analyseAutomate.TexteOptionnel6 = analyseAutomatePostDto.TexteOptionnel6;
            analyseAutomate.CodeRetour1 = analyseAutomatePostDto.CodeRetour1;
            analyseAutomate.IdAutomate = analyseAutomatePostDto.IdAutomate;
            analyseAutomate.Ligne5ApresResult = analyseAutomatePostDto.Ligne5ApresResult;
            analyseAutomate.Coefficient = analyseAutomatePostDto.Coefficient;
            analyseAutomate.Formule = analyseAutomatePostDto.Formule;
            analyseAutomate.CodeAller5 = analyseAutomatePostDto.CodeAller5;
            analyseAutomate.AnalyseAutomatecol = analyseAutomatePostDto.AnalyseAutomatecol;
            analyseAutomate.Paillasse2 = analyseAutomatePostDto.Paillasse2;

        }

        private async Task RemoveAnalyseAutomateLinks(int analyseAutomateId)
        {
            var commautoAnalyseautomates = await _myDbContext.CommautoAnalyseautomate
                .Where(x => x.IdAnalyseAutomate == analyseAutomateId).ToListAsync();
            _myDbContext.CommautoAnalyseautomate.RemoveRange(commautoAnalyseautomates);

            var evaluationsAnalyseautomates = await _myDbContext.EvaluationsAnalyseautomate
                .Where(x => x.IdAnalyseAutomate == analyseAutomateId).ToListAsync();
            _myDbContext.EvaluationsAnalyseautomate.RemoveRange(evaluationsAnalyseautomates);

            var analyseautomateFonctionsreflexes = await _myDbContext.AnalyseautomateFonctionsreflexes
                .Where(x => x.IdAnalyseAutomate == analyseAutomateId).ToListAsync();
            _myDbContext.AnalyseautomateFonctionsreflexes.RemoveRange(analyseautomateFonctionsreflexes);
            
            var incidencedesflagsAnalyseautomates = await _myDbContext.IncidencedesflagsAnalyseautomate
                .Where(x => x.IdAnalyseAutomate == analyseAutomateId).ToListAsync();
            _myDbContext.IncidencedesflagsAnalyseautomate.RemoveRange(incidencedesflagsAnalyseautomates);

            var transfbasicAnalyseautomates = await _myDbContext.TransfbasicAnalyseautomate
                .Where(x => x.IdAnalyseAutomate == analyseAutomateId).ToListAsync();
            _myDbContext.TransfbasicAnalyseautomate.RemoveRange(transfbasicAnalyseautomates);

            var analyseautomateTransformations = await _myDbContext.AnalyseautomateTransformations
                .Where(x => x.IdAnalyseAutomate == analyseAutomateId).ToListAsync();
            _myDbContext.AnalyseautomateTransformations.RemoveRange(analyseautomateTransformations);

            var analyseAutomateValidations = await _myDbContext.AnalyseAutomateValidations
                .Where(x => x.IdAnalyseAutomate == analyseAutomateId).ToListAsync();
            _myDbContext.AnalyseAutomateValidations.RemoveRange(analyseAutomateValidations);

        }

        private async Task CreateAnalyseAutomateLinks(AnalyseAutomate analyseAutomate, AnalyseAutomateFullPostDto analyseAutomateFullPostDto)
        {
            foreach (var item in analyseAutomateFullPostDto.CommAutos)
            {
                await _myDbContext.CommautoAnalyseautomate.AddAsync(new CommautoAnalyseautomate
                {
                    IdAnalyseAutomateNavigation = analyseAutomate,
                    IdCommAutoNavigation = _commAutoRepo.MakeCommAuto(item)
                });
            }

            foreach (var item in analyseAutomateFullPostDto.Evaluations)
            {
                await _myDbContext.EvaluationsAnalyseautomate.AddAsync(new EvaluationsAnalyseautomate
                {
                    IdAnalyseAutomateNavigation = analyseAutomate,
                    IdEvaluationsNavigation = _evaluationRepo.MakeEvaluation(item)
                });
            }

            foreach (var item in analyseAutomateFullPostDto.FonctionReflexes)
            {
                await _myDbContext.AnalyseautomateFonctionsreflexes.AddAsync(new AnalyseautomateFonctionsreflexes
                {
                    IdAnalyseAutomateNavigation = analyseAutomate,
                    IdFonctionsReflexesNavigation = _fonctionReflexesRepo.MakeFonctionsreflexes(item)
                });
            }

            foreach (var item in analyseAutomateFullPostDto.IncidenceDesFlags)
            {
                await _myDbContext.IncidencedesflagsAnalyseautomate.AddAsync(new IncidencedesflagsAnalyseautomate
                {
                    IdAnalyseAutomateNavigation = analyseAutomate,
                    IdIncidenceDesFlagsNavigation = _incidenceDesFlagsRepo.MakeIncidencedesflags(item)
                });
            }

            foreach (var item in analyseAutomateFullPostDto.TransBasics)
            {
                await _myDbContext.TransfbasicAnalyseautomate.AddAsync(new TransfbasicAnalyseautomate
                {
                    IdAnalyseAutomateNavigation = analyseAutomate,
                    IdTransfbasicNavigation = _transBasicRepo.MakeTransfbasic(item)
                });
            }

            foreach (var item in analyseAutomateFullPostDto.Transformations)
            {
                await _myDbContext.AnalyseautomateTransformations.AddAsync(new AnalyseautomateTransformations
                {
                    IdAnalyseAutomateNavigation = analyseAutomate,
                    IdTransformationsNavigation = _transformationRepo.MakeTransformations(item)
                });
            }

            foreach (var item in analyseAutomateFullPostDto.Validations)
            {
                await _myDbContext.AnalyseAutomateValidations.AddAsync(new AnalyseAutomateValidations
                {
                    IdAnalyseAutomateNavigation = analyseAutomate,
                    IdValidationsNavigation = _validationRepo.MakeValidations(item)
                });
            }
        }

        async Task<bool> IAnalyseAutomateRepo.CreateFullAnalyseAutomateCatalogue(AnalyseAutomateFullPostDto analyseAutomatePostDto, Catalogue catalogue)
        {
            foreach (var item in analyseAutomatePostDto.AnalyseAutomate)
            {
                var analyseAutomate = makeAnalyseAutomate(item);
                await _myDbContext.AnalyseAutomate.AddAsync(analyseAutomate);

                await CreateAnalyseAutomateLinks(analyseAutomate, analyseAutomatePostDto);

                await _myDbContext.AnalyseAutomateCatalog.AddAsync(new AnalyseAutomateCatalog
                {
                    IdCatalogue = catalogue.IdCatalogue,
                    IdAnalyseAutomateNavigation = analyseAutomate
                });
            }

            await _myDbContext.SaveChangesAsync();

            return true;
        }
    }
}
