﻿using System.Collections.Generic;
using MasterDataManagement.Domain.Models;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Repositories;
using MasterDataManagement.DTO.CommAuto;
using Microsoft.EntityFrameworkCore;
using MasterDataManagement.Infrastructure.ManagementDbContext;

namespace MasterDataManagement.Infrastructure.Repositories
{
    public class CommAutoRepo : ICommAutoRepo
    {
        private readonly MyDbContext _myDbContext;

        public CommAutoRepo(MyDbContext myDbContext)
        {
            _myDbContext = myDbContext;
        }
        async Task ICommAutoRepo.CreateCommAuto(CommAutoPostDto commAutoPostDto)
        {
            var commAuto = ((ICommAutoRepo)this).MakeCommAuto(commAutoPostDto);
            await _myDbContext.Commauto.AddAsync(commAuto);
            await _myDbContext.SaveChangesAsync();
        }
        Commauto ICommAutoRepo.MakeCommAuto(CommAutoPostDto commAutoPostDto)
        {
            return new Commauto
            {
                Ordre = commAutoPostDto.Ordre,
                Condition = commAutoPostDto.Condition,
                Valeur = commAutoPostDto.Valeur
            };
        }

        async Task<Commauto> ICommAutoRepo.GetCommAutoById(int idCommAuto)
        {
            var commAuto = await _myDbContext.Commauto.FirstOrDefaultAsync(x => x.IdCommAuto == idCommAuto);
            return commAuto;
        }

        async Task<Commauto> ICommAutoRepo.UpdateCommAuto(Commauto commAuto, CommAutoPostDto commAutoPostDto)
        {

            commAuto.Ordre = commAutoPostDto.Ordre;
            commAuto.Condition = commAutoPostDto.Condition;
            commAuto.Valeur = commAutoPostDto.Valeur;

            await _myDbContext.SaveChangesAsync();

            return commAuto;
        }


        async Task<List<Commauto>> ICommAutoRepo.GetCommAutos()
        {
            var commAutos = await _myDbContext.Commauto.ToListAsync();

            return commAutos;
        }
        async Task ICommAutoRepo.DeleteCommAuto(Commauto commAuto)
        {
            _myDbContext.Commauto.Remove(commAuto);
            await _myDbContext.SaveChangesAsync();
        }

        async Task<bool> ICommAutoRepo.ExistAll(List<int> commAutosIds)
        {
            foreach (var item in commAutosIds)
            {
                if (!await _myDbContext.Commauto.AnyAsync(x => x.IdCommAuto == item))
                    return false;
            }
            return true;
        }
    }
}
