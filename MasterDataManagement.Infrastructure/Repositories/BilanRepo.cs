﻿using System.Collections.Generic;
using MasterDataManagement.Domain.Models;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Repositories;
using MasterDataManagement.DTO.Bilan;
using Microsoft.EntityFrameworkCore;
using MasterDataManagement.Infrastructure.ManagementDbContext;

namespace MasterDataManagement.Infrastructure.Repositories
{
    public class BilanRepo : IBilanRepo
    {
        private readonly MyDbContext _myDbContext;

        public BilanRepo(MyDbContext myDbContext)
        {
            _myDbContext = myDbContext;
        }
        async Task IBilanRepo.CreateBilan(BilanPostDto bilanPostDto)
        {
            var bilan = ((IBilanRepo)this).MakeBilan(bilanPostDto);

            await _myDbContext.Bilan.AddAsync(bilan);
            await _myDbContext.SaveChangesAsync();
        }

        Bilan IBilanRepo.MakeBilan(BilanPostDto bilanPostDto)
        {
            return new Bilan
            {
                Analyse1 = bilanPostDto.Analyse1,
                Analyse10 = bilanPostDto.Analyse10,
                Analyse11 = bilanPostDto.Analyse11,
                Analyse12 = bilanPostDto.Analyse12,
                Analyse13 = bilanPostDto.Analyse13,
                Analyse14 = bilanPostDto.Analyse14,
                Analyse15 = bilanPostDto.Analyse15,
                Analyse16 = bilanPostDto.Analyse16,
                Analyse17 = bilanPostDto.Analyse17,
                Analyse18 = bilanPostDto.Analyse18,
                Analyse19 = bilanPostDto.Analyse19,
                Analyse2 = bilanPostDto.Analyse2,
                Analyse20 = bilanPostDto.Analyse20,
                Analyse21 = bilanPostDto.Analyse21,
                Analyse22 = bilanPostDto.Analyse22,
                Analyse23 = bilanPostDto.Analyse23,
                Analyse24 = bilanPostDto.Analyse24,
                Analyse3 = bilanPostDto.Analyse3,
                Analyse4 = bilanPostDto.Analyse4,
                Analyse5 = bilanPostDto.Analyse5,
                Analyse6 = bilanPostDto.Analyse6,
                Analyse7 = bilanPostDto.Analyse7,
                Analyse8 = bilanPostDto.Analyse8,
                Analyse9 = bilanPostDto.Analyse9
            };
        }

        async Task<Bilan> IBilanRepo.GetBilanById(int idBilan)
        {
            var bilan = await _myDbContext.Bilan.FirstOrDefaultAsync(x => x.IdBilan == idBilan);
            return bilan;
        }

        async Task<Bilan> IBilanRepo.UpdateBilan(Bilan bilan, BilanPostDto bilanPostDto)
        {

            bilan.Analyse1 = bilanPostDto.Analyse1;
            bilan.Analyse10 = bilanPostDto.Analyse10;
            bilan.Analyse11 = bilanPostDto.Analyse11;
            bilan.Analyse12 = bilanPostDto.Analyse12;
            bilan.Analyse13 = bilanPostDto.Analyse13;
            bilan.Analyse14 = bilanPostDto.Analyse14;
            bilan.Analyse15 = bilanPostDto.Analyse15;
            bilan.Analyse16 = bilanPostDto.Analyse16;
            bilan.Analyse17 = bilanPostDto.Analyse17;
            bilan.Analyse18 = bilanPostDto.Analyse18;
            bilan.Analyse19 = bilanPostDto.Analyse19;
            bilan.Analyse2 = bilanPostDto.Analyse2;
            bilan.Analyse20 = bilanPostDto.Analyse20;
            bilan.Analyse21 = bilanPostDto.Analyse21;
            bilan.Analyse22 = bilanPostDto.Analyse22;
            bilan.Analyse23 = bilanPostDto.Analyse23;
            bilan.Analyse24 = bilanPostDto.Analyse24;
            bilan.Analyse3 = bilanPostDto.Analyse3;
            bilan.Analyse4 = bilanPostDto.Analyse4;
            bilan.Analyse5 = bilanPostDto.Analyse5;
            bilan.Analyse6 = bilanPostDto.Analyse6;
            bilan.Analyse7 = bilanPostDto.Analyse7;
            bilan.Analyse8 = bilanPostDto.Analyse8;
            bilan.Analyse9 = bilanPostDto.Analyse9;

            await _myDbContext.SaveChangesAsync();

            return bilan;
        }


        async Task<List<Bilan>> IBilanRepo.GetBilans()
        {
            var bilans = await _myDbContext.Bilan.ToListAsync();

            return bilans;
        }
        async Task IBilanRepo.DeleteBilan(Bilan bilan)
        {
            _myDbContext.Bilan.Remove(bilan);
            await _myDbContext.SaveChangesAsync();
        }

        async Task<bool> IBilanRepo.ExistAll(List<int> bilansIds)
        {
            foreach (var item in bilansIds)
            {
                if (!await _myDbContext.Bilan.AnyAsync(x => x.IdBilan == item))
                    return false;
            }
            return true;
        }
    }
}
