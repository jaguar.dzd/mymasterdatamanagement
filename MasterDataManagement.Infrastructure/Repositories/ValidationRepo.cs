﻿using System.Collections.Generic;
using MasterDataManagement.Domain.Models;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Repositories;
using MasterDataManagement.DTO.Validation;
using Microsoft.EntityFrameworkCore;
using MasterDataManagement.Infrastructure.ManagementDbContext;

namespace MasterDataManagement.Infrastructure.Repositories
{
    public class ValidationRepo : IValidationRepo
    {
        private readonly MyDbContext _myDbContext;

        public ValidationRepo(MyDbContext myDbContext)
        {
            _myDbContext = myDbContext;
        }
        async Task IValidationRepo.CreateValidation(ValidationPostDto validationPostDto)
        {
            var v = ((IValidationRepo)this).MakeValidations(validationPostDto);
            await _myDbContext.Validations.AddAsync(v);
            await _myDbContext.SaveChangesAsync();
        }
        Validations IValidationRepo.MakeValidations(ValidationPostDto validationPostDto)
        {
            return new Validations
            {
                Max = validationPostDto.Max,
                Min = validationPostDto.Min,
                Origine = validationPostDto.Origine,
                Service = validationPostDto.Service,
                Sexe = validationPostDto.Sexe,
                Unites = validationPostDto.Unites,
                ValBasse = validationPostDto.ValBasse,
                ValHaute = validationPostDto.ValHaute,
                ValTresBasse = validationPostDto.ValTresBasse,
                ValTresHaute = validationPostDto.ValTresHaute
            };
        }

        async Task<Validations> IValidationRepo.GetValidationById(int idValidation)
        {
            var validation = await _myDbContext.Validations.FirstOrDefaultAsync(x => x.IdValidations == idValidation);
            return validation;
        }

        async Task<Validations> IValidationRepo.UpdateValidation(Validations validation, ValidationPostDto validationPostDto)
        {
            validation.Max = validationPostDto.Max;
            validation.Min = validationPostDto.Min;
            validation.Origine = validationPostDto.Origine;
            validation.Service = validationPostDto.Service;
            validation.Sexe = validationPostDto.Sexe;
            validation.Unites = validationPostDto.Unites;
            validation.ValBasse = validationPostDto.ValBasse;
            validation.ValHaute = validationPostDto.ValHaute;
            validation.ValTresBasse = validationPostDto.ValTresBasse;
            validation.ValTresHaute = validationPostDto.ValTresHaute;

            await _myDbContext.SaveChangesAsync();

            return validation;
        }


        async Task<List<Validations>> IValidationRepo.GetValidations()
        {
            var validations = await _myDbContext.Validations.ToListAsync();

            return validations;
        }
        async Task IValidationRepo.DeleteValidation(Validations validation)
        {
            _myDbContext.Validations.Remove(validation);
            await _myDbContext.SaveChangesAsync();
        }

        async Task<bool> IValidationRepo.ExistAll(List<int> validationsIds)
        {
            foreach (var item in validationsIds)
            {
                if (!await _myDbContext.Validations.AnyAsync(x => x.IdValidations == item))
                    return false;
            }
            return true;
        }
    }
}
