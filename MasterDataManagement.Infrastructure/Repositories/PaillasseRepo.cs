﻿using System.Collections.Generic;
using MasterDataManagement.Domain.Models;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Repositories;
using MasterDataManagement.DTO.Paillasse;
using Microsoft.EntityFrameworkCore;
using MasterDataManagement.Infrastructure.ManagementDbContext;

namespace MasterDataManagement.Infrastructure.Repositories
{
    public class PaillasseRepo : IPaillasseRepo
    {
        private readonly MyDbContext _myDbContext;

        public PaillasseRepo(MyDbContext myDbContext)
        {
            _myDbContext = myDbContext;
        }
        async Task IPaillasseRepo.CreatePaillasse(PaillassePostDto paillassePostDto)
        {
            await _myDbContext.Paillasse.AddAsync(new Paillasse
            {
                NomPaillasse = paillassePostDto.NomPaillasse
            });
            await _myDbContext.SaveChangesAsync();
        }

        async Task<Paillasse> IPaillasseRepo.GetPaillasseById(int idPaillasse)
        {
            var paillasse = await _myDbContext.Paillasse.FirstOrDefaultAsync(x => x.IdPaillasse == idPaillasse);
            return paillasse;
        }

        async Task<Paillasse> IPaillasseRepo.UpdatePaillasse(Paillasse paillasse, PaillassePostDto paillassePostDto)
        {

            paillasse.NomPaillasse = paillassePostDto.NomPaillasse;

            await _myDbContext.SaveChangesAsync();

            return paillasse;
        }


        async Task<List<Paillasse>> IPaillasseRepo.GetPaillasses()
        {
            var paillasses = await _myDbContext.Paillasse.ToListAsync();

            return paillasses;
        }
        async Task IPaillasseRepo.DeletePaillasse(Paillasse paillasse)
        {
            _myDbContext.Paillasse.Remove(paillasse);
            await _myDbContext.SaveChangesAsync();
        }
    }
}
