﻿using System.Collections.Generic;
using MasterDataManagement.Domain.Models;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Repositories;
using MasterDataManagement.DTO.Evaluation;
using Microsoft.EntityFrameworkCore;
using MasterDataManagement.Infrastructure.ManagementDbContext;

namespace MasterDataManagement.Infrastructure.Repositories
{
    public class EvaluationRepo : IEvaluationRepo
    {
        private readonly MyDbContext _myDbContext;

        public EvaluationRepo(MyDbContext myDbContext)
        {
            _myDbContext = myDbContext;
        }
        async Task IEvaluationRepo.CreateEvaluation(EvaluationPostDto evaluationPostDto)
        {
            var eval = ((IEvaluationRepo)this).MakeEvaluation(evaluationPostDto);
            await _myDbContext.Evaluations.AddAsync(eval);
            await _myDbContext.SaveChangesAsync();
        }
        Evaluations IEvaluationRepo.MakeEvaluation(EvaluationPostDto evaluationPostDto)
        {
            return new Evaluations
            {
                Ordre = evaluationPostDto.Ordre,
                Condition = evaluationPostDto.Condition,
                NouvelleValeur = evaluationPostDto.NouvelleValeur
            };
        }


        async Task<Evaluations> IEvaluationRepo.GetEvaluationById(int idEvaluation)
        {
            var evaluation = await _myDbContext.Evaluations.FirstOrDefaultAsync(x => x.IdEvaluations == idEvaluation);
            return evaluation;
        }

        async Task<Evaluations> IEvaluationRepo.UpdateEvaluation(Evaluations evaluation, EvaluationPostDto evaluationPostDto)
        {

            evaluation.Condition = evaluationPostDto.Condition;
            evaluation.Ordre = evaluationPostDto.Ordre;
            evaluation.NouvelleValeur = evaluationPostDto.NouvelleValeur;

            await _myDbContext.SaveChangesAsync();

            return evaluation;
        }


        async Task<List<Evaluations>> IEvaluationRepo.GetEvaluations()
        {
            var evaluations = await _myDbContext.Evaluations.ToListAsync();

            return evaluations;
        }
        async Task IEvaluationRepo.DeleteEvaluation(Evaluations evaluation)
        {
            _myDbContext.Evaluations.Remove(evaluation);
            await _myDbContext.SaveChangesAsync();
        }

        async Task<bool> IEvaluationRepo.ExistAll(List<int> evaluationsId)
        {
            foreach (var item in evaluationsId)
            {
                if (!await _myDbContext.Evaluations.AnyAsync(x => x.IdEvaluations == item))
                    return false;
            }
            return true;
        }
    }
}
