﻿using System.Collections.Generic;
using MasterDataManagement.Domain.Models;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Repositories;
using MasterDataManagement.DTO.GroupeAnalyse;
using Microsoft.EntityFrameworkCore;
using MasterDataManagement.Infrastructure.ManagementDbContext;

namespace MasterDataManagement.Infrastructure.Repositories
{
    public class GroupeAnalyseRepo : IGroupeAnalyseRepo
    {
        private readonly MyDbContext _myDbContext;

        public GroupeAnalyseRepo(MyDbContext myDbContext)
        {
            _myDbContext = myDbContext;
        }
        async Task IGroupeAnalyseRepo.CreateGroupeAnalyse(GroupeAnalysePostDto groupeAnalysePostDto)
        {
            await _myDbContext.Groupeanalyse.AddAsync(new Groupeanalyse
            {
                NomGroupe = groupeAnalysePostDto.NomGroupe
                
            });
            await _myDbContext.SaveChangesAsync();
        }

        async Task<Groupeanalyse> IGroupeAnalyseRepo.GetGroupeAnalyseById(int idGroupeAnalyse)
        {
            var groupeAnalyse = await _myDbContext.Groupeanalyse.FirstOrDefaultAsync(x => x.IdGroupeAnalyse == idGroupeAnalyse);
            return groupeAnalyse;
        }

        async Task<Groupeanalyse> IGroupeAnalyseRepo.UpdateGroupeAnalyse(Groupeanalyse groupeAnalyse, GroupeAnalysePostDto groupeAnalysePostDto)
        {

            groupeAnalyse.NomGroupe = groupeAnalysePostDto.NomGroupe;

            await _myDbContext.SaveChangesAsync();

            return groupeAnalyse;
        }


        async Task<List<Groupeanalyse>> IGroupeAnalyseRepo.GetGroupeAnalyses()
        {
            var groupeAnalyses = await _myDbContext.Groupeanalyse.ToListAsync();

            return groupeAnalyses;
        }
        async Task IGroupeAnalyseRepo.DeleteGroupeAnalyse(Groupeanalyse groupeAnalyse)
        {
            _myDbContext.Groupeanalyse.Remove(groupeAnalyse);
            await _myDbContext.SaveChangesAsync();
        }
    }
}
