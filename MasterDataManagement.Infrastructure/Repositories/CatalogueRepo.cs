﻿using System.Collections.Generic;
using MasterDataManagement.Domain.Models;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Repositories;
using MasterDataManagement.DTO.Catalogue;
using Microsoft.EntityFrameworkCore;
using MasterDataManagement.Infrastructure.ManagementDbContext;
using System;
using System.Linq;
using System.Xml.Schema;
using MasterDataManagement.Domain.Others;
using MasterDataManagement.DTO.Enum;

namespace MasterDataManagement.Infrastructure.Repositories
{
    public class CatalogueRepo : ICatalogueRepo
    {
        private readonly MyDbContext _myDbContext;
        private readonly IAnalyseRepo _analyseRepo;
        private readonly IAutomateRepo _automateRepo;

        public CatalogueRepo(MyDbContext myDbContext, IAnalyseRepo analyseRepo, IAutomateRepo automateRepo)
        {
            _myDbContext = myDbContext;
            this._analyseRepo = analyseRepo;
            this._automateRepo = automateRepo;
        }

        async Task ICatalogueRepo.CreateCatalogue(CataloguePostDto cataloguePostDto)
        {
            var catalogue = await ((ICatalogueRepo)this).MakeCatalogue(cataloguePostDto);
            await _myDbContext.Catalogue.AddAsync(catalogue);
            await _myDbContext.SaveChangesAsync();
        }

        async Task<Catalogue> ICatalogueRepo.MakeCatalogue(CataloguePostDto cataloguePostDto)
        {
            var existWithSameName = await _myDbContext.Catalogue.FirstOrDefaultAsync(x => x.NomCatalogue == cataloguePostDto.NomCatalogue);

            return new Catalogue
            {
                NomCatalogue = cataloguePostDto.NomCatalogue,
                Statut = CatalogueStatut.EnAttenteDeValidation,
                Version = existWithSameName == null? 1: existWithSameName.Version + 1,
                IsActivated = true,
                CreationDate = DateTime.UtcNow,
                CreatorName = cataloguePostDto.CreatorName
            };
        }

        async Task<Catalogue> ICatalogueRepo.GetCatalogueById(int idCatalogue)
        {
            var catalogue = await _myDbContext.Catalogue.FirstOrDefaultAsync(x => x.IdCatalogue == idCatalogue);
            return catalogue;
        }

        async Task<Catalogue> ICatalogueRepo.UpdateCatalogue(Catalogue catalogue, CataloguePostDto cataloguePostDto)
        {
            UpdateCatalogueDetail(catalogue, cataloguePostDto);

            await _myDbContext.SaveChangesAsync();

            return catalogue;
        }

        private static void UpdateCatalogueDetail(Catalogue catalogue, CataloguePostDto cataloguePostDto)
        {
            catalogue.NomCatalogue = cataloguePostDto.NomCatalogue;
            catalogue.CreatorName = cataloguePostDto.CreatorName;
            catalogue.CreationDate = DateTime.UtcNow;
        }

        async Task<List<Catalogue>> ICatalogueRepo.GetCatalogues()
        {
            var catalogues = await _myDbContext.Catalogue.ToListAsync();

            return catalogues;
        }

        async Task<List<Catalogue>> ICatalogueRepo.GetCataloguesWithDetails()
        {
            var catalogues = await _myDbContext.Catalogue
                .Include(x => x.AnalyseCatalogue).ThenInclude(x => x.AnalyseIdAnalyseNavigation)
                .Include(x => x.AutomateCatalogue).ThenInclude(x => x.IdAutomateNavigation)
                .Include(x => x.InstanceCatalogue).ThenInclude(x => x.IdInstanceNavigation)
                .ToListAsync();

            return catalogues;
        }



        async Task ICatalogueRepo.DeleteCatalogue(Catalogue catalogue)
        {
            _myDbContext.Catalogue.Remove(catalogue);
            await _myDbContext.SaveChangesAsync();
        }

        async Task<bool> ICatalogueRepo.LinkCatalogueToAutomates(int idCatalogue, List<int> automates)
        {
            foreach (var item in automates)
            {
                if (await _myDbContext.AutomateCatalogue.AnyAsync(x => x.IdCatalogue == idCatalogue && x.IdAutomate == item))
                    continue;

                await _myDbContext.AutomateCatalogue.AddAsync(new AutomateCatalogue
                {
                    IdCatalogue = idCatalogue,
                    IdAutomate = item
                });
            }
            await _myDbContext.SaveChangesAsync();

            return true;
        }

        async Task<bool> ICatalogueRepo.LinkCatalogueToAnalyses(int idCatalogue, List<int> analyses)
        {
            foreach (var item in analyses)
            {
                if (await _myDbContext.AnalyseCatalogue.AnyAsync(x =>
                    x.CatalogueIdCatalogue == idCatalogue && x.AnalyseIdAnalyse == item))
                    continue;

                await _myDbContext.AnalyseCatalogue.AddAsync(new AnalyseCatalogue
                {
                    CatalogueIdCatalogue = idCatalogue,
                    AnalyseIdAnalyse = item
                });
            }
            await _myDbContext.SaveChangesAsync();

            return true;
        }

        async Task<bool> ICatalogueRepo.LinkCatalogueToAnalyaseAutomates(int idCatalogue, List<int> analyseAutomatesIds)
        {
            foreach (var item in analyseAutomatesIds)
            {
                var analyseAutomateCatalog = await _myDbContext.AnalyseAutomateCatalog.FirstOrDefaultAsync(x =>
                    x.IdCatalogue == idCatalogue && x.IdAnalyseAutomate == item);

                if (analyseAutomateCatalog != null)
                    continue;

                await _myDbContext.AnalyseAutomateCatalog.AddAsync(new AnalyseAutomateCatalog
                {
                    IdCatalogue = idCatalogue,
                    IdAnalyseAutomate = item
                });
            }
            await _myDbContext.SaveChangesAsync();

            return true;
        }

        async Task<bool> ICatalogueRepo.CreateFullCatalogue(CatalogueFullPostDto cataloguePostDto)
        {
            if (!await _analyseRepo.ExistAll(cataloguePostDto.Analyses))
                return false;

            if (!await _automateRepo.ExistAll(cataloguePostDto.Automates))
                return false;

            var catalogue = await ((ICatalogueRepo)this).MakeCatalogue(cataloguePostDto.Catalogue);

            await _myDbContext.Catalogue.AddAsync(catalogue);

            await CreateCatalogueLinks(cataloguePostDto, catalogue);

            await _myDbContext.SaveChangesAsync();

            return true;
        }

        async Task ICatalogueRepo.UpdateFullCatalogue(Catalogue catagloue, CatalogueFullPostDto cataloguePostDto)
        {
            UpdateCatalogueDetail(catagloue, cataloguePostDto.Catalogue);

            await RemoveCatalogueLinks(catagloue);

            await CreateCatalogueLinks(cataloguePostDto, catagloue);

            await _myDbContext.SaveChangesAsync();
        }

        private async Task CreateCatalogueLinks(CatalogueFullPostDto cataloguePostDto, Catalogue catalogue)
        {
            foreach (var analyse in cataloguePostDto.Analyses)
            {
                await _myDbContext.AnalyseCatalogue.AddAsync(new AnalyseCatalogue
                {
                    AnalyseIdAnalyse = analyse,
                    CatalogueIdCatalogueNavigation = catalogue
                });
            }

            foreach (var automate in cataloguePostDto.Automates)
            {
                await _myDbContext.AutomateCatalogue.AddAsync(new AutomateCatalogue
                {
                    IdAutomate = automate,
                    IdCatalogueNavigation = catalogue
                });
            }
        }

        private async Task RemoveCatalogueLinks(Catalogue catagloue)
        {
            var anaCata = await _myDbContext.AnalyseCatalogue.Where(x => x.CatalogueIdCatalogue == catagloue.IdCatalogue).ToListAsync();
            _myDbContext.AnalyseCatalogue.RemoveRange(anaCata);

            var autoCata = await _myDbContext.AutomateCatalogue.Where(x => x.IdCatalogue == catagloue.IdCatalogue).ToListAsync();
            _myDbContext.AutomateCatalogue.RemoveRange(autoCata);
        }

        async Task<bool> ICatalogueRepo.DuplicateCatalogue(CatalogueDuplicatePostDto catalogueDuplicate)
        {
            using (var transact = await _myDbContext.Database.BeginTransactionAsync())
            {
                try
                {
                    var catalogue = await _myDbContext.Catalogue.FirstOrDefaultAsync(x => x.IdCatalogue == catalogueDuplicate.IdCatalogue);
                    if (catalogue == null)
                        return false;

                    var newCataloge = new Catalogue
                    {
                        NomCatalogue = catalogueDuplicate.NouveauNom,
                        Version = catalogue.Version + 1,
                        CreatorName = catalogueDuplicate.CreatorName, // fetch it from user token
                        CreationDate = DateTime.UtcNow,
                        Statut = CatalogueStatut.EnAttenteDeValidation
                    };

                    if (catalogueDuplicate.DuplicationOption == DuplicationOptionEnum.WithAnalyse)
                    {
                        await DuplicateAnalyse(newCataloge, catalogueDuplicate.IdCatalogue);
                    }
                    else if (catalogueDuplicate.DuplicationOption == DuplicationOptionEnum.WithAutomate)
                    {
                        await DuplicateAutomate(newCataloge, catalogueDuplicate.IdCatalogue);
                    }
                    else if (catalogueDuplicate.DuplicationOption == DuplicationOptionEnum.WithAnalyseAndAutomate)
                    {
                        await DuplicateAnalyse(newCataloge, catalogueDuplicate.IdCatalogue);
                        await DuplicateAutomate(newCataloge, catalogueDuplicate.IdCatalogue);
                    }
                    else
                    {
                        return false;
                    }

                    await _myDbContext.SaveChangesAsync();
                    await transact.CommitAsync();

                    return true;
                }
                catch (Exception)
                {
                    await transact.RollbackAsync();
                    throw;
                }
            }
        }

        public async Task<List<Catalogue>> GetCataloguesByStatus(string status)
        {
            var catalogues = await _myDbContext.Catalogue
                .Include(x => x.InstanceCatalogue).ThenInclude(x => x.IdInstanceNavigation)
                .Where(x => x.Statut == status)
                .ToListAsync();

            return catalogues;
        }

        public async Task<List<Catalogue>> GetUserInstancesCataloguesByStatus(int idUser, string status)
        {
            var catalogues = await _myDbContext.Catalogue
                .Include(x => x.InstanceCatalogue).ThenInclude(x => x.IdInstanceNavigation)
                .Where(x => x.InstanceCatalogue.Any(ic => ic.Statut == status && ic.IdInstanceNavigation.UserInstance.Any(ui => ui.IdUser == idUser)))
                .ToListAsync();

            return catalogues;
        }

        private async Task DuplicateAutomate(Catalogue newCataloge, int idCatalogueSource)
        {
            var automateIds = await _myDbContext.AutomateCatalogue.Where(x => x.IdCatalogue == idCatalogueSource).Select(x => x.IdAutomate).ToListAsync();
            foreach (var item in automateIds)
            {
                _myDbContext.AutomateCatalogue.Add(new AutomateCatalogue
                {
                    IdCatalogueNavigation = newCataloge,
                    IdAutomate = item
                });
            }
        }

        private async Task DuplicateAnalyse(Catalogue cataloge, int idCatalogueSource)
        {
            var analyseIds = await _myDbContext.AnalyseCatalogue.Where(x => x.CatalogueIdCatalogue == idCatalogueSource).Select(x => x.AnalyseIdAnalyse).ToListAsync();
            foreach (var item in analyseIds)
            {
                _myDbContext.AnalyseCatalogue.Add(new AnalyseCatalogue
                {
                    CatalogueIdCatalogueNavigation = cataloge,
                    AnalyseIdAnalyse = item
                });
            }
        }

        async Task<CatalogueChangeStateResponseDto> ICatalogueRepo.ChangeState(int catalogueId)
        {
            var catalogue = await _myDbContext.Catalogue.FirstOrDefaultAsync(x => x.IdCatalogue == catalogueId);
            if (catalogue == null)
            {
                return null;
            }

            if (await _myDbContext.InstanceCatalogue.AnyAsync(x => x.IdCatalogue == catalogueId))
            {
                return new CatalogueChangeStateResponseDto { Message = "Cette action est impossible, ce catalogue est lié à un site", Success = false };
            }

            catalogue.IsActivated = !catalogue.IsActivated;

            await _myDbContext.SaveChangesAsync();

            return new CatalogueChangeStateResponseDto { Message = null, Success = true };
        }

        async Task<Catalogue> ICatalogueRepo.ValidateOrReject(int catalogueId, ValidateOrRejectActionEnum action)
        {
            var catalogue = await _myDbContext.Catalogue.FirstOrDefaultAsync(x => x.IdCatalogue == catalogueId && 
                                                                            x.Statut == CatalogueStatut.EnAttenteDeValidation);
            if (catalogue == null)
            {
                return null;
            }

            if (action == ValidateOrRejectActionEnum.Validate)
            {
                var existingCatalogue = await _myDbContext.Catalogue.FirstOrDefaultAsync(x => x.NomCatalogue == catalogue.NomCatalogue && 
                x.Statut == CatalogueStatut.Valider);

                if (existingCatalogue != null)
                {
                    existingCatalogue.Statut = CatalogueStatut.Obsolete;
                }

                catalogue.Statut = CatalogueStatut.Valider;
            }
            else if (action == ValidateOrRejectActionEnum.Reject)
            {
                catalogue.Statut = CatalogueStatut.Rejeter;
            }
            else
            {
                return null;
            }

            await _myDbContext.SaveChangesAsync();
            return catalogue;
        }

        public async Task<bool> LinkCatalogueToInstances(int idCatalogue, List<int> instancesIds)
        {
            foreach (var item in instancesIds)
            {
                if (await _myDbContext.InstanceCatalogue.AnyAsync(x => x.IdCatalogue == idCatalogue && x.IdInstance == item))
                    continue;

                await _myDbContext.InstanceCatalogue.AddAsync(new InstanceCatalogue
                {
                    IdCatalogue = idCatalogue,
                    IdInstance = item,
                    Statut = CatalogueInstanceStatut.EnAttenteDeApprobation
                });
            }
            await _myDbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> ExistAll(List<int> cataloguesIds)
        {

            foreach (var item in cataloguesIds)
            {
                if (!await _myDbContext.Catalogue.AnyAsync(x => x.IdCatalogue == item))
                    return false;
            }
            return true;

        }

        public async Task<bool> LinkCataloguesToAnalyses(List<int> cataloguesIds, List<int> analysesIds)
        {
            foreach (var catalogue in cataloguesIds)
            {
                foreach (var analyse in analysesIds)
                {
                    if (await _myDbContext.AnalyseCatalogue.AnyAsync(x => x.CatalogueIdCatalogue == catalogue && x.AnalyseIdAnalyse == analyse))
                        continue;

                    await _myDbContext.AnalyseCatalogue.AddAsync(new AnalyseCatalogue
                    {
                        AnalyseIdAnalyse = analyse,
                        CatalogueIdCatalogue = catalogue
                    });
                }

            }
            await _myDbContext.SaveChangesAsync();

            return true;
        }
    }
}
