﻿using System.Collections.Generic;
using System.Linq;
using MasterDataManagement.Domain.Models;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using MasterDataManagement.Infrastructure.ManagementDbContext;

namespace MasterDataManagement.Infrastructure.Repositories
{
    public class UserInstanceRepo : IUserInstanceRepo
    {
        private readonly MyDbContext _myDbContext;

        public UserInstanceRepo(MyDbContext myDbContext)
        {
            _myDbContext = myDbContext;
        }


        async Task IUserInstanceRepo.CreateUserInstances(ushort idUtilisateur, List<byte> idInstances)
        {

            var utilisateurInstances = await _myDbContext.UserInstance.Where(x => x.IdUser == idUtilisateur).ToListAsync();

            foreach (var idInstance in idInstances)
            {
                if (utilisateurInstances.Any(x => x.IdInstance == idInstance))
                    continue;

                await _myDbContext.UserInstance.AddAsync(new UserInstance
                {
                    IdInstance = idInstance,
                    IdUser = idUtilisateur
                });
            }

            await _myDbContext.SaveChangesAsync();
        }

        async Task IUserInstanceRepo.Delete(ushort idUser, List<int> idInstances)
        {
            var toRemove = await _myDbContext.UserInstance.Where(x => x.IdUser == idUser && idInstances.Contains(x.IdInstance)).ToListAsync();
            _myDbContext.UserInstance.RemoveRange(toRemove);
            await _myDbContext.SaveChangesAsync();
        }

        async Task<List<Instance>> IUserInstanceRepo.GetUserInstances(ushort idUtilisateur)
        {
            var utilisateurInstances = await _myDbContext.UserInstance
                .Where(x => x.IdUser == idUtilisateur).Select(x => x.IdInstanceNavigation)
                .ToListAsync();

            return utilisateurInstances;
        }
    }
}
