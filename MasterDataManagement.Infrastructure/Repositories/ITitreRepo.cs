﻿using MasterDataManagement.Domain.Models;
using MasterDataManagement.Domain.Repositories;
using MasterDataManagement.DTO.Titre;
using MasterDataManagement.Infrastructure.ManagementDbContext;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MasterDataManagement.Infrastructure.Repositories
{
    public class TitreRepo : ITitreRepo
    {
        private readonly MyDbContext context;

        public TitreRepo(MyDbContext context)
        {
            this.context = context;
        }
        async Task ITitreRepo.CreateTitre(TitrePostDto titrePostDto)
        {
            await context.Titres.AddAsync(new Titres
            {
                Ordre = titrePostDto.Ordre,
                Style = titrePostDto.Style,
                Titre = titrePostDto.Titre
            });
            await context.SaveChangesAsync();
        }

        async Task ITitreRepo.DeleteTitre(Titres titre)
        {
            context.Titres.Remove(titre);
            await context.SaveChangesAsync();
        }

        async Task<Titres> ITitreRepo.GetTitreById(int idTitres)
        {
            var titre = await context.Titres.FirstOrDefaultAsync(x => x.IdTitres == idTitres);
            return titre;
        }

        async Task<List<Titres>> ITitreRepo.GetTitres()
        {
            var titres = await context.Titres.ToListAsync();
            return titres;
        }

        async Task<Titres> ITitreRepo.UpdateTitre(Titres titre, TitrePutDto titrePutDto)
        {
            titre.Ordre = titrePutDto.Ordre;
            titre.Style = titrePutDto.Style;
            titre.Titre = titrePutDto.Titre;

            await context.SaveChangesAsync();

            return titre;
        }
    }
}
