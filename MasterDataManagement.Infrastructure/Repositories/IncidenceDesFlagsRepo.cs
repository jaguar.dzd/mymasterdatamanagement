﻿using MasterDataManagement.Domain.Models;
using MasterDataManagement.Domain.Repositories;
using MasterDataManagement.DTO.IncidencesDesFlags;
using MasterDataManagement.Infrastructure.ManagementDbContext;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MasterDataManagement.Infrastructure.Repositories
{
    public class IncidenceDesFlagsRepo : IIncidenceDesFlagsRepo
    {
        private readonly MyDbContext _myDbContext;

        public IncidenceDesFlagsRepo(MyDbContext myDbContext)
        {
            this._myDbContext = myDbContext;
        }
        async Task IIncidenceDesFlagsRepo.CreateIncidenceDesFlags(IncidencedesflagsPostDto data)
        {
            var incidenceDesFlag = ((IIncidenceDesFlagsRepo)this).MakeIncidencedesflags(data);
            await _myDbContext.Incidencedesflags.AddAsync(incidenceDesFlag);

            await _myDbContext.SaveChangesAsync();
        }
        Incidencedesflags IIncidenceDesFlagsRepo.MakeIncidencedesflags(IncidencedesflagsPostDto data)
        {
            return new Incidencedesflags
            {
                ValeurDuFlag = data.ValeurDuFlag,
                ValeurDuResultat = data.ValeurDuResultat
            };
        }

        async Task IIncidenceDesFlagsRepo.DeleteIncidenceDesFlags(Incidencedesflags incidencedesflags)
        {
            _myDbContext.Incidencedesflags.Remove(incidencedesflags);
            await _myDbContext.SaveChangesAsync();
        }

        async Task<bool> IIncidenceDesFlagsRepo.ExistAll(List<int> incidenceDesFlagsIds)
        {
            foreach (var item in incidenceDesFlagsIds)
            {
                if (!await _myDbContext.Incidencedesflags.AnyAsync(x => x.Idincidencedesflags == item))
                    return false;
            }
            return true;
        }

        async Task<List<Incidencedesflags>> IIncidenceDesFlagsRepo.GetIncidenceDesFlags()
        {
            return await _myDbContext.Incidencedesflags.ToListAsync();
        }

        async Task<Incidencedesflags> IIncidenceDesFlagsRepo.GetIncidenceDesFlagsById(int idIncidenceDesFlags)
        {
            return await _myDbContext.Incidencedesflags.FirstOrDefaultAsync(x => x.Idincidencedesflags == idIncidenceDesFlags);
        }

        async Task<Incidencedesflags> IIncidenceDesFlagsRepo.UpdateIncidenceDesFlags(Incidencedesflags incidencedesflags, IncidencedesflagsPutDto incidencedesflagsPutDto)
        {
            incidencedesflags.ValeurDuFlag = incidencedesflagsPutDto.ValeurDuFlag;

            incidencedesflags.ValeurDuResultat = incidencedesflagsPutDto.ValeurDuResultat;
            
            await _myDbContext.SaveChangesAsync();
            return incidencedesflags;
        }
    }
}
