﻿using System.Collections.Generic;
using MasterDataManagement.Domain.Models;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Repositories;
using MasterDataManagement.DTO.Secteur;
using Microsoft.EntityFrameworkCore;
using MasterDataManagement.Infrastructure.ManagementDbContext;

namespace MasterDataManagement.Infrastructure.Repositories
{
    public class SecteurRepo : ISecteurRepo
    {
        private readonly MyDbContext _myDbContext;

        public SecteurRepo(MyDbContext myDbContext)
        {
            _myDbContext = myDbContext;
        }
        async Task ISecteurRepo.CreateSecteur(SecteurPostDto secteurPostDto)
        {
            await _myDbContext.Secteur.AddAsync(new Secteur
            {
               SecteurNom = secteurPostDto.SecteurNom

            });
            await _myDbContext.SaveChangesAsync();
        }

        async Task<Secteur> ISecteurRepo.GetSecteurById(int idSecteur)
        {
            var secteur = await _myDbContext.Secteur.FirstOrDefaultAsync(x => x.IdSecteur == idSecteur);
            return secteur;
        }

        async Task<Secteur> ISecteurRepo.UpdateSecteur(Secteur secteur, SecteurPostDto secteurPostDto)
        {

            secteur.SecteurNom = secteurPostDto.SecteurNom;
            
            await _myDbContext.SaveChangesAsync();

            return secteur;
        }


        async Task<List<Secteur>> ISecteurRepo.GetSecteurs()
        {
            var secteurs = await _myDbContext.Secteur.ToListAsync();

            return secteurs;
        }
        async Task ISecteurRepo.DeleteSecteur(Secteur secteur)
        {
            _myDbContext.Secteur.Remove(secteur);
            await _myDbContext.SaveChangesAsync();
        }
    }
}
