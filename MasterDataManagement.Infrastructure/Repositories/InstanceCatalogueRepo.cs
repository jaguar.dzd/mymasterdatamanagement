﻿using MasterDataManagement.Domain.Others;
using MasterDataManagement.Domain.Repositories;
using MasterDataManagement.DTO.Enum;
using MasterDataManagement.DTO.InstanceCatalogue;
using MasterDataManagement.Infrastructure.ManagementDbContext;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MasterDataManagement.Infrastructure.Repositories
{
    public class InstanceCatalogueRepo : IInstanceCatalogueRepo
    {
        private readonly MyDbContext myDbContext;

        public InstanceCatalogueRepo(MyDbContext myDbContext)
        {
            this.myDbContext = myDbContext;
        }

        async Task<bool> IInstanceCatalogueRepo.ApproveOrRefuseCatalogueApplication(List<InstanceCatalogueDetailPostDto> data)
        {
            foreach (var item in data)
            {
                var obj = await myDbContext.InstanceCatalogue.FirstOrDefaultAsync(x => x.IdInstance == item.InstanceId && 
                                                                                        x.IdCatalogue == item.CatalogueId);
                if (obj == null)
                    return false;

                if (item.Action == ApproveOrRefuseCatalogueInstanceEnum.Approve)
                {
                    obj.Statut = CatalogueInstanceStatut.ApplicationCatagloueApprouver;
                }
                else if (item.Action == ApproveOrRefuseCatalogueInstanceEnum.Refuse)
                {
                    obj.Statut = CatalogueInstanceStatut.ApplicationCatagloueRefuser;
                }
            }

            await myDbContext.SaveChangesAsync();

            return true;
        }
    }
}
