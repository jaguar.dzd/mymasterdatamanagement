﻿using System.Collections.Generic;
using MasterDataManagement.Domain.Models;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Repositories;
using MasterDataManagement.DTO.Instance;
using Microsoft.EntityFrameworkCore;
using MasterDataManagement.Infrastructure.ManagementDbContext;
using System.Linq;
using MasterDataManagement.Domain.Others;

namespace MasterDataManagement.Infrastructure.Repositories
{
    public class InstanceRepo : IInstanceRepo
    {
        private readonly MyDbContext _myDbContext;

        public InstanceRepo(MyDbContext myDbContext)
        {
            _myDbContext = myDbContext;
        }
        async Task IInstanceRepo.CreateInstance(InstancePostDto instancePostDto)
        {
            await _myDbContext.Instance.AddAsync(new Instance
            {
                AdresseSite = instancePostDto.AdresseSite,
                Code = instancePostDto.Code,
                EmailContactSite = instancePostDto.EmailContactSite,
                IpInstance = instancePostDto.IpInstance,
                Nom = instancePostDto.Nom,
                NomContactSite = instancePostDto.NomContactSite,
                NomMasterInstance = instancePostDto.NomMasterInstance,
                TelContactSite = instancePostDto.TelContactSite,
                TelSite = instancePostDto.TelSite
            });
            await _myDbContext.SaveChangesAsync();
        }

        async Task<Instance> IInstanceRepo.GetInstanceById(byte idInstance)
        {
            var instance = await _myDbContext.Instance.FirstOrDefaultAsync(x => x.IdInstance == idInstance);
            return instance;
        }

        async Task<Instance> IInstanceRepo.UpdateInstance(Instance instance, InstancePostDto instancePostDto)
        {
            instance.IpInstance = instancePostDto.IpInstance;
            instance.AdresseSite = instancePostDto.AdresseSite;
            instance.Code = instancePostDto.Code;
            instance.EmailContactSite = instancePostDto.EmailContactSite;
            instance.Nom = instancePostDto.Nom;
            instance.NomContactSite = instancePostDto.NomContactSite;
            instance.NomMasterInstance = instancePostDto.NomMasterInstance;
            instance.TelContactSite = instancePostDto.TelContactSite;
            instance.TelSite = instancePostDto.TelSite;

            await _myDbContext.SaveChangesAsync();

            return instance;
        }


        async Task<List<Instance>> IInstanceRepo.GetInstances()
        {
            var instances = await _myDbContext.Instance.ToListAsync();

            return instances;
        }

        async Task IInstanceRepo.DeleteInstance(Instance instance)
        {
            _myDbContext.Instance.Remove(instance);
            await _myDbContext.SaveChangesAsync();
        }

        async Task<bool> IInstanceRepo.ExistAll(List<int> idInstances)
        {
            foreach (var item in idInstances)
            {
                if (!await _myDbContext.Instance.AnyAsync(x => item == x.IdInstance))
                    return false;
            }
            return true;
        }

        async Task IInstanceRepo.LinkInstanceToCatalogues(int idCatalogue, List<int> instancesIds)
        {
            foreach (var item in instancesIds)
            {
                if (await _myDbContext.InstanceCatalogue.AnyAsync(x => x.IdInstance == item && idCatalogue == x.IdCatalogue))
                    continue;
                await _myDbContext.InstanceCatalogue.AddAsync(new InstanceCatalogue
                {
                    IdCatalogue = idCatalogue,
                    IdInstance = item,
                    Statut = CatalogueInstanceStatut.EnAttenteDeApprobation
                });
            }
            await _myDbContext.SaveChangesAsync();
        }

        async Task IInstanceRepo.RemoveInstanceFromCatalogues(int idCatalogue, List<int> instancesIds)
        {
            var data = await _myDbContext.InstanceCatalogue.Where(x => x.IdCatalogue == idCatalogue && instancesIds.Contains(x.IdInstance)).ToListAsync();
            _myDbContext.InstanceCatalogue.RemoveRange(data);
            await _myDbContext.SaveChangesAsync();
        }

        public Task<List<Instance>> GetCatalogueInstances(int idCatalogue)
        {
            var instances = _myDbContext.InstanceCatalogue.Where(x => x.IdCatalogue == idCatalogue)
                .Select(x => x.IdInstanceNavigation).ToListAsync();
            return instances;
        }
    }
}
