﻿using System;
using MasterDataManagement.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace MasterDataManagement.Infrastructure.ManagementDbContext
{
    public partial class MyDbContext : DbContext
    {
        public MyDbContext()
        {
        }

        public MyDbContext(DbContextOptions<MyDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Analyse> Analyse { get; set; }
        public virtual DbSet<AnalyseAutomate> AnalyseAutomate { get; set; }
        public virtual DbSet<AnalyseAutomateCatalog> AnalyseAutomateCatalog { get; set; }
        public virtual DbSet<AnalyseAutomateValidations> AnalyseAutomateValidations { get; set; }
        public virtual DbSet<AnalyseBilan> AnalyseBilan { get; set; }
        public virtual DbSet<AnalyseCatalogue> AnalyseCatalogue { get; set; }
        public virtual DbSet<AnalyseCommauto> AnalyseCommauto { get; set; }
        public virtual DbSet<AnalyseEvaluations> AnalyseEvaluations { get; set; }
        public virtual DbSet<AnalyseFonctionsreflexes> AnalyseFonctionsreflexes { get; set; }
        public virtual DbSet<AnalyseIncidencedesflags> AnalyseIncidencedesflags { get; set; }
        public virtual DbSet<AnalyseTransfbasic> AnalyseTransfbasic { get; set; }
        public virtual DbSet<AnalyseTransformations> AnalyseTransformations { get; set; }
        public virtual DbSet<AnalyseValidations> AnalyseValidations { get; set; }
        public virtual DbSet<AnalyseautomateFonctionsreflexes> AnalyseautomateFonctionsreflexes { get; set; }
        public virtual DbSet<AnalyseautomateTransformations> AnalyseautomateTransformations { get; set; }
        public virtual DbSet<Automate> Automate { get; set; }
        public virtual DbSet<AutomateCatalogue> AutomateCatalogue { get; set; }
        public virtual DbSet<Bilan> Bilan { get; set; }
        public virtual DbSet<BilanAnalyseautomate> BilanAnalyseautomate { get; set; }
        public virtual DbSet<Catalogue> Catalogue { get; set; }
        public virtual DbSet<Commauto> Commauto { get; set; }
        public virtual DbSet<CommautoAnalyseautomate> CommautoAnalyseautomate { get; set; }
        public virtual DbSet<Droit> Droit { get; set; }
        public virtual DbSet<DroitUser> DroitUser { get; set; }
        public virtual DbSet<Evaluations> Evaluations { get; set; }
        public virtual DbSet<EvaluationsAnalyseautomate> EvaluationsAnalyseautomate { get; set; }
        public virtual DbSet<Fonctionsreflexes> Fonctionsreflexes { get; set; }
        public virtual DbSet<Groupeanalyse> Groupeanalyse { get; set; }
        public virtual DbSet<Incidencedesflags> Incidencedesflags { get; set; }
        public virtual DbSet<IncidencedesflagsAnalyseautomate> IncidencedesflagsAnalyseautomate { get; set; }
        public virtual DbSet<Instance> Instance { get; set; }
        public virtual DbSet<InstanceCatalogue> InstanceCatalogue { get; set; }
        public virtual DbSet<Paillasse> Paillasse { get; set; }
        public virtual DbSet<Secteur> Secteur { get; set; }
        public virtual DbSet<Soustitres> Soustitres { get; set; }
        public virtual DbSet<Titres> Titres { get; set; }
        public virtual DbSet<Transfbasic> Transfbasic { get; set; }
        public virtual DbSet<TransfbasicAnalyseautomate> TransfbasicAnalyseautomate { get; set; }
        public virtual DbSet<Transformations> Transformations { get; set; }
        public virtual DbSet<Typedetube> Typedetube { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserInstance> UserInstance { get; set; }
        public virtual DbSet<Validations> Validations { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseMySql("server=localhost;port=3308;database=masterdatamanagement;user=root;treattinyasboolean=true", x => x.ServerVersion("8.0.18-mysql"));
//            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Analyse>(entity =>
            {
                entity.HasKey(e => e.IdAnalyse)
                    .HasName("PRIMARY");

                entity.ToTable("analyse");

                entity.HasIndex(e => e.GroupeAnalyseIdGroupeAnalyse)
                    .HasName("fk_analyse_GroupeAnalyse1_idx");

                entity.HasIndex(e => e.PaillasseIdPaillasse)
                    .HasName("fk_dico_analyse_Paillasse1_idx");

                entity.HasIndex(e => e.SousTitresIdSousTitres)
                    .HasName("fk_dico_analyse_SousTitres1_idx");

                entity.HasIndex(e => e.TitresIdTitres)
                    .HasName("fk_dico_analyse_Titres1_idx");

                entity.HasIndex(e => e.TypeDeTubeIdTypeDeTube)
                    .HasName("fk_dico_analyse_TypeDeTube1_idx");

                entity.Property(e => e.IdAnalyse)
                    .HasColumnName("id_analyse")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Arrondi).HasColumnType("tinyint(4)");

                entity.Property(e => e.AutreCode)
                    .HasColumnName("Autre_Code")
                    .HasColumnType("varchar(120)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Code)
                    .HasColumnType("varchar(120)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CodeAller1)
                    .HasColumnType("varchar(4)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CodeAller2)
                    .HasColumnType("varchar(4)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CodeAller3)
                    .HasColumnType("varchar(4)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CodeAller4)
                    .HasColumnType("varchar(4)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CodeAller5)
                    .HasColumnType("varchar(4)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CodeAller6)
                    .HasColumnType("varchar(4)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CodeAller7)
                    .HasColumnType("varchar(4)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CodeAller8)
                    .HasColumnType("varchar(4)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CodeAller9)
                    .HasColumnType("varchar(4)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CodeRetour1)
                    .HasColumnType("varchar(4)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CodeRetour2)
                    .HasColumnName("CodeREtour2")
                    .HasColumnType("varchar(4)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CodeTransmExComplem)
                    .HasColumnType("varchar(120)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CodeTransmission1)
                    .HasColumnName("Code_Transmission 1")
                    .HasColumnType("varchar(120)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CodeTransmission2)
                    .HasColumnName("Code_Transmission 2")
                    .HasColumnType("varchar(120)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Coefficient).HasColumnType("smallint(6)");

                entity.Property(e => e.Decimales1).HasColumnType("smallint(6)");

                entity.Property(e => e.Decimales2).HasColumnType("smallint(6)");

                entity.Property(e => e.Deltacheck).HasColumnType("smallint(6)");

                entity.Property(e => e.DureeValidite).HasColumnType("smallint(6)");

                entity.Property(e => e.EtOuEgalA)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.EtOuSupA)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.EtatSortieConnexion).HasColumnType("tinyint(4)");

                entity.Property(e => e.FacteurDilution).HasColumnType("smallint(6)");

                entity.Property(e => e.Formule)
                    .HasColumnType("varchar(100)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.FormuleX1)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.FormuleX2)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.GroupeAnalyseIdGroupeAnalyse)
                    .HasColumnName("GroupeAnalyse_idGroupeAnalyse")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdgroupeAnalyseValidation)
                    .HasColumnName("IDGroupeAnalyseValidation")
                    .HasColumnType("varchar(120)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Impression).HasColumnType("int(11)");

                entity.Property(e => e.IsResultatArenvoyer).HasColumnName("IsResultatARenvoyer");

                entity.Property(e => e.IsValidationConditionCq).HasColumnName("IsValidationConditionCQ");

                entity.Property(e => e.IsanalyseInscriteCq).HasColumnName("ISAnalyseInscriteCQ");

                entity.Property(e => e.Ligne1ApresResult)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Ligne1AvantResult)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Ligne2ApresResult)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Ligne2AvantResult)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Ligne3ApresResult)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Ligne3AvantResult)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Ligne4ApresResult)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Ligne4AvantResult)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Ligne5ApresResult)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Ligne5AvantResult)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Ligne6ApresResult)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Longueur).HasColumnType("smallint(6)");

                entity.Property(e => e.Minvaleur).HasColumnType("smallint(6)");

                entity.Property(e => e.MoyenneMobileTotalpoints).HasColumnType("smallint(6)");

                entity.Property(e => e.MoyenneMobileTriParGroupe).HasColumnType("smallint(6)");

                entity.Property(e => e.NbrAnalyseMinCalcul).HasColumnType("smallint(6)");

                entity.Property(e => e.Paillasse2)
                    .HasColumnType("varchar(120)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.PaillasseIdPaillasse)
                    .HasColumnName("Paillasse_idPaillasse")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Rang).HasColumnType("smallint(6)");

                entity.Property(e => e.ReglesDeWestgard).HasColumnType("smallint(6)");

                entity.Property(e => e.RepasserAvecLaDilution)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.SiInfA)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.SousTitre)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.SousTitresIdSousTitres)
                    .HasColumnName("SousTitres_idSousTitres")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Texte)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TexteOptionnel1)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TexteOptionnel10)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TexteOptionnel11)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TexteOptionnel12)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TexteOptionnel13)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TexteOptionnel14)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TexteOptionnel15)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TexteOptionnel2)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TexteOptionnel3)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TexteOptionnel4)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TexteOptionnel5)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TexteOptionnel6)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TexteOptionnel7)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TexteOptionnel8)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TexteOptionnel9)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Titre)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TitresIdTitres)
                    .HasColumnName("Titres_idTitres")
                    .HasColumnType("int(11)");

                entity.Property(e => e.TypeDeTubeIdTypeDeTube)
                    .HasColumnName("TypeDeTube_idTypeDeTube")
                    .HasColumnType("int(11)");

                entity.Property(e => e.TypeResult)
                    .HasColumnType("varchar(120)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Unites1)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Unites2)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.ValeurAajouter)
                    .HasColumnName("ValeurAAjouter")
                    .HasColumnType("smallint(6)");

                entity.Property(e => e.ValeurParDefaut)
                    .HasColumnName("valeurParDefaut")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.WestgardPersoT).HasColumnType("tinyint(4)");

                entity.Property(e => e.WestgardPersoX).HasColumnType("tinyint(4)");

                entity.HasOne(d => d.GroupeAnalyseIdGroupeAnalyseNavigation)
                    .WithMany(p => p.Analyse)
                    .HasForeignKey(d => d.GroupeAnalyseIdGroupeAnalyse)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("fk_analyse_GroupeAnalyse1");

                entity.HasOne(d => d.PaillasseIdPaillasseNavigation)
                    .WithMany(p => p.Analyse)
                    .HasForeignKey(d => d.PaillasseIdPaillasse)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("fk_dico_analyse_Paillasse1");

                entity.HasOne(d => d.SousTitresIdSousTitresNavigation)
                    .WithMany(p => p.Analyse)
                    .HasForeignKey(d => d.SousTitresIdSousTitres)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("fk_dico_analyse_SousTitres1");

                entity.HasOne(d => d.TitresIdTitresNavigation)
                    .WithMany(p => p.Analyse)
                    .HasForeignKey(d => d.TitresIdTitres)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("fk_dico_analyse_Titres1");

                entity.HasOne(d => d.TypeDeTubeIdTypeDeTubeNavigation)
                    .WithMany(p => p.Analyse)
                    .HasForeignKey(d => d.TypeDeTubeIdTypeDeTube)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("fk_dico_analyse_TypeDeTube1");
            });

            modelBuilder.Entity<AnalyseAutomate>(entity =>
            {
                entity.HasKey(e => e.IdAnalyseAutomate)
                    .HasName("PRIMARY");

                entity.ToTable("analyse_automate");

                entity.HasIndex(e => e.IdAnalyse)
                    .HasName("fk_Analyse_automate_analyse1_idx");

                entity.HasIndex(e => e.IdAutomate)
                    .HasName("fk_Analyse_automate_Automate1_idx");

                entity.HasIndex(e => e.IdPaillasse)
                    .HasName("fk_Analyse_automate_Paillasse1_idx");

                entity.HasIndex(e => e.IdTypeDeTube)
                    .HasName("fk_Analyse_automate_TypeDeTube1_idx");

                entity.Property(e => e.IdAnalyseAutomate)
                    .HasColumnName("idAnalyse_automate")
                    .HasColumnType("int(11)");

                entity.Property(e => e.AnalyseAutomatecol)
                    .HasColumnName("Analyse_automatecol")
                    .HasColumnType("varchar(4)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Arrondi).HasColumnType("smallint(6)");

                entity.Property(e => e.CodeAller1)
                    .HasColumnType("varchar(4)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CodeAller2)
                    .HasColumnType("varchar(4)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CodeAller3)
                    .HasColumnType("varchar(4)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CodeAller4)
                    .HasColumnType("varchar(4)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CodeAller5)
                    .HasColumnType("varchar(4)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CodeAller6)
                    .HasColumnType("varchar(4)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CodeAller7)
                    .HasColumnType("varchar(4)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CodeAller8)
                    .HasColumnType("varchar(4)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CodeAller9)
                    .HasColumnType("varchar(4)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CodeRetour1)
                    .HasColumnType("varchar(4)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CodeRetour2)
                    .HasColumnName("CodeREtour2")
                    .HasColumnType("varchar(4)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Coefficient).HasColumnType("smallint(6)");

                entity.Property(e => e.Decimales1).HasColumnType("smallint(6)");

                entity.Property(e => e.Decimales2).HasColumnType("smallint(6)");

                entity.Property(e => e.EtOuEgalA)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.EtOuSupA)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.FacteurDilution).HasColumnType("smallint(6)");

                entity.Property(e => e.Formule)
                    .HasColumnType("varchar(100)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.FormuleX1)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.FormuleX2)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.IdAnalyse).HasColumnType("int(11)");

                entity.Property(e => e.IdAutomate).HasColumnType("int(11)");

                entity.Property(e => e.IdPaillasse)
                    .HasColumnName("idPaillasse")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdTypeDeTube)
                    .HasColumnName("idTypeDeTube")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Ligne1ApresResult)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Ligne1AvantResult)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Ligne2ApresResult)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Ligne2AvantResult)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Ligne3ApresResult)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Ligne3AvantResult)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Ligne4ApresResult)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Ligne4AvantResult)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Ligne5ApresResult)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Ligne5AvantResult)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Ligne6ApresResult)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Longueur).HasColumnType("smallint(6)");

                entity.Property(e => e.Paillasse2)
                    .HasColumnType("varchar(120)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.RepasserAvecLaDilution)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.SiInfA)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.SousTitre)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TexteOptionnel1)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TexteOptionnel10)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TexteOptionnel11)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TexteOptionnel12)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TexteOptionnel13)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TexteOptionnel14)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TexteOptionnel15)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TexteOptionnel2)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TexteOptionnel3)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TexteOptionnel4)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TexteOptionnel5)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TexteOptionnel6)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TexteOptionnel7)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TexteOptionnel8)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TexteOptionnel9)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Titre)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Unites1)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Unites2)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.ValeurAajouter)
                    .HasColumnName("ValeurAAjouter")
                    .HasColumnType("smallint(6)");

                entity.HasOne(d => d.IdAnalyseNavigation)
                    .WithMany(p => p.AnalyseAutomate)
                    .HasForeignKey(d => d.IdAnalyse)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Analyse_automate_analyse1");

                entity.HasOne(d => d.IdAutomateNavigation)
                    .WithMany(p => p.AnalyseAutomate)
                    .HasForeignKey(d => d.IdAutomate)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Analyse_automate_Automate1");

                entity.HasOne(d => d.IdPaillasseNavigation)
                    .WithMany(p => p.AnalyseAutomate)
                    .HasForeignKey(d => d.IdPaillasse)
                    .HasConstraintName("fk_Analyse_automate_Paillasse1");

                entity.HasOne(d => d.IdTypeDeTubeNavigation)
                    .WithMany(p => p.AnalyseAutomate)
                    .HasForeignKey(d => d.IdTypeDeTube)
                    .HasConstraintName("fk_Analyse_automate_TypeDeTube1");
            });

            modelBuilder.Entity<AnalyseAutomateCatalog>(entity =>
            {
                entity.HasKey(e => e.IdAnalyseAutomateCatalogue)
                    .HasName("PRIMARY");

                entity.ToTable("analyse_automate_catalog");

                entity.HasIndex(e => e.IdAnalyseAutomate)
                    .HasName("fk_Analyse_automate_has_Catalogue_Analyse_automate1_idx");

                entity.HasIndex(e => e.IdCatalogue)
                    .HasName("fk_Analyse_automate_has_Catalogue_Catalogue1_idx");

                entity.Property(e => e.IdAnalyseAutomateCatalogue)
                    .HasColumnName("Id_AnalyseAutomate_Catalogue")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdAnalyseAutomate)
                    .HasColumnName("IdAnalyse_automate")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdCatalogue).HasColumnType("int(11)");

                entity.HasOne(d => d.IdAnalyseAutomateNavigation)
                    .WithMany(p => p.AnalyseAutomateCatalog)
                    .HasForeignKey(d => d.IdAnalyseAutomate)
                    .HasConstraintName("fk_Analyse_automate_has_Catalogue_Analyse_automate1");

                entity.HasOne(d => d.IdCatalogueNavigation)
                    .WithMany(p => p.AnalyseAutomateCatalog)
                    .HasForeignKey(d => d.IdCatalogue)
                    .HasConstraintName("fk_Analyse_automate_has_Catalogue_Catalogue1");
            });

            modelBuilder.Entity<AnalyseAutomateValidations>(entity =>
            {
                entity.HasKey(e => new { e.IdAnalyseAutomate, e.IdValidations })
                    .HasName("PRIMARY");

                entity.ToTable("analyse_automate_validations");

                entity.HasIndex(e => e.IdAnalyseAutomate)
                    .HasName("fk_Analyse_automate_has_Validations_Analyse_automate1_idx");

                entity.HasIndex(e => e.IdValidations)
                    .HasName("fk_Analyse_automate_has_Validations_Validations1_idx");

                entity.Property(e => e.IdAnalyseAutomate)
                    .HasColumnName("idAnalyseAutomate")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdValidations).HasColumnType("int(11)");

                entity.HasOne(d => d.IdAnalyseAutomateNavigation)
                    .WithMany(p => p.AnalyseAutomateValidations)
                    .HasForeignKey(d => d.IdAnalyseAutomate)
                    .HasConstraintName("fk_Analyse_automate_has_Validations_Analyse_automate1");

                entity.HasOne(d => d.IdValidationsNavigation)
                    .WithMany(p => p.AnalyseAutomateValidations)
                    .HasForeignKey(d => d.IdValidations)
                    .HasConstraintName("fk_Analyse_automate_has_Validations_Validations1");
            });

            modelBuilder.Entity<AnalyseBilan>(entity =>
            {
                entity.HasKey(e => new { e.IdAnalyse, e.IdBilan })
                    .HasName("PRIMARY");

                entity.ToTable("analyse_bilan");

                entity.HasIndex(e => e.IdAnalyse)
                    .HasName("fk_dico_analyse_has_Bilan_dico_analyse1_idx");

                entity.HasIndex(e => e.IdBilan)
                    .HasName("fk_dico_analyse_has_Bilan_Bilan1_idx");

                entity.Property(e => e.IdAnalyse)
                    .HasColumnName("id_analyse")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdBilan)
                    .HasColumnName("idBilan")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.IdAnalyseNavigation)
                    .WithMany(p => p.AnalyseBilan)
                    .HasForeignKey(d => d.IdAnalyse)
                    .HasConstraintName("fk_dico_analyse_has_Bilan_dico_analyse1");

                entity.HasOne(d => d.IdBilanNavigation)
                    .WithMany(p => p.AnalyseBilan)
                    .HasForeignKey(d => d.IdBilan)
                    .HasConstraintName("fk_dico_analyse_has_Bilan_Bilan1");
            });

            modelBuilder.Entity<AnalyseCatalogue>(entity =>
            {
                entity.HasKey(e => new { e.AnalyseIdAnalyse, e.CatalogueIdCatalogue })
                    .HasName("PRIMARY");

                entity.ToTable("analyse_catalogue");

                entity.HasIndex(e => e.AnalyseIdAnalyse)
                    .HasName("fk_analyse_has_Catalogue_analyse1_idx");

                entity.HasIndex(e => e.CatalogueIdCatalogue)
                    .HasName("fk_analyse_has_Catalogue_Catalogue1_idx");

                entity.Property(e => e.AnalyseIdAnalyse)
                    .HasColumnName("analyse_id_analyse")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CatalogueIdCatalogue)
                    .HasColumnName("Catalogue_idCatalogue")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.AnalyseIdAnalyseNavigation)
                    .WithMany(p => p.AnalyseCatalogue)
                    .HasForeignKey(d => d.AnalyseIdAnalyse)
                    .HasConstraintName("fk_analyse_has_Catalogue_analyse1");

                entity.HasOne(d => d.CatalogueIdCatalogueNavigation)
                    .WithMany(p => p.AnalyseCatalogue)
                    .HasForeignKey(d => d.CatalogueIdCatalogue)
                    .HasConstraintName("fk_analyse_has_Catalogue_Catalogue1");
            });

            modelBuilder.Entity<AnalyseCommauto>(entity =>
            {
                entity.HasKey(e => new { e.IdAnalyse, e.IdCommAuto })
                    .HasName("PRIMARY");

                entity.ToTable("analyse_commauto");

                entity.HasIndex(e => e.IdAnalyse)
                    .HasName("fk_analyse_has_CommAuto_analyse1_idx");

                entity.HasIndex(e => e.IdCommAuto)
                    .HasName("fk_analyse_has_CommAuto_CommAuto1_idx");

                entity.Property(e => e.IdAnalyse)
                    .HasColumnName("Id_analyse")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdCommAuto).HasColumnType("int(11)");

                entity.HasOne(d => d.IdAnalyseNavigation)
                    .WithMany(p => p.AnalyseCommauto)
                    .HasForeignKey(d => d.IdAnalyse)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_analyse_has_CommAuto_analyse1");

                entity.HasOne(d => d.IdCommAutoNavigation)
                    .WithMany(p => p.AnalyseCommauto)
                    .HasForeignKey(d => d.IdCommAuto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_analyse_has_CommAuto_CommAuto1");
            });

            modelBuilder.Entity<AnalyseEvaluations>(entity =>
            {
                entity.HasKey(e => new { e.IdAnalyse, e.IdEvaluations })
                    .HasName("PRIMARY");

                entity.ToTable("analyse_evaluations");

                entity.HasIndex(e => e.IdAnalyse)
                    .HasName("fk_dico_analyse_has_Evaluations_dico_analyse1_idx");

                entity.HasIndex(e => e.IdEvaluations)
                    .HasName("fk_dico_analyse_has_Evaluations_Evaluations1_idx");

                entity.Property(e => e.IdAnalyse)
                    .HasColumnName("id_analyse")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdEvaluations)
                    .HasColumnName("idEvaluations")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.IdAnalyseNavigation)
                    .WithMany(p => p.AnalyseEvaluations)
                    .HasForeignKey(d => d.IdAnalyse)
                    .HasConstraintName("fk_dico_analyse_has_Evaluations_dico_analyse1");

                entity.HasOne(d => d.IdEvaluationsNavigation)
                    .WithMany(p => p.AnalyseEvaluations)
                    .HasForeignKey(d => d.IdEvaluations)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_dico_analyse_has_Evaluations_Evaluations1");
            });

            modelBuilder.Entity<AnalyseFonctionsreflexes>(entity =>
            {
                entity.HasKey(e => new { e.IdAnalyse, e.IdFonctionsReflexes })
                    .HasName("PRIMARY");

                entity.ToTable("analyse_fonctionsreflexes");

                entity.HasIndex(e => e.IdAnalyse)
                    .HasName("fk_dico_analyse_has_FonctionsReflexes_dico_analyse1_idx");

                entity.HasIndex(e => e.IdFonctionsReflexes)
                    .HasName("fk_dico_analyse_has_FonctionsReflexes_FonctionsReflexes1_idx");

                entity.Property(e => e.IdAnalyse)
                    .HasColumnName("id_analyse")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdFonctionsReflexes)
                    .HasColumnName("idFonctionsReflexes")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.IdAnalyseNavigation)
                    .WithMany(p => p.AnalyseFonctionsreflexes)
                    .HasForeignKey(d => d.IdAnalyse)
                    .HasConstraintName("fk_dico_analyse_has_FonctionsReflexes_dico_analyse1");

                entity.HasOne(d => d.IdFonctionsReflexesNavigation)
                    .WithMany(p => p.AnalyseFonctionsreflexes)
                    .HasForeignKey(d => d.IdFonctionsReflexes)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_dico_analyse_has_FonctionsReflexes_FonctionsReflexes1");
            });

            modelBuilder.Entity<AnalyseIncidencedesflags>(entity =>
            {
                entity.HasKey(e => new { e.IdAnalyse, e.Idincidencedesflags })
                    .HasName("PRIMARY");

                entity.ToTable("analyse_incidencedesflags");

                entity.HasIndex(e => e.IdAnalyse)
                    .HasName("fk_analyse_has_incidencedesflags_analyse1_idx");

                entity.HasIndex(e => e.Idincidencedesflags)
                    .HasName("fk_analyse_has_incidencedesflags_incidencedesflags1_idx");

                entity.Property(e => e.IdAnalyse)
                    .HasColumnName("Id_analyse")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Idincidencedesflags).HasColumnType("int(11)");

                entity.HasOne(d => d.IdAnalyseNavigation)
                    .WithMany(p => p.AnalyseIncidencedesflags)
                    .HasForeignKey(d => d.IdAnalyse)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_analyse_has_incidencedesflags_analyse1");

                entity.HasOne(d => d.IdincidencedesflagsNavigation)
                    .WithMany(p => p.AnalyseIncidencedesflags)
                    .HasForeignKey(d => d.Idincidencedesflags)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_analyse_has_incidencedesflags_incidencedesflags1");
            });

            modelBuilder.Entity<AnalyseTransfbasic>(entity =>
            {
                entity.HasKey(e => new { e.IdAnalyse, e.IdTransfbasic })
                    .HasName("PRIMARY");

                entity.ToTable("analyse_transfbasic");

                entity.HasIndex(e => e.IdAnalyse)
                    .HasName("fk_analyse_has_Transfbasic_analyse1_idx");

                entity.HasIndex(e => e.IdTransfbasic)
                    .HasName("fk_analyse_has_Transfbasic_Transfbasic1_idx");

                entity.Property(e => e.IdAnalyse)
                    .HasColumnName("Id_analyse")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdTransfbasic).HasColumnType("int(11)");

                entity.HasOne(d => d.IdAnalyseNavigation)
                    .WithMany(p => p.AnalyseTransfbasic)
                    .HasForeignKey(d => d.IdAnalyse)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_analyse_has_Transfbasic_analyse1");

                entity.HasOne(d => d.IdTransfbasicNavigation)
                    .WithMany(p => p.AnalyseTransfbasic)
                    .HasForeignKey(d => d.IdTransfbasic)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_analyse_has_Transfbasic_Transfbasic1");
            });

            modelBuilder.Entity<AnalyseTransformations>(entity =>
            {
                entity.HasKey(e => new { e.IdAnalyse, e.IdTransformations })
                    .HasName("PRIMARY");

                entity.ToTable("analyse_transformations");

                entity.HasIndex(e => e.IdAnalyse)
                    .HasName("fk_analyse_has_Transformations_analyse1_idx");

                entity.HasIndex(e => e.IdTransformations)
                    .HasName("fk_analyse_has_Transformations_Transformations1_idx");

                entity.Property(e => e.IdAnalyse)
                    .HasColumnName("Id_analyse")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdTransformations).HasColumnType("int(11)");

                entity.HasOne(d => d.IdAnalyseNavigation)
                    .WithMany(p => p.AnalyseTransformations)
                    .HasForeignKey(d => d.IdAnalyse)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_analyse_has_Transformations_analyse1");

                entity.HasOne(d => d.IdTransformationsNavigation)
                    .WithMany(p => p.AnalyseTransformations)
                    .HasForeignKey(d => d.IdTransformations)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_analyse_has_Transformations_Transformations1");
            });

            modelBuilder.Entity<AnalyseValidations>(entity =>
            {
                entity.HasKey(e => new { e.IdAnalyse, e.IdValidations })
                    .HasName("PRIMARY");

                entity.ToTable("analyse_validations");

                entity.HasIndex(e => e.IdAnalyse)
                    .HasName("fk_dico_analyse_has_Validations_dico_analyse1_idx");

                entity.HasIndex(e => e.IdValidations)
                    .HasName("fk_dico_analyse_has_Validations_Validations1_idx");

                entity.Property(e => e.IdAnalyse)
                    .HasColumnName("id_analyse")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdValidations)
                    .HasColumnName("idValidations")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.IdAnalyseNavigation)
                    .WithMany(p => p.AnalyseValidations)
                    .HasForeignKey(d => d.IdAnalyse)
                    .HasConstraintName("fk_dico_analyse_has_Validations_dico_analyse1");

                entity.HasOne(d => d.IdValidationsNavigation)
                    .WithMany(p => p.AnalyseValidations)
                    .HasForeignKey(d => d.IdValidations)
                    .HasConstraintName("fk_dico_analyse_has_Validations_Validations1");
            });

            modelBuilder.Entity<AnalyseautomateFonctionsreflexes>(entity =>
            {
                entity.HasKey(e => new { e.IdAnalyseAutomate, e.IdFonctionsReflexes })
                    .HasName("PRIMARY");

                entity.ToTable("analyseautomate_fonctionsreflexes");

                entity.HasIndex(e => e.IdAnalyseAutomate)
                    .HasName("fk_Analyse_automate_has_FonctionsReflexes_Analyse_automate1_idx");

                entity.HasIndex(e => e.IdFonctionsReflexes)
                    .HasName("fk_Analyse_automate_has_FonctionsReflexes_FonctionsReflexes_idx");

                entity.Property(e => e.IdAnalyseAutomate).HasColumnType("int(11)");

                entity.Property(e => e.IdFonctionsReflexes).HasColumnType("int(11)");

                entity.HasOne(d => d.IdAnalyseAutomateNavigation)
                    .WithMany(p => p.AnalyseautomateFonctionsreflexes)
                    .HasForeignKey(d => d.IdAnalyseAutomate)
                    .HasConstraintName("fk_Analyse_automate_has_FonctionsReflexes_Analyse_automate1");

                entity.HasOne(d => d.IdFonctionsReflexesNavigation)
                    .WithMany(p => p.AnalyseautomateFonctionsreflexes)
                    .HasForeignKey(d => d.IdFonctionsReflexes)
                    .HasConstraintName("fk_Analyse_automate_has_FonctionsReflexes_FonctionsReflexes1");
            });

            modelBuilder.Entity<AnalyseautomateTransformations>(entity =>
            {
                entity.HasKey(e => new { e.IdAnalyseAutomate, e.IdTransformations })
                    .HasName("PRIMARY");

                entity.ToTable("analyseautomate_transformations");

                entity.HasIndex(e => e.IdAnalyseAutomate)
                    .HasName("fk_Analyse_automate_has_Transformations_Analyse_automate1_idx");

                entity.HasIndex(e => e.IdTransformations)
                    .HasName("fk_Analyse_automate_has_Transformations_Transformations1_idx");

                entity.Property(e => e.IdAnalyseAutomate)
                    .HasColumnName("Id_AnalyseAutomate")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdTransformations).HasColumnType("int(11)");

                entity.HasOne(d => d.IdAnalyseAutomateNavigation)
                    .WithMany(p => p.AnalyseautomateTransformations)
                    .HasForeignKey(d => d.IdAnalyseAutomate)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Analyse_automate_has_Transformations_Analyse_automate1");

                entity.HasOne(d => d.IdTransformationsNavigation)
                    .WithMany(p => p.AnalyseautomateTransformations)
                    .HasForeignKey(d => d.IdTransformations)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Analyse_automate_has_Transformations_Transformations1");
            });

            modelBuilder.Entity<Automate>(entity =>
            {
                entity.HasKey(e => e.IdAutomate)
                    .HasName("PRIMARY");

                entity.ToTable("automate");

                entity.HasIndex(e => e.SecteurIdSecteur)
                    .HasName("fk_dico_automate_Secteur1_idx");

                entity.Property(e => e.IdAutomate).HasColumnType("int(11)");

                entity.Property(e => e.AnalyseLeft)
                    .HasColumnName("Analyse_left")
                    .HasColumnType("varchar(2)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.AnalyseRight)
                    .HasColumnName("Analyse_right")
                    .HasColumnType("varchar(3)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Canal1).HasColumnType("tinyint(3) unsigned");

                entity.Property(e => e.Canal2).HasColumnType("tinyint(3) unsigned");

                entity.Property(e => e.ChargementAutoAcquisition)
                    .HasColumnName("Chargement_auto_acquisition")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Code)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CodePourReception)
                    .HasColumnName("Code_pour_Reception")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CommentaireLeft)
                    .HasColumnName("Commentaire_left")
                    .HasColumnType("varchar(2)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CommentaireRight)
                    .HasColumnName("Commentaire_right")
                    .HasColumnType("varchar(3)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Connexion)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.ControleEclair)
                    .HasColumnName("controle_eclair")
                    .HasColumnType("tinyint(3) unsigned");

                entity.Property(e => e.ContrôleDeFlux)
                    .HasColumnName("Contrôle_de_flux")
                    .HasColumnType("tinyint(3) unsigned");

                entity.Property(e => e.CoutPatientTypeAutomate)
                    .HasColumnName("cout_patient_type_automate")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.DilutionLeft)
                    .HasColumnName("Dilution_left")
                    .HasColumnType("varchar(2)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.DilutionRight)
                    .HasColumnName("Dilution_right")
                    .HasColumnType("varchar(3)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.DocumentProvisoir)
                    .HasColumnName("Document_provisoir")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.ExceptionsValidationAutomate)
                    .HasColumnName("Exceptions_Validation_Automate")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.FlagLeft)
                    .HasColumnName("Flag_left")
                    .HasColumnType("varchar(2)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.FlagRight)
                    .HasColumnName("Flag_right")
                    .HasColumnType("varchar(3)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.FormatFichePaillasse)
                    .HasColumnName("Format_fiche_paillasse")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.FormatNomPatientCq)
                    .HasColumnName("Format_nom_patient_CQ")
                    .HasColumnType("tinyint(3) unsigned");

                entity.Property(e => e.Identification1Left)
                    .HasColumnName("Identification_1_left")
                    .HasColumnType("varchar(2)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Identification1Right)
                    .HasColumnName("Identification_1_right")
                    .HasColumnType("varchar(3)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Identification2Left)
                    .HasColumnName("Identification_2_left")
                    .HasColumnType("varchar(2)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Identification2Right)
                    .HasColumnName("Identification_2_right")
                    .HasColumnType("varchar(3)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.IdtubeOrIgrImpose)
                    .HasColumnName("IDTube_or_IgrImpose")
                    .HasColumnType("tinyint(3) unsigned");

                entity.Property(e => e.ImprimanteCb)
                    .HasColumnName("Imprimante_CB")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Increment)
                    .HasColumnType("varchar(3)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.IsAfficherJournalConnex).HasColumnName("is_afficher_journal_connex");

                entity.Property(e => e.IsBoutonForcerNonAutorized).HasColumnName("is_bouton_forcer_non_autorized");

                entity.Property(e => e.IsControleQualiteActif).HasColumnName("is_Controle_Qualite_actif");

                entity.Property(e => e.IsDesValidationBiologique).HasColumnName("Is_Des_Validation_Biologique");

                entity.Property(e => e.IsForcerModeEscalve).HasColumnName("is_Forcer_Mode_Escalve");

                entity.Property(e => e.IsGereDemandesSupression).HasColumnName("Is_gere_Demandes_Supression");

                entity.Property(e => e.IsIgnorerLeLotCrConnexion).HasColumnName("is_ignorer_le_lot_CR_Connexion");

                entity.Property(e => e.IsInscrireAutoDansListe).HasColumnName("is_inscrire_auto_dans_liste");

                entity.Property(e => e.IsLotsProbatoires).HasColumnName("is_lots_probatoires");

                entity.Property(e => e.IsNotArchiverTubes).HasColumnName("Is_Not_Archiver_Tubes");

                entity.Property(e => e.IsPreanalyticAutomate).HasColumnName("is_preanalytic_automate");

                entity.Property(e => e.IsPrefixerPidNumDemande).HasColumnName("Is_Prefixer_PID_Num_Demande");

                entity.Property(e => e.IsRespectArboYyyymm).HasColumnName("is_respect_arbo_yyyymm");

                entity.Property(e => e.IsValidateAutoCq).HasColumnName("is_validate_auto_CQ");

                entity.Property(e => e.IsValidateResultpatientAutoCq).HasColumnName("is_validate_resultpatient_autoCQ");

                entity.Property(e => e.ModeValidation)
                    .HasColumnName("Mode_validation")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.ModelCompteRendu)
                    .HasColumnName("Model_compte_rendu")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.MonNom)
                    .HasColumnName("Mon_Nom")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.NbPosition)
                    .HasColumnName("NB_Position")
                    .HasColumnType("varchar(3)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.NbrBits)
                    .HasColumnName("Nbr_bits")
                    .HasColumnType("tinyint(3) unsigned");

                entity.Property(e => e.NbreCarTronq)
                    .HasColumnName("nbre_car_tronq")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.NomPatient)
                    .HasColumnName("Nom_patient")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.NumAutomate)
                    .HasColumnName("Num_Automate")
                    .HasColumnType("smallint(5)");

                entity.Property(e => e.OrdreLiaisonLis)
                    .HasColumnName("Ordre_Liaison_LIS")
                    .HasColumnType("tinyint(3) unsigned");

                entity.Property(e => e.OrdreValidation)
                    .HasColumnName("Ordre_validation")
                    .HasColumnType("tinyint(3) unsigned");

                entity.Property(e => e.Parite).HasColumnType("tinyint(3) unsigned");

                entity.Property(e => e.PieceJointesToSend)
                    .HasColumnName("piece_jointes_to_send")
                    .HasColumnType("tinyint(3) unsigned");

                entity.Property(e => e.PlateauDepart)
                    .HasColumnName("Plateau_Depart")
                    .HasColumnType("varchar(3)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Port).HasColumnType("tinyint(3) unsigned");

                entity.Property(e => e.PosDepart)
                    .HasColumnName("Pos_Depart")
                    .HasColumnType("varchar(3)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.ProprietaireAutomate)
                    .HasColumnName("Proprietaire_automate")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Protocole)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.RepWhereDupliquer)
                    .HasColumnName("rep_where_dupliquer")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.ResultatLeft)
                    .HasColumnName("Resultat_left")
                    .HasColumnType("varchar(2)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.ResultatRight)
                    .HasColumnName("Resultat_right")
                    .HasColumnType("varchar(3)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Secteur)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.SecteurIdSecteur)
                    .HasColumnName("Secteur_idSecteur")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SeqDepart)
                    .HasColumnName("Seq_Depart")
                    .HasColumnType("varchar(3)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Start1)
                    .HasColumnName("Start_1")
                    .HasColumnType("varchar(2)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Start10)
                    .HasColumnName("Start_10")
                    .HasColumnType("varchar(2)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Start2)
                    .HasColumnName("Start_2")
                    .HasColumnType("varchar(2)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Start3)
                    .HasColumnName("Start_3")
                    .HasColumnType("varchar(2)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Start4)
                    .HasColumnName("Start_4")
                    .HasColumnType("varchar(2)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Start5)
                    .HasColumnName("Start_5")
                    .HasColumnType("varchar(2)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Start6)
                    .HasColumnName("Start_6")
                    .HasColumnType("varchar(2)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Start7)
                    .HasColumnName("Start_7")
                    .HasColumnType("varchar(2)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Start8)
                    .HasColumnName("Start_8")
                    .HasColumnType("varchar(2)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Start9)
                    .HasColumnName("Start_9")
                    .HasColumnType("varchar(2)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Stop1)
                    .HasColumnName("Stop_1")
                    .HasColumnType("varchar(2)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Stop10)
                    .HasColumnName("Stop_10")
                    .HasColumnType("varchar(2)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Stop2)
                    .HasColumnName("Stop_2")
                    .HasColumnType("varchar(2)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Stop3)
                    .HasColumnName("Stop_3")
                    .HasColumnType("varchar(2)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Stop4)
                    .HasColumnName("Stop_4")
                    .HasColumnType("varchar(2)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Stop5)
                    .HasColumnName("Stop_5")
                    .HasColumnType("varchar(2)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Stop6)
                    .HasColumnName("Stop_6")
                    .HasColumnType("varchar(2)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Stop7)
                    .HasColumnName("Stop_7")
                    .HasColumnType("varchar(2)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Stop8)
                    .HasColumnName("Stop_8")
                    .HasColumnType("varchar(2)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Stop9)
                    .HasColumnName("Stop_9")
                    .HasColumnType("varchar(2)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.StopBits)
                    .HasColumnName("Stop_bits")
                    .HasColumnType("tinyint(3) unsigned");

                entity.Property(e => e.TailleMax)
                    .HasColumnName("Taille_Max")
                    .HasColumnType("varchar(6)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Tempo).HasColumnType("smallint(6)");

                entity.Property(e => e.Trace).HasColumnType("smallint(6)");

                entity.Property(e => e.TrameOrEchantillon)
                    .HasColumnName("Trame_or_Echantillon")
                    .HasColumnType("tinyint(3) unsigned");

                entity.Property(e => e.TrameResultatPosition)
                    .HasColumnName("trame_resultat_Position")
                    .HasColumnType("varchar(3)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TrameResultatValeur)
                    .HasColumnName("trame_resultat_valeur")
                    .HasColumnType("varchar(2)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TubeN)
                    .HasColumnName("Tube_n")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TypeCb)
                    .HasColumnName("Type_CB")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TypeInstrument)
                    .HasColumnName("type_instrument")
                    .HasColumnType("tinyint(3) unsigned");

                entity.Property(e => e.TypePort)
                    .HasColumnName("typePort")
                    .HasColumnType("varchar(10)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TypePrelevementLeft)
                    .HasColumnName("Type_prelevement_left")
                    .HasColumnType("varchar(2)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TypePrelevementRight)
                    .HasColumnName("Type_prelevement_right")
                    .HasColumnType("varchar(3)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TypeTupeInstrument)
                    .HasColumnName("type_tupe_instrument")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.ValeurParDefaut)
                    .HasColumnName("Valeur_par_defaut")
                    .HasColumnType("smallint(5) unsigned");

                entity.Property(e => e.ValeurPersonnalisee)
                    .HasColumnName("Valeur_Personnalisee")
                    .HasColumnType("smallint(5) unsigned");

                entity.Property(e => e.ValmethodeAutre1)
                    .HasColumnName("valmethode_Autre1")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.ValmethodeAutre2)
                    .HasColumnName("valmethode_Autre2")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.ValmethodeDetailDesTechniques)
                    .HasColumnName("valmethode_detail_des_techniques")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.ValmethodeFournisseur)
                    .HasColumnName("valmethode_fournisseur")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.ValmethodeIdentification)
                    .HasColumnName("valmethode_identification")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.VitesseTransmission)
                    .HasColumnName("Vitesse_transmission")
                    .HasColumnType("tinyint(3) unsigned");

                entity.HasOne(d => d.SecteurIdSecteurNavigation)
                    .WithMany(p => p.Automate)
                    .HasForeignKey(d => d.SecteurIdSecteur)
                    .HasConstraintName("fk_dico_automate_Secteur1");
            });

            modelBuilder.Entity<AutomateCatalogue>(entity =>
            {
                entity.HasKey(e => new { e.IdAutomate, e.IdCatalogue })
                    .HasName("PRIMARY");

                entity.ToTable("automate_catalogue");

                entity.HasIndex(e => e.IdAutomate)
                    .HasName("fk_Automate_has_Catalogue_Automate1_idx");

                entity.HasIndex(e => e.IdCatalogue)
                    .HasName("fk_Automate_has_Catalogue_Catalogue1_idx");

                entity.Property(e => e.IdAutomate).HasColumnType("int(11)");

                entity.Property(e => e.IdCatalogue).HasColumnType("int(11)");

                entity.HasOne(d => d.IdAutomateNavigation)
                    .WithMany(p => p.AutomateCatalogue)
                    .HasForeignKey(d => d.IdAutomate)
                    .HasConstraintName("fk_Automate_has_Catalogue_Automate1");

                entity.HasOne(d => d.IdCatalogueNavigation)
                    .WithMany(p => p.AutomateCatalogue)
                    .HasForeignKey(d => d.IdCatalogue)
                    .HasConstraintName("fk_Automate_has_Catalogue_Catalogue1");
            });

            modelBuilder.Entity<Bilan>(entity =>
            {
                entity.HasKey(e => e.IdBilan)
                    .HasName("PRIMARY");

                entity.ToTable("bilan");

                entity.Property(e => e.IdBilan)
                    .HasColumnName("idBilan")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Analyse1)
                    .HasColumnType("varchar(20)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Analyse10)
                    .HasColumnType("varchar(20)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Analyse11)
                    .HasColumnType("varchar(20)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Analyse12)
                    .HasColumnType("varchar(20)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Analyse13)
                    .HasColumnType("varchar(20)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Analyse14)
                    .HasColumnType("varchar(20)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Analyse15)
                    .HasColumnType("varchar(20)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Analyse16)
                    .HasColumnType("varchar(20)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Analyse17)
                    .HasColumnType("varchar(20)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Analyse18)
                    .HasColumnType("varchar(20)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Analyse19)
                    .HasColumnType("varchar(20)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Analyse2)
                    .HasColumnType("varchar(20)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Analyse20)
                    .HasColumnType("varchar(20)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Analyse21)
                    .HasColumnType("varchar(20)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Analyse22)
                    .HasColumnType("varchar(20)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Analyse23)
                    .HasColumnType("varchar(20)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Analyse24)
                    .HasColumnType("varchar(20)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Analyse3)
                    .HasColumnType("varchar(20)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Analyse4)
                    .HasColumnType("varchar(20)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Analyse5)
                    .HasColumnType("varchar(20)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Analyse6)
                    .HasColumnType("varchar(20)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Analyse7)
                    .HasColumnType("varchar(20)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Analyse8)
                    .HasColumnType("varchar(20)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Analyse9)
                    .HasColumnType("varchar(20)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");
            });

            modelBuilder.Entity<BilanAnalyseautomate>(entity =>
            {
                entity.HasKey(e => new { e.IdBilan, e.IdAnalyseAutomate })
                    .HasName("PRIMARY");

                entity.ToTable("bilan_analyseautomate");

                entity.HasIndex(e => e.IdAnalyseAutomate)
                    .HasName("fk_Bilan_has_Analyse_automate_Analyse_automate1_idx");

                entity.HasIndex(e => e.IdBilan)
                    .HasName("fk_Bilan_has_Analyse_automate_Bilan1_idx");

                entity.Property(e => e.IdBilan).HasColumnType("int(11)");

                entity.Property(e => e.IdAnalyseAutomate)
                    .HasColumnName("IdAnalyse_automate")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.IdAnalyseAutomateNavigation)
                    .WithMany(p => p.BilanAnalyseautomate)
                    .HasForeignKey(d => d.IdAnalyseAutomate)
                    .HasConstraintName("fk_Bilan_has_Analyse_automate_Analyse_automate1");

                entity.HasOne(d => d.IdBilanNavigation)
                    .WithMany(p => p.BilanAnalyseautomate)
                    .HasForeignKey(d => d.IdBilan)
                    .HasConstraintName("fk_Bilan_has_Analyse_automate_Bilan1");
            });

            modelBuilder.Entity<Catalogue>(entity =>
            {
                entity.HasKey(e => e.IdCatalogue)
                    .HasName("PRIMARY");

                entity.ToTable("catalogue");

                entity.Property(e => e.IdCatalogue)
                    .HasColumnName("idCatalogue")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CreationDate).HasColumnType("timestamp");

                entity.Property(e => e.CreatorName)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.NomCatalogue)
                    .HasColumnName("Nom_Catalogue")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Statut)
                    .HasColumnName("statut")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Version).HasColumnType("int(11)");
            });

            modelBuilder.Entity<Commauto>(entity =>
            {
                entity.HasKey(e => e.IdCommAuto)
                    .HasName("PRIMARY");

                entity.ToTable("commauto");

                entity.Property(e => e.IdCommAuto)
                    .HasColumnName("idCommAuto")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Condition)
                    .HasColumnType("varchar(120)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Ordre).HasColumnType("tinyint(4)");

                entity.Property(e => e.Valeur).HasColumnType("smallint(6)");
            });

            modelBuilder.Entity<CommautoAnalyseautomate>(entity =>
            {
                entity.HasKey(e => new { e.IdCommAuto, e.IdAnalyseAutomate })
                    .HasName("PRIMARY");

                entity.ToTable("commauto_analyseautomate");

                entity.HasIndex(e => e.IdAnalyseAutomate)
                    .HasName("fk_CommAuto_has_Analyse_automate_Analyse_automate1_idx");

                entity.HasIndex(e => e.IdCommAuto)
                    .HasName("fk_CommAuto_has_Analyse_automate_CommAuto1_idx");

                entity.Property(e => e.IdCommAuto).HasColumnType("int(11)");

                entity.Property(e => e.IdAnalyseAutomate)
                    .HasColumnName("Id_AnalyseAutomate")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.IdAnalyseAutomateNavigation)
                    .WithMany(p => p.CommautoAnalyseautomate)
                    .HasForeignKey(d => d.IdAnalyseAutomate)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_CommAuto_has_Analyse_automate_Analyse_automate1");

                entity.HasOne(d => d.IdCommAutoNavigation)
                    .WithMany(p => p.CommautoAnalyseautomate)
                    .HasForeignKey(d => d.IdCommAuto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_CommAuto_has_Analyse_automate_CommAuto1");
            });

            modelBuilder.Entity<Droit>(entity =>
            {
                entity.HasKey(e => e.IdDroit)
                    .HasName("PRIMARY");

                entity.ToTable("droit");

                entity.Property(e => e.IdDroit)
                    .HasColumnName("idDroit")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Droit1)
                    .HasColumnName("Droit")
                    .HasColumnType("varchar(70)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");
            });

            modelBuilder.Entity<DroitUser>(entity =>
            {
                entity.HasKey(e => new { e.IdDroitUser, e.IdUser })
                    .HasName("PRIMARY");

                entity.ToTable("droit_user");

                entity.HasIndex(e => e.IdDroit)
                    .HasName("fk_Droit_has_User_Droit1_idx");

                entity.HasIndex(e => e.IdUser)
                    .HasName("fk_Droit_has_User_User1_idx");

                entity.Property(e => e.IdDroitUser)
                    .HasColumnName("Id_DroitUser")
                    .HasColumnType("int(11)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.IdUser).HasColumnType("int(11)");

                entity.Property(e => e.IdDroit).HasColumnType("int(11)");

                entity.HasOne(d => d.IdDroitNavigation)
                    .WithMany(p => p.DroitUser)
                    .HasForeignKey(d => d.IdDroit)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Droit_has_User_Droit1");

                entity.HasOne(d => d.IdUserNavigation)
                    .WithMany(p => p.DroitUser)
                    .HasForeignKey(d => d.IdUser)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Droit_has_User_User1");
            });

            modelBuilder.Entity<Evaluations>(entity =>
            {
                entity.HasKey(e => e.IdEvaluations)
                    .HasName("PRIMARY");

                entity.ToTable("evaluations");

                entity.Property(e => e.IdEvaluations)
                    .HasColumnName("idEvaluations")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Condition)
                    .HasColumnType("varchar(120)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.NouvelleValeur).HasColumnType("smallint(6)");

                entity.Property(e => e.Ordre).HasColumnType("tinyint(4)");
            });

            modelBuilder.Entity<EvaluationsAnalyseautomate>(entity =>
            {
                entity.HasKey(e => new { e.IdEvaluations, e.IdAnalyseAutomate })
                    .HasName("PRIMARY");

                entity.ToTable("evaluations_analyseautomate");

                entity.HasIndex(e => e.IdAnalyseAutomate)
                    .HasName("fk_Evaluations_has_Analyse_automate_Analyse_automate1_idx");

                entity.HasIndex(e => e.IdEvaluations)
                    .HasName("fk_Evaluations_has_Analyse_automate_Evaluations1_idx");

                entity.Property(e => e.IdEvaluations).HasColumnType("int(11)");

                entity.Property(e => e.IdAnalyseAutomate)
                    .HasColumnName("IdAnalyse_automate")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.IdAnalyseAutomateNavigation)
                    .WithMany(p => p.EvaluationsAnalyseautomate)
                    .HasForeignKey(d => d.IdAnalyseAutomate)
                    .HasConstraintName("fk_Evaluations_has_Analyse_automate_Analyse_automate1");

                entity.HasOne(d => d.IdEvaluationsNavigation)
                    .WithMany(p => p.EvaluationsAnalyseautomate)
                    .HasForeignKey(d => d.IdEvaluations)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Evaluations_has_Analyse_automate_Evaluations1");
            });

            modelBuilder.Entity<Fonctionsreflexes>(entity =>
            {
                entity.HasKey(e => e.IdFonctionsReflexes)
                    .HasName("PRIMARY");

                entity.ToTable("fonctionsreflexes");

                entity.Property(e => e.IdFonctionsReflexes)
                    .HasColumnName("idFonctionsReflexes")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Condition)
                    .HasColumnType("varchar(120)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Ordre).HasColumnType("tinyint(4)");

                entity.Property(e => e.Scenario)
                    .HasColumnType("varchar(120)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");
            });

            modelBuilder.Entity<Groupeanalyse>(entity =>
            {
                entity.HasKey(e => e.IdGroupeAnalyse)
                    .HasName("PRIMARY");

                entity.ToTable("groupeanalyse");

                entity.Property(e => e.IdGroupeAnalyse)
                    .HasColumnName("idGroupeAnalyse")
                    .HasColumnType("int(11)");

                entity.Property(e => e.NomGroupe)
                    .HasColumnName("Nom_Groupe")
                    .HasColumnType("varchar(150)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");
            });

            modelBuilder.Entity<Incidencedesflags>(entity =>
            {
                entity.HasKey(e => e.Idincidencedesflags)
                    .HasName("PRIMARY");

                entity.ToTable("incidencedesflags");

                entity.Property(e => e.Idincidencedesflags)
                    .HasColumnName("idincidencedesflags")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ValeurDuFlag)
                    .HasColumnType("varchar(3)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.ValeurDuResultat)
                    .HasColumnType("varchar(3)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");
            });

            modelBuilder.Entity<IncidencedesflagsAnalyseautomate>(entity =>
            {
                entity.HasKey(e => new { e.IdIncidenceDesFlags, e.IdAnalyseAutomate })
                    .HasName("PRIMARY");

                entity.ToTable("incidencedesflags_analyseautomate");

                entity.HasIndex(e => e.IdAnalyseAutomate)
                    .HasName("fk_incidencedesflags_has_Analyse_automate_Analyse_automate1_idx");

                entity.HasIndex(e => e.IdIncidenceDesFlags)
                    .HasName("fk_incidencedesflags_has_Analyse_automate_incidencedesflags_idx");

                entity.Property(e => e.IdIncidenceDesFlags).HasColumnType("int(11)");

                entity.Property(e => e.IdAnalyseAutomate)
                    .HasColumnName("Id_AnalyseAutomate")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.IdAnalyseAutomateNavigation)
                    .WithMany(p => p.IncidencedesflagsAnalyseautomate)
                    .HasForeignKey(d => d.IdAnalyseAutomate)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_incidencedesflags_has_Analyse_automate_Analyse_automate1");

                entity.HasOne(d => d.IdIncidenceDesFlagsNavigation)
                    .WithMany(p => p.IncidencedesflagsAnalyseautomate)
                    .HasForeignKey(d => d.IdIncidenceDesFlags)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_incidencedesflags_has_Analyse_automate_incidencedesflags1");
            });

            modelBuilder.Entity<Instance>(entity =>
            {
                entity.HasKey(e => e.IdInstance)
                    .HasName("PRIMARY");

                entity.ToTable("instance");

                entity.Property(e => e.IdInstance).HasColumnType("int(11)");

                entity.Property(e => e.AdresseSite)
                    .IsRequired()
                    .HasColumnName("Adresse_site")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnType("varchar(20)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.EmailContactSite)
                    .IsRequired()
                    .HasColumnName("Email_contact_site")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.IpInstance)
                    .IsRequired()
                    .HasColumnName("IP_Instance")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Nom)
                    .IsRequired()
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.NomContactSite)
                    .IsRequired()
                    .HasColumnName("Nom_Contact_site")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.NomMasterInstance)
                    .IsRequired()
                    .HasColumnName("Nom_Master_instance")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TelContactSite)
                    .IsRequired()
                    .HasColumnName("tel_contact_site")
                    .HasColumnType("varchar(30)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.TelSite)
                    .IsRequired()
                    .HasColumnName("Tel_site")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");
            });

            modelBuilder.Entity<InstanceCatalogue>(entity =>
            {
                entity.HasKey(e => new { e.IdInstance, e.IdCatalogue })
                    .HasName("PRIMARY");

                entity.ToTable("instance_catalogue");

                entity.HasIndex(e => e.IdCatalogue)
                    .HasName("fk_instance_has_Catalogue_Catalogue1_idx");

                entity.HasIndex(e => e.IdInstance)
                    .HasName("fk_instance_has_Catalogue_instance1_idx");

                entity.Property(e => e.IdInstance).HasColumnType("int(11)");

                entity.Property(e => e.IdCatalogue).HasColumnType("int(11)");

                entity.Property(e => e.Statut)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.HasOne(d => d.IdCatalogueNavigation)
                    .WithMany(p => p.InstanceCatalogue)
                    .HasForeignKey(d => d.IdCatalogue)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_instance_has_Catalogue_Catalogue1");

                entity.HasOne(d => d.IdInstanceNavigation)
                    .WithMany(p => p.InstanceCatalogue)
                    .HasForeignKey(d => d.IdInstance)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_instance_has_Catalogue_instance1");
            });

            modelBuilder.Entity<Paillasse>(entity =>
            {
                entity.HasKey(e => e.IdPaillasse)
                    .HasName("PRIMARY");

                entity.ToTable("paillasse");

                entity.Property(e => e.IdPaillasse)
                    .HasColumnName("idPaillasse")
                    .HasColumnType("int(11)");

                entity.Property(e => e.NomPaillasse)
                    .HasColumnName("Nom_Paillasse")
                    .HasColumnType("varchar(150)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");
            });

            modelBuilder.Entity<Secteur>(entity =>
            {
                entity.HasKey(e => e.IdSecteur)
                    .HasName("PRIMARY");

                entity.ToTable("secteur");

                entity.Property(e => e.IdSecteur)
                    .HasColumnName("idSecteur")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SecteurNom)
                    .HasColumnType("varchar(120)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");
            });

            modelBuilder.Entity<Soustitres>(entity =>
            {
                entity.HasKey(e => e.IdSousTitres)
                    .HasName("PRIMARY");

                entity.ToTable("soustitres");

                entity.Property(e => e.IdSousTitres)
                    .HasColumnName("idSousTitres")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Ordre).HasColumnType("tinyint(4)");

                entity.Property(e => e.Stitre)
                    .HasColumnName("STitre")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Style).HasColumnType("tinyint(4)");
            });

            modelBuilder.Entity<Titres>(entity =>
            {
                entity.HasKey(e => e.IdTitres)
                    .HasName("PRIMARY");

                entity.ToTable("titres");

                entity.Property(e => e.IdTitres)
                    .HasColumnName("idTitres")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Ordre).HasColumnType("tinyint(4)");

                entity.Property(e => e.Style).HasColumnType("tinyint(4)");

                entity.Property(e => e.Titre)
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");
            });

            modelBuilder.Entity<Transfbasic>(entity =>
            {
                entity.HasKey(e => e.IdTransfbasic)
                    .HasName("PRIMARY");

                entity.ToTable("transfbasic");

                entity.Property(e => e.IdTransfbasic)
                    .HasColumnName("idTransfbasic")
                    .HasColumnType("int(11)");

                entity.Property(e => e.OperateurLogique)
                    .HasColumnType("varchar(2)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Valeur1).HasColumnType("smallint(6)");

                entity.Property(e => e.ValeurResultat).HasColumnType("smallint(6)");
            });

            modelBuilder.Entity<TransfbasicAnalyseautomate>(entity =>
            {
                entity.HasKey(e => new { e.IdTransfbasic, e.IdAnalyseAutomate })
                    .HasName("PRIMARY");

                entity.ToTable("transfbasic_analyseautomate");

                entity.HasIndex(e => e.IdAnalyseAutomate)
                    .HasName("fk_Transfbasic_has_Analyse_automate_Analyse_automate1_idx");

                entity.HasIndex(e => e.IdTransfbasic)
                    .HasName("fk_Transfbasic_has_Analyse_automate_Transfbasic1_idx");

                entity.Property(e => e.IdTransfbasic).HasColumnType("int(11)");

                entity.Property(e => e.IdAnalyseAutomate)
                    .HasColumnName("Id_AnalyseAutomate")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.IdAnalyseAutomateNavigation)
                    .WithMany(p => p.TransfbasicAnalyseautomate)
                    .HasForeignKey(d => d.IdAnalyseAutomate)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Transfbasic_has_Analyse_automate_Analyse_automate1");

                entity.HasOne(d => d.IdTransfbasicNavigation)
                    .WithMany(p => p.TransfbasicAnalyseautomate)
                    .HasForeignKey(d => d.IdTransfbasic)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Transfbasic_has_Analyse_automate_Transfbasic1");
            });

            modelBuilder.Entity<Transformations>(entity =>
            {
                entity.HasKey(e => e.IdTransformations)
                    .HasName("PRIMARY");

                entity.ToTable("transformations");

                entity.Property(e => e.IdTransformations)
                    .HasColumnName("idTransformations")
                    .HasColumnType("int(11)");

                entity.Property(e => e.OperateurLogique)
                    .HasColumnType("varchar(2)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Valeur1).HasColumnType("smallint(6)");

                entity.Property(e => e.ValeurResultat).HasColumnType("smallint(6)");
            });

            modelBuilder.Entity<Typedetube>(entity =>
            {
                entity.HasKey(e => e.IdTypeDeTube)
                    .HasName("PRIMARY");

                entity.ToTable("typedetube");

                entity.Property(e => e.IdTypeDeTube)
                    .HasColumnName("idTypeDeTube")
                    .HasColumnType("int(11)");

                entity.Property(e => e.NomTypeDuTube)
                    .HasColumnName("Nom_TypeDuTube")
                    .HasColumnType("varchar(150)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasKey(e => e.IdUser)
                    .HasName("PRIMARY");

                entity.ToTable("user");

                entity.Property(e => e.IdUser).HasColumnType("int(11)");

                entity.Property(e => e.Email)
                    .HasColumnType("varchar(80)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Nom)
                    .HasColumnName("nom")
                    .HasColumnType("varchar(40)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Prenom)
                    .HasColumnName("prenom")
                    .HasColumnType("varchar(40)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasColumnType("varchar(70)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");
            });

            modelBuilder.Entity<UserInstance>(entity =>
            {
                entity.HasKey(e => new { e.IdUser, e.IdInstance })
                    .HasName("PRIMARY");

                entity.ToTable("user_instance");

                entity.HasIndex(e => e.IdInstance)
                    .HasName("fk_utilisateur_has_instance_instance1_idx");

                entity.HasIndex(e => e.IdUser)
                    .HasName("fk_utilisateur_has_instance_utilisateur_idx");

                entity.Property(e => e.IdUser).HasColumnType("int(11)");

                entity.Property(e => e.IdInstance).HasColumnType("int(11)");

                entity.HasOne(d => d.IdInstanceNavigation)
                    .WithMany(p => p.UserInstance)
                    .HasForeignKey(d => d.IdInstance)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_utilisateur_has_instance_instance1");

                entity.HasOne(d => d.IdUserNavigation)
                    .WithMany(p => p.UserInstance)
                    .HasForeignKey(d => d.IdUser)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_utilisateur_has_instance_utilisateur");
            });

            modelBuilder.Entity<Validations>(entity =>
            {
                entity.HasKey(e => e.IdValidations)
                    .HasName("PRIMARY");

                entity.ToTable("validations");

                entity.Property(e => e.IdValidations)
                    .HasColumnName("idValidations")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Max).HasColumnType("tinyint(4)");

                entity.Property(e => e.Min).HasColumnType("tinyint(4)");

                entity.Property(e => e.Origine)
                    .HasColumnType("varchar(120)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Service)
                    .HasColumnType("varchar(120)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Sexe)
                    .HasColumnType("varchar(20)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Unites)
                    .HasColumnType("varchar(120)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
