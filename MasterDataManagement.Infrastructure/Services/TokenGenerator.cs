﻿
using MasterDataManagement.Domain.Others;
using MasterDataManagement.Domain.Services;
using MasterDataManagement.DTO.Authentication;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace MasterDataManagement.Infrastructure.Services
{
    public class TokenGenerator : ITokenGenerator
    {
        private readonly IConfiguration configuration;

        public TokenGenerator(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        GeneratedTokenInfo ITokenGenerator.GenerateToken(TokenDto data)
        {
            var clientClaims = new ClaimsIdentity(new Claim[]
                {
                    new Claim(nameof(TokenDto.UserId), data.UserId)
                });

            return generateToken(clientClaims, 1);
        }

        private GeneratedTokenInfo generateToken(ClaimsIdentity claims, int expiresInDays)
        {
            try
            {
                var secret = configuration.GetSection("AppSettings:Secret")?.Value;
                var key = Encoding.ASCII.GetBytes(secret);

                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Issuer = AuthInfo.Issuers[0],
                    Subject = claims,
                    Expires = DateTime.UtcNow.AddDays(expiresInDays),
                    IssuedAt = DateTime.UtcNow,
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
                    Audience = AuthInfo.Audiences[0]
                };

                var tokenHandler = new JwtSecurityTokenHandler();

                var securityToken = tokenHandler.CreateToken(tokenDescriptor);

                var token = tokenHandler.WriteToken(securityToken);

                return new GeneratedTokenInfo
                {
                    IssuedAt = tokenDescriptor.IssuedAt,
                    ExpiresAt = tokenDescriptor.Expires,
                    Token = $@"Bearer {token}"
                };
            }
            catch (Exception e)
            {

                throw e;
            }
        }
    }
}
