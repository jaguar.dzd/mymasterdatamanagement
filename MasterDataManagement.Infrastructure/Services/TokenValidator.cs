﻿
using MasterDataManagement.Domain.Others;
using MasterDataManagement.Domain.Services;
using MasterDataManagement.DTO.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace MasterDataManagement.Infrastructure.Services
{
    public class TokenValidator : ITokenValidator
    {
        private readonly IConfiguration configuration;
        private readonly IHttpContextAccessor httpContextAccessor;

        public TokenValidator(IConfiguration configuration, IHttpContextAccessor httpContext)
        {
            this.configuration = configuration;
            this.httpContextAccessor = httpContext;
        }

        TokenDto ITokenValidator.ValidateUserToken()
        {
            var token = tokenExtractor();
            if (string.IsNullOrWhiteSpace(token))
            {
                return null;
            }

            var claims = GetClaims(token);
            if (claims == null)
                return null;

            var userId = claims.FindFirst(x => x.Type == nameof(TokenDto.UserId));

            if (userId?.Value == null)
                return null;

            return new TokenDto { UserId = userId.Value };
        }

        private string tokenExtractor()
        {
            var authorize = httpContextAccessor.HttpContext.Request.Headers.FirstOrDefault(x => x.Key.Equals("authorization", StringComparison.InvariantCultureIgnoreCase));

            if (string.IsNullOrWhiteSpace(authorize.Value))
            {
                return null;
            }

            return authorize.Value.ToString().Replace("Bearer ", "");
        }


        private ClaimsPrincipal GetClaims(string token)
        {
            var secret = configuration.GetSection("AppSettings:Secret")?.Value;
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(secret);

            var validationParam = new TokenValidationParameters
            {
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidIssuers = AuthInfo.Issuers,
                ValidAudiences = AuthInfo.Audiences
            };

            return tokenHandler.ValidateToken(token, validationParam, out SecurityToken validatedToken);
        }
    }
}
