﻿namespace MasterDataManagement.DTO.Paillasse
{
    public class PaillasseGetDto
    {
        public int IdPaillasse { get; set; }
        public string NomPaillasse { get; set; }
    }
}
