﻿namespace MasterDataManagement.DTO.Paillasse
{
    public class PaillassePostDto
    {
        public int IdPaillasse { get; set; }
        public string NomPaillasse { get; set; }
    }
}
