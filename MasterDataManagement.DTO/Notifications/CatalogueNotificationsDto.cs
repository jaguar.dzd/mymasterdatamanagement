﻿using System;
using System.Collections.Generic;
using MasterDataManagement.DTO.Instance;

namespace MasterDataManagement.DTO.Notifications
{
    public class CatalogueNotificationsDto
    {
        public List<EnAttenteDeValidationDto> EnAttenteDeValidation { get; set; } = new List<EnAttenteDeValidationDto>();
        public List<EnAttenteDeAprobationDto> EnAttenteDeAprobation { get; set; } = new List<EnAttenteDeAprobationDto>();
        public List<RejeterDto> Rejeter { get; set; } = new List<RejeterDto>();
        public List<RefuserDto> Refuser { get; set; } = new List<RefuserDto>();
    }

    public class EnAttenteDeValidationDto
    {
        public int IdCatalogue { get; set; }
        public DateTime? CreationDate { get; set; }
        public string NomCatalogue { get; set; }
        public int Version { get; set; }
        public string CreatorName { get; set; }
    }

    public class EnAttenteDeAprobationDto
    {
        public int IdCatalogue { get; set; }
        public DateTime? CreationDate { get; set; }
        public string NomCatalogue { get; set; }
        public int Version { get; set; }
        public string Requester { get; set; }
        public  List<InstanceGetDto> Instances { get; set; }
}

    public class RejeterDto
    {
        public int IdCatalogue { get; set; }
        public DateTime? CreationDate { get; set; }
        public string NomCatalogue { get; set; }
        public int Version { get; set; }

    }

    public class RefuserDto
    {
        public int IdCatalogue { get; set; }
        public DateTime? CreationDate { get; set; }
        public string NomCatalogue { get; set; }
        public int Version { get; set; }
        public List<InstanceGetDto> Instances { get; set; }
    }
}
