﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.DTO.AnalyseAutomate
{
    public class LinkAnalyseAutomateToTransformations
    {
        public List<int> TransformationsIds { get; set; }
        public List<int> IdAnalyseAutomates { get; set; }
    }
}
