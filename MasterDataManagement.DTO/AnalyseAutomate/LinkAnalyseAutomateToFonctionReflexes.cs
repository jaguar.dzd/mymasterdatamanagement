﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.DTO.AnalyseAutomate
{
    public class LinkAnalyseAutomateToFonctionReflexes
    {
        public List<int> FonctionReflexesIds { get; set; }
        public List<int> IdAnalyseAutomates { get; set; }
    }
}
