﻿using System.ComponentModel.DataAnnotations;

namespace MasterDataManagement.DTO.AnalyseAutomate
{
    public class AnalyseAutomatePostDto
    {
        public int IdAnalyseAutomate { get; set; }
        public int IdAnalyse { get; set; }
        public ushort IdAutomate { get; set; }
        public int? IdPaillasse { get; set; }
        [MaxLength(120)]
        public string Paillasse2 { get; set; }
        public int? IdTypeDeTube { get; set; }
        [MaxLength(50)]
        public string Unites1 { get; set; }
        [MaxLength(50)]
        public string Unites2 { get; set; }
        public short? Decimales1 { get; set; }
        public short? Decimales2 { get; set; }
        public short? Coefficient { get; set; }
        public short? Longueur { get; set; }
        public string FormuleX1 { get; set; }
        public string FormuleX2 { get; set; }
        public string Formule { get; set; }
        public string Titre { get; set; }
        public string SousTitre { get; set; }
        public string Ligne1AvantResult { get; set; }
        public string Ligne2AvantResult { get; set; }
        public string Ligne3AvantResult { get; set; }
        public string Ligne4AvantResult { get; set; }
        public string Ligne5AvantResult { get; set; }
        public string Ligne1ApresResult { get; set; }
        public string Ligne2ApresResult { get; set; }
        public string Ligne3ApresResult { get; set; }
        public string Ligne4ApresResult { get; set; }
        public string Ligne5ApresResult { get; set; }
        public string Ligne6ApresResult { get; set; }
        public string TexteOptionnel1 { get; set; }
        public string TexteOptionnel2 { get; set; }
        public string TexteOptionnel3 { get; set; }
        public string TexteOptionnel4 { get; set; }
        public string TexteOptionnel5 { get; set; }
        public string TexteOptionnel6 { get; set; }
        public string TexteOptionnel7 { get; set; }
        public string TexteOptionnel8 { get; set; }
        public string TexteOptionnel9 { get; set; }
        public string TexteOptionnel10 { get; set; }
        public string TexteOptionnel11 { get; set; }
        public string TexteOptionnel12 { get; set; }
        public string TexteOptionnel13 { get; set; }
        public string TexteOptionnel14 { get; set; }
        public string TexteOptionnel15 { get; set; }
        public short? FacteurDilution { get; set; }
        public short? ValeurAajouter { get; set; }
        public short? Arrondi { get; set; }
        [MaxLength(4)]
        public string CodeAller1 { get; set; }
        [MaxLength(4)]
        public string CodeAller2 { get; set; }
        [MaxLength(4)]
        public string CodeAller3 { get; set; }
        [MaxLength(4)]
        public string CodeAller4 { get; set; }
        [MaxLength(4)]
        public string CodeAller5 { get; set; }
        [MaxLength(4)]
        public string CodeAller6 { get; set; }
        [MaxLength(4)]
        public string CodeAller7 { get; set; }
        [MaxLength(4)]
        public string CodeAller8 { get; set; }
        [MaxLength(4)]
        public string CodeAller9 { get; set; }
        [MaxLength(4)]
        public string AnalyseAutomatecol { get; set; }
        [MaxLength(4)]
        public string CodeRetour1 { get; set; }
        [MaxLength(4)]
        public string CodeRetour2 { get; set; }
        public bool? IsCreationAutomatiqueAnalyse { get; set; }
        public bool? GenererFlag { get; set; }
        public double? DifValeurSupa { get; set; }
        public double? MinvaleurEntrePassages { get; set; }
        public bool? MiseControleAvecDilution { get; set; }
        [MaxLength(50)]
        public string SiInfA { get; set; }
        [MaxLength(50)]
        public string EtOuSupA { get; set; }
        [MaxLength(50)]
        public string EtOuEgalA { get; set; }
        [MaxLength(50)]
        public string RepasserAvecLaDilution { get; set; }
        public bool? IsMiseEnControleSurCondi { get; set; }
    }
}
