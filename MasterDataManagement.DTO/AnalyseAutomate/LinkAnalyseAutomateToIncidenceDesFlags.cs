﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.DTO.AnalyseAutomate
{
    public class LinkAnalyseAutomateToIncidenceDesFlags
    {
        public List<int> IncidenceDesFlagsIds { get; set; }
        public List<int> IdAnalyseAutomates { get; set; }
    }
}
