﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.DTO.AnalyseAutomate
{
    public class LinkAnalyseToTransBasic
    {
        public List<int> TransBasicIds { get; set; }
        public List<int> IdAnalyseAutomates { get; set; }
    }
}
