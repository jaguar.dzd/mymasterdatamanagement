﻿using MasterDataManagement.DTO.Analyse;
using MasterDataManagement.DTO.Bilan;
using MasterDataManagement.DTO.Catalogue;
using MasterDataManagement.DTO.CommAuto;
using MasterDataManagement.DTO.Evaluation;
using MasterDataManagement.DTO.FonctionReflexes;
using MasterDataManagement.DTO.IncidencesDesFlags;
using MasterDataManagement.DTO.Site;
using MasterDataManagement.DTO.TransBasic;
using MasterDataManagement.DTO.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.DTO.AnalyseAutomate
{
    public class AnalyseAutomatePostFullDto
    {
        public int AnalyseId { get; set; }
        public int AutomateId { get; set; }
        public List<TransBasicPostDto> TransBasics { get; set; }
        public List<TransformationPostDto> Transformations { get; set; }
        public List<IncidencedesflagsPostDto> IncidenceDesFlags  { get; set; }
        public List<CommAutoPostDto> CommAutos { get; set; }
        public List<BilanPostDto> Bilans { get; set; }
        public List<ValidationPostDto> Validations { get; set; }
        public List<EvaluationPostDto> Evaluations { get; set; }
        public List<FonctionReflexesPostDto> FonctionReflexes { get; set; }
        public List<CataloguePostDto> Catalogues { get; set; }
    }
}
