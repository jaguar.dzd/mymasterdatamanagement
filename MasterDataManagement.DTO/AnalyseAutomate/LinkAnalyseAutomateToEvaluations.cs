﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.DTO.AnalyseAutomate
{
    public class LinkAnalyseAutomateToEvaluations
    {
        public List<int> Evaluationsds { get; set; }
        public List<int> IdAnalyseAutomates { get; set; }
    }
}
