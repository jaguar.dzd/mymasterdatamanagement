﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.DTO.AnalyseAutomate
{
    public class LinkAnalyseAutomateToValidations
    {
        public List<int> ValidationsIds { get; set; }
        public List<int> IdAnalyseAutomates { get; set; }
    }
}
