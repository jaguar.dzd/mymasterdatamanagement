﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.DTO.AnalyseAutomate
{
    public class LinkAnalyseAutomateToBilans
    {
        public List<int> BilansIds { get; set; }
        public List<int> IdAnalyseAutomates { get; set; }
    }
}
