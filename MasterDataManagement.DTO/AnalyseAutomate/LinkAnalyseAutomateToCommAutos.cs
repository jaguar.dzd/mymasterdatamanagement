﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.DTO.AnalyseAutomate
{
    public class LinkAnalyseAutomateToCommAutos
    {
        public List<int> CommAutosIds { get; set; }
        public List<int> IdAnalyseAutomates { get; set; }
    }
}
