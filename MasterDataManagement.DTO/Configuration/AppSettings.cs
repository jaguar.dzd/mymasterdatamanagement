﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.DTO.Configuration
{
    public class AppSettings
    {
        public string Secret { get; set; }
    }
}
