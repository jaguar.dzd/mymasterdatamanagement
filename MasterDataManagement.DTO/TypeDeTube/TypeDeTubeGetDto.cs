﻿namespace MasterDataManagement.DTO.TypeDeTube
{
    public class TypeDeTubeGetDto
    {
        public int IdTypeDeTube { get; set; }
        public string NomTypeDuTube { get; set; }
    }
}
