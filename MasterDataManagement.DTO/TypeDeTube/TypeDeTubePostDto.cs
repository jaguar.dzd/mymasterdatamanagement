﻿namespace MasterDataManagement.DTO.TypeDeTube
{
    public class TypeDeTubePostDto
    {
        public int IdTypeDeTube { get; set; }
        public string NomTypeDuTube { get; set; }
    }
}
