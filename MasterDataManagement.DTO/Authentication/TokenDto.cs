﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.DTO.Authentication
{
    public class TokenDto
    {
        public string UserId { get; set; }
    }
}
