﻿using MasterDataManagement.DTO.Droit;
using MasterDataManagement.DTO.Instance;
using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.DTO.Authentication
{
    public class GeneratedTokenInfo
    {
        public DateTime? IssuedAt { get; set; }
        public DateTime? ExpiresAt { get; set; }
        public string Token { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string UserName { get; set; }
        public List<DroitGetDto> Droits { get; set; }
        public List<InstanceGetDto> Sites { get; set; }
    }
}
