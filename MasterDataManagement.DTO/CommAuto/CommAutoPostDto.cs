﻿using System.ComponentModel.DataAnnotations;

namespace MasterDataManagement.DTO.CommAuto
{
    public class CommAutoPostDto
    {
        public int IdCommAuto { get; set; }
        public sbyte? Ordre { get; set; }
        [MaxLength(120)]
        public string Condition { get; set; }
        public short? Valeur { get; set; }
    }
}
