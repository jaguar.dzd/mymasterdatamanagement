﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.DTO.TransBasic
{
    public class TransBasicPutDto : TransBasicPostDto
    {
        public int IdTransfbasic { get; set; }
    }
}
