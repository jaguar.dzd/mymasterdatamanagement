﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.DTO.TransBasic
{
    public class TransBasicGetDto
    {
        public int IdTransfbasic { get; set; }
        public string OperateurLogique { get; set; }
        public short? Valeur1 { get; set; }
        public short? ValeurResultat { get; set; }
    }
}
