﻿using System.ComponentModel.DataAnnotations;

namespace MasterDataManagement.DTO.FonctionReflexes
{
    public class FonctionReflexesPostDto
    {
        public int IdFonctionsReflexes { get; set; }
        public sbyte? Ordre { get; set; }
        [MaxLength(120)]
        public string Condition { get; set; }
        [MaxLength(120)]
        public string Scenario { get; set; }
    }
}
