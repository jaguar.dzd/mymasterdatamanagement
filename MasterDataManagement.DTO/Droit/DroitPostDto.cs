﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MasterDataManagement.DTO.Droit
{
    public class DroitPostDto
    {
        public int IdDroit { get; set; }
        [MaxLength(70)]
        public string Droit { get; set; }
    }
}
