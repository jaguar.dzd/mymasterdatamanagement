﻿using System.ComponentModel.DataAnnotations;

namespace MasterDataManagement.DTO.Automate
{
    public class AutomateGetDto
    {
        public ushort IdAutomate { get; set; }
        public ushort? NumAutomate { get; set; }
        public string ProprietaireAutomate { get; set; }
        public string Code { get; set; }
        public string Secteur { get; set; }
        public string TypeCb { get; set; }
        public string ImprimanteCb { get; set; }
        public string ModeValidation { get; set; }
        public byte? OrdreValidation { get; set; }
        public byte? OrdreLiaisonLis { get; set; }
        public string Connexion { get; set; }
        public string ChargementAutoAcquisition { get; set; }
        public bool? IsGereDemandesSupression { get; set; }
        public bool? IsDesValidationBiologique { get; set; }
        public bool? IsNotArchiverTubes { get; set; }
        public ushort? ValeurParDefaut { get; set; }
        public ushort? ValeurPersonnalisee { get; set; }
        public string FormatFichePaillasse { get; set; }
        public string ModelCompteRendu { get; set; }
        public string DocumentProvisoir { get; set; }
        public string Protocole { get; set; }
        [MaxLength(2)]
        public string Start1 { get; set; }
        [MaxLength(2)]
        public string Start2 { get; set; }
        [MaxLength(2)]
        public string Start3 { get; set; }
        [MaxLength(2)]
        public string Start4 { get; set; }
        [MaxLength(2)]
        public string Start5 { get; set; }
        [MaxLength(2)]
        public string Start6 { get; set; }
        [MaxLength(2)]
        public string Start7 { get; set; }
        [MaxLength(2)]
        public string Start8 { get; set; }
        [MaxLength(2)]
        public string Start9 { get; set; }
        [MaxLength(2)]
        public string Start10 { get; set; }
        [MaxLength(2)]
        public string Stop1 { get; set; }
        [MaxLength(2)]
        public string Stop2 { get; set; }
        [MaxLength(2)]
        public string Stop3 { get; set; }
        [MaxLength(2)]
        public string Stop4 { get; set; }
        [MaxLength(2)]
        public string Stop5 { get; set; }
        [MaxLength(2)]
        public string Stop6 { get; set; }
        [MaxLength(2)]
        public string Stop7 { get; set; }
        [MaxLength(2)]
        public string Stop8 { get; set; }
        [MaxLength(2)]
        public string Stop9 { get; set; }
        [MaxLength(2)]
        public string Stop10 { get; set; }
        [MaxLength(2)]
        public string Identification1Left { get; set; }
        [MaxLength(2)]
        public string Identification2Left { get; set; }
        [MaxLength(2)]
        public string TypePrelevementLeft { get; set; }
        [MaxLength(2)]
        public string AnalyseLeft { get; set; }
        [MaxLength(2)]
        public string ResultatLeft { get; set; }
        [MaxLength(2)]
        public string DilutionLeft { get; set; }
        [MaxLength(2)]
        public string CommentaireLeft { get; set; }
        [MaxLength(2)]
        public string FlagLeft { get; set; }
        [MaxLength(3)]
        public string Identification1Right { get; set; }
        [MaxLength(3)]
        public string Identification2Right { get; set; }
        [MaxLength(3)]
        public string TypePrelevementRight { get; set; }
        [MaxLength(3)]
        public string AnalyseRight { get; set; }
        [MaxLength(3)]
        public string ResultatRight { get; set; }
        [MaxLength(3)]
        public string DilutionRight { get; set; }
        [MaxLength(3)]
        public string CommentaireRight { get; set; }
        [MaxLength(3)]
        public string FlagRight { get; set; }
        [MaxLength(3)]
        public string Increment { get; set; }
        [MaxLength(3)]
        public string NbPosition { get; set; }
        [MaxLength(3)]
        public string PlateauDepart { get; set; }
        [MaxLength(3)]
        public string PosDepart { get; set; }
        [MaxLength(3)]
        public string SeqDepart { get; set; }
        [MaxLength(6)]
        public string TailleMax { get; set; }
        public bool? IsForcerModeEscalve { get; set; }
        public short? Trace { get; set; }
        public short? Tempo { get; set; }
        public string MonNom { get; set; }
        public string CodePourReception { get; set; }
        public string ExceptionsValidationAutomate { get; set; }
        public bool? IsPrefixerPidNumDemande { get; set; }
        public byte? Port { get; set; }
        [MaxLength(10)]
        public string TypePort { get; set; }
        public byte? Canal1 { get; set; }
        public byte? Canal2 { get; set; }
        public byte? VitesseTransmission { get; set; }
        public byte? NbrBits { get; set; }
        public byte? StopBits { get; set; }
        public byte? Parite { get; set; }
        public byte? ContrôleDeFlux { get; set; }
        public bool? IsControleQualiteActif { get; set; }
        public byte? TrameOrEchantillon { get; set; }
        [MaxLength(2)]
        public string TrameResultatValeur { get; set; }
        [MaxLength(3)]
        public string TrameResultatPosition { get; set; }
        public byte? IdtubeOrIgrImpose { get; set; }
        public string TubeN { get; set; }
        public string NbreCarTronq { get; set; }
        public string NomPatient { get; set; }
        public bool? IsLotsProbatoires { get; set; }
        public bool? IsIgnorerLeLotCrConnexion { get; set; }
        public bool? IsInscrireAutoDansListe { get; set; }
        public byte? FormatNomPatientCq { get; set; }
        public bool? IsValidateAutoCq { get; set; }
        public bool? IsValidateResultpatientAutoCq { get; set; }
        public bool? IsBoutonForcerNonAutorized { get; set; }
        public byte? ControleEclair { get; set; }
        public byte? TypeInstrument { get; set; }
        public string TypeTupeInstrument { get; set; }
        public string ValmethodeIdentification { get; set; }
        public string ValmethodeFournisseur { get; set; }
        public string ValmethodeDetailDesTechniques { get; set; }
        public string ValmethodeAutre1 { get; set; }
        public string ValmethodeAutre2 { get; set; }
        public string CoutPatientTypeAutomate { get; set; }
        public bool? IsPreanalyticAutomate { get; set; }
        public bool? IsAfficherJournalConnex { get; set; }
        public string RepWhereDupliquer { get; set; }
        public bool? IsRespectArboYyyymm { get; set; }
        public byte? PieceJointesToSend { get; set; }
    }
}
