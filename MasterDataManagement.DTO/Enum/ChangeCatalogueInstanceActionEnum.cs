﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace MasterDataManagement.DTO.Enum
{
    public enum ApproveOrRefuseCatalogueInstanceEnum
    {
        [Description("Approve")]
        Approve = 1,
        [Description("Refuse")]
        Refuse = 2
    }
}