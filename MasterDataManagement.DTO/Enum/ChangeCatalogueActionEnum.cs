﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace MasterDataManagement.DTO.Enum
{
    public enum ValidateOrRejectActionEnum
    {
        [Description("Validate")]
        Validate = 1,
        [Description("Reject")]
        Reject = 2
    }
}
