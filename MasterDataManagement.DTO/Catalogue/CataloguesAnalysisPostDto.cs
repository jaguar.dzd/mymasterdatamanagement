﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.DTO.Catalogue
{
    public class CataloguesAnalysisPostDto
    {
        public List<int> Catalogues { get; set; }
        public List<int> Analysis { get; set; }
    }
}
