﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MasterDataManagement.DTO.Catalogue
{
    public class CatalogueUpdateDto : CataloguePostDto
    {
        public int IdCatalogue { get; set; }
    }
}
