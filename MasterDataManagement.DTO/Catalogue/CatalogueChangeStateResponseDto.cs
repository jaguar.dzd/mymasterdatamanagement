﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.DTO.Catalogue
{
    public class CatalogueChangeStateResponseDto
    {
        public string Message { get; set; }
        public bool Success { get; set; }
    }
}
