﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MasterDataManagement.DTO.Catalogue
{
    public class CataloguePostDto
    {
        public string NomCatalogue { get; set; }
        [MaxLength(50)]
        public string CreatorName { get; set; }
    }
}
