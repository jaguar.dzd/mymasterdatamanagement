﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MasterDataManagement.DTO.Catalogue
{
    public class CataloguesAnalysesPostDto
    {
        public List<int> CataloguesIds { get; set; }
        public List<int> AnalysesIds { get; set; }
    }
}
