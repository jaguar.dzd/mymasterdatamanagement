﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MasterDataManagement.DTO.Catalogue
{
    public class CatalogueDuplicatePostDto
    {
        public int IdCatalogue { get; set; }
        public string NouveauNom { get; set; }
        public DuplicationOptionEnum DuplicationOption { get; set; }
        [MaxLength(50)]
        public string CreatorName { get; set; }
    }

    public enum DuplicationOptionEnum
    {
        WithAnalyse = 1,
        WithAutomate = 2,
        WithAnalyseAndAutomate = 3
    }
}
