﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MasterDataManagement.DTO.Analyse;
using MasterDataManagement.DTO.Automate;
using MasterDataManagement.DTO.Instance;

namespace MasterDataManagement.DTO.Catalogue
{
    public class CatalogueGetDto
    {
        public int IdCatalogue { get; set; }
        public string NomCatalogue { get; set; }

        public int Version { get; set; }
        [MaxLength(50)]
        public string Statut { get; set; }
    }


    public class CatalogueDetailDto
    {
        public int IdCatalogue { get; set; }
        public string NomCatalogue { get; set; }
        public int? Version { get; set; }
        [MaxLength(50)]
        public string Statut { get; set; }
        public int NbAnalyses { get; set; }
        public int NbAutomates { get; set; }
        public List<InstanceGetDto> Sites { get; set; }
        public List<AnalyseGetDto> Analyses { get; set; }
        public List<AutomateGetDto> Automates { get; set; }
        public bool? IsActivated { get; set; }
    }
}
