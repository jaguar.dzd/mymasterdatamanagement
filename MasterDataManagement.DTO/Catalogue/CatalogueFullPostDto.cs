﻿using MasterDataManagement.DTO.Analyse;
using MasterDataManagement.DTO.Automate;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MasterDataManagement.DTO.Catalogue
{
    public class CatalogueFullPostDto
    {
        public CatalogueUpdateDto Catalogue { get; set; }
        public List<int> Analyses { get; set; }
        public List<int> Automates { get; set; }
    }
}
