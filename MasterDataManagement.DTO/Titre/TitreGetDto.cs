﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.DTO.Titre
{
    public class TitreGetDto
    {
        public int IdTitres { get; set; }
        public sbyte? Ordre { get; set; }
        public string Titre { get; set; }
        public sbyte? Style { get; set; }
    }
}
