﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.DTO.Titre
{
    public class TitrePutDto : TitrePostDto
    {
        public int IdTitres { get; set; }
    }
}
