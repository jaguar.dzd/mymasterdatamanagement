﻿using MasterDataManagement.DTO.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.DTO.InstanceCatalogue
{
    public class InstanceCataloguePostDto
    {
        public List<InstanceCatalogueDetailPostDto> Data { get; set; }
    }

    public class InstanceCatalogueDetailPostDto
    {
        public int CatalogueId { get; set; }
        public int InstanceId { get; set; }
        public ApproveOrRefuseCatalogueInstanceEnum Action { get; set; }
    }
}
