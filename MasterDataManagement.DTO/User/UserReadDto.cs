﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.DTO.User
{
    public class UserReadDto
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
