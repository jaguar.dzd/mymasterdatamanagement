﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MasterDataManagement.DTO.User
{
    public class UserPostDto
    {
        [Required]
        public string Email { get; set; }
        [Required] 
        public string FirstName { get; set; }
        [Required] 
        public string LastName { get; set; }
        [Required] 
        public string MotDePasse { get; set; }
        public string Username { get; set; }
    }
}
