﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.DTO.User
{
    public class UserPostResponseDto
    {
        public bool Success { get; set; }
        public string Reason { get; set; }
    }
}
