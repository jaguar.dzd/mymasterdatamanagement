﻿
using System;
using System.Collections.Generic;
using System.Text;
using MasterDataManagement.DTO.Droit;
using MasterDataManagement.DTO.Instance;

namespace MasterDataManagement.DTO.User
{
    public class UserDetailGetDto
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Id { get; set; }
        public List<DroitGetDto> Droits { get; set; }
        public List<InstanceGetDto> Sites { get; set; }
    }
}
