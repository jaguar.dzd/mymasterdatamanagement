﻿using System.ComponentModel.DataAnnotations;

namespace MasterDataManagement.DTO.Secteur
{
    public class SecteurGetDto
    {
        public int IdSecteur { get; set; }
        [MaxLength(120)]
        public string SecteurNom { get; set; }
    }
}
