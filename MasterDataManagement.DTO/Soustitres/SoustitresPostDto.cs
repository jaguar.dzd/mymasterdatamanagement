﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.DTO.Soustitres
{
    public class SoustitresPostDto
    {
        public sbyte? Ordre { get; set; }
        public string Stitre { get; set; }
        public sbyte? Style { get; set; }
    }
}
