﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.DTO.Soustitres
{
    public class SoustitresPutDto : SoustitresPostDto
    {
        public int IdSousTitres { get; set; }
    }
}
