﻿using System.ComponentModel.DataAnnotations;

namespace MasterDataManagement.DTO.GroupeAnalyse
{
    public class GroupeAnalyseGetDto
    {
        public int IdGroupeAnalyse { get; set; }
        [MaxLength(150)]
        public string NomGroupe { get; set; }
    }
}
