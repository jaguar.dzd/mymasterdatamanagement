﻿using System.ComponentModel.DataAnnotations;

namespace MasterDataManagement.DTO.GroupeAnalyse
{
    public class GroupeAnalysePostDto
    {
        public int IdGroupeAnalyse { get; set; }
        [MaxLength(150)]
        public string NomGroupe { get; set; }
    }
}
