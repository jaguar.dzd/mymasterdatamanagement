﻿using System.ComponentModel.DataAnnotations;

namespace MasterDataManagement.DTO.Instance
{
    public class InstanceGetDto
    {
        public int IdInstance { get; set; }
        
        public string Code { get; set; }
        
        public string Nom { get; set; }
        
        public string NomMasterInstance { get; set; }
        
        public string NomContactSite { get; set; }
        
        public string EmailContactSite { get; set; }
        
        public string TelContactSite { get; set; }
        
        public string AdresseSite { get; set; }
        
        public string TelSite { get; set; }
        
        public string IpInstance { get; set; }
    }
}
