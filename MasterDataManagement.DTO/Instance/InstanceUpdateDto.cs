﻿using System.ComponentModel.DataAnnotations;

namespace MasterDataManagement.DTO.Instance
{
    public class InstanceUpdateDto : InstancePostDto
    {
        public byte IdInstance { get; set; }
    }
}
