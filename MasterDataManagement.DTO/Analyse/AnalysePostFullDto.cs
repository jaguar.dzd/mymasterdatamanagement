﻿using MasterDataManagement.DTO.Bilan;
using MasterDataManagement.DTO.Catalogue;
using MasterDataManagement.DTO.CommAuto;
using MasterDataManagement.DTO.Evaluation;
using MasterDataManagement.DTO.FonctionReflexes;
using MasterDataManagement.DTO.IncidencesDesFlags;
using MasterDataManagement.DTO.Site;
using MasterDataManagement.DTO.TransBasic;
using MasterDataManagement.DTO.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.DTO.Analyse
{
    public class AnalysePostFullDto
    {
        public AnalysePostDto Analyse { get; set; }
        public List<TransBasicPostDto> TransBasics { get; set; }
        public List<TransformationPostDto> Transformations { get; set; }
        public List<IncidencedesflagsPostDto> IncidenceDesFlags { get; set; }
        public List<CommAutoPostDto> CommAutos { get; set; }
        public List<BilanPostDto> Bilans { get; set; }
        public List<ValidationPostDto> Validations { get; set; }
        public List<EvaluationPostDto> Evaluations { get; set; }
        public List<FonctionReflexesPostDto> FonctionReflexes { get; set; }
    }
}
