﻿using System.ComponentModel.DataAnnotations;

namespace MasterDataManagement.DTO.Analyse
{
    public class AnalysePostDto
    {
        public int IdAnalyse { get; set; }
        [MaxLength(120)]
        public string Code { get; set; }
        [MaxLength(120)]
        public string CodeTransmission1 { get; set; }
        [MaxLength(120)]
        public string CodeTransmission2 { get; set; }
        [MaxLength(120)]
        public string AutreCode { get; set; }
        public short? Rang { get; set; }
        public string Texte { get; set; }
        [MaxLength(120)]
        public string Paillasse2 { get; set; }
        public int? PaillasseIdPaillasse { get; set; }
        public int? TypeDeTubeIdTypeDeTube { get; set; }
        public bool? IsBilanOrProfil { get; set; }
        [MaxLength(120)]
        public string CodeTransmExComplem { get; set; }
        public bool? IsOneInstanceParPatient { get; set; }
        public bool? HasGraphique { get; set; }
        public string IdgroupeAnalyseValidation { get; set; }
        public bool? IsResultatArenvoyer { get; set; }
        public bool? MemeSiNi { get; set; }
        public int? Impression { get; set; }
        public bool? HasNotCommentaireAuto { get; set; }
        [MaxLength(120)]
        public string TypeResult { get; set; }
        [MaxLength(50)]
        public string Unites1 { get; set; }
        [MaxLength(50)]
        public string Unites2 { get; set; }
        public short? Decimales1 { get; set; }
        public short? Decimales2 { get; set; }
        public short? Coefficient { get; set; }
        public short? Longueur { get; set; }
        [MaxLength(50)]
        public string ValeurParDefaut { get; set; }
        [MaxLength(50)]
        public string FormuleX1 { get; set; }
        [MaxLength(50)]
        public string FormuleX2 { get; set; }
        [MaxLength(100)]
        public string Formule { get; set; }
        public string Titre { get; set; }
        public string SousTitre { get; set; }
        public string Ligne1AvantResult { get; set; }
        public string Ligne2AvantResult { get; set; }
        public string Ligne3AvantResult { get; set; }
        public string Ligne4AvantResult { get; set; }
        public string Ligne5AvantResult { get; set; }
        public string Ligne1ApresResult { get; set; }
        public string Ligne2ApresResult { get; set; }
        public string Ligne3ApresResult { get; set; }
        public string Ligne4ApresResult { get; set; }
        public string Ligne5ApresResult { get; set; }
        public string Ligne6ApresResult { get; set; }
        public string TexteOptionnel1 { get; set; }
        public string TexteOptionnel2 { get; set; }
        public string TexteOptionnel3 { get; set; }
        public string TexteOptionnel4 { get; set; }
        public string TexteOptionnel5 { get; set; }
        public string TexteOptionnel6 { get; set; }
        public string TexteOptionnel7 { get; set; }
        public string TexteOptionnel8 { get; set; }
        public string TexteOptionnel9 { get; set; }
        public string TexteOptionnel10 { get; set; }
        public string TexteOptionnel11 { get; set; }
        public string TexteOptionnel12 { get; set; }
        public string TexteOptionnel13 { get; set; }
        public string TexteOptionnel14 { get; set; }
        public string TexteOptionnel15 { get; set; }
        public short? Deltacheck { get; set; }
        public short? Minvaleur { get; set; }
        public short? DureeValidite { get; set; }
        public bool? IsanalyseInscriteCq { get; set; }
        public short? ReglesDeWestgard { get; set; }
        public sbyte? WestgardPersoX { get; set; }
        public sbyte? WestgardPersoT { get; set; }
        public bool? IsValidationConditionCq { get; set; }
        public bool? IsControleEclairActifInactif { get; set; }
        public short? MoyenneMobileTotalpoints { get; set; }
        public short? MoyenneMobileTriParGroupe { get; set; }
        public bool? HasComptage { get; set; }
        public short? NbrAnalyseMinCalcul { get; set; }
        public double? SeuilRepasse { get; set; }
        public short? FacteurDilution { get; set; }
        public short? ValeurAajouter { get; set; }
        public sbyte? Arrondi { get; set; }
        [MaxLength(4)]
        public string CodeAller1 { get; set; }
        [MaxLength(4)]
        public string CodeAller2 { get; set; }
        [MaxLength(4)]
        public string CodeAller3 { get; set; }
        [MaxLength(4)]
        public string CodeAller4 { get; set; }
        [MaxLength(4)]
        public string CodeAller5 { get; set; }
        [MaxLength(4)]
        public string CodeAller6 { get; set; }
        [MaxLength(4)]
        public string CodeAller7 { get; set; }
        [MaxLength(4)]
        public string CodeAller8 { get; set; }
        [MaxLength(4)]
        public string CodeAller9 { get; set; }
        [MaxLength(4)]
        public string CodeRetour1 { get; set; }
        [MaxLength(4)]
        public string CodeRetour2 { get; set; }
        public bool? IsCreationAutomatiqueAnalyse { get; set; }
        public bool? GenererFlag { get; set; }
        public double? DifValeurSupa { get; set; }
        public double? MinvaleurEntrePassages { get; set; }
        public bool? MiseControleSiAlarmDelta { get; set; }
        public bool? MiseControleSiDeltaAbsent { get; set; }
        public bool? MiseControleAvecDilution { get; set; }
        [MaxLength(50)]
        public string SiInfA { get; set; }
        [MaxLength(50)]
        public string EtOuSupA { get; set; }
        [MaxLength(50)]
        public string EtOuEgalA { get; set; }
        [MaxLength(50)]
        public string RepasserAvecLaDilution { get; set; }
        public bool? IsMiseEnControleSurCondi { get; set; }
        public sbyte? EtatSortieConnexion { get; set; }
        public bool? IsThisAnalyseProfil { get; set; }
        public int? TitresIdTitres { get; set; }
        public int? SousTitresIdSousTitres { get; set; }
        public int? GroupeAnalyseIdGroupeAnalyse { get; set; }

    }
}
