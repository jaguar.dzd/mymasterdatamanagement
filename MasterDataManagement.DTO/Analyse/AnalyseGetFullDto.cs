﻿using MasterDataManagement.DTO.Bilan;
using MasterDataManagement.DTO.Catalogue;
using MasterDataManagement.DTO.CommAuto;
using MasterDataManagement.DTO.Evaluation;
using MasterDataManagement.DTO.FonctionReflexes;
using MasterDataManagement.DTO.IncidencesDesFlags;
using MasterDataManagement.DTO.Site;
using MasterDataManagement.DTO.TransBasic;
using MasterDataManagement.DTO.Transformation;
using MasterDataManagement.DTO.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.DTO.Analyse
{
    public class AnalyseGetFullDto
    {
        public AnalyseGetDto Analyse { get; set; }
        public List<TransBasicGetDto> TransBasics { get; set; }
        public List<TransformationGetDto> Transformations { get; set; }
        public List<IncidenceDesFlagsGetDto> IncidenceDesFlags  { get; set; }
        public List<CommAutoGetDto> CommAutos { get; set; }
        public List<BilanGetDto> Bilans { get; set; }
        public List<ValidationGetDto> Validations { get; set; }
        public List<EvaluationGetDto> Evaluations { get; set; }
        public List<FonctionReflexesGetDto> FonctionReflexes { get; set; }
    }
}
