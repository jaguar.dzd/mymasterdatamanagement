﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.DTO.IncidencesDesFlags
{
    public class IncidencedesflagsPutDto : IncidencedesflagsPostDto
    {
        public int Idincidencedesflags { get; set; }
    }
}
