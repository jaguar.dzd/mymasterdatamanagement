﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.DTO.IncidencesDesFlags
{
    public class IncidenceDesFlagsGetDto
    {
        public int Idincidencedesflags { get; set; }
        public string ValeurDuFlag { get; set; }
        public string ValeurDuResultat { get; set; }
    }
}
