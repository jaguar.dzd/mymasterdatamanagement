﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.DTO.IncidencesDesFlags
{
    public class IncidencedesflagsPostDto
    {
        public string ValeurDuFlag { get; set; }
        public string ValeurDuResultat { get; set; }
    }
}
