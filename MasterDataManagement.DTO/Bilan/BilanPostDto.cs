﻿using System.ComponentModel.DataAnnotations;

namespace MasterDataManagement.DTO.Bilan
{
    public class BilanPostDto
    {
        public int IdBilan { get; set; }
        [MaxLength(20)]
        public string Analyse1 { get; set; }
        [MaxLength(20)]
        public string Analyse2 { get; set; }
        [MaxLength(20)]
        public string Analyse3 { get; set; }
        [MaxLength(20)]
        public string Analyse4 { get; set; }
        [MaxLength(20)]
        public string Analyse5 { get; set; }
        [MaxLength(20)]
        public string Analyse6 { get; set; }
        [MaxLength(20)]
        public string Analyse7 { get; set; }
        [MaxLength(20)]
        public string Analyse8 { get; set; }
        [MaxLength(20)]
        public string Analyse9 { get; set; }
        [MaxLength(20)]
        public string Analyse10 { get; set; }
        [MaxLength(20)]
        public string Analyse11 { get; set; }
        [MaxLength(20)]
        public string Analyse12 { get; set; }
        [MaxLength(20)]
        public string Analyse13 { get; set; }
        [MaxLength(20)]
        public string Analyse14 { get; set; }
        [MaxLength(20)]
        public string Analyse15 { get; set; }
        [MaxLength(20)]
        public string Analyse16 { get; set; }
        [MaxLength(20)]
        public string Analyse17 { get; set; }
        [MaxLength(20)]
        public string Analyse18 { get; set; }
        [MaxLength(20)]
        public string Analyse19 { get; set; }
        [MaxLength(20)]
        public string Analyse20 { get; set; }
        [MaxLength(20)]
        public string Analyse21 { get; set; }
        [MaxLength(20)]
        public string Analyse22 { get; set; }
        [MaxLength(20)]
        public string Analyse23 { get; set; }
        [MaxLength(20)]
        public string Analyse24 { get; set; }
    }
}
