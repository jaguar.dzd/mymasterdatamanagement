﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.DTO.Transformation
{
    public class TransformationGetDto
    {
        public int IdTransformations { get; set; }
        public string OperateurLogique { get; set; }
        public short? Valeur1 { get; set; }
        public short? ValeurResultat { get; set; }
    }
}
