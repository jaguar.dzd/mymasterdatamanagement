﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDataManagement.DTO.Site
{
    public class TransformationPutDto : TransformationPostDto
    {
        public int IdTransformations { get; set; }
    }
}
