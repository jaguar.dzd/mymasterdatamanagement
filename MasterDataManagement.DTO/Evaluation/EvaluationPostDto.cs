﻿using System.ComponentModel.DataAnnotations;

namespace MasterDataManagement.DTO.Evaluation
{
    public class EvaluationPostDto
    {
        public int IdEvaluations { get; set; }
        public sbyte? Ordre { get; set; }
        [MaxLength(120)]
        public string Condition { get; set; }
        public short? NouvelleValeur { get; set; }
    }
}
