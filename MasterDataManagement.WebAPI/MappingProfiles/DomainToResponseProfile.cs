﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MasterDataManagement.Domain.Models;
using MasterDataManagement.Domain.Others;
using MasterDataManagement.DTO.Analyse;
using MasterDataManagement.DTO.AnalyseAutomate;
using MasterDataManagement.DTO.Automate;
using MasterDataManagement.DTO.Bilan;
using MasterDataManagement.DTO.Catalogue;
using MasterDataManagement.DTO.CommAuto;
using MasterDataManagement.DTO.Droit;
using MasterDataManagement.DTO.Evaluation;
using MasterDataManagement.DTO.FonctionReflexes;
using MasterDataManagement.DTO.GroupeAnalyse;
using MasterDataManagement.DTO.IncidencesDesFlags;
using MasterDataManagement.DTO.Instance;
using MasterDataManagement.DTO.Paillasse;
using MasterDataManagement.DTO.Secteur;
using MasterDataManagement.DTO.Soustitres;
using MasterDataManagement.DTO.Titre;
using MasterDataManagement.DTO.TransBasic;
using MasterDataManagement.DTO.Transformation;
using MasterDataManagement.DTO.TypeDeTube;
using MasterDataManagement.DTO.Validation;

namespace MasterDataManagement.WebAPI.MappingProfiles
{
    /// <summary>
    /// 
    /// </summary>
    public class DomainToResponseProfile : Profile
    {
        /// <summary>
        /// 
        /// </summary>
        public DomainToResponseProfile()
        {
            CreateMap<Analyse, AnalyseGetDto>();
            CreateMap<Catalogue, CatalogueGetDto>();
            CreateMap<Commauto, CommAutoGetDto>();
            CreateMap<Evaluations, EvaluationGetDto>();
            CreateMap<Fonctionsreflexes, FonctionReflexesGetDto>();
            CreateMap<Groupeanalyse, GroupeAnalyseGetDto>();
            CreateMap<Instance, InstanceGetDto>();
            CreateMap<Paillasse, PaillasseGetDto>();
            CreateMap<Typedetube, TypeDeTubeGetDto>();
            CreateMap<Validations, ValidationGetDto>();
            CreateMap<Automate, AutomateGetDto>();
            CreateMap<Incidencedesflags, IncidenceDesFlagsGetDto>();
            CreateMap<Transformations, TransformationGetDto>();
            CreateMap<Soustitres, SoustitresGetDto>();
            CreateMap<Secteur, SecteurGetDto>();
            CreateMap<Bilan, BilanGetDto>();
            CreateMap<Titres, TitreGetDto>();
            CreateMap<Droit, DroitGetDto>();
            CreateMap<AnalyseAutomate, AnalyseAutomateGetDto>();
            CreateMap<Transfbasic, TransBasicGetDto>();
            CreateMap<FullAnalyse, AnalyseGetFullDto>();
        }
    }
}
