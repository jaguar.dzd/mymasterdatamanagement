﻿using MasterDataManagement.Domain.Bll;
using MasterDataManagement.Domain.Repositories;
using MasterDataManagement.Domain.Services;
using MasterDataManagement.Infrastructure;
using MasterDataManagement.Infrastructure.Repositories;
using MasterDataManagement.Infrastructure.Services;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace MasterDataManagement.WebAPI.IocConfig
{
    /// <summary>
    /// 
    /// </summary>
    public static class IocExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public static void ConfigureIoc(this IServiceCollection services)
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            ConfigureServices(services);

            ConfigureRepositories(services);
            
            ConfigureBusinessLogic(services);
            
            ConfigureOtherThings(services);
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<ITokenGenerator, TokenGenerator>();
            services.AddScoped<ITokenValidator, TokenValidator>();
            services.AddScoped<IHelperService, HelperService>();
        }

        private static void ConfigureRepositories(IServiceCollection services)
        {
            services.AddScoped<IUserRepo, UserRepo>();
            services.AddScoped<IAutomateRepo, AutomateRepo>();
            services.AddScoped<IInstanceRepo, InstanceRepo>();
            services.AddScoped<ICatalogueRepo, CatalogueRepo>();
            services.AddScoped<ICommAutoRepo, CommAutoRepo>();
            services.AddScoped<IEvaluationRepo, EvaluationRepo>();
            services.AddScoped<IFonctionReflexesRepo, FonctionReflexesRepo>();
            services.AddScoped<IGroupeAnalyseRepo, GroupeAnalyseRepo>();
            services.AddScoped<IPaillasseRepo, PaillasseRepo>();
            services.AddScoped<ITypeDeTubeRepo, TypeDeTubeRepo>();
            services.AddScoped<IValidationRepo, ValidationRepo>();
            services.AddScoped<IAnalyseRepo, AnalyseRepo>();
            services.AddScoped<IIncidenceDesFlagsRepo, IncidenceDesFlagsRepo>();
            services.AddScoped<ITransformationRepo, TransformationRepo>();
            services.AddScoped<ISoustitresRepo, SoustitresRepo>();
            services.AddScoped<IBilanRepo, BilanRepo>();
            services.AddScoped<ISecteurRepo, SecteurRepo>();
            services.AddScoped<IUserInstanceRepo, UserInstanceRepo>();
            services.AddScoped<ITitreRepo, TitreRepo>();
            services.AddScoped<IDroitRepo, DroitRepo>();
            services.AddScoped<IAnalyseAutomateRepo, AnalyseAutomateRepo>();
            services.AddScoped<ITransBasicRepo, TransBasicRepo>();
            services.AddScoped<IInstanceCatalogueRepo, InstanceCatalogueRepo>();
        }

        private static void ConfigureBusinessLogic(IServiceCollection services)
        {
            services.AddScoped<IAuthBll, AuthBll>();
            services.AddScoped<IUserBll, UserBll>();
            services.AddScoped<IAutomateBll, AutomateBll>();
            services.AddScoped<IInstanceBll, InstanceBll>();
            services.AddScoped<ICatalogueBll, CatalogueBll>();
            services.AddScoped<ICommAutoBll, CommAutoBll>();
            services.AddScoped<IEvaluationBll, EvaluationBll>();
            services.AddScoped<IFonctionReflexesBll, FonctionReflexesBll>();
            services.AddScoped<IGroupeAnalyseBll, GroupeAnalyseBll>();
            services.AddScoped<IPaillasseBll, PaillasseBll>();
            services.AddScoped<ITypeDeTubeBll, TypeDeTubeBll>();
            services.AddScoped<IValidationBll, ValidationBll>();
            services.AddScoped<IAnalyseBll, AnalyseBll>();
            services.AddScoped<IIncidenceDesFlagsBll, IncidenceDesFlagsBll>();
            services.AddScoped<ITransformationBll, TransformationBll>();
            services.AddScoped<ISoustitresBll, SoustitresBll>();
            services.AddScoped<IBilanBll, BilanBll>();
            services.AddScoped<ISecteurBll, SecteurBll>();
            services.AddScoped<IUserInstanceBll, UserInstanceBll>();
            services.AddScoped<ITitreBll, TitreBll>();
            services.AddScoped<IDroitBll, DroitBll>();
            services.AddScoped<IAnalyseAutomateBll, AnalyseAutomateBll>();
            services.AddScoped<ITransBasicBll, TransBasicBll>();
        }

        private static void ConfigureOtherThings(IServiceCollection services)
        {
            //throw new NotImplementedException();
        }
    }
}
