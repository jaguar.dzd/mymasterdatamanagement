﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using MasterDataManagement.Domain.Bll;
using MasterDataManagement.DTO.Automate;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace MasterDataManagement.WebAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AutomateController : ControllerBase
    {
        private readonly IAutomateBll _automateBll;
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        public AutomateController(IAutomateBll automateBll, IMapper mapper)
        {
            _automateBll = automateBll;
            _mapper = mapper;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idAutomate"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{idAutomate}")]
        public async Task<ActionResult<AutomateGetDto>> GetAutomate(ushort idAutomate)
        {
            var automate = await _automateBll.GetAutomateById(idAutomate);

            if (automate == null)
                return BadRequest("automate_not_found");

            var result = _mapper.Map<AutomateGetDto>(automate);

            return Ok(result);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<AutomateGetDto>>> GetAutomates()
        {
            var automates = await _automateBll.GetAutomates();

            var result = _mapper.Map<List<AutomateGetDto>>(automates);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="automatePostDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateAutomate(AutomatePostDto automatePostDto)
        {
            await _automateBll.CreateAutomate(automatePostDto);

            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="automatePostDtos"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("CreateAutomates")]
        public async Task<IActionResult> CreateAutomates(List<AutomatePostDto> automatePostDtos)
        {
            await _automateBll.CreateAutomates(automatePostDtos);

            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="automatePostDto"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateAutomate(AutomatePostDto automatePostDto)
        {
            var automate = await _automateBll.UpdateAutomate(automatePostDto);

            if (automate == null)
                return BadRequest("automate_not_found");

            var result = _mapper.Map<AutomateGetDto>(automate);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idAutomate"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{idAutomate}")]
        public async Task<IActionResult> DeleteAutomate(ushort idAutomate)
        {
            var automate = await _automateBll.DeleteAutomate(idAutomate);
           
            if (automate == null)
                return BadRequest("automate_not_found");

            return Ok();
        }
    }
}
