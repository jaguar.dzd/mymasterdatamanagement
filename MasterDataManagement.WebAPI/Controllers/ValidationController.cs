﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using MasterDataManagement.Domain.Bll;
using MasterDataManagement.DTO.Validation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace MasterDataManagement.WebAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ValidationController : ControllerBase
    {
        private readonly IValidationBll _validationBll;
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        public ValidationController(IValidationBll validationBll, IMapper mapper)
        {
            _validationBll = validationBll;
            _mapper = mapper;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idValidation"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{idValidation}")]
        public async Task<ActionResult<ValidationGetDto>> GetValidation(int idValidation)
        {
            var validation = await _validationBll.GetValidation(idValidation);

            if (validation == null)
                return BadRequest("Validation_not_found");

            var result = _mapper.Map<ValidationGetDto>(validation);

            return Ok(result);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<ValidationGetDto>>> GetValidations()
        {
            var validations = await _validationBll.GetValidations();

            var result = _mapper.Map<List<ValidationGetDto>>(validations);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="validationPostDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateValidation(ValidationPostDto validationPostDto)
        {
            await _validationBll.CreateValidation(validationPostDto);

            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="validationPostDto"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateValidation(ValidationPostDto validationPostDto)
        {
            var validation = await _validationBll.UpdateValidation(validationPostDto);

            if (validation == null)
                return BadRequest("Validation_not_found");

            var result = _mapper.Map<ValidationGetDto>(validation);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idValidation"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{idValidation}")]
        public async Task<IActionResult> DeleteValidation(int idValidation)
        {
            var validation = await _validationBll.DeleteValidation(idValidation);

            if (validation == null)
                return BadRequest("Validation_not_found");

            return Ok();
        }
    }
}
