﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using MasterDataManagement.Domain.Bll;
using MasterDataManagement.DTO.Site;
using MasterDataManagement.DTO.Transformation;
using Microsoft.AspNetCore.Mvc;

namespace MasterDataManagement.WebAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class TransformationController : ControllerBase
    {
        private readonly ITransformationBll transformationBll;
        private readonly IMapper mapper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transformationBll"></param>
        /// <param name="mapper"></param>
        public TransformationController(ITransformationBll transformationBll, IMapper mapper)
        {
            this.transformationBll = transformationBll;
            this.mapper = mapper;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idTransformation"></param>
        /// <returns></returns>
        [HttpGet] 
        [Route("{idTransformation}")]
        public async Task<ActionResult<TransformationGetDto>> GetTransformation(int idTransformation)
        {
            var transformation = await transformationBll.GetTransformation(idTransformation);
            if (transformation == null)
            {
                return BadRequest("Transformation_not_found");
            }

            var result = mapper.Map<TransformationGetDto>(transformation);
            
            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<TransformationGetDto>>> GetTransformations()
        {
            var transformations = await transformationBll.GetTransformations();

            var result = mapper.Map<List<TransformationGetDto>>(transformations);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transformationPostDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateTransformation(TransformationPostDto transformationPostDto)
        {
            await transformationBll.CreateTransformation(transformationPostDto);
            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transformationPutDto"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateTransformation(TransformationPutDto transformationPutDto)
        {
            var transf = await transformationBll.UpdateTransformation(transformationPutDto);
            if (transf == null)
            {
                return BadRequest("Transformation_not_found");
            }

            var result = mapper.Map<TransformationGetDto>(transf);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idTransformation"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{idTransformation}")]
        public async Task<IActionResult> DeleteTransformation(int idTransformation)
        {
            var transf = await transformationBll.DeleteTypeDeTube(idTransformation);

            if (transf == null)
                return BadRequest("Transformation_not_found");

            return Ok();
        }
    }
}