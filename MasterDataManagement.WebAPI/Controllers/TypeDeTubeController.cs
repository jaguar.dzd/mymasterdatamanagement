﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using MasterDataManagement.Domain.Bll;
using MasterDataManagement.DTO.TypeDeTube;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace MasterDataManagement.WebAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TypeDeTubeController : ControllerBase
    {
        private readonly ITypeDeTubeBll _typeDeTubeBll;
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        public TypeDeTubeController(ITypeDeTubeBll typeDeTubeBll, IMapper mapper)
        {
            _typeDeTubeBll = typeDeTubeBll;
            _mapper = mapper;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idTypeDeTube"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{idTypeDeTube}")]
        public async Task<ActionResult<TypeDeTubeGetDto>> GetTypeDeTube(int idTypeDeTube)
        {
            var typeDeTube = await _typeDeTubeBll.GetTypeDeTube(idTypeDeTube);

            if (typeDeTube == null)
                return BadRequest("TypeDeTube_not_found");

            var result = _mapper.Map<TypeDeTubeGetDto>(typeDeTube);

            return Ok(result);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<TypeDeTubeGetDto>>> GetTypeDeTubes()
        {
            var typeDeTubes = await _typeDeTubeBll.GetTypeDeTubes();

            var result = _mapper.Map<List<TypeDeTubeGetDto>>(typeDeTubes);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="typeDeTubePostDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateTypeDeTube(TypeDeTubePostDto typeDeTubePostDto)
        {
            await _typeDeTubeBll.CreateTypeDeTube(typeDeTubePostDto);

            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="typeDeTubePostDto"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateTypeDeTube(TypeDeTubePostDto typeDeTubePostDto)
        {
            var typeDeTube = await _typeDeTubeBll.UpdateTypeDeTube(typeDeTubePostDto);

            if (typeDeTube == null)
                return BadRequest("TypeDeTube_not_found");

            var result = _mapper.Map<TypeDeTubeGetDto>(typeDeTube);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idTypeDeTube"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{idTypeDeTube}")]
        public async Task<IActionResult> DeleteTypeDeTube(int idTypeDeTube)
        {
            var typeDeTube = await _typeDeTubeBll.DeleteTypeDeTube(idTypeDeTube);

            if (typeDeTube == null)
                return BadRequest("TypeDeTube_not_found");

            return Ok();
        }
    }
}
