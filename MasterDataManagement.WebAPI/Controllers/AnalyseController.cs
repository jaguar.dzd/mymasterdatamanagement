﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using AutoMapper;
using CsvHelper;
using MasterDataManagement.Domain.Bll;
using MasterDataManagement.DTO.Analyse;
using Microsoft.AspNetCore.Mvc;


namespace MasterDataManagement.WebAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class AnalyseController : ControllerBase
    {
        private readonly IAnalyseBll _analyseBll;
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        public AnalyseController(IAnalyseBll analyseBll, IMapper mapper)
        {
            _analyseBll = analyseBll;
            _mapper = mapper;
        }


        /// <summary>
        /// Get analyse by id
        /// </summary>
        /// <param name="idAnalyse"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{idAnalyse}")]
        public async Task<ActionResult<AnalyseGetDto>> GetAnalyse(int idAnalyse)
        {
            var analyse = await _analyseBll.GetAnalyse(idAnalyse);

            if (analyse == null)
                return BadRequest("Analyse_not_found");

            var result = _mapper.Map<AnalyseGetDto>(analyse);

            return Ok(result);
        }


        /// <summary>
        /// Get all analyse
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<AnalyseGetDto>>> GetAnalyses()
        {
            var analyses = await _analyseBll.GetAnalyses();

            var result = _mapper.Map<List<AnalyseGetDto>>(analyses);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idAnalyse"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetFullAnalyse/{idAnalyse}")]
        public async Task<ActionResult<AnalyseGetFullDto>> GetFullAnalyse(int idAnalyse)
        {
            var analyses = await _analyseBll.GetFullAnalyse(idAnalyse);

            var result = _mapper.Map<AnalyseGetFullDto>(analyses);

            return Ok(result);
        }


        /// <summary>
        /// Create one analyse
        /// </summary>
        /// <param name="analysePostDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateAnalyse(AnalysePostDto analysePostDto)
        {
            await _analyseBll.CreateAnalyse(analysePostDto);

            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="analyse"></param>
        /// <param name="isReplacingExistingvalues"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("CreateOrUpdateAnalyse")]
        public async Task<IActionResult> CreateOrUpdateAnalyse([FromBody]List<AnalysePostDto> analyse, bool isReplacingExistingvalues)
        {
            await _analyseBll.CreateOrUpdateAnalyses(analyse, isReplacingExistingvalues);

            return Ok();
        }

        /// <summary>
        /// Create many Analyse
        /// </summary>
        /// <param name="analysePostDtos"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("CreateAnalyses")]
        public async Task<IActionResult> CreateAnalyses(List<AnalysePostDto> analysePostDtos)
        {
            await _analyseBll.CreateAnalyses(analysePostDtos);

            return Ok();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="analyse"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("CreateFullAnalyses")]
        public async Task<IActionResult> CreateFullAnalyses(AnalysePostFullDto analyse)
        {
            await _analyseBll.CreateFullAnalyses(analyse);

            return Ok();
        }

    
        /// <summary>
        /// 
        /// </summary>
        /// <param name="analysePostFullDto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateFullAnalyses")]
        public async Task<IActionResult> UpdateFullAnalyses(AnalysePostFullDto analysePostFullDto)
        {
            await _analyseBll.UpdateFullAnalyses(analysePostFullDto);

            return Ok();
        }

        /// <summary>
        /// Update Analyse
        /// </summary>
        /// <param name="analysePostDto"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateAnalyse(AnalysePostDto analysePostDto)
        {
            var analyse = await _analyseBll.UpdateAnalyse(analysePostDto);

            if (analyse == null)
                return BadRequest("Analyse_not_found");

            var result = _mapper.Map<AnalyseGetDto>(analyse);

            return Ok(result);
        }

        /// <summary>
        /// Delete Analyse
        /// </summary>
        /// <param name="idAnalyse"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{idAnalyse}")]
        public async Task<IActionResult> DeleteAnalyse(int idAnalyse)
        {
            var analyse = await _analyseBll.DeleteAnalyse(idAnalyse);

            if (analyse == null)
                return BadRequest("Analyse_not_found");

            return Ok();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idAnalyse"></param>
        /// <param name="validationsIds"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("LinkAnalyseToValidations")]
        public async Task<IActionResult> LinkAnalyseToValidations([FromQuery]int idAnalyse, [FromBody] List<int> validationsIds)
        {
            await _analyseBll.LinkAnalyseToValidations(idAnalyse, validationsIds);

            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idAnalyse"></param>
        /// <param name="bilansIds"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("LinkAnalyseToBilans")]
        public async Task<IActionResult> LinkAnalyseToBilans([FromQuery]int idAnalyse, [FromBody] List<int> bilansIds)
        {
            await _analyseBll.LinkAnalyseToBilans(idAnalyse, bilansIds);

            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idAnalyse"></param>
        /// <param name="fonctionReflexesId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("LinkAnalyseToFonctionReflexes")]
        public async Task<IActionResult> LinkAnalyseToFonctionReflexes([FromQuery]int idAnalyse, [FromBody] List<int> fonctionReflexesId)
        {
            await _analyseBll.LinkAnalyseToFonctionReflexes(idAnalyse, fonctionReflexesId);

            return Ok();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idAnalyse"></param>
        /// <param name="evaluationsId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("LinkAnalyseToEvaluations")]
        public async Task<IActionResult> LinkAnalyseToEvaluations([FromQuery]int idAnalyse, [FromBody] List<int> evaluationsId)
        {
            await _analyseBll.LinkAnalyseToEvaluations(idAnalyse, evaluationsId);

            return Ok();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idAnalyse"></param>
        /// <param name="transBasicsIds"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("LinkAnalyseToTransBasic")]
        public async Task<IActionResult> LinkAnalyseToTransBasics([FromQuery]int idAnalyse, [FromBody] List<int> transBasicsIds)
        {
            await _analyseBll.LinkAnalyseToTransBasics(idAnalyse, transBasicsIds);

            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idAnalyse"></param>
        /// <param name="transformationsIds"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("LinkAnalyseToTransformations")]
        public async Task<IActionResult> LinkAnalyseToTransformations([FromQuery]int idAnalyse, [FromBody] List<int> transformationsIds)
        {
            await _analyseBll.LinkAnalyseToTransformations(idAnalyse, transformationsIds);

            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idAnalyse"></param>
        /// <param name="incidenceDesFlagsIds"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("LinkAnalyseToIncidenceDesFlags")]
        public async Task<IActionResult> LinkAnalyseToIncidenceDesFlags([FromQuery]int idAnalyse, [FromBody] List<int> incidenceDesFlagsIds)
        {
            await _analyseBll.LinkAnalyseToIncidenceDesFlags(idAnalyse, incidenceDesFlagsIds);

            return Ok();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idAnalyse"></param>
        /// <param name="commAutosIds"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("LinkAnalyseToCommAutos")]
        public async Task<IActionResult> LinkAnalyseToCommAutos([FromQuery]int idAnalyse, [FromBody] List<int> commAutosIds)
        {
            await _analyseBll.LinkAnalyseToCommAutos(idAnalyse, commAutosIds);

            return Ok();
        }
    }
}
