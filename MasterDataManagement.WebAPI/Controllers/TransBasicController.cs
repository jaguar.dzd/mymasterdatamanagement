﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using MasterDataManagement.Domain.Bll;
using MasterDataManagement.DTO.Site;
using MasterDataManagement.DTO.TransBasic;
using Microsoft.AspNetCore.Mvc;

namespace MasterDataManagement.WebAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class TransBasicController : ControllerBase
    {
        private readonly ITransBasicBll _transBasicBll;
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transBasicBll"></param>
        /// <param name="mapper"></param>
        public TransBasicController(ITransBasicBll transBasicBll, IMapper mapper)
        {
            this._transBasicBll = transBasicBll;
            this._mapper = mapper;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idTransBasic"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{idTransBasic}")]
        public async Task<ActionResult<TransBasicGetDto>> GetTransBasic(int idTransBasic)
        {
            var transBasic = await _transBasicBll.GetTransBasic(idTransBasic);
            if (transBasic == null)
            {
                return BadRequest("TransBasic_not_found");
            }

            var result = _mapper.Map<TransBasicGetDto>(transBasic);
            
            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<TransBasicGetDto>>> GetTransBasics()
        {
            var transBasics = await _transBasicBll.GetTransBasics();

            var result = _mapper.Map<List<TransBasicGetDto>>(transBasics);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transBasicPostDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateTransBasic(TransBasicPostDto transBasicPostDto)
        {
            await _transBasicBll.CreateTransBasic(transBasicPostDto);
            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transBasicPutDto"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateTransBasic(TransBasicPutDto transBasicPutDto)
        {
            var transf = await _transBasicBll.UpdateTransBasic(transBasicPutDto);
            if (transf == null)
            {
                return BadRequest("TransBasic_not_found");
            }

            var result = _mapper.Map<TransBasicGetDto>(transf);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idTransBasic"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{idTransBasic}")]
        public async Task<IActionResult> DeleteTransBasic(int idTransBasic)
        {
            var transf = await _transBasicBll.DeleteTypeDeTube(idTransBasic);

            if (transf == null)
                return BadRequest("TypeDeTube_not_found");

            return Ok();
        }
    }
}