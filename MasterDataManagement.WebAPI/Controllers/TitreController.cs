﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using MasterDataManagement.Domain.Bll;
using MasterDataManagement.DTO.Titre;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MasterDataManagement.WebAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class TitreController : ControllerBase
    {
        private readonly ITitreBll _titreBll;
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="titreBll"></param>
        /// <param name="mapper"></param>
        public TitreController(ITitreBll titreBll, IMapper mapper)
        {
            _titreBll = titreBll;
            _mapper = mapper;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idTitre"></param>
        /// <returns></returns>
        [HttpGet] 
        [Route("{idTitre}")]
        public async Task<ActionResult<TitreGetDto>> GetTitre(int idTitre)
        {
            var titre = await _titreBll.GetTitre(idTitre);
            if (titre == null)
            {
                return BadRequest("Titre_not_found");
            }

            var result = _mapper.Map<TitreGetDto>(titre);
            
            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<TitreGetDto>>> GetTitres()
        {
            var Titres = await _titreBll.GetTitres();

            var result = _mapper.Map<List<TitreGetDto>>(Titres);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="titrePostDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateTitre(TitrePostDto titrePostDto)
        {
            await _titreBll.CreateTitre(titrePostDto);
            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="titrePutDto"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateTitre(TitrePutDto titrePutDto)
        {
            var transf = await _titreBll.UpdateTitre(titrePutDto);
            if (transf == null)
            {
                return BadRequest("Titre_not_found");
            }

            var result = _mapper.Map<TitreGetDto>(transf);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idTitre"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{idTitre}")]
        public async Task<IActionResult> DeleteTitre(int idTitre)
        {
            var transf = await _titreBll.DeleteTitre(idTitre);

            if (transf == null)
                return BadRequest("TypeDeTube_not_found");

            return Ok();
        }
    }
}