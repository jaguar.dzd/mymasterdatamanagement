﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using MasterDataManagement.Domain.Bll;
using MasterDataManagement.DTO.GroupeAnalyse;
using MasterDataManagement.DTO.IncidencesDesFlags;
using Microsoft.AspNetCore.Mvc;


namespace MasterDataManagement.WebAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class IncidenceDesFlagsController : ControllerBase
    {
        private readonly IIncidenceDesFlagsBll incidenceDesFlagsBll;
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        public IncidenceDesFlagsController(IIncidenceDesFlagsBll incidenceDesFlagsBll, IMapper mapper)
        {
            this.incidenceDesFlagsBll = incidenceDesFlagsBll;
            _mapper = mapper;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idIncidenceDesFlags"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{idIncidenceDesFlags}")]
        public async Task<ActionResult<IncidenceDesFlagsGetDto>> GetIncidenceDesFlags(int idIncidenceDesFlags)
        {
            var incidence = await incidenceDesFlagsBll.GetIncidenceDesFlag(idIncidenceDesFlags);

            if (incidence == null)
                return BadRequest("IncidenceDesFlags_not_found");

            var result = _mapper.Map<IncidenceDesFlagsGetDto>(incidence);

            return Ok(result);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<IncidenceDesFlagsGetDto>>> GetIncidenceDesFlags()
        {
            var incidence = await incidenceDesFlagsBll.GetIncidenceDesFlags();

            var result = _mapper.Map<List<IncidenceDesFlagsGetDto>>(incidence);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="incidencedesflagsPostDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateIncidenceDesFlags(IncidencedesflagsPostDto incidencedesflagsPostDto)
        {
            await incidenceDesFlagsBll.CreateIncidenceDesFlags(incidencedesflagsPostDto);

            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="incidencedesflagsPutDto"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateIncidenceDesFlags(IncidencedesflagsPutDto incidencedesflagsPutDto)
        {
            var incidence = await incidenceDesFlagsBll.UpdateIncidenceDesFlags(incidencedesflagsPutDto);

            if (incidence == null)
                return BadRequest("IncidenceDesFlags_not_found");

            var result = _mapper.Map<IncidenceDesFlagsGetDto>(incidence);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idIncidenceDesFlags"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{idGroupeAnalyse}")]
        public async Task<IActionResult> DeleteIncidenceDesFlags(int idIncidenceDesFlags)
        {
            var incidence = await incidenceDesFlagsBll.DeleteIncidenceDesFlags(idIncidenceDesFlags);

            if (incidence == null)
                return BadRequest("IncidenceDesFlags_not_found");

            return Ok();
        }
    }
}
