﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using MasterDataManagement.Domain.Bll;
using MasterDataManagement.DTO.GroupeAnalyse;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace MasterDataManagement.WebAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class GroupeAnalyseController : ControllerBase
    {
        private readonly IGroupeAnalyseBll _groupeAnalyseBll;
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        public GroupeAnalyseController(IGroupeAnalyseBll groupeAnalyseBll, IMapper mapper)
        {
            _groupeAnalyseBll = groupeAnalyseBll;
            _mapper = mapper;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idGroupeAnalyse"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{idGroupeAnalyse}")]
        public async Task<ActionResult<GroupeAnalyseGetDto>> GetGroupeAnalyse(int idGroupeAnalyse)
        {
            var groupeAnalyse = await _groupeAnalyseBll.GetGroupeAnalyse(idGroupeAnalyse);

            if (groupeAnalyse == null)
                return BadRequest("GroupeAnalyse_not_found");

            var result = _mapper.Map<GroupeAnalyseGetDto>(groupeAnalyse);

            return Ok(result);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<GroupeAnalyseGetDto>>> GetGroupeAnalyses()
        {
            var groupeAnalyses = await _groupeAnalyseBll.GetGroupeAnalyses();

            var result = _mapper.Map<List<GroupeAnalyseGetDto>>(groupeAnalyses);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="groupeAnalysePostDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateGroupeAnalyse(GroupeAnalysePostDto groupeAnalysePostDto)
        {
            await _groupeAnalyseBll.CreateGroupeAnalyse(groupeAnalysePostDto);

            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="groupeAnalysePostDto"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateGroupeAnalyse(GroupeAnalysePostDto groupeAnalysePostDto)
        {
            var groupeAnalyse = await _groupeAnalyseBll.UpdateGroupeAnalyse(groupeAnalysePostDto);

            if (groupeAnalyse == null)
                return BadRequest("GroupeAnalyse_not_found");

            var result = _mapper.Map<GroupeAnalyseGetDto>(groupeAnalyse);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idGroupeAnalyse"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{idGroupeAnalyse}")]
        public async Task<IActionResult> DeleteGroupeAnalyse(int idGroupeAnalyse)
        {
            var groupeAnalyse = await _groupeAnalyseBll.DeleteGroupeAnalyse(idGroupeAnalyse);

            if (groupeAnalyse == null)
                return BadRequest("GroupeAnalyse_not_found");

            return Ok();
        }
    }
}
