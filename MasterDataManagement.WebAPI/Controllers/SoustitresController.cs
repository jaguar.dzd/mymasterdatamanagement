﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using MasterDataManagement.Domain.Bll;
using MasterDataManagement.DTO.Site;
using MasterDataManagement.DTO.Soustitres;
using Microsoft.AspNetCore.Mvc;

namespace MasterDataManagement.WebAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class SoustitresController : ControllerBase
    {
        private readonly ISoustitresBll _soustitresBll;
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="soustitresBll"></param>
        /// <param name="mapper"></param>
        public SoustitresController(ISoustitresBll soustitresBll, IMapper mapper)
        {
            _soustitresBll = soustitresBll;
            _mapper = mapper;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idSoustitres"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{idSoustitres}")]
        public async Task<ActionResult<SoustitresGetDto>> GetSoustitres(int idSoustitres)
        {
            var soustitres = await _soustitresBll.GetSoustitres(idSoustitres);
            if (soustitres == null)
            {
                return BadRequest("Soustitres_not_found");
            }

            var result = _mapper.Map<SoustitresGetDto>(soustitres);
            
            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<SoustitresGetDto>>> GetSoustitress()
        {
            var soustitress = await _soustitresBll.GetSoustitress();

            var result = _mapper.Map<List<SoustitresGetDto>>(soustitress);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="soustitresPostDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateSoustitres(SoustitresPostDto soustitresPostDto)
        {
            await _soustitresBll.CreateSoustitres(soustitresPostDto);
            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="soustitresPutDto"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateSoustitres(SoustitresPutDto soustitresPutDto)
        {
            var soustitress = await _soustitresBll.UpdateSoustitres(soustitresPutDto);
            if (soustitress == null)
            {
                return BadRequest("Soustitres_not_found");
            }

            var result = _mapper.Map<SoustitresGetDto>(soustitress);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idSoustitres"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{idSoustitres}")]
        public async Task<IActionResult> DeleteSoustitres(int idSoustitres)
        {
            var soustitress = await _soustitresBll.DeleteSoustitres(idSoustitres);

            if (soustitress == null)
                return BadRequest("TypeDeTube_not_found");

            return Ok();
        }
    }
}