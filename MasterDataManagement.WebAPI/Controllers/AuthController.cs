﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MasterDataManagement.Domain.Bll;
using MasterDataManagement.DTO.Authentication;
using MasterDataManagement.DTO.Droit;
using MasterDataManagement.DTO.Instance;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MasterDataManagement.WebAPI.Controllers
{
    /// <summary>
    /// This is the authentication controller
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    [AllowAnonymous]
    public class AuthController : ControllerBase
    {
        private readonly IAuthBll authBll;
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="authBll"></param>
        public AuthController(IAuthBll authBll, IMapper mapper)
        {
            this.authBll = authBll;
            this._mapper = mapper;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="auth"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<GeneratedTokenInfo>> Post(AuthPostDto auth)
        {
            var response = await authBll.Authenticate(auth);
            if (response == null)
            {
                return BadRequest("wrong_credentials");
            }

            var user = response.Value.user;
            var droits = user.DroitUser.Select(x => x.IdDroitNavigation).ToList();
            var sites = user.UserInstance.Select(x => x.IdInstanceNavigation).ToList();

            var token = response.Value.token;
            

            token.Nom = user.Nom;
            token.Prenom = user.Prenom;
            token.UserName = user.Username;
            token.Droits = _mapper.Map<List<DroitGetDto>>(droits);
            token.Sites = _mapper.Map<List<InstanceGetDto>>(sites);


            return Ok(token);
        }
    }
}
