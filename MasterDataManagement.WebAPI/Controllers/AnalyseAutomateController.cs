﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using MasterDataManagement.Domain.Bll;
using MasterDataManagement.DTO.AnalyseAutomate;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace MasterDataManagement.WebAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AnalyseAutomateController : ControllerBase
    {
        private readonly IAnalyseAutomateBll _analyseAutomateBll;
        private readonly IMapper _mapper;
        /// <summary>
        /// 
        /// </summary>
        public AnalyseAutomateController(IAnalyseAutomateBll analyseAutomateBll, IMapper mapper)
        {
            _analyseAutomateBll = analyseAutomateBll;
            _mapper = mapper;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idAnalyseAutomate"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{idAnalyseAutomate}")]
        public async Task<ActionResult<AnalyseAutomateGetDto>> GetAnalyseAutomate(int idAnalyseAutomate)
        {
            var analyseAutomate = await _analyseAutomateBll.GetAnalyseAutomate(idAnalyseAutomate);

            if (analyseAutomate == null)
                return BadRequest("AnalyseAutomate_not_found");

            var result = _mapper.Map<AnalyseAutomateGetDto>(analyseAutomate);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<AnalyseAutomateGetDto>>> GetAnalyseAutomates()
        {
            var analyseAutomates = await _analyseAutomateBll.GetAnalyseAutomates();

            var result = _mapper.Map<List<AnalyseAutomateGetDto>>(analyseAutomates);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="analyseAutomatePostDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateAnalyseAutomate(AnalyseAutomatePostDto analyseAutomatePostDto)
        {
            await _analyseAutomateBll.CreateAnalyseAutomate(analyseAutomatePostDto);

            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="analyseAutomatePostDto"></param>
        /// <param name="catalogueId"></param>
        /// <returns></returns>
        [Route("CreateFullAndLinkToCatalogue/{catalogueId}")]
        [HttpPost]
        public async Task<IActionResult> CreateAndLinkToCatalogue([FromBody]AnalyseAutomateFullPostDto analyseAutomatePostDto, int catalogueId)
        {
            var result = await _analyseAutomateBll.CreateAndLinkFullAnalyseAutomate(analyseAutomatePostDto, catalogueId);
            if (!result)
                return BadRequest("catalogue_or_analyse_automate");

            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="analyseAutomatePostDto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("CreateFullAnalyseAutomate")]
        public async Task<IActionResult> CreateFullAnalyseAutomate(AnalyseAutomateFullPostDto analyseAutomatePostDto)
        {
            var result = await _analyseAutomateBll.CreateFullAnalyseAutomate(analyseAutomatePostDto);

            if (!result)
                return BadRequest("analyse_automate_not_valid");

            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="analyseAutomateFullPostDto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateFullAnalyseAutomate")]
        public async Task<IActionResult> UpdateFullAnalyseAutomate(AnalyseAutomateFullPostDto analyseAutomateFullPostDto)
        {
            var result = await _analyseAutomateBll.UpdateFullAnalyseAutomate(analyseAutomateFullPostDto);

            if (!result)
                return BadRequest("analyse_automate_not_valid");

            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="analyseAutomatePostDto"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("UpdateAnalyseAutomate")]
        public async Task<IActionResult> UpdateAnalyseAutomate(AnalyseAutomatePostDto analyseAutomatePostDto)
        {
            var analyseAutomate = await _analyseAutomateBll.UpdateAnalyseAutomate(analyseAutomatePostDto);

            if (analyseAutomate == null)
                return BadRequest();

            var result = _mapper.Map<AnalyseAutomateGetDto>(analyseAutomate);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idAnalyseAutomate"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{idAnalyseAutomate}")]
        public async Task<IActionResult> DeleteAnalyseAutomate(int idAnalyseAutomate)
        {
            var analyseAutomate = await _analyseAutomateBll.DeleteAnalyseAutomate(idAnalyseAutomate);

            if (analyseAutomate == null)
                return BadRequest("AnalyseAutomate_not_found");

            return Ok();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("LinkAnalyseAutomatesToValidations")]
        public async Task<IActionResult> LinkAnalyseAutomateToValidations(LinkAnalyseAutomateToValidations data)
        {
            await _analyseAutomateBll.LinkAnalyseAutomateToValidations(data);

            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("LinkAnalyseAutomatesToBilans")]
        public async Task<IActionResult> LinkAnalyseAutomateToBilans(LinkAnalyseAutomateToBilans data)
        {
            await _analyseAutomateBll.LinkAnalyseAutomateToBilans(data);

            return Ok();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("LinkAnalyseAutomatesToFonctionReflexes")]
        public async Task<IActionResult> LinkAnalyseAutomateToFonctionReflexes(LinkAnalyseAutomateToFonctionReflexes data)
        {
            await _analyseAutomateBll.LinkAnalyseAutomateToFonctionReflexes(data);

            return Ok();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("LinkAnalyseAutomatesToEvaluations")]
        public async Task<IActionResult> LinkAnalyseAutomateToEvaluations(LinkAnalyseAutomateToEvaluations data)
        {
            await _analyseAutomateBll.LinkAnalyseAutomateToEvaluations(data);

            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("LinkAnalyseAutomatesToTransBasics")]
        public async Task<IActionResult> LinkAnalyseAutomateToTransBasics(LinkAnalyseToTransBasic data)
        {
            await _analyseAutomateBll.LinkAnalyseAutomateToTransBasics(data);

            return Ok();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("LinkAnalyseAutomatesToTransformations")]
        public async Task<IActionResult> LinkAnalyseAutomateToTransformations(LinkAnalyseAutomateToTransformations data)
        {
            await _analyseAutomateBll.LinkAnalyseAutomateToTransformations(data);

            return Ok();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("LinkAnalyseAutomatesToIncidenceDesFlags")]
        public async Task<IActionResult> LinkAnalyseAutomateToIncidenceDesFlags(LinkAnalyseAutomateToIncidenceDesFlags data)
        {
            await _analyseAutomateBll.LinkAnalyseAutomateToIncidenceDesFlags(data);

            return Ok();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("LinkAnalyseAutomatesToCommAutos")]
        public async Task<IActionResult> LinkAnalyseAutomateToCommAutos(LinkAnalyseAutomateToCommAutos data)
        {
            await _analyseAutomateBll.LinkAnalyseAutomateToCommAutos(data);

            return Ok();
        }

    }
}
