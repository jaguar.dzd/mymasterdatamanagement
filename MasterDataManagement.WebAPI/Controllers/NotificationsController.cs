﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MasterDataManagement.Domain.Bll;
using MasterDataManagement.DTO.Droit;
using MasterDataManagement.DTO.Instance;
using MasterDataManagement.DTO.Notifications;
using MasterDataManagement.DTO.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace MasterDataManagement.WebAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class NotificationsController : ControllerBase
    {
        private readonly ICatalogueBll _catalogueBll;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="catalogueBll"></param>
        public NotificationsController(ICatalogueBll catalogueBll)
        {
            _catalogueBll = catalogueBll;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idUser"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CatalogueNotificationsDto>> GetCatalogueNotifications()
        {
            var result = await _catalogueBll.GetCatalogueNotifications();

            return Ok(result);
        }
    }
}
