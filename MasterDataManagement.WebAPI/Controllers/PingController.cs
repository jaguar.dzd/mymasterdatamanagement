﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasterDataManagement.DTO;
using MasterDataManagement.Infrastructure.ManagementDbContext;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MasterDataManagement.WebAPI.Controllers
{
    /// <summary>
    /// This is the authentication controller
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    [AllowAnonymous]
    public class PingController : ControllerBase
    {
        private readonly MyDbContext _myDbContext;

        public PingController(MyDbContext myDbContext)
        {
            _myDbContext = myDbContext;
        }
        /// <summary>
        /// Returns the string `ping called`
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Get()
        {
            return Ok(DateTime.UtcNow);
        }

        [HttpGet]
        [Route("testdb")]
        public async Task<IActionResult> GetDb()
        {
            var users = await _myDbContext.User.FirstOrDefaultAsync();

            return Ok(new { User = users });
        }
    }
}
