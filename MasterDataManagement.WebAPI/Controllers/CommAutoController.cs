﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using MasterDataManagement.Domain.Bll;
using MasterDataManagement.DTO.CommAuto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace MasterDataManagement.WebAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CommAutoController : ControllerBase
    {
        private readonly ICommAutoBll _commAutoBll;
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        public CommAutoController(ICommAutoBll commAutoBll, IMapper mapper)
        {
            _commAutoBll = commAutoBll;
            _mapper = mapper;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCommAuto"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{idCommAuto}")]
        public async Task<ActionResult<CommAutoGetDto>> GetCommAuto(int idCommAuto)
        {
            var commAuto = await _commAutoBll.GetCommAuto(idCommAuto);

            if (commAuto == null)
                return BadRequest("CommAuto_not_found");

            var result = _mapper.Map<CommAutoGetDto>(commAuto);

            return Ok(result);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<CommAutoGetDto>>> GetCommAutos()
        {
            var commAutos = await _commAutoBll.GetCommAutos();

            var result = _mapper.Map<List<CommAutoGetDto>>(commAutos);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="commAutoPostDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCommAuto(CommAutoPostDto commAutoPostDto)
        {
            await _commAutoBll.CreateCommAuto(commAutoPostDto);

            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="commAutoPostDto"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateCommAuto(CommAutoPostDto commAutoPostDto)
        {
            var commAuto = await _commAutoBll.UpdateCommAuto(commAutoPostDto);

            if (commAuto == null)
                return BadRequest("CommAuto_not_found");

            var result = _mapper.Map<CommAutoGetDto>(commAuto);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCommAuto"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{idCommAuto}")]
        public async Task<IActionResult> DeleteCommAuto(int idCommAuto)
        {
            var commAuto = await _commAutoBll.DeleteCommAuto(idCommAuto);

            if (commAuto == null)
                return BadRequest("CommAuto_not_found");

            return Ok();
        }
    }
}
