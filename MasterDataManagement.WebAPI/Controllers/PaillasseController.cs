﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using MasterDataManagement.Domain.Bll;
using MasterDataManagement.DTO.Paillasse;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace MasterDataManagement.WebAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PaillasseController : ControllerBase
    {
        private readonly IPaillasseBll _paillasseBll;
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        public PaillasseController(IPaillasseBll paillasseBll, IMapper mapper)
        {
            _paillasseBll = paillasseBll;
            _mapper = mapper;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idPaillasse"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{idPaillasse}")]
        public async Task<ActionResult<PaillasseGetDto>> GetPaillasse(int idPaillasse)
        {
            var paillasse = await _paillasseBll.GetPaillasse(idPaillasse);

            if (paillasse == null)
                return BadRequest("Paillasse_not_found");

            var result = _mapper.Map<PaillasseGetDto>(paillasse);

            return Ok(result);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PaillasseGetDto>>> GetPaillasses()
        {
            var paillasses = await _paillasseBll.GetPaillasses();

            var result = _mapper.Map<List<PaillasseGetDto>>(paillasses);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="paillassePostDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreatePaillasse(PaillassePostDto paillassePostDto)
        {
            await _paillasseBll.CreatePaillasse(paillassePostDto);

            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="paillassePostDto"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdatePaillasse(PaillassePostDto paillassePostDto)
        {
            var paillasse = await _paillasseBll.UpdatePaillasse(paillassePostDto);

            if (paillasse == null)
                return BadRequest("Paillasse_not_found");

            var result = _mapper.Map<PaillasseGetDto>(paillasse);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idPaillasse"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{idPaillasse}")]
        public async Task<IActionResult> DeletePaillasse(int idPaillasse)
        {
            var paillasse = await _paillasseBll.DeletePaillasse(idPaillasse);

            if (paillasse == null)
                return BadRequest("Paillasse_not_found");

            return Ok();
        }
    }
}
