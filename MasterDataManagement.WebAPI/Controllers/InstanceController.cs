﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using MasterDataManagement.Domain.Bll;
using MasterDataManagement.DTO.Instance;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace MasterDataManagement.WebAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class InstanceController : ControllerBase
    {
        private readonly IInstanceBll _instanceBll;
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        public InstanceController(IInstanceBll instanceBll, IMapper mapper)
        {
            _instanceBll = instanceBll;
            _mapper = mapper;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idInstance"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{idInstance}")]
        public async Task<ActionResult<InstanceGetDto>> GetInstance(byte idInstance)
        {
            var instance = await _instanceBll.GetInstance(idInstance);

            if (instance == null)
                return BadRequest("instance_not_found");

            var result = _mapper.Map<InstanceGetDto>(instance);

            return Ok(result);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<InstanceGetDto>>> GetInstances()
        {
            var instances = await _instanceBll.GetInstances();

            var result = _mapper.Map<List<InstanceGetDto>>(instances);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instancePostDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateInstance(InstancePostDto instancePostDto)
        {
            await _instanceBll.CreateInstance(instancePostDto);

            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instancePostDto"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateInstance(InstanceUpdateDto instancePostDto)
        {
            var instance = await _instanceBll.UpdateInstance(instancePostDto);

            if (instance == null)
                return BadRequest("instance_not_found");

            var result = _mapper.Map<InstanceGetDto>(instance);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idInstance"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{idInstance}")]
        public async Task<IActionResult> DeleteInstance(byte idInstance)
        {
            var instance = await _instanceBll.DeleteInstance(idInstance);

            if (instance == null)
                return BadRequest("instance_not_found");

            return Ok();
        }
    }
}
