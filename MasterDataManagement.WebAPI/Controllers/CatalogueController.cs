﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MasterDataManagement.Domain.Bll;
using MasterDataManagement.DTO.Analyse;
using MasterDataManagement.DTO.Automate;
using MasterDataManagement.DTO.Catalogue;
using MasterDataManagement.DTO.Enum;
using MasterDataManagement.DTO.Instance;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace MasterDataManagement.WebAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CatalogueController : ControllerBase
    {
        private readonly ICatalogueBll _catalogueBll;
        private readonly IMapper _mapper;
        /// <summary>
        /// 
        /// </summary>
        public CatalogueController(ICatalogueBll catalogueBll, IMapper mapper)
        {
            _catalogueBll = catalogueBll;
            _mapper = mapper;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCatalogue"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{idCatalogue}")]
        public async Task<ActionResult<CatalogueDetailDto>> GetCatalogue(int idCatalogue)
        {
            var catalogue = await _catalogueBll.GetCatalogue(idCatalogue);

            if (catalogue == null)
                return BadRequest("Catalogue_not_found");

            var analyses = await _catalogueBll.GetCatalogueAnalyses(idCatalogue);
            var automates = await _catalogueBll.GetCatalogueAutomates(idCatalogue);
            var instances = await _catalogueBll.GetCatalogueInstances(idCatalogue);

            var result = new CatalogueDetailDto
            {
                Statut = catalogue.Statut,
                Version = catalogue.Version,
                IdCatalogue = catalogue.IdCatalogue,
                NomCatalogue = catalogue.NomCatalogue,
                Analyses = _mapper.Map<List<AnalyseGetDto>>(analyses),
                Automates = _mapper.Map<List<AutomateGetDto>>(automates),
                Sites = _mapper.Map<List<InstanceGetDto>>(instances),
                NbAnalyses = analyses.Count,
                NbAutomates = automates.Count,
                IsActivated = catalogue.IsActivated
            };

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<CatalogueDetailDto>>> GetCatalogues()
        {
            var catalogues = await _catalogueBll.GetCataloguesWithDetails();

            var result = new List<CatalogueDetailDto>();

            foreach (var catalogue in catalogues)
            {
                var analyses = catalogue.AnalyseCatalogue.Select(x => x.AnalyseIdAnalyseNavigation).ToList();
                var automates = catalogue.AutomateCatalogue.Select(x => x.IdAutomateNavigation).ToList();
                var instances = catalogue.InstanceCatalogue.Select(x => x.IdInstanceNavigation).ToList();

                var catalogueDetail = new CatalogueDetailDto
                {
                    Statut = catalogue.Statut,
                    Version = catalogue.Version,
                    IdCatalogue = catalogue.IdCatalogue,
                    NomCatalogue = catalogue.NomCatalogue,
                    IsActivated = catalogue.IsActivated,
                    Analyses = _mapper.Map<List<AnalyseGetDto>>(analyses),
                    Automates = _mapper.Map<List<AutomateGetDto>>(automates),
                    Sites = _mapper.Map<List<InstanceGetDto>>(instances),
                    NbAnalyses = analyses.Count,
                    NbAutomates = automates.Count
                };

                result.Add(catalogueDetail);
            }

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cataloguePostDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCatalogue(CataloguePostDto cataloguePostDto)
        {
            if (string.IsNullOrWhiteSpace(cataloguePostDto?.CreatorName))
            {
                return BadRequest("Il faut mentionner le nom du créateur du catalogue !");
            }
            await _catalogueBll.CreateCatalogue(cataloguePostDto);

            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cataloguePostDto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("CreateFullCatalogue")]
        public async Task<IActionResult> CreateFullCatalogue(CatalogueFullPostDto cataloguePostDto)
        {
            if (string.IsNullOrWhiteSpace(cataloguePostDto?.Catalogue?.CreatorName))
            {
                return BadRequest("Il faut mentionner le nom du créateur du catalogue !");
            }

            var result = await _catalogueBll.CreateFullCatalogue(cataloguePostDto);
            if (!result)
                return BadRequest("analyse_id_or_automate_id_not_found");

            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cataloguePostDto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateFullCatalogue")]
        public async Task<IActionResult> UpdateFullCatalogue(CatalogueFullPostDto cataloguePostDto)
        {
            await _catalogueBll.UpdateFullCatalogue(cataloguePostDto);

            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="catalogueDuplicate"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("DuplicateCatalogue")]
        public async Task<IActionResult> DuplicateCatalogue(CatalogueDuplicatePostDto catalogueDuplicate)
        {
            var result = await _catalogueBll.DuplicateCatalogue(catalogueDuplicate);
            if (result == false)
                return BadRequest("catalogue_not_found");

            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="catalogueId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("ChangeState")]
        public async Task<ActionResult<CatalogueChangeStateResponseDto>> ChangeState(int catalogueId)
        {
            var response = await _catalogueBll.ChangeState(catalogueId);
            if (response == null)
                return BadRequest("catalogue_not_found_or_action_is_wrong");

            return Ok(response);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="catalogueId"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("ValidateOrReject")]
        public async Task<IActionResult> ValidateOrReject(int catalogueId, ValidateOrRejectActionEnum action)
        {
            var response = await _catalogueBll.ValidateOrReject(catalogueId, action);
            if (response == null)
                return BadRequest("catalogue_not_found_or_action_is_wrong");

            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCatalogue"></param>
        /// <param name="automatesIds"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("LinkCatalogueToAutomates")]
        public async Task<IActionResult> LinkCatalogueToAutomates([FromQuery]int idCatalogue, [FromBody] List<int> automatesIds)
        {
            var result = await _catalogueBll.LinkCatalogueToAutomates(idCatalogue, automatesIds);
            if (result == false)
            {
                return BadRequest("catalogue_or_automate_not_found");
            }

            return Ok();
        }

       /// <summary>
       /// 
       /// </summary>
       /// <param name="idCatalogue"></param>
       /// <param name="analysesIds"></param>
       /// <returns></returns>
        [HttpPost]
        [Route("LinkCatalogueToAnalyases")]
        public async Task<IActionResult> LinkCatalogueToAnalyases([FromQuery]int idCatalogue, [FromBody] List<int> analysesIds)
        {
            await _catalogueBll.LinkCatalogueToAnalyases(idCatalogue, analysesIds);

            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cataloguePostDto"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateCatalogue(CatalogueUpdateDto cataloguePostDto)
        {
            var catalogue = await _catalogueBll.UpdateCatalogue(cataloguePostDto);

            if (catalogue == null)
                return BadRequest("Catalogue_not_found");

            var result = _mapper.Map<CatalogueGetDto>(catalogue);

            return Ok(result);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCatalogue"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{idCatalogue}")]
        public async Task<IActionResult> DeleteCatalogue(int idCatalogue)
        {
            var catalogue = await _catalogueBll.DeleteCatalogue(idCatalogue);

            if (catalogue == null)
                return BadRequest("Catalogue_not_found");

            return Ok();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCatalogue"></param>
        /// <param name="analysesAutomatesIds"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("LinkCatalogueToAnalyseAutomates")]
        public async Task<IActionResult> LinkCatalogueToAnalyseAutomates([FromQuery]int idCatalogue, [FromBody] List<int> analysesAutomatesIds)
        {
            await _catalogueBll.LinkCatalogueToAnalyaseAutomates(idCatalogue, analysesAutomatesIds);

            return Ok();
        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="idCatalogue"></param>
        ///// <param name="instancesIds"></param>
        ///// <returns></returns>
        //[HttpPost]
        //[Route("LinkCatalogueToInstances")]
        //public async Task<IActionResult> LinkCatalogueToInstances([FromQuery]int idCatalogue, [FromBody] List<int> instancesIds)
        //{
        //    var result = await _catalogueBll.LinkCatalogueToInstances(idCatalogue, instancesIds);
        //    if (!result)
        //        return BadRequest("catalogue_or_instance_not_found");

        //    return Ok();
        //}


        /// <summary>
        /// 
        /// </summary>
        /// <param name="cataloguesAnalysesPostDto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("LinkCataloguesToAnalyses")]
        public async Task<IActionResult> LinkCataloguesToAnalyases(CataloguesAnalysesPostDto cataloguesAnalysesPostDto)
        {
            var result = cataloguesAnalysesPostDto != null && await _catalogueBll.LinkCataloguesToAnalyases(cataloguesAnalysesPostDto.CataloguesIds, cataloguesAnalysesPostDto.AnalysesIds);
            if (!result)
                return BadRequest("catalogue_or_analyse_not_found");

            return Ok();
        }
    }
}
