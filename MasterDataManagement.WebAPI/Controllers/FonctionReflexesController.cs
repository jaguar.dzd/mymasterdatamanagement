﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using MasterDataManagement.Domain.Bll;
using MasterDataManagement.DTO.FonctionReflexes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace MasterDataManagement.WebAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class FonctionReflexesController : ControllerBase
    {
        private readonly IFonctionReflexesBll _fonctionReflexesBll;
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        public FonctionReflexesController(IFonctionReflexesBll fonctionReflexesBll, IMapper mapper)
        {
            _fonctionReflexesBll = fonctionReflexesBll;
            _mapper = mapper;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idFonctionReflexes"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{idFonctionReflexes}")]
        public async Task<ActionResult<FonctionReflexesGetDto>> GetFonctionReflexes(int idFonctionReflexes)
        {
            var fonctionReflexes = await _fonctionReflexesBll.GetFonctionReflexes(idFonctionReflexes);

            if (fonctionReflexes == null)
                return BadRequest("FonctionReflexes_not_found");

            var result = _mapper.Map<FonctionReflexesGetDto>(fonctionReflexes);

            return Ok(result);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<FonctionReflexesGetDto>>> GetFonctionReflexes()
        {
            var fonctionsReflexes = await _fonctionReflexesBll.GetFonctionReflexess();

            var result = _mapper.Map<List<FonctionReflexesGetDto>>(fonctionsReflexes);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fonctionReflexesPostDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateFonctionReflexes(FonctionReflexesPostDto fonctionReflexesPostDto)
        {
            await _fonctionReflexesBll.CreateFonctionReflexes(fonctionReflexesPostDto);

            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fonctionReflexesPostDto"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateFonctionReflexes(FonctionReflexesPostDto fonctionReflexesPostDto)
        {
            var fonctionReflexes = await _fonctionReflexesBll.UpdateFonctionReflexes(fonctionReflexesPostDto);

            if (fonctionReflexes == null)
                return BadRequest("FonctionReflexes_not_found");

            var result = _mapper.Map<FonctionReflexesGetDto>(fonctionReflexes);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idFonctionReflexes"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{idFonctionReflexes}")]
        public async Task<IActionResult> DeleteFonctionReflexes(int idFonctionReflexes)
        {
            var fonctionReflexes = await _fonctionReflexesBll.DeleteFonctionReflexes(idFonctionReflexes);

            if (fonctionReflexes == null)
                return BadRequest("FonctionReflexes_not_found");

            return Ok();
        }
    }
}
