﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using MasterDataManagement.Domain.Bll;
using MasterDataManagement.DTO.Secteur;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace MasterDataManagement.WebAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class SecteurController : ControllerBase
    {
        private readonly ISecteurBll _secteurBll;
        private readonly IMapper _mapper;
        /// <summary>
        /// 
        /// </summary>
        public SecteurController(ISecteurBll secteurBll, IMapper mapper)
        {
            _secteurBll = secteurBll;
            _mapper = mapper;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idSecteur"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{idSecteur}")]
        public async Task<ActionResult<SecteurGetDto>> GetSecteur(int idSecteur)
        {
            var secteur = await _secteurBll.GetSecteur(idSecteur);

            if (secteur == null)
                return BadRequest("Secteur_not_found");

            var result = _mapper.Map<SecteurGetDto>(secteur);

            return Ok(result);

        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<SecteurGetDto>>> GetSecteurs()
        {
            var secteurs = await _secteurBll.GetSecteurs();

            var result = _mapper.Map<List<SecteurGetDto>>(secteurs);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="secteurPostDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateSecteur(SecteurPostDto secteurPostDto)
        {
            await _secteurBll.CreateSecteur(secteurPostDto);

            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="secteurPostDto"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateSecteur(SecteurPostDto secteurPostDto)
        {
            var secteur = await _secteurBll.UpdateSecteur(secteurPostDto);

            if (secteur == null)
                return BadRequest("Secteur_not_found");

            var result = _mapper.Map<SecteurGetDto>(secteur);

            return Ok(result);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idSecteur"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{idSecteur}")]
        public async Task<IActionResult> DeleteSecteur(int idSecteur)
        {
            var secteur = await _secteurBll.DeleteSecteur(idSecteur);

            if (secteur == null)
                return BadRequest("Secteur_not_found");

            return Ok();
        }
    }
}
