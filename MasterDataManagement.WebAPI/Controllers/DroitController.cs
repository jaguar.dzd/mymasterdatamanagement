﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using MasterDataManagement.Domain.Bll;
using MasterDataManagement.DTO.Droit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace MasterDataManagement.WebAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DroitController : ControllerBase
    {
        private readonly IDroitBll _droitBll;
        private readonly IMapper _mapper;
        /// <summary>
        /// 
        /// </summary>
        public DroitController(IDroitBll droitBll, IMapper mapper)
        {
            _droitBll = droitBll;
            _mapper = mapper;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idDroit"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{idDroit}")]
        public async Task<ActionResult<DroitGetDto>> GetDroit(int idDroit)
        {
            var droit = await _droitBll.GetDroit(idDroit);

            if (droit == null)
                return BadRequest("Droit_not_found");

            var result = _mapper.Map<DroitGetDto>(droit);

            return Ok(result);

        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<DroitGetDto>>> GetDroits()
        {
            var droits = await _droitBll.GetDroits();

            var result = _mapper.Map<List<DroitGetDto>>(droits);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="droitPostDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateDroit(DroitPostDto droitPostDto)
        {
            await _droitBll.CreateDroit(droitPostDto);

            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="droitPostDto"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateDroit(DroitPostDto droitPostDto)
        {
            var droit = await _droitBll.UpdateDroit(droitPostDto);

            if (droit == null)
                return BadRequest("Droit_not_found");

            var result = _mapper.Map<DroitGetDto>(droit);

            return Ok(result);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idDroit"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{idDroit}")]
        public async Task<IActionResult> DeleteDroit(int idDroit)
        {
            var droit = await _droitBll.DeleteDroit(idDroit);

            if (droit == null)
                return BadRequest("Droit_not_found");

            return Ok();
        }
    }
}
