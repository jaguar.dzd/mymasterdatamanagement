﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using MasterDataManagement.Domain.Bll;
using MasterDataManagement.DTO.Evaluation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace MasterDataManagement.WebAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class EvaluationController : ControllerBase
    {
        private readonly IEvaluationBll _evaluationBll;
        private readonly IMapper _mapper;
        /// <summary>
        /// 
        /// </summary>
        public EvaluationController(IEvaluationBll evaluationBll, IMapper mapper)
        {
            _evaluationBll = evaluationBll;
            _mapper = mapper;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idEvaluation"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{idEvaluation}")]
        public async Task<ActionResult<EvaluationGetDto>> GetEvaluation(int idEvaluation)
        {
            var evaluation = await _evaluationBll.GetEvaluation(idEvaluation);

            if (evaluation == null)
                return BadRequest("Evaluation_not_found");

            var result = _mapper.Map<EvaluationGetDto>(evaluation);
           
            return Ok(result);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<EvaluationGetDto>>> GetEvaluations()
        {
            var evaluations = await _evaluationBll.GetEvaluations();

            var result = _mapper.Map<List<EvaluationGetDto>>(evaluations);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="evaluationPostDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateEvaluation(EvaluationPostDto evaluationPostDto)
        {
            await _evaluationBll.CreateEvaluation(evaluationPostDto);

            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="evaluationPostDto"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateEvaluation(EvaluationPostDto evaluationPostDto)
        {
            var evaluation = await _evaluationBll.UpdateEvaluation(evaluationPostDto);

            if (evaluation == null)
                return BadRequest("Evaluation_not_found");

            var result = _mapper.Map<EvaluationGetDto>(evaluation);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idEvaluation"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{idEvaluation}")]
        public async Task<IActionResult> DeleteEvaluation(int idEvaluation)
        {
            var evaluation = await _evaluationBll.DeleteEvaluation(idEvaluation);

            if (evaluation == null)
                return BadRequest("Evaluation_not_found");

            return Ok();
        }
    }
}
