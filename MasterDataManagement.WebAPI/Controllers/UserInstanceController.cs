﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using MasterDataManagement.Domain.Bll;
using MasterDataManagement.DTO.Instance;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace MasterDataManagement.WebAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class UserInstanceController : ControllerBase
    {
        private readonly IUserInstanceBll _utilisateurInstanceBll;
        private readonly IMapper _mapper;
     

        /// <summary>
        /// 
        /// </summary>
        public UserInstanceController(IUserInstanceBll utilisateurInstanceBll, IMapper mapper)
        {
            _utilisateurInstanceBll = utilisateurInstanceBll;
            _mapper = mapper;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idUtilisateur"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{idUtilisateur}")]
        public async Task<ActionResult<List<InstanceGetDto>>> GetUtilisateurInstances(ushort idUtilisateur)
        {
            var utilisateurInstances = await _utilisateurInstanceBll.GetUserInstances(idUtilisateur);

            var result = _mapper.Map<List<InstanceGetDto>>(utilisateurInstances);

            return Ok(result);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idUtilisateur"></param>
        /// <param name="idInstances"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("{idUtilisateur}")]
        public async Task<IActionResult> CreateUtilisateurInstances(ushort idUtilisateur , List<byte> idInstances)
        {
            var utilisateur = await _utilisateurInstanceBll.CreateUserInstances(idUtilisateur , idInstances);

            if (utilisateur == null)
                return BadRequest("Utilisateur_Not_Found");

            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idUser"></param>
        /// <param name="idInstances"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{idUser}")]
        public async Task<IActionResult> DeleteUserFromInstances(ushort idUser, [FromBody] List<int> idInstances)
        {
            var success = await _utilisateurInstanceBll.DeleteUserFromInstances(idUser, idInstances);

            if (!success)
                return BadRequest("Utilisateur_Or_Instance_Not_Found");

            return Ok();
        }


    }
}
