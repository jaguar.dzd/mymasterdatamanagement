﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasterDataManagement.Domain.Bll;
using MasterDataManagement.DTO.Enum;
using MasterDataManagement.DTO.InstanceCatalogue;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MasterDataManagement.WebAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class InstanceCatalogueController : ControllerBase
    {
        private readonly IInstanceBll _instanceBll;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instanceBll"></param>
        public InstanceCatalogueController(IInstanceBll instanceBll)
        {
            this._instanceBll = instanceBll;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCatalogue"></param>
        /// <param name="instancesIds"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("LinkInstanceToCatalogues")]
        public async Task<IActionResult> LinkInstanceToCatalogues(int idCatalogue, [FromBody]List<int> instancesIds)
        {
            if (await _instanceBll.LinkInstanceToCatalogues(idCatalogue, instancesIds))
            {
                return Ok();
            }
            return BadRequest("catalogue_or_instance_not_found");
        }

        [HttpPost]
        [Route("ApproveOrRefuse")]
        public async Task<IActionResult> ApproveOrRefuse(InstanceCataloguePostDto data)
        {
            if (await _instanceBll.ApproveOrRefuseCatalogueApplication(data))
            {
                return Ok();
            }

            return BadRequest("catalogue_or_instance_not_found");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCatalogue"></param>
        /// <param name="instancesIds"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("RemoveInstanceFromCatalogues")]
        public async Task<IActionResult> RemoveInstanceFromCatalogues(int idCatalogue, [FromBody]List<int> instancesIds)
        {
            if (await _instanceBll.RemoveInstanceFromCatalogues(idCatalogue, instancesIds))
            {
                return Ok();
            }

            return BadRequest("catalogue_or_instance_not_found");
        }
    }
}