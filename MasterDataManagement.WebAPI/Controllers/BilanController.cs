﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using MasterDataManagement.Domain.Bll;
using MasterDataManagement.DTO.Bilan;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace MasterDataManagement.WebAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class BilanController : ControllerBase
    {
        private readonly IBilanBll _bilanBll;
        private readonly IMapper _mapper;
        /// <summary>
        /// 
        /// </summary>
        public BilanController(IBilanBll bilanBll, IMapper mapper)
        {
            _bilanBll = bilanBll;
            _mapper = mapper;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idBilan"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{idBilan}")]
        public async Task<ActionResult<BilanGetDto>> GetBilan(int idBilan)
        {
            var bilan = await _bilanBll.GetBilan(idBilan);

            if (bilan == null)
                return BadRequest("Bilan_not_found");

            var result = _mapper.Map<BilanGetDto>(bilan);

            return Ok(result);

        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<BilanGetDto>>> GetBilans()
        {
            var bilans = await _bilanBll.GetBilans();

            var result = _mapper.Map<List<BilanGetDto>>(bilans);

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bilanPostDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateBilan(BilanPostDto bilanPostDto)
        {
            await _bilanBll.CreateBilan(bilanPostDto);

            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bilanPostDto"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateBilan(BilanPostDto bilanPostDto)
        {
            var bilan = await _bilanBll.UpdateBilan(bilanPostDto);

            if (bilan == null)
                return BadRequest("Bilan_not_found");

            var result = _mapper.Map<BilanGetDto>(bilan);

            return Ok(result);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idBilan"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{idBilan}")]
        public async Task<IActionResult> DeleteBilan(int idBilan)
        {
            var bilan = await _bilanBll.DeleteBilan(idBilan);

            if (bilan == null)
                return BadRequest("Bilan_not_found");

            return Ok();
        }
    }
}
