﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MasterDataManagement.Domain.Bll;
using MasterDataManagement.DTO.Droit;
using MasterDataManagement.DTO.Instance;
using MasterDataManagement.DTO.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace MasterDataManagement.WebAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserBll _userBll;
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userBll"></param>
        /// <param name="mapper"></param>
        public UserController(IUserBll userBll, IMapper mapper)
        {
            _userBll = userBll;
            _mapper = mapper;
        }


        // must authenticate later
        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        //[Authorize]
        public async Task<ActionResult<UserPostResponseDto>> CreateUser(UserPostDto user)
        {
            var result = await _userBll.CreateUser(user);
            return Ok(result);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        //[Authorize]
        public async Task<ActionResult<List<UserDetailGetDto>>> GetUsers()
        {
            var users = await _userBll.GetUsersWithDetails();

            var result = new List<UserDetailGetDto>();
            
            foreach (var user in users)
            {
                var droits = user.DroitUser.Select(x => x.IdDroitNavigation).ToList();
                var sites = user.UserInstance.Select(x => x.IdInstanceNavigation).ToList();

                var userDetailGetDto = new UserDetailGetDto
                {
                    Email = user.Email,
                    FirstName = user.Nom,
                    LastName = user.Prenom,
                    Id = user.IdUser,
                    Droits = _mapper.Map<List<DroitGetDto>>(droits),
                    Sites = _mapper.Map<List<InstanceGetDto>>(sites)
                };

                result.Add(userDetailGetDto);
            }

            return Ok(result);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("AddDroits")]
        public async Task<IActionResult> AddDroits([FromBody] List<int> droitsIds, [FromQuery]ushort idUser)
        {
            await _userBll.AddDroits(idUser, droitsIds);
            return Ok();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("RemoveDroits")]
        public async Task<IActionResult> RemoveDroits([FromBody] List<int> droitsIds, [FromQuery]ushort idUser)
        {
            await _userBll.RemoveDroits(idUser, droitsIds);
            return Ok();
        }
    }
}
