using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;
using System;
using System.IO;

namespace MasterDataManagement.WebAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
               .MinimumLevel.Information()
               .MinimumLevel.Override("Microsoft.AspNetCore", LogEventLevel.Warning)
               .Enrich.FromLogContext()
               // 10,000 items max in buffer, then drop
               .WriteTo.Async(x => x.File(
                   Path.Combine(Directory.GetCurrentDirectory(), "Log", "error-.log"),
                   rollingInterval: RollingInterval.Day,
                   outputTemplate: @"{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {RequestId} {Message:lj} {NewLine}{RequestBody:lj}",
                   rollOnFileSizeLimit: true,
                   flushToDiskInterval: TimeSpan.FromSeconds(1),
                   restrictedToMinimumLevel: LogEventLevel.Error))
               // 10,000 items max in buffer, then drop
               .WriteTo.Async(x => x.File(
                   Path.Combine(Directory.GetCurrentDirectory(), "Log", "other-.log"),
                   rollingInterval: RollingInterval.Day,
                   outputTemplate: @"{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {RequestId} {Message:lj} {NewLine}{RequestBody:lj}{NewLine}{ResponseBody:lj}{NewLine}{Exception}",
                   rollOnFileSizeLimit: true,
                   flushToDiskInterval: TimeSpan.FromSeconds(1),
                   restrictedToMinimumLevel: LogEventLevel.Information))
               .CreateLogger();

            try
            {
                Log.Information("Starting up");
                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Application start-up failed");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseSerilog()
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseUrls("http://*:5000");
                    webBuilder.UseStartup<Startup>();
                });
    }
}
