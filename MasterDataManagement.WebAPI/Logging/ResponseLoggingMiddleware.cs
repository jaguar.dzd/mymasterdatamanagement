﻿using Microsoft.AspNetCore.Http;
using Serilog;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace MasterDataManagement.WebAPI.Logging
{
    public class ResponseLoggingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IDiagnosticContext _diagnosticContext;

        public ResponseLoggingMiddleware(RequestDelegate next, IDiagnosticContext diagnosticContext)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _diagnosticContext = diagnosticContext ?? throw new ArgumentNullException(nameof(diagnosticContext));
        }

        public async Task Invoke(HttpContext context)
        {
            using var responseBodyMemoryStream = new MemoryStream();
            var originalResponseBodyStream = context.Response.Body;
            context.Response.Body = responseBodyMemoryStream;

            try
            {
                await _next(context);
            }
            finally
            {
                throw new Exception();
                var responseBody = await ReadStreamAsStringAsync(context.Response.Body);
                _diagnosticContext.Set("ResponseBody", responseBody);
                await responseBodyMemoryStream.CopyToAsync(originalResponseBodyStream);

            }
        }

        private async Task<string> ReadStreamAsStringAsync(Stream stream)
        {
            stream.Seek(0, SeekOrigin.Begin);
            using var reader = new StreamReader(stream, Encoding.UTF8, true, leaveOpen: true);
            var content = await reader.ReadToEndAsync();
            stream.Position = 0;
            return content;
        }
    }
}
