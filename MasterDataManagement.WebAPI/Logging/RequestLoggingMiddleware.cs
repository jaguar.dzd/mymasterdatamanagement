﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace MasterDataManagement.WebAPI.Logging
{
    public class RequestLoggingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IDiagnosticContext _diagnosticContext;
        private readonly ILogger<RequestLoggingMiddleware> _logger;

        public RequestLoggingMiddleware(RequestDelegate next, IDiagnosticContext diagnosticContext,
            ILoggerFactory loggerFactory)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _diagnosticContext = diagnosticContext ?? throw new ArgumentNullException(nameof(diagnosticContext));
            _logger = loggerFactory.CreateLogger<RequestLoggingMiddleware>();
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await LogRequestAsync(context.Request);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error in reading request");
            }

            await _next(context);
        }

        private async Task LogRequestAsync(HttpRequest request)
        {
            request.EnableBuffering();
            var requestBody = await ReadStreamAsStringAsync(request.Body);

            _diagnosticContext.Set("RequestBody", requestBody);
        }

        private async Task<string> ReadStreamAsStringAsync(Stream stream)
        {
            stream.Seek(0, SeekOrigin.Begin);

            using var reader = new StreamReader(stream, Encoding.UTF8, true, leaveOpen: true);
            var content = await reader.ReadToEndAsync();
            stream.Position = 0;
            return content;
        }
    }
}
